/* -----------------------------------------------------------------
                             Bitmap Loader

     This module handles registration of bitmap codecs and loading
   bitmaps into memory via these codecs.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 


#ifndef IMAGEINFO_HPP
#define IMAGEINFO_HPP

#include "RStr.hpp"
#include "IntArray.hpp"
#include "image/BitmapLoader.hpp"


/*
  Refactor RSpriteManager::TileSpritesOnCanvas to compile bitmaps into an atlas.  
    This should be a stand-alone tool.  Output the atlas, and a text file that 
    contains the stripped file name, x,y pos, w,h, orig w,h, and offset to orig origin.
*/

//-----------------------------------------------------------------------------
class ImageInfo
  {
  private:
  
  public:
    RStr  strFilename;
    
    // track the number of rows/columns where data is blanked out by the alpha channel (alpha 0)
    INT   iClearTop;
    INT   iClearBottom;
    INT   iClearLeft;
    INT   iClearRight;

    INT  iWidth;
    INT  iHeight;
    INT  iDepth;
    INT  iNumChannels;
    
    INT  iAtlasX;
    INT  iAtlasY;
    
    FLOAT  fTextureX;
    FLOAT  fTextureY;
    
    BitmapLoader      bitmapLoader;
    IntArray          aiImage;
    ImageBoxIterator  itrImage;
    
  public:
                        ImageInfo       ();
                        ~ImageInfo      ();
    
    VOID                LoadImage       (const char *  szFilenameIn);
    
    VOID                CalcBounds      (VOID);

    const char *        GetFilename     (VOID)        {return strFilename.AsChar ();};
    
    INT                 GetWidth        (VOID)        {return iWidth;};
    INT                 GetHeight       (VOID)        {return iHeight;};

    INT                 GetOpaqueWidth  (VOID)        {return (iWidth -  iClearLeft - iClearRight);};
    INT                 GetOpaqueHeight (VOID)        {return (iHeight - iClearTop - iClearBottom);};
    
    VOID                SetAtlasX       (INT  iIn)    {iAtlasX = iIn;};
    VOID                SetAtlasY       (INT  iIn)    {iAtlasY = iIn;};
    INT                 GetAtlasX       (VOID)        {return iAtlasX;};
    INT                 GetAtlasY       (VOID)        {return iAtlasY;};
    
    INT                 GetClearTop     (VOID)        {return iClearTop;};
    INT                 GetClearBottom  (VOID)        {return iClearBottom;};
    INT                 GetClearLeft    (VOID)        {return iClearLeft;};
    INT                 GetClearRight   (VOID)        {return iClearRight;};
    
    ImageBoxIterator &  GetIterator     (VOID)        {itrImage.SetBuffer (aiImage.GetRawBuffer (), iWidth, iHeight, iDepth, iNumChannels); return itrImage;};
    
  };
  
#endif // IMAGEINFO_HPP
