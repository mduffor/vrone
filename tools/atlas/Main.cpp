
#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include <stdio.h>
#include "FilePath.hpp"
#include "ImageInfo.hpp"
#include "FloatArray.hpp"
#include "Color8U.hpp"

#include "image/BitmapLoader.hpp"
#include "image/CodecPam.hpp"
#include "image/CodecPng.hpp"

#include "Main.hpp"

//-----------------------------------------------------------------------------
int main (int argc, char *argv[])
  {
  DebugMessagesFactory::Initialize ();
  DBG_INFO ("Hello World");
  FilePath  filePath;
  
  RStr       strFileOut;
  
  PtrArray   arrayImageInfo;

  BitmapLoader::RegisterCodec (new CodecPam);
  BitmapLoader::RegisterCodec (new CodecPng);
  
  // TODO: Expand this to a full command line parser
  // open files from the command line
  for (INT  iIndex = 1; iIndex < argc; ++iIndex)
    {
    // check for args
    if (streq (argv[iIndex], "-o"))
      {
      strFileOut = argv[iIndex + 1];
      ++iIndex;
      }
    else
      {
      ImageInfo *  pImageInfo = new ImageInfo ();
      
      pImageInfo->LoadImage (argv[iIndex]);
      pImageInfo->CalcBounds ();
      
      printf ("Main image %s size %dx%d opaque %dx%d\n", pImageInfo->GetFilename(), pImageInfo->GetWidth(), pImageInfo->GetHeight(), pImageInfo->GetOpaqueWidth(), pImageInfo->GetOpaqueHeight());
      
      arrayImageInfo.Append ((PVOID)pImageInfo);
      }
    }
  printf ("Output to : %s\n", strFileOut.AsChar ());
  for (int iIndex = 0; iIndex < arrayImageInfo.Length (); ++iIndex)
    {
    printf (" File %d : %s\n", iIndex, ((ImageInfo *)arrayImageInfo[iIndex])->GetFilename ());
    }
    
  INT       iAtlasWidth = 0;
  INT       iAtlasHeight = 0;
  IntArray  arrayAtlasPixels;
    
  TileImagesOnBitmap (&arrayImageInfo,
                      iAtlasWidth,
                      iAtlasHeight,
                      &arrayAtlasPixels);

  printf ("Output Atlas %dx%d\n", iAtlasWidth, iAtlasHeight);                      
                      
  ImageBoxIterator itrAtlas (arrayAtlasPixels.GetRawBuffer (),
                             iAtlasWidth,
                             iAtlasHeight,
                             1,
                             4, // num channels
                             0, 0, 0, // start
                             iAtlasWidth,
                             iAtlasHeight,
                             1);
                             
  BitmapLoader  loaderAtlas (itrAtlas);
  
  
  loaderAtlas.SetCodecByFilename  (strFileOut.AsChar ());
  loaderAtlas.Save ();

  // save bitmap to disk
  loaderAtlas.parserBuffer.WriteToFile (strFileOut.AsChar ());

  // create the atlas info
  RStrParser  parserAtlasInfo;

  // write the header
  INT  iTotalNumImages = arrayImageInfo.Length ();
  INT  iPage = 0;
  parserAtlasInfo.AppendFormat ("info name=\"%s\" sizeW=%d sizeH=%d\n", FilePath::GetFilenameNoExtFromPath (strFileOut.AsChar ()), 
                                                                        iAtlasWidth,
                                                                        iAtlasHeight);

  parserAtlasInfo.AppendFormat ("page id=%d file=\"%s\"\n", iPage,
                                                            strFileOut.AsChar ());
  parserAtlasInfo.AppendFormat ("images count=%d\n", iTotalNumImages);
  
  // write the per-image info
  for (INT  iIndex = 0; iIndex < iTotalNumImages; ++iIndex)
    {
    ImageInfo *  pInfo = (ImageInfo *) arrayImageInfo[iIndex];
    
    parserAtlasInfo.AppendFormat ("image id=\"%s\" x=%d y=%d width=%d height=%d sizeW=%d sizeH=%d xoffset=%d yoffset=%d page=%d\n", 
                                  FilePath::GetFilenameNoExtFromPath (pInfo->GetFilename ()), 
                                  pInfo->GetAtlasX (),
                                  pInfo->GetAtlasY (),
                                  pInfo->GetOpaqueWidth (),
                                  pInfo->GetOpaqueHeight (),
                                  pInfo->GetWidth (),
                                  pInfo->GetHeight (),
                                  -pInfo->GetClearLeft (),
                                  -pInfo->GetClearTop (),
                                  iPage);
    };
  
  RStr  strInfoFilename = FilePath::GetPathNoExt (strFileOut.AsChar ());
  strInfoFilename.AppendChars (".atlas");
  
  parserAtlasInfo.WriteToFile (strInfoFilename.AsChar ());

  
  BitmapLoader::UnregisterAllCodecs  ();
  
  return (0);
  }
  


//------------------------------------------------------------------------------
VOID TileImagesOnBitmap (PtrArray *   parrayImageInfo,
                         INT &        iPixWidthOut,
                         INT &        iPixHeightOut,
                         IntArray *   paiAtlasPixelsOut)
  {
  //printf ("TileSpritesOnCanvas - Begin\n");
  // Pass through all the sprites across all the books:
  
  INT    iMaxTexSize = 4096;
  INT    iTotalNumImages = 0;
  INT    iUnitPixelWidth   = 0;
  INT    iUnitPixelHeight  = 0;
  INT    iMaxUnitPixelWidth   = 0;
  INT    iMaxUnitPixelHeight  = 0;
  INT    iMinUnitPixelWidth   = 65000;
  INT    iMinUnitPixelHeight  = 65000;
  INT    iImageWidth;
  INT    iImageHeight;
  INT    iMaxUnitWidth  = 0;
  INT    iMaxUnitHeight = 0;
  
  INT    iImageIndex;

  
  ASSERT (paiAtlasPixelsOut != NULL);
  
  // Count how many there are
  iTotalNumImages = parrayImageInfo->Length ();
  
  // Record the largest and smallest width,height pairs.
  for (iImageIndex = 0; iImageIndex < iTotalNumImages; ++iImageIndex)
    {
    iImageWidth  = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetOpaqueWidth ();
    iImageHeight = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetOpaqueHeight ();
    
    if ((iImageWidth != 0) && (iImageHeight != 0))
      {
      iMaxUnitPixelWidth  = RMax (iMaxUnitPixelWidth,  iImageWidth);
      iMaxUnitPixelHeight = RMax (iMaxUnitPixelHeight, iImageHeight);
      iMinUnitPixelWidth  = RMin (iMinUnitPixelWidth,  iImageWidth);
      iMinUnitPixelHeight = RMin (iMinUnitPixelHeight, iImageHeight);
      };
    };

    
  //printf ("Max image dimensions are %dx%d.  Min %dx%d.  Total image count %d \n", iMaxUnitPixelWidth, iMaxUnitPixelHeight, iMinUnitPixelWidth, iMinUnitPixelHeight, iTotalNumImages);
  
  iUnitPixelWidth  = 1;//iMinUnitPixelWidth;
  iUnitPixelHeight = 1;//iMinUnitPixelHeight; 
  
  // Allocate temp arrays of iTotalNumImages size
  IntArray  aiUnitWidth   (iTotalNumImages);
  IntArray  aiUnitHeight  (iTotalNumImages);
  IntArray  aiUnitArea    (iTotalNumImages);
  IntArray  aiSortedIndex (iTotalNumImages);
  IntArray  aiImageIndex  (iTotalNumImages);
    
  // Pass through all the images a second time:
  INT   iTotalUnitArea = 0;
  for (iImageIndex = 0; iImageIndex < iTotalNumImages; ++iImageIndex)
    {
    iImageWidth  = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetOpaqueWidth ();
    iImageHeight = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetOpaqueHeight ();
    
    if ((iImageWidth != 0) && (iImageHeight != 0))
      {
      // Calculate unitWidth and unitHeight, with a unit being minWidth x minHeight
      aiUnitWidth  [iImageIndex] = (iImageWidth  + iUnitPixelWidth  - 1) / iUnitPixelWidth;
      aiUnitHeight [iImageIndex] = (iImageHeight + iUnitPixelHeight - 1) / iUnitPixelHeight;
      
      // Calculate maxUnitWidth and maxUnitHeight    
      iMaxUnitWidth  = RMax (iMaxUnitWidth,  aiUnitWidth  [iImageIndex]); 
      iMaxUnitHeight = RMax (iMaxUnitHeight, aiUnitHeight [iImageIndex]); 
      
      // Calculate unitArea for sprite entry (unitWidth x unitHeight)
      aiUnitArea [iImageIndex] = aiUnitWidth [iImageIndex] * aiUnitHeight [iImageIndex];
      
      // Sum total unit areas (totalUnitArea)
      iTotalUnitArea += aiUnitArea [iImageIndex];
      
      // store image index (for sorted access later)
      aiImageIndex [iImageIndex] = iImageIndex;
      };
    };

  // Sort sprites by unitArea.
  SortIndexes (aiUnitArea, aiSortedIndex);
  
  // Find the smallest square that would contain totalUnitArea, that is at least
  //   maxUnitWidth wide and maxUnitHeight tall.  This is borderUnitWidth
  //   x borderUnitHeight.  The square size is calculated in pixels.
  INT  iTexSidePixels = 1;
  INT  iTexWidthInUnits  = 0;
  INT  iTexHeightInUnits = 0;
  for (iTexSidePixels = 1; iTexSidePixels < iMaxTexSize; iTexSidePixels *= 2)
    {
    iTexWidthInUnits  = (iTexSidePixels / iUnitPixelWidth);
    iTexHeightInUnits = (iTexSidePixels / iUnitPixelHeight);
    
    if ((iTexSidePixels >= (iMaxUnitWidth * iUnitPixelWidth)) &&
        (iTexSidePixels >= (iMaxUnitHeight * iUnitPixelHeight)) &&
        ((iTexWidthInUnits * iTexHeightInUnits) >= iTotalUnitArea))
      {
      // potentally found a texture size
      break;
      };
    };

  INT  iNumTriesToAllocateTex = 0;
  INT  iMaxTriesToAllocateTex = 1;
  for (; iNumTriesToAllocateTex < iMaxTriesToAllocateTex; ++iNumTriesToAllocateTex)
    {
    //printf ("TileSpritesOnCanvas %d\n", __LINE__);
    
    BOOL  bRetryAllocatingTex = FALSE;
    
    // Create an int array of (borderUnitWidth x borderUnitHeight) to track 
    //   allocated units. allocatedUnits.  Initialize to -1.
    printf ("Tex Side Pixels %d  UnitPixelWidth %d\n", iTexSidePixels, iUnitPixelWidth);
    iTexWidthInUnits  = (iTexSidePixels / iUnitPixelWidth);
    iTexHeightInUnits = (iTexSidePixels / iUnitPixelHeight);
    
    IntArray *  paiAllocatedUnits = new IntArray (iTexWidthInUnits * iTexHeightInUnits);
    paiAllocatedUnits->InitValues (0, iTexWidthInUnits * iTexHeightInUnits, -1);
    
    // Step through each image a third time (this time use the temp arrays)
    for (iImageIndex = 0; iImageIndex < iTotalNumImages; ++iImageIndex)
      {
      //printf ("TileSpritesOnCanvas %d (image %d) texW %d texH %d unitWH %dx%d\n", __LINE__, iImageIndex, iTexWidthInUnits, iTexHeightInUnits, aiUnitWidth  [iImageIndex], aiUnitHeight [iImageIndex]);
      
      //   Step through allocatedUnits.
      BOOL  bSlotFound = 0;
      for (INT  iUnitIndexY = 0; iUnitIndexY < iTexHeightInUnits; ++iUnitIndexY)
        {
        for (INT  iUnitIndexX = 0; iUnitIndexX < iTexWidthInUnits; ++iUnitIndexX)
          {
          //     Find the first area that will store the image.  Reserve it in allocatedUnits
          //     and track its location in textureX and textureY.
              //printf ("Sprite %d %d checking grid %d %d\n", uCurrBook, uCurrPage, iUnitIndexX, iUnitIndexY);
          bSlotFound = 1;
          for (INT  iIndexY = 0; iIndexY < aiUnitHeight  [iImageIndex]; ++iIndexY)
            {
            for (INT  iIndexX = 0; iIndexX < aiUnitWidth  [iImageIndex]; ++iIndexX)
              {
              if ((*paiAllocatedUnits) [((iUnitIndexY + iIndexY) * iTexWidthInUnits) + 
                                        (iUnitIndexX + iIndexX)] != -1)
                {
                //printf ("Taken\n");
                bSlotFound = 0;
                break;
                };
              };
            if (! bSlotFound) break; // if any square is taken, move on to the next
            };
          if (bSlotFound)
            {
            // this unit area is available
            
            for (INT  iIndexY = 0; iIndexY < aiUnitHeight  [iImageIndex]; ++iIndexY)
              {
              for (INT  iIndexX = 0; iIndexX < aiUnitWidth  [iImageIndex]; ++iIndexX)
                {
                (*paiAllocatedUnits) [((iUnitIndexY + iIndexY) * iTexWidthInUnits) + 
                                      (iUnitIndexX + iIndexX)] = iImageIndex;
                };
              };
            // store the texture coordinates for the image.  
            ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->SetAtlasX (iUnitIndexX * iUnitPixelWidth);
            ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->SetAtlasY (iUnitIndexY * iUnitPixelHeight);
            
            break; // done with this image
            }; // not taken
          };
        if (bSlotFound) break; // done with this image
        };
      
      if (! bSlotFound)
        {
        // If we cannot fit a image, increase the smallest square size and jump
        //   back to allocating allocatedUnits again. (try one more time).
        iMaxTriesToAllocateTex = 2;
        bRetryAllocatingTex = 1;
        iTexSidePixels *= 2;
        break;
        };
      if (bRetryAllocatingTex) break;
      };
    delete paiAllocatedUnits;
    }; // num tries to allocate tex
  
  // now you can create and fill out the atlas.
  iPixWidthOut  = iTexSidePixels;
  iPixHeightOut = iTexSidePixels;
  paiAtlasPixelsOut->SetLength (iPixWidthOut * iPixHeightOut);
  paiAtlasPixelsOut->InitValues (0, paiAtlasPixelsOut->Length (), MakeRGBA (0, 0, 0, 0));

  ImageBoxIterator itrAtlas (paiAtlasPixelsOut->GetRawBuffer (),
                             iPixWidthOut,
                             iPixHeightOut,
                             1,
                             4, // 4 channels
                                                      
                             0,0,0, // box start
                                                      
                             iPixWidthOut,
                             iPixHeightOut,
                             1);
  
  // copy your images to the atlas
  Color8U  clrPixel;
  for (iImageIndex = 0; iImageIndex < iTotalNumImages; ++iImageIndex)
    {
    INT  iAtlasX = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetAtlasX ();
    INT  iAtlasY = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetAtlasY ();

    iImageWidth  = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetOpaqueWidth ();
    iImageHeight = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetOpaqueHeight ();
    
    // copy the image
    
    itrAtlas.DefineActiveRegion (iAtlasX,
                                 iAtlasY,
                                 0,
                                 iImageWidth,
                                 iImageHeight,
                                 1);

    ImageBoxIterator &  itrImage = ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetIterator ();
    
    itrImage.DefineActiveRegion (((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetClearLeft (),
                                 ((ImageInfo *)((*parrayImageInfo)[iImageIndex]))->GetClearTop (),
                                 0,
                                 iImageWidth,
                                 iImageHeight,
                                 1);

    for (INT  iIndexY = 0; iIndexY < iImageHeight; ++iIndexY)
      {
      itrAtlas.SetBoxPosition (0, iIndexY);
      itrImage.SetBoxPosition (0, iIndexY);
      
      for (INT  iIndexX = 0; iIndexX < iImageWidth; ++iIndexX)
        {
        clrPixel = itrImage.GetPixel8U ();
        itrAtlas.Set (clrPixel.R (), clrPixel.G (), clrPixel.B (), clrPixel.A ());
        };
      };
    };
  };                                      


  