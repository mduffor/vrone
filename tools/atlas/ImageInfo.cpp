/* -----------------------------------------------------------------
                             Image Info

     This class stores data about bitmaps being encoded onto the 
   atlas.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "image/BitmapLoader.hpp"
#include "ImageInfo.hpp"


//========================================================================
//  ImageInfo
//========================================================================

//------------------------------------------------------------------------
ImageInfo::ImageInfo ()
  {
  
  strFilename = "";
  iClearTop = iClearBottom = iClearLeft = iClearRight = 0;
  iWidth = iHeight = iDepth =  0;
  iNumChannels = 0;
  iAtlasX = iAtlasY = 0;
  };

//------------------------------------------------------------------------
ImageInfo::~ImageInfo ()
  {
  };

//------------------------------------------------------------------------
VOID ImageInfo::LoadImage (const char *  szFilenameIn)
  {
  strFilename = szFilenameIn;
  EStatus  status = bitmapLoader.LoadIntoBuffer (szFilenameIn,
                                                 &aiImage,
                                                 iWidth,
                                                 iHeight,
                                                 iDepth,
                                                 iNumChannels,
                                                 TRUE);  
  itrImage.SetBuffer (aiImage.GetRawBuffer (),
                      iWidth,
                      iHeight,
                      iDepth,
                      iNumChannels);
  itrImage.DefineActiveRegion (0, 0, 0, iWidth, iHeight, iDepth);
  };                                                 

//------------------------------------------------------------------------
VOID ImageInfo::CalcBounds (VOID)
  {
  INT  iRowIndex;
  INT  iColIndex;
  
  INT  iTMax = 0;
  INT  iBMin = iHeight - 1;
  INT  iLMax = 0;
  INT  iRMin = iWidth - 1;

  
  //itrImage.SetBoxPosition (0, 0);
  //Color8U colorTest = itrImage.GetPixel8U ();
  //printf ("Color 0,0 = %d %d %d %d\n", colorTest.R(), colorTest.G(), colorTest.B(), colorTest.A());
  
  //itrImage.SetBoxPosition (10, 12);
  //colorTest = itrImage.GetPixel8U ();
  //printf ("Color 10,12 = %d %d %d %d\n", colorTest.R(), colorTest.G(), colorTest.B(), colorTest.A());
  
  // eliminate top empty lines
  for (iRowIndex = 0; iRowIndex < iHeight; ++iRowIndex)
    {
    itrImage.SetBoxPosition (0, iRowIndex);
    for (iColIndex = 0; iColIndex < iWidth; ++iColIndex)
      {
      Color8U color = itrImage.GetPixel8U ();

      if (color.A () != 0) 
        {
        iTMax = iRowIndex;
        iLMax = iRMin = iColIndex;
        // break out of outer most loop
        iRowIndex = iHeight;
        break;
        }
      }
    }
  
  // eliminate bottom empty lines
  for (iRowIndex = iBMin; iRowIndex > 0; --iRowIndex)
    {
    itrImage.SetBoxPosition (0, iRowIndex);
    for (iColIndex = 0; iColIndex < iWidth; ++iColIndex)
      {
      Color8U color = itrImage.GetPixel8U ();

      if (color.A () != 0) 
        {
        iBMin = iRowIndex;
        iLMax = RMin (iLMax, iColIndex);
        iRMin = RMax (iRMin, iColIndex);
        // break out of outer most loop
        iRowIndex = -1;
        break;
        }
      }
    }
  
  // eliminate left empty lines
  for (iRowIndex = iTMax + 1; iRowIndex < iBMin; ++iRowIndex)
    {
    itrImage.SetBoxPosition (0, iRowIndex);
    for (iColIndex = 0; iColIndex < iLMax; ++iColIndex)
      {
      Color8U color = itrImage.GetPixel8U ();

      if (color.A () != 0)
        {
        iLMax = iColIndex;
        break;
        }
      }
    }
  
  // eliminate right empty lines
  for (iRowIndex = iTMax; iRowIndex <= iBMin; ++iRowIndex)
    {
    itrImage.SetBoxPosition (iRMin, iRowIndex);
    for (iColIndex = iRMin; iColIndex < iWidth; ++iColIndex)
      {
      Color8U color = itrImage.GetPixel8U ();

      if (color.A () != 0)
        {
        iRMin = RMax (iRMin, iColIndex);
        }
      }
    }
    
  iClearTop    = iTMax;
  iClearBottom = iHeight - 1 - iBMin;
  iClearLeft   = iLMax;
  iClearRight  = iWidth - 1 - iRMin;
  };
  
  
  