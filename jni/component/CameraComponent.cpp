/* -----------------------------------------------------------------
                             Camera Component

     This module implements a component for holding Camera data.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "component/CameraComponent.hpp"

//-----------------------------------------------------------------------------
//  CameraComponent
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
CameraComponent::CameraComponent ()
  {
  strType = szCameraComponentID;
  
  //pattrFocalLength      = (AttrFloat*) AddAttr ("focalLength", szFloatAttrIdentifier);
  pattrHorizFOV         = (AttrFloat*) AddAttr ("horizFOV", szFloatAttrIdentifier);
  pattrNearClip         = (AttrFloat*) AddAttr ("nearClip", szFloatAttrIdentifier);
  pattrFarClip          = (AttrFloat*) AddAttr ("farClip", szFloatAttrIdentifier);
  pattrApertureX        = (AttrFloat*) AddAttr ("apertureX", szFloatAttrIdentifier);
  pattrApertureY        = (AttrFloat*) AddAttr ("apertureY", szFloatAttrIdentifier);
  pattrOrthoWidth       = (AttrFloat*) AddAttr ("orthoWidth", szFloatAttrIdentifier);
  pattrOrthoHeight      = (AttrFloat*) AddAttr ("orthoHeight", szFloatAttrIdentifier);
  pattrOrthographic     = (AttrInt*)   AddAttr ("orthographic", szIntAttrIdentifier);
  };
  
//-----------------------------------------------------------------------------
CameraComponent::~CameraComponent ()
  {
  };


//-----------------------------------------------------------------------------
Attr *  CameraComponent::SetAttr (const char *  szNameIn,
                                  const char *  szValueIn)  
  {
  Attr *  pattrChanged = Component::SetAttr (szNameIn, szValueIn);

  if (pattrChanged == NULL) 
    {
    return NULL;
    }

  // handle any changes that result from setting new values in attrs
  if (pattrChanged == (Attr *) pattrHorizFOV)
    {
    camera.SetHorizFOV (pattrHorizFOV->Value ());
    };

  if ((pattrChanged == (Attr *) pattrNearClip) || (pattrChanged == (Attr *) pattrFarClip))
    {
    camera.SetNearFar (pattrNearClip->Value (), pattrFarClip->Value ());
    };
    
  // Actually, we need some other variable other than viewport.
  //if ((pattrChanged == (Attr *) pattrApertureX) || (pattrChanged == (Attr *) pattrApertureY))
  //  {
  //  camera.SetViewport (pattrApertureX->Value (), pattrApertureY->Value ());
  //  };

  if ((pattrChanged == (Attr *) pattrOrthoWidth) || (pattrChanged == (Attr *) pattrOrthoHeight))
    {
    camera.SetOrthoSize (pattrOrthoWidth->Value (), pattrOrthoHeight->Value ());
    };
    
  if (pattrChanged == (Attr *) pattrOrthographic)
    {
    camera.SetOrthographic (pattrOrthographic->Value () != 0);
    };
  return pattrChanged;
  }