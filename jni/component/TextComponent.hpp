/* -----------------------------------------------------------------
                             TextComponent

     This module implements a component for holding text data.
    

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef TEXTCOMPONENT_HPP
#define TEXTCOMPONENT_HPP

#include "Types.hpp"
#include "AttrString.hpp"
#include "Component.hpp"
#include "component/MeshComponent.hpp"
#include "component/TransformComponent.hpp"
#include "mesh/BitmapFont.hpp"


const char szTextComponentID[] = "Text";

//-----------------------------------------------------------------------------
class TextComponent : public MeshComponent
  {
  private:
  
    BitmapFont *          pFont;
    RStr                  strActiveFont;
    AttrString *          pattrFontName;
    AttrString *          pattrText;
    TransformComponent *  pCachedTransform;
    
  public:
                          TextComponent  ();
                          
    virtual               ~TextComponent ();
    
    static  const char *  Identifier       (VOID)                         {return szTextComponentID;};
    
    virtual Component *   GetInterface     (const char *  szTypeIn)       {if streq(szTypeIn, szTextComponentID) return ((Component *) this); return MeshComponent::GetInterface (szTypeIn);};
            
    virtual Component *   Instantiate      (VOID) const                   {return new TextComponent;};
    
    virtual Attr *        SetAttr          (const char *  szNameIn,
                                            const char *  szValueIn);
    
    virtual VOID          AddTriangleStrip (RMatrix &      matParentWorldInv,
                                            DisplayMesh *  pDisplayMesh);

    VOID                  CacheFont       (VOID);
    
    VOID                  CacheTransform   (VOID);
    
    virtual const char *  GetMaterialName  (VOID);
    
  };
  
#endif // TEXTCOMPONENT_HPP
