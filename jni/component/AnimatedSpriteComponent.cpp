/* -----------------------------------------------------------------
                             AnimatedSpriteComponent

     This module implements a component for animating a sprite component.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "AnimatedSpriteComponent.hpp"
#include "Shell.hpp"

//-----------------------------------------------------------------------------
//  AnimatedSpriteComponent
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
AnimatedSpriteComponent::AnimatedSpriteComponent ()
  {
  strType = szAnimatedSpriteComponentID;
  pclipCurr = NULL;
  fFPS = 6.0f;
  fClipTime = 0.0f;
  iCurrFrame = iLastFrame = -1;
  
  pattrClipQueue  = (AttrStringArray*) AddAttr ("clipQueue", szStringArrayAttrIdentifier);
  };
  
//-----------------------------------------------------------------------------
AnimatedSpriteComponent::~AnimatedSpriteComponent ()
  {
  };

//-----------------------------------------------------------------------------
VOID  AnimatedSpriteComponent::OnUpdate  (VOID)
  {
  if (pclipCurr == NULL) 
    {
    if (pattrClipQueue->Value().Length () >= 1)
      {
      pclipCurr = FindClip (pattrClipQueue->Value()[0].AsChar ());
      if (pclipCurr == NULL) return;
      }
    else
      {
      return;
      }
    };
  
  fClipTime += FLOAT(Shell::GetTimeDelta ()) / 1000.0f;

  iCurrFrame = INT (floor (fClipTime * fFPS));

  if (iCurrFrame != iLastFrame)
    {
    iLastFrame = iCurrFrame;
    if (iCurrFrame >= pclipCurr->GetNumFrames ())
      {
      // reached end of animation.
      iCurrFrame = 0;
      fClipTime = 0.0f;
      
      if (pattrClipQueue->Value().Length () > 1)
        {
        // there is another animation queued after this.  Play it.
        pattrClipQueue->Value().Remove (0);
        }
      else
        {
        // loop this anim
        // TODO: Trigger callback so animation state machine can advance, or other
        //  animations/effects/events can be launched.
        }
      };
    pclipCurr = FindClip (pattrClipQueue->Value()[0].AsChar ());
    if (pclipCurr != NULL)
      {
      SetAtlasName (pclipCurr->GetAtlasName ());
      SetSpriteName (pclipCurr->GetSpriteName (iCurrFrame));
      MarkAsDirty ();
      }
    };
  };

//-----------------------------------------------------------------------------
SpriteClipComponent *  AnimatedSpriteComponent::FindClip (const char *  szClipNameIn)
  {
  // TODO: We should probably cache or serialize a list of animations, so we can 
  //  keep them elsewhere in the heirarcy and re-use them, but for now I'm just
  //  searching for the clips on the same object.
  
  for (Component*  pcmpCurr = GetFirstSibling ();
       pcmpCurr->IsValid ();
       pcmpCurr = pcmpCurr->Next ())
    {
    Component *  pcmpInterface = pcmpCurr->GetInterface (SpriteClipComponent::Identifier ());
    if (pcmpInterface != NULL)
      {
      SpriteClipComponent *  pcmpClip = (SpriteClipComponent *) pcmpInterface;
      
      if (streq (pcmpClip->GetClipName (), szClipNameIn))
        {
        // found the matching clip
        return (pcmpClip);
        };
      };
    };
  return (NULL);
  };

//-----------------------------------------------------------------------------
VOID  AnimatedSpriteComponent::Play  (const char *  szClipNameIn,
                                      BOOL          bPlayImmediately)
  {
  if (bPlayImmediately)
    {
    pattrClipQueue->Value().Clear ();
    pclipCurr = NULL;
    iCurrFrame = 0;
    fClipTime = 0.0f;
    };
  pattrClipQueue->Value().Append (szClipNameIn);
  };
  
//-----------------------------------------------------------------------------
Attr *  AnimatedSpriteComponent::SetAttr (const char *  szNameIn,
                                          const char *  szValueIn)
  {
  Attr * pattrChanged = SpriteComponent::SetAttr (szNameIn, szValueIn);
  
  // handle any changes that result from setting new values in attrs
  if ((pattrChanged != NULL) && (pattrChanged == (Attr *) pattrAtlasName))
    {
    if (!streq (strActiveAtlas.AsChar(), szValueIn))
      {
      // atlas name has changed
      pAtlas = NULL;
      CacheAtlas ();
      };
    }
  return pattrChanged;
  };

