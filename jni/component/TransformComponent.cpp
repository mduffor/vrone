/* -----------------------------------------------------------------
                             Transform Component

     This module implements a component for holding Transform data.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "TransformComponent.hpp"

//-----------------------------------------------------------------------------
//  TransformComponent
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TransformComponent::TransformComponent ()
  {
  strType = szTransformComponentID;
  
  pattrTx = (AttrFloat*) AddAttr ("tx", szFloatAttrIdentifier);
  pattrTy = (AttrFloat*) AddAttr ("ty", szFloatAttrIdentifier);
  pattrTz = (AttrFloat*) AddAttr ("tz", szFloatAttrIdentifier);

  pattrRx = (AttrFloat*) AddAttr ("rx", szFloatAttrIdentifier);
  pattrRy = (AttrFloat*) AddAttr ("ry", szFloatAttrIdentifier);
  pattrRz = (AttrFloat*) AddAttr ("rz", szFloatAttrIdentifier);

  pattrSx = (AttrFloat*) AddAttr ("sx", szFloatAttrIdentifier);
  pattrSy = (AttrFloat*) AddAttr ("sx", szFloatAttrIdentifier);
  pattrSz = (AttrFloat*) AddAttr ("sx", szFloatAttrIdentifier);
  
  
  
  };
  
//-----------------------------------------------------------------------------
TransformComponent::~TransformComponent ()
  {
  };


//-----------------------------------------------------------------------------
Attr *  TransformComponent::SetAttr (const char *  szNameIn,
                                     const char *  szValueIn)  
  {
  Attr *  pattrChanged = Component::SetAttr (szNameIn, szValueIn);

  if (pattrChanged == NULL) 
    {
    return NULL;
    }
  
  // handle any changes that result from setting new values in attrs
  if ((pattrChanged == (Attr *) pattrTx) ||
      (pattrChanged == (Attr *) pattrTy) ||
      (pattrChanged == (Attr *) pattrTz))
    {
    transform.SetPosition (pattrTx->Value (), pattrTy->Value (), pattrTz->Value ());
    };
  if ((pattrChanged == (Attr *) pattrRx) ||
      (pattrChanged == (Attr *) pattrRy) ||
      (pattrChanged == (Attr *) pattrRz) ||
      (pattrChanged == (Attr *) pattrRotateOrder))
    {
    transform.SetEuler (pattrRx->Value (), pattrRy->Value (), pattrRz->Value (), (Euler::EOrder) pattrRotateOrder->Value ());
    };
  if ((pattrChanged == (Attr *) pattrSx) ||
      (pattrChanged == (Attr *) pattrSy) ||
      (pattrChanged == (Attr *) pattrSz))
    {
    transform.SetScale (pattrSx->Value (), pattrSy->Value (), pattrSz->Value ());
    };
  return pattrChanged;
  }