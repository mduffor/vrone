/* -----------------------------------------------------------------
                             Mesh Component

     This module implements a component for holding Mesh data.
   It is designed to be a base class from which other specific mesh
   types derive.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef MESHCOMPONENT_HPP
#define MESHCOMPONENT_HPP

#include "Types.hpp"
#include "Component.hpp"
#include "mesh/DisplayMesh.hpp"
#include "RMatrix.hpp"

const char szMeshComponentID[] = "Mesh";

//-----------------------------------------------------------------------------
class MeshComponent : public Component
  {
  private:
    
  public:
                          MeshComponent    ();
    virtual               ~MeshComponent   ();
    
    static  const char *  Identifier       (VOID)                          {return szMeshComponentID;};
    
    virtual Component *   GetInterface     (const char *  szTypeIn)        {if streq(szTypeIn, szMeshComponentID) return ((Component *) this); return (NULL);};
            
    virtual Component *   Instantiate      (VOID) const                    {return new MeshComponent;};
    
    virtual VOID          AddTriangleStrip (RMatrix &      matParentWorldInv,
                                            DisplayMesh *  pDisplayMesh)   {};
                                            
    virtual const char *  GetMaterialName  (VOID);
                                            
  };
  
#endif // MESHCOMPONENT_HPP
