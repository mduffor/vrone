/* -----------------------------------------------------------------
                             Font Component

     This module implements a component for interfacing with the 
   Font class.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "FontComponent.hpp"
#include "Material.hpp"


//-----------------------------------------------------------------------------
//  FontComponent
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
FontComponent::FontComponent ()
  {
  pFont = NULL;
  strType = szFontComponentID;
  
  pattrFontName = (AttrString*) AddAttr ("fontName", szStringAttrIdentifier);
  
  };
  
//-----------------------------------------------------------------------------
FontComponent::~FontComponent ()
  {
  };

//-----------------------------------------------------------------------------
Attr *  FontComponent::SetAttr (const char *  szNameIn,
                                 const char *  szValueIn)  
  {
  Attr *  pattrChanged = Component::SetAttr (szNameIn, szValueIn);

  if (pattrChanged == NULL) 
    {
    return NULL;
    };
    
  // handle any changes that result from setting new values in attrs
  if (pattrChanged == (Attr *) pattrFontName)
    {
    LoadFont (pattrFontName->Value ().AsChar ());
    };
  return (pattrChanged);
  };

//------------------------------------------------------------------------------
VOID FontComponent::LoadFont (const char *  szFontFileName)
  {
  RStrParser  parserFontDef;  parserFontDef.ReadFromFile (szFontFileName);
  
  pFont = new BitmapFont ();
  pFont->ParseFont (parserFontDef.AsChar ());
  
  new Material (pFont->GetMaterialName (), "ShdMaskedColor");
  Material *  pmtlFont = Material::FindByName (pFont->GetMaterialName ());
  
  if (pmtlFont != NULL)
    {
    DBG_INFO ("Setting material's diffuse texture %s", pFont->GetTexturePath ());
    pmtlFont->SetDiffuseTexture (pFont->GetTexturePath ());
    }
  else
    {
    DBG_INFO ("Failed to load material %s", pFont->GetName ());
    };
  }
