/* -----------------------------------------------------------------
                             Camera Component

     This module implements a component for holding Camera data.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef CAMERACOMPONENT_HPP
#define CAMERACOMPONENT_HPP

#include "Types.hpp"
#include "Component.hpp"
#include "Camera.hpp"
#include "AttrFloat.hpp"
#include "AttrInt.hpp"

const char szCameraComponentID[] = "Camera";

//-----------------------------------------------------------------------------
class CameraComponent : public Component
  {
  private:
    Camera             camera;

    //AttrFloat *        pattrFocalLength;
    AttrFloat *        pattrHorizFOV;
    AttrFloat *        pattrNearClip;
    AttrFloat *        pattrFarClip;
    AttrFloat *        pattrApertureX;
    AttrFloat *        pattrApertureY;
    AttrFloat *        pattrOrthoWidth;
    AttrFloat *        pattrOrthoHeight;
    AttrInt *          pattrOrthographic;
    
  public:
                          CameraComponent  ();
    virtual               ~CameraComponent ();
    
    static  const char *  Identifier       (VOID)                          {return szCameraComponentID;};
    
    virtual Component *   GetInterface     (const char *  szTypeIn)        {if streq(szTypeIn, szCameraComponentID) return ((Component *) this); return (NULL);};
            
    virtual Component *   Instantiate      (VOID) const                    {return new CameraComponent;};
    
    virtual Camera &      GetCamera        (VOID)                          {return camera;};
    
    virtual Attr *        SetAttr          (const char *  szNameIn,
                                            const char *  szValueIn);     
  };
  
#endif // CAMERACOMPONENT_HPP
