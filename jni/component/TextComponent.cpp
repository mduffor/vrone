/* -----------------------------------------------------------------
                             TextComponent

     This module implements a component for holding text data.
    

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "TextComponent.hpp"

//-----------------------------------------------------------------------------
//  TextComponent
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
TextComponent::TextComponent ()
  {
  strType = szTextComponentID;
  
  pCachedTransform = NULL;
  pFont = NULL;
  
  pattrFontName = (AttrString*) AddAttr ("fontName", szStringAttrIdentifier);
  pattrText     = (AttrString*) AddAttr ("text", szStringAttrIdentifier);
  };
  
//-----------------------------------------------------------------------------
TextComponent::~TextComponent ()
  {
  };

//-----------------------------------------------------------------------------
VOID  TextComponent::AddTriangleStrip (RMatrix &      matParentWorldInv,
                                       DisplayMesh *  pDisplayMesh)
  {
  CacheTransform ();
  CacheFont ();
  if (pFont == NULL)
    {
    return;
    };
    
  RMatrix  matFont;
  if (pCachedTransform == NULL)
    {
    matFont = matParentWorldInv;
    }
  else
    {
    matFont = matParentWorldInv * pCachedTransform->GetTransform().WorldMatrix();
    };
  // add triangle strips.
  pFont->AddTriangleStrip (pattrText->Value().AsChar (),
                           BitmapFont::Center,
                           1,
                           pDisplayMesh);
  };

//-----------------------------------------------------------------------------
Attr *  TextComponent::SetAttr (const char *  szNameIn,
                                const char *  szValueIn)
  {
  Attr * pattrChanged = Component::SetAttr (szNameIn, szValueIn);
  
  // handle any changes that result from setting new values in attrs
  if ((pattrChanged != NULL) && (pattrChanged == (Attr *) pattrFontName))
    {
    if (!streq (strActiveFont.AsChar(), szValueIn))
      {
      // atlas name has changed
      pFont = NULL;
      CacheFont ();
      };
    }
  return pattrChanged;
  };

//-----------------------------------------------------------------------------
VOID  TextComponent::CacheFont (VOID)
  {
  if (pFont != NULL) return;
  pFont = BitmapFont::FindByName (pattrFontName->Value().AsChar ());
  if (pFont != NULL)
    {
    strActiveFont = pattrFontName->Value();
    }
  else
    {
    DBG_ERROR ("TextComponent was unable cache Font named : %s", pattrFontName->Value().AsChar ());
    }
  };

//-----------------------------------------------------------------------------
VOID  TextComponent::CacheTransform (VOID)
  {
  if (pCachedTransform != NULL) {return;};
  
  Component *  pSearch = this->GetFirstSibling ();
  
  while (pSearch->IsValid ())
    {
    Component *  pcmpInterface = GetInterface ("Transform");
    if (pcmpInterface != NULL)
      {
      pCachedTransform = (TransformComponent *) pcmpInterface;
      return;
      };
    pSearch = pSearch->Next();
    };
  };
  
//-----------------------------------------------------------------------------
const char *  TextComponent::GetMaterialName  (VOID)
  {
  CacheFont (); 
  ASSERT (pFont != NULL)
  if (pFont == NULL)
    {
    DBG_ERROR ("TextComponent was unable to find Font named : %s", pattrFontName->Value().AsChar ());
    return NULL;
    };
    
  return (pFont->GetMaterialName ());
  };
  
  
