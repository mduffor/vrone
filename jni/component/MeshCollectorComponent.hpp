/* -----------------------------------------------------------------
                             Mesh Collector Component

     This module implements a component for collecting Mesh data.
   It scans for MeshCollectorComponents on child objects, and compiles them
   into a single DisplayMesh.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef MESHCOLLECTORCOMPONENT_HPP
#define MESHCOLLECTORCOMPONENT_HPP

#include "Types.hpp"
#include "Component.hpp"
#include "component/TransformComponent.hpp"
#include "mesh/DisplayMesh.hpp"
#include "RMatrix.hpp"
#include "AttrString.hpp"

const char szMeshCollectorComponentID[] = "MeshCollector";

//-----------------------------------------------------------------------------
class MeshCollectorComponent : public Component
  {
  private:
    
    INT                   iVersionSum;
    AttrString *          pattrMaterialName;
    RStr                  strActiveMaterialName;
    DisplayMesh           meshDisplay;    
    TransformComponent *  pCachedTransform;
    
  public:
                          MeshCollectorComponent    ();
                          
    virtual               ~MeshCollectorComponent   ();
    
    static  const char *  Identifier       (VOID)                          {return szMeshCollectorComponentID;};
    
    virtual Component *   GetInterface     (const char *  szTypeIn)        {if streq(szTypeIn, szMeshCollectorComponentID) return ((Component *) this); return (NULL);};
            
    virtual Component *   Instantiate      (VOID) const                    {return new MeshCollectorComponent;};
    
    virtual Attr *        SetAttr          (const char *  szNameIn,
                                            const char *  szValueIn);
                                            
    virtual VOID          OnPostUpdate     (VOID);
    
    VOID                  CacheTransform   (VOID);
    
  };
  
#endif // MESHCOMPONENT_HPP
