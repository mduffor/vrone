/* -----------------------------------------------------------------
                             SpriteClipComponent

     This module implements a component for holding sprite data.
    

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "SpriteClipComponent.hpp"

//-----------------------------------------------------------------------------
//  SpriteClipComponent
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
SpriteClipComponent::SpriteClipComponent ()
  {
  strType = szSpriteClipComponentID;
  
  pattrAtlasName  = (AttrString*) AddAttr ("atlasName", szStringAttrIdentifier);
  pattrClipName   = (AttrString*) AddAttr ("clipName", szStringAttrIdentifier);
  pattrSpriteSeq  = (AttrStringArray*) AddAttr ("spriteSeq", szStringArrayAttrIdentifier);
  };
  
//-----------------------------------------------------------------------------
SpriteClipComponent::~SpriteClipComponent ()
  {
  };

//-----------------------------------------------------------------------------
Attr *  SpriteClipComponent::SetAttr (const char *  szNameIn,
                                      const char *  szValueIn)
  {
  Attr * pattrChanged = Component::SetAttr (szNameIn, szValueIn);
  
  // handle any changes that result from setting new values in attrs
  return pattrChanged;
  };

//-----------------------------------------------------------------------------
const char *  SpriteClipComponent::GetAtlasName  (VOID)
  {
  return (pattrAtlasName->Value().AsChar ());
  };
    
//-----------------------------------------------------------------------------
const char *  SpriteClipComponent::GetSpriteName  (INT iFrameIn)
  {
  INT  iNumFrames = GetNumFrames ();
  INT  iCurrFrame = iFrameIn % iNumFrames;
  return (pattrSpriteSeq->ArrayValue(iCurrFrame).AsChar ());
  };

  