/* -----------------------------------------------------------------
                             Mesh Collector Component

     This module implements a component for collecting Mesh data.
   It scans for MeshCollectorComponents on child objects, and compiles them
   into a single DisplayMesh.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "component/MeshCollectorComponent.hpp"
#include "component/MeshComponent.hpp"
#include "component/TransformComponent.hpp"
#include "Material.hpp"
#include "Node.hpp"
#include "NodeDelegate.hpp"



//-----------------------------------------------------------------------------
class MeshCollectorCheckDirty : public NodeDelegate
  {
  public:
    INT           iVersionSum;
    const char *  pszMaterialName;
  
  public:
                  MeshCollectorCheckDirty          (const char *  pszMaterialIn) {iVersionSum = 0; pszMaterialName = pszMaterialIn;};
    virtual       ~MeshCollectorCheckDirty         () {};

    virtual BOOL  VisitNode             (Node *  pnodeIn)    {return TRUE;};
             
    virtual VOID  VisitComponent        (Node *       pnodeIn,
                                         Component *  pcomponentIn)   
                                           {
                                           Component *  pcmpInterface = pcomponentIn->GetInterface (MeshComponent::Identifier());
                                           if (pcmpInterface != NULL)
                                             {
                                             MeshComponent *  pcmpMesh = (MeshComponent *) pcmpInterface;
                                             if (streq (pcmpMesh->GetMaterialName(), pszMaterialName))
                                               {
                                               iVersionSum += pcmpMesh->Version ();
                                               };
                                             };
                                           };
                                         
    INT           VersionSum            (VOID)   {return iVersionSum;};
  };

//-----------------------------------------------------------------------------
class MeshCollectorAddStrips : public NodeDelegate
  {
  public:
    DisplayMesh *         pDisplayMesh;
    const char *          pszMaterialName;
    
    RMatrix *             pmatParentWorldInv;
    TransformComponent *  pCachedTransform;
  
  public:
                  MeshCollectorAddStrips          (const char *  pszMaterialIn) {pDisplayMesh = NULL; pszMaterialName = pszMaterialIn;};
    virtual       ~MeshCollectorAddStrips         () {};

    virtual BOOL  VisitNode             (Node *  pnodeIn)    {return TRUE;};
             
    virtual VOID  VisitComponent        (Node *       pnodeIn,
                                         Component *  pcomponentIn)   
                                           {
                                           Component *  pcmpMesh = pcomponentIn->GetInterface (szMeshComponentID);
                                           if (pcmpMesh != NULL)
                                             {
                                             if (streq (((MeshComponent *)pcmpMesh)->GetMaterialName(), pszMaterialName))
                                               {
                                               ((MeshComponent *)pcmpMesh)->AddTriangleStrip (*pmatParentWorldInv,
                                                                                              pDisplayMesh);
                                               };
                                             };
                                           };
                                         
    VOID          SetDisplayMesh        (DisplayMesh *  pmeshIn)   {pDisplayMesh = pmeshIn;};
    VOID          SetParentInvMatrix    (RMatrix *      pmatIn)    {pmatParentWorldInv = pmatIn;};
  };  

//-----------------------------------------------------------------------------
//  MeshCollectorComponent
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
MeshCollectorComponent::MeshCollectorComponent ()
  {
  strType = szMeshCollectorComponentID;
  iVersionSum = 0;
  
  pattrMaterialName  = (AttrString*) AddAttr ("mtlName", szStringAttrIdentifier);
  strActiveMaterialName.Empty ();
  pCachedTransform = NULL;
  
  // REFACTOR (9/1/14) The element counts depends on the shader.  For now, I'm 
  //  hard-coding it for the unlit shader such that it has no normals.
  meshDisplay.SetElementCounts (3, 0, 2);
  };
  
//-----------------------------------------------------------------------------
MeshCollectorComponent::~MeshCollectorComponent ()
  {
  
  };

//-----------------------------------------------------------------------------
VOID  MeshCollectorComponent::OnPostUpdate (VOID)
  {
  DBG_INFO ("MeshCollectorComponent::OnPostUpdate");
  // check if anything has moved or changed (upgraded version).  Exit if nothing needs updating.

  MeshCollectorCheckDirty  delegateCheckDirty (pattrMaterialName->Value().AsChar());
  
  delegateCheckDirty.RecurseNodeTree ((Node *) pParentNode);

  if ((iVersionSum == 0) || (delegateCheckDirty.VersionSum () != iVersionSum))
    {
    // if something is dirty, clear and rebuild display mesh
    iVersionSum = delegateCheckDirty.VersionSum ();
    
    CacheTransform ();
    RMatrix  matParentWorldInv;
    if (pCachedTransform == NULL)
      {
      matParentWorldInv.Identity ();
      }
    else
      {
      matParentWorldInv = pCachedTransform->GetTransform().WorldMatrix ();
      matParentWorldInv.Invert ();
      }
    
    // scan heirarchy for nodes with this material, and build display mesh
    meshDisplay.Reset ();
    MeshCollectorAddStrips  delegateSetStrips (pattrMaterialName->Value().AsChar());
    delegateSetStrips.SetDisplayMesh (&meshDisplay);
    delegateSetStrips.SetParentInvMatrix (&matParentWorldInv);
    
    delegateSetStrips.RecurseNodeTree ((Node *) pParentNode);
    };
  }

//-----------------------------------------------------------------------------
Attr *  MeshCollectorComponent::SetAttr (const char *  szNameIn,
                                         const char *  szValueIn)  
  {
  Attr *  pattrChanged = Component::SetAttr (szNameIn, szValueIn);
  
  // handle any changes that result from setting new values in attrs
  if ((pattrChanged != NULL) && (pattrChanged == (Attr *) pattrMaterialName))
    {
    Material *  pmtlCommon = Material::FindByName (pattrMaterialName->Value().AsChar ());
    if (pmtlCommon != NULL)
      {
      if (!strActiveMaterialName.IsEmpty ())
        {
        Material *  pmtlPrev = Material::FindByName (strActiveMaterialName.AsChar ());
        if (pmtlPrev != NULL)
          {
          pmtlPrev->RemoveMesh (&meshDisplay);
          }
        }
      strActiveMaterialName = pattrMaterialName->Value();
      pmtlCommon->AddMesh (&meshDisplay);
      }
    else
      {
      DBG_WARNING ("Unable to find %s mesh collector material", pattrMaterialName->Value().AsChar ());
      }    
    }
  return pattrChanged;
  }
  
//-----------------------------------------------------------------------------
VOID  MeshCollectorComponent::CacheTransform (VOID)
  {
  if (pCachedTransform != NULL) {return;};
  
  Component *  pSearch = this->GetFirstSibling ();
  
  while (pSearch->IsValid ())
    {
    Component *  pcmpInterface = GetInterface ("Transform");
    if (pcmpInterface != NULL)
      {
      pCachedTransform = (TransformComponent *) pcmpInterface;
      return;
      };
    pSearch = pSearch->Next();
    };
  };  
  



