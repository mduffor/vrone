/* -----------------------------------------------------------------
                          Component Init

     This module gives a central point for initializing components
   in the component/ subdirectory.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "Types.hpp"
#include "Component.hpp"
#include "Node.hpp"
#include "component/MeshComponent.hpp"
#include "component/SpriteComponent.hpp"
#include "component/AnimatedSpriteComponent.hpp"
#include "component/SpriteClipComponent.hpp"
#include "component/MeshCollectorComponent.hpp"
#include "component/AtlasComponent.hpp"
#include "component/TransformComponent.hpp"
#include "component/CameraComponent.hpp"
#include "component/FontComponent.hpp"
#include "component/TextComponent.hpp"

//-----------------------------------------------------------------------------
VOID  ComponentInit (VOID)
  {
  //Node::AddComponentTemplate (static_cast<Component*>(new MeshComponent ())); // MeshComponent is a base class
  Node::AddComponentTemplate (new SpriteComponent);
  Node::AddComponentTemplate (new AnimatedSpriteComponent);
  Node::AddComponentTemplate (new SpriteClipComponent);
  Node::AddComponentTemplate (new MeshCollectorComponent);
  Node::AddComponentTemplate (new AtlasComponent);
  Node::AddComponentTemplate (new TransformComponent);
  Node::AddComponentTemplate (new CameraComponent);
  Node::AddComponentTemplate (new FontComponent);
  Node::AddComponentTemplate (new TextComponent);
  };

  
  
  
  