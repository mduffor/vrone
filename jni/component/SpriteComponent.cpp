/* -----------------------------------------------------------------
                             SpriteComponent

     This module implements a component for holding sprite data.
    

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "SpriteComponent.hpp"

//-----------------------------------------------------------------------------
//  SpriteComponent
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
SpriteComponent::SpriteComponent ()
  {
  strType = szSpriteComponentID;
  
  pCachedTransform = NULL;
  pAtlas = NULL;
  
  pattrAtlasName  = (AttrString*) AddAttr ("atlasName", szStringAttrIdentifier);
  pattrSpriteName = (AttrString*) AddAttr ("spriteName", szStringAttrIdentifier);
  };
  
//-----------------------------------------------------------------------------
SpriteComponent::~SpriteComponent ()
  {
  };

//-----------------------------------------------------------------------------
VOID  SpriteComponent::AddTriangleStrip (RMatrix &      matParentWorldInv,
                                         DisplayMesh *  pDisplayMesh)
  {
  CacheTransform ();
  CacheAtlas ();
  if (pAtlas == NULL)
    {
    return;
    };
    
  RMatrix  matAtlas;
  if (pCachedTransform == NULL)
    {
    matAtlas = matParentWorldInv;
    }
  else
    {
    matAtlas = matParentWorldInv * pCachedTransform->GetTransform().WorldMatrix();
    };
  // find sprite in atlas
  // add triangle strips.
  pAtlas->AddTriangleStrip (pattrSpriteName->Value().AsChar (),
                            Atlas::Center,
                            1,
                            matAtlas,
                            pDisplayMesh);
  };

//-----------------------------------------------------------------------------
Attr *  SpriteComponent::SetAttr (const char *  szNameIn,
                                  const char *  szValueIn)
  {
  Attr * pattrChanged = MeshComponent::SetAttr (szNameIn, szValueIn);
  
  // handle any changes that result from setting new values in attrs
  if ((pattrChanged != NULL) && (pattrChanged == (Attr *) pattrAtlasName))
    {
    if (!streq (strActiveAtlas.AsChar(), szValueIn))
      {
      // atlas name has changed
      pAtlas = NULL;
      CacheAtlas ();
      };
    }
  return pattrChanged;
  };

//-----------------------------------------------------------------------------
VOID SpriteComponent::SetAtlasName (const char *  szAtlasNameIn)
  {
  if (streq (szAtlasNameIn, strActiveAtlas.AsChar ()))
    {
    // this Atlas is already set.
    return;
    };
  // atlas changed
  pAtlas = NULL;
  CacheAtlas ();
  }
    
//-----------------------------------------------------------------------------
VOID  SpriteComponent::SetSpriteName (const char *  szSpriteNameIn)
  {
  pattrSpriteName->Value().Set (szSpriteNameIn);
  };

//-----------------------------------------------------------------------------
VOID  SpriteComponent::CacheAtlas (VOID)
  {
  if (pAtlas != NULL) return;
  pAtlas = Atlas::FindByName (pattrAtlasName->Value().AsChar ());
  if (pAtlas != NULL)
    {
    strActiveAtlas = pattrAtlasName->Value();
    pAtlas->CreateMaterial ();
    }
  else
    {
    DBG_ERROR ("SpriteComponent was unable cache Atlas named : %s", pattrAtlasName->Value().AsChar ());
    }
  };

//-----------------------------------------------------------------------------
VOID  SpriteComponent::CacheTransform (VOID)
  {
  if (pCachedTransform != NULL) {return;};
  
  Component *  pSearch = this->GetFirstSibling ();
  
  while (pSearch->IsValid ())
    {
    Component *  pcmpInterface = GetInterface ("Transform");
    if (pcmpInterface != NULL)
      {
      pCachedTransform = (TransformComponent *) pcmpInterface;
      return;
      };
    pSearch = pSearch->Next();
    };
  };
  
//-----------------------------------------------------------------------------
const char *  SpriteComponent::GetMaterialName  (VOID)
  {
  CacheAtlas (); 
  ASSERT (pAtlas != NULL)
  if (pAtlas == NULL)
    {
    DBG_ERROR ("SpriteComponent was unable to find Atlas named : %s", pattrAtlasName->Value().AsChar ());
    return NULL;
    };
    
  return (pAtlas->GetMaterialName ());
  };
  
  
