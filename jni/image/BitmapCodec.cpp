/* -----------------------------------------------------------------
                             Bitmap Codec

     This module implements the base class from which bitmap codecs
   inherit.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "image/BitmapCodec.hpp"

//-----------------------------------------------------------------------------
//  BitmapCodec
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
BitmapCodec::BitmapCodec  ()
  {
  Initialize ();
  };


//-----------------------------------------------------------------------------
BitmapCodec::BitmapCodec  (const BitmapCodec & codecIn)
  {
  Initialize ();

  strCodecName = codecIn.strCodecName;
  };


//-----------------------------------------------------------------------------
BitmapCodec::~BitmapCodec ()
  {

  };


//-----------------------------------------------------------------------------
VOID  BitmapCodec::Initialize  (VOID)
  {
  iBitmapWidth  = 0;
  iBitmapHeight = 0;
  iBitmapDepth  = 0;
  iNumLayers    = 0;

  strCodecName = "Undefined";
  parserBuffer = NULL;
  };


//-----------------------------------------------------------------------------
BOOL  BitmapCodec::IsValidExtension   (const char *  szExtensionIn)
  {
  RStr      strLowerCurr;
  RStrArray listExtensions =  GetFileExtensions ();


  for (INT32  iIndex = 0; iIndex < listExtensions.Length (); ++iIndex)
    {
    if (listExtensions [iIndex].CompareNoCase (szExtensionIn) == 0)
      {
      // found the extension
      return (true);
      };
    };
  return (false);
  };


//-----------------------------------------------------------------------------
BOOL  BitmapCodec::DecodeLines  (ImageBoxIterator &  itrImageBox,
                                 INT                 iFirstLineIn,
                                 INT                 iLastLineIn)
  {
  return (false);
  };


//-----------------------------------------------------------------------------
BOOL  BitmapCodec::DecodeRect  (ImageBoxIterator &  itrImageBox,
                                INT                 iStartX,
                                INT                 iStartY,
                                INT                 iEndX,        // exclusive end
                                INT                 iEndY)        // exclusive end
  {
  return (false);
  };


//-----------------------------------------------------------------------------
EStatus  BitmapCodec::EncodeLines  (RStrParser &        parserIn,
                                    ImageBoxIterator &  itrImageBox,
                                    INT                 iFirstLineIn,
                                    INT                 iLastLineIn)
  {
  return (EStatus (EStatus::kFailure, "Codec routine EncodeLines () not defined."));
  };


//-----------------------------------------------------------------------------
EStatus  BitmapCodec::EncodeRect  (RStrParser &        parserIn,
                                   ImageBoxIterator &  itrImageBox,
                                   INT                 iStartX,
                                   INT                 iStartY,
                                   INT                 iEndX,        // exclusive end
                                   INT                 iEndY)        // exclusive end
  {
  return (EStatus (EStatus::kFailure, "Codec routine EncodeRect () not defined."));
  };
