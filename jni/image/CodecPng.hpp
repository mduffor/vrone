/* -----------------------------------------------------------------
                             PNG Codec

     This class implements an encoder/decoder for the 
   PNG image format, using libpng.
   
   https://github.com/julienr/libpng-android
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef PNGCODEC_HPP
#define PNGCODEC_HPP

#include "Types.hpp"
#include "RStr.hpp"
#include "BitmapCodec.hpp"
#define PNG_DEBUG 3
#include <png.h>

//------------------------------------------------------------------------
class CodecPng : public BitmapCodec
  {
  public:
    UINT32         uMaxVal;

    png_structp    pPngRead;
    png_structp    pPngWrite;
    png_infop      pPngInfo;

  private:

    VOID               Initialize (VOID);
    
  public:

                       CodecPng           ();

                       CodecPng           (const  CodecPng &  codecIn);

                       ~CodecPng          ();

    const CodecPng &   operator =         (const CodecPng & codecIn);

    BitmapCodec *      Clone              (VOID);

    RStrArray          GetFileExtensions  (VOID);

    RStr               GetLayerName       (INT  iLayerNumberIn);

    EStatus            VerifyBuffer       (RStrParser &  parserIn);

    BOOL               SupportsRectDecode (VOID)      {return (false);};

    BOOL               SupportsLineDecode (VOID)      {return (true);};

    BOOL               SupportsDecode     (VOID)      {return (true);};

    BOOL               SupportsEncode     (VOID)      {return (true);};

                       /// returns whether or not the codec supports encoding of an arbitrary rectangle
    BOOL               SupportsRectEncode (VOID)      {return (false);};

                       /// returns whether or not the codec supports encoding of line by line data
    BOOL               SupportsLineEncode (VOID)      {return (true);};

    BOOL               DecodeLines        (ImageBoxIterator &  itrImageBox,
                                           INT                 iFirstLineIn,
                                           INT                 iLastLineIn);

    BOOL               DecodeRect         (ImageBoxIterator &  itrImageBox,
                                           INT                 iStartX,
                                           INT                 iStartY,
                                           INT                 iEndX,          ///< exclusive end
                                           INT                 iEndY);         ///< exclusive end

    EStatus            EncodeLines        (RStrParser &        parserIn,
                                           ImageBoxIterator &  itrImageBox,
                                           INT                 iFirstLineIn,
                                           INT                 iLastLineIn);   ///< exclusive end

    EStatus            EncodeRect         (RStrParser &        parserIn,
                                           ImageBoxIterator &  itrImageBox,
                                           INT                 iStartX,
                                           INT                 iStartY,
                                           INT                 iEndX,          ///> exclusive end
                                           INT                 iEndY);         ///> exclusive end

    virtual VOID       Debug              (const char *  szFilename, 
                                           INT           iLineNum);
  };


















#endif // PNGCODEC_HPP

