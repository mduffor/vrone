/* -----------------------------------------------------------------
                             PNG Codec

     This class implements an encoder/decoder for the 
   PNG image format, using libpng.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "CodecPng.hpp"
#include "RStr.hpp"
#include <string.h>
#include <stdlib.h>

// Reference:    https://github.com/julienr/libpng-android

#define PNGSIGSIZE 8

//------------------------------------------------------------------------
CodecPng::CodecPng  ()
  {
  Initialize ();
  };

//------------------------------------------------------------------------
CodecPng::CodecPng  (const  CodecPng &  codecIn)
  {
  // copy constructor
  Initialize ();
  *this = codecIn;
  };


//------------------------------------------------------------------------
CodecPng::~CodecPng ()
  {
  if (pPngInfo != NULL)
    {
    png_destroy_info_struct(pPngRead, &pPngInfo);
    pPngInfo = NULL;
    }
  if (pPngRead != NULL)
    {
    png_destroy_read_struct(&pPngRead, NULL, NULL);
    pPngRead = NULL;
    }
  };


//------------------------------------------------------------------------
BitmapCodec *  CodecPng::Clone  (VOID)
  {

  return (new CodecPng (*this));
  };


//------------------------------------------------------------------------
const CodecPng &  CodecPng::operator = (const CodecPng & codecIn)
  {

  iNumBytesPerChan = codecIn.iNumBytesPerChan;
  
  iBitmapWidth     = codecIn.iBitmapWidth ;
  iBitmapHeight    = codecIn.iBitmapHeight;
  iBitmapDepth     = codecIn.iBitmapDepth ; 
  iNumLayers       = codecIn.iNumLayers;

  strCodecName     = codecIn.strCodecName;

  return *this;
  };

//------------------------------------------------------------------------
VOID CodecPng::Initialize ()
  {
  strCodecName = "PNG Codec";

  iNumBytesPerChan = 1;
  pPngRead = NULL;
  pPngInfo = NULL;
  
  };


//------------------------------------------------------------------------
RStrArray  CodecPng::GetFileExtensions  (VOID)
  {
  RStrArray  listReturn;

  listReturn.Append (RStr ("PNG"));
  return (listReturn);
  };


//------------------------------------------------------------------------
RStr  CodecPng::GetLayerName  (INT  iLayerNumberIn)
  {
  // only one layer
  return (RStr ("RGBA"));
  };

//------------------------------------------------------------------------
void UserReadData (png_structp pngPtr, 
                   png_bytep   data, 
                   png_size_t  length)
  {
  RStrParser * parserBuffer = (RStrParser *)png_get_io_ptr(pngPtr);

  parserBuffer->ReadChars ((char *)data, length);
  };

//------------------------------------------------------------------------
void UserWriteData (png_structp pngPtr, 
                    png_bytep   data, 
                    png_size_t  length)
  {
  RStrParser * parserBuffer = (RStrParser *)png_get_io_ptr(pngPtr);

  parserBuffer->WriteChars ((char *)data, length);
  };

//------------------------------------------------------------------------
void UserErrorCallback (png_structp png_ptr,
                        png_const_charp error_msg)
  {
  // png_voidp error_ptr = png_get_error_ptr(png_ptr);
  DBG_ERROR ("PNG Codec: %s", error_msg);
  };

//------------------------------------------------------------------------
void UserWarningCallback (png_structp png_ptr,
                          png_const_charp warning_msg)
  {
  DBG_WARNING ("PNG Codec: %s", warning_msg);
  };
  
//------------------------------------------------------------------------
EStatus CodecPng::VerifyBuffer (RStrParser &  parserIn)
  {
  // Reference: http://www.piko3d.net/tutorials/libpng-tutorial-loading-png-files-from-streams/
  // Reference: http://zarb.org/~gc/html/libpng.html
  
  // initialize default values
  iBitmapWidth     = 1;
  iBitmapHeight    = 1;
  iBitmapDepth     = 1;
  iNumLayers       = 1;
  iNumChannels     = 0;
  iNumBytesPerChan = 1;

  parserBuffer = &parserIn; 
  // prepare the parser buffer to read the header
  parserBuffer->ResetCursor ();

  if (png_sig_cmp ((unsigned char *)parserBuffer->GetBufferPtr(), 0, PNGSIGSIZE))
    {
    return (EStatus (EStatus::kFailure, "PNG Codec: File is not a PNG file."));
    }
  parserBuffer->SkipChars(PNGSIGSIZE);

  // initialize libpng
  
  pPngRead = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  
  if (pPngRead == NULL)
    {
    return (EStatus (EStatus::kFailure, "PNG Codec: png_create_read_struct failed."));
    }
  pPngInfo = png_create_info_struct (pPngRead);
  if (pPngInfo == NULL)
    {
    png_destroy_read_struct(&pPngRead, NULL, NULL);
    pPngRead = NULL;
    return (EStatus (EStatus::kFailure, "PNG Codec: png_create_info_struct failed."));
    }
  /*  
  if (setjmp (png_jmpbuf (pPngRead)))
    {
    png_destroy_read_struct(&pPngRead, NULL, NULL);
    pPngRead = NULL;
    return (EStatus (EStatus::kFailure, "PNG Codec: Error setting up IO."));
    }
  */
  
  png_set_read_fn (pPngRead,(png_voidp)parserBuffer, UserReadData);
  png_set_error_fn (pPngRead, (png_voidp)NULL, UserErrorCallback, UserWarningCallback);
  
  png_set_sig_bytes(pPngRead, PNGSIGSIZE);

  png_read_info(pPngRead, pPngInfo);

  iBitmapHeight    = png_get_image_height(pPngRead, pPngInfo);
  iBitmapWidth     = png_get_image_width(pPngRead, pPngInfo);
  iNumChannels     = png_get_channels(pPngRead, pPngInfo);
  iNumLayers       = 1;
  iNumBytesPerChan = (png_get_bit_depth(pPngRead, pPngInfo) + 7) / 8;
  
  //DBG_INFO ("Verify Buffer width %d height %d numchan %d, bpc %d", iBitmapWidth, iBitmapHeight, iNumChannels, iNumBytesPerChan);
  
  switch (iNumChannels)
    {
    case 1:  strChannelContents = "R"; break;
    case 2:  strChannelContents = "RG"; break;
    case 3:  strChannelContents = "RGB"; break;
    case 4:  strChannelContents = "RGBA"; break;
    default: break;
    };  
  
  png_uint_32 color_type = png_get_color_type(pPngRead, pPngInfo);  

  switch (color_type) 
    {
    case PNG_COLOR_TYPE_PALETTE:
         png_set_palette_to_rgb(pPngRead);
         iNumChannels = 3;           
         break;

    case PNG_COLOR_TYPE_GRAY:
         if (png_get_bit_depth(pPngRead, pPngInfo) < 8)
           {
           png_set_expand_gray_1_2_4_to_8(pPngRead);
           iNumBytesPerChan = 1;
           }
         break;
    }  
  
  // if the image has a transperancy set, convert it to a full Alpha channel.

  if (png_get_valid (pPngRead, pPngInfo, PNG_INFO_tRNS)) 
    {
    png_set_tRNS_to_alpha (pPngRead);
    iNumChannels += 1;
    }
  
  // Round precision down to 8 bits per channel
  if (iNumBytesPerChan == 2)
    {
    png_set_strip_16 (pPngRead);  
    iNumBytesPerChan = 1;
    }
  
  //INT iNumberOfPasses = png_set_interlace_handling (pPngRead);
  png_set_interlace_handling (pPngRead);
  png_read_update_info (pPngRead, pPngInfo);

  //DBG_INFO ("Verify Buffer read struct %x  pngInfo %x this %x", pPngRead, pPngInfo, this);
  
  // this is a valid buffer
  return (EStatus::kSuccess);
  };

  
//------------------------------------------------------------------------
BOOL CodecPng::DecodeLines  (ImageBoxIterator &  itrImageBox,
                             INT                 iFirstLineIn,
                             INT                 iLastLineIn)
  {
  INT  iIndexY;
  INT  iIncrement = 1;
  INT  iStart     = iFirstLineIn;
  INT  iEnd       = iLastLineIn + 1;

  //itrImageBox.SetBoxPosition (0, iStart);

  // Read the file
  /*
  if (setjmp(png_jmpbuf(pPngRead)))
    {
    DBG_INFO ("DecodeLines ");
    png_destroy_read_struct(&pPngRead, NULL, NULL);
    pPngRead = NULL;
    return (false);
    }    
  */
  
  // step through each line (top down)
  INT  iNumRowBytes = png_get_rowbytes(pPngRead, pPngInfo);
  
  png_bytep  pRow = (png_bytep) malloc(iNumRowBytes);
  //DBG_INFO ("DecodeLines start %d end %d inc %d", iStart, iEnd, iIncrement);
  for (iIndexY = iStart; iIndexY != iEnd; iIndexY += iIncrement)
    {
    // debugging
    //DBG_INFO ("Reading line %d", iIndexY);
    
    itrImageBox.SetBoxPosition (0, iIndexY);
    
    png_read_row (pPngRead, pRow, NULL);
    
    UINT                uRunLength = (UINT) iBitmapWidth;
    png_bytep           pCurr = pRow;
    
    do
      {
      //DBG_INFO ("%d R %d G %d B %d A %d", uRunLength, UINT(pCurr[0]), UINT(pCurr[1]), UINT(pCurr[2]), UINT(pCurr[3]));
      itrImageBox.Set (UINT(pCurr[0]), UINT(pCurr[1]), UINT(pCurr[2]), UINT(pCurr[3]));

      --uRunLength;
      pCurr += iNumChannels;
      } while (uRunLength);
    };

  free (pRow);
  return (false);
  };


//------------------------------------------------------------------------
BOOL CodecPng::DecodeRect  (ImageBoxIterator &  itrImageBox,
                            INT                 iStartX,
                            INT                 iStartY,
                            INT                 iEndX,        // exclusive end
                            INT                 iEndY)        // exclusive end
  {
  // TODO: Not yet supported
  return (false);
  };


//------------------------------------------------------------------------
EStatus  CodecPng::EncodeLines  (RStrParser &        parserIn,
                                 ImageBoxIterator &  itrImageBox,
                                 INT                 iFirstLineIn,
                                 INT                 iLastLineIn)
  {
  return (EncodeRect (parserIn,
                      itrImageBox,
                      itrImageBox.GetStartPixelX (),
                      iFirstLineIn,
                      itrImageBox.GetEndPixelX (),
                      iLastLineIn));
  };


//------------------------------------------------------------------------
EStatus  CodecPng::EncodeRect  (RStrParser &        parserIn,
                                ImageBoxIterator &  itrImageBox,
                                INT                 iStartX,
                                INT                 iStartY,
                                INT                 iEndX,        // exclusive end
                                INT                 iEndY)        // exclusive end
  {
  INT              iIncrement = 1;


  iBitmapWidth  = iEndX - iStartX;
  iBitmapHeight = iEndY - iStartY;
  iBitmapDepth  = 0;
  iNumChannels  = itrImageBox.GetChanCount ();

  pPngWrite = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!pPngWrite)
    {
    return (EStatus (EStatus::kFailure, "PNG Codec: png_create_write_struct failed."));
    }

  pPngInfo = png_create_info_struct (pPngWrite);
  if (!pPngInfo)
    {
    return (EStatus (EStatus::kFailure, "PNG Codec: png_create_info_struct failed."));
    }

  //if (setjmp(png_jmpbuf(pPngWrite)))
  //  abort_("[write_png_file] Error during init_io");

  //png_init_io (pPngWrite, fp);

  png_set_write_fn (pPngWrite,(png_voidp)&parserIn, UserWriteData, NULL);
  png_set_error_fn (pPngWrite, (png_voidp)NULL, UserErrorCallback, UserWarningCallback);

  // write header 
  png_set_IHDR (pPngWrite, pPngInfo, iBitmapWidth, iBitmapHeight,
                8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
                PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_write_info (pPngWrite, pPngInfo);


  // write bytes 
  
  // step through each line (top down)
  for (INT iIndexY = iStartY; iIndexY != iEndY; iIndexY += iIncrement)
    {
    // debugging
    //DBG_INFO ("Writing line %d", iIndexY);
    
    itrImageBox.SetBoxPosition (iStartX, iIndexY);
    
    png_write_row (pPngWrite, (png_const_bytep) itrImageBox.GetDataPtr ());
    };
  
  // end write 
  png_write_end (pPngWrite, NULL);

  return (EStatus::kSuccess);
  };
  
//------------------------------------------------------------------------
VOID CodecPng::Debug (const char *  szFilename, 
                      INT           iLineNum)
  {
  DBG_INFO ("%s:(%d) PNG Debug read struct %x  pngInfo %x this %x", szFilename, iLineNum, pPngRead, pPngInfo, this);
  };
  

