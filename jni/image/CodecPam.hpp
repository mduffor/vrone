/* -----------------------------------------------------------------
                             PAM Codec

     This class implements an encoder/decoder for the 
   PAM (Portable Arbitrary Map) image format (.PBM, .PGM, .PPM, and .PNM)
   
   http://netpbm.sourceforge.net/doc/pam.html
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef PAMCODEC_HPP
#define PAMCODEC_HPP

#include "Types.hpp"
#include "RStr.hpp"
#include "BitmapCodec.hpp"

//------------------------------------------------------------------------
class CodecPam : public BitmapCodec
  {
  private:

    UINT32         uMaxVal;

    FLOAT          fChannelDivisor;

  private:

    VOID               Initialize (VOID);

    EStatus            ParseP4Header      (RStrParser &  parserIn);

    EStatus            ParseP5Header      (RStrParser &  parserIn);

    EStatus            ParseP6Header      (RStrParser &  parserIn);

    EStatus            ParseP7Header      (RStrParser &  parserIn);

    VOID               WriteP7Header      (RStrParser &  parserIn);
    
    VOID               WriteP6Header      (RStrParser &  parserIn);
    
    VOID               WriteP5Header      (RStrParser &  parserIn);

    EStatus            SetMaxVal          (UINT32  uMaxValIn);

    VOID               ReadPamLineUnclipped  (RStrParser &        parserIn, 
                                              ImageBoxIterator &  itrImageBox);

    VOID               WritePamLineUnclipped (RStrParser &        parserIn, 
                                              ImageBoxIterator &  itrImageBox);
    
    EStatus            ReadPixel             (RStrParser &  parserIn,
                                              UINT &  uRedOut,
                                              UINT &  uGreenOut,
                                              UINT &  uBlueOut,
                                              UINT &  uAlphaOut,
                                              UINT    uCurrX,
                                              UINT &  uTemp);
    
  public:

                       CodecPam           ();

                       CodecPam           (const  CodecPam &  codecIn);

                       ~CodecPam          ();

    const CodecPam &   operator =         (const CodecPam & codecIn);

    BitmapCodec *      Clone              (VOID);

    RStrArray          GetFileExtensions  (VOID);

    RStr               GetLayerName       (INT  iLayerNumberIn);

    EStatus            VerifyBuffer       (RStrParser &  parserIn);

    BOOL               SupportsRectDecode (VOID)      {return (false);};

    BOOL               SupportsLineDecode (VOID)      {return (true);};

    BOOL               SupportsDecode     (VOID)      {return (true);};

    BOOL               SupportsEncode     (VOID)      {return (true);};

                       /// returns whether or not the codec supports encoding of an arbitrary rectangle
    BOOL               SupportsRectEncode (VOID)      {return (false);};

                       /// returns whether or not the codec supports encoding of line by line data
    BOOL               SupportsLineEncode (VOID)      {return (true);};

    BOOL               DecodeLines        (ImageBoxIterator &  itrImageBox,
                                           INT                 iFirstLineIn,
                                           INT                 iLastLineIn);

    BOOL               DecodeRect         (ImageBoxIterator &  itrImageBox,
                                           INT                 iStartX,
                                           INT                 iStartY,
                                           INT                 iEndX,          ///< exclusive end
                                           INT                 iEndY);         ///< exclusive end

    EStatus            EncodeLines        (RStrParser &        parserIn,
                                           ImageBoxIterator &  itrImageBox,
                                           INT                 iFirstLineIn,
                                           INT                 iLastLineIn);   ///< exclusive end

    EStatus            EncodeRect         (RStrParser &        parserIn,
                                           ImageBoxIterator &  itrImageBox,
                                           INT                 iStartX,
                                           INT                 iStartY,
                                           INT                 iEndX,          ///> exclusive end
                                           INT                 iEndY);         ///> exclusive end
    
    virtual VOID       Debug              (const char *  szFilename, 
                                           INT           iLineNum);

  };


















#endif // PAMCODEC_HPP

