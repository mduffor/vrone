/* -----------------------------------------------------------------
                             PAM Codec

     This class implements an encoder/decoder for the 
   PAM (Portable Arbitrary Map) image format (.PBM, .PGM, .PPM, and .PNM)
   
   http://netpbm.sourceforge.net/doc/pam.html
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "CodecPam.hpp"
#include "RStr.hpp"
#include <string.h>

// TODO:  Still need to implement support for reading and writing files where
//         the maxval is something other than 255

RStr  astrPamTypeNames [7] = {"No Image Data",
                              "Colormapped - uncompressed",
                              "Truecolor - uncompressed",
                              "Monochrome - uncompressed",
                              "Colormapped - compressed",
                              "Truecolor - compressed",
                              "Monochrome - compressed"};

#pragma pack (1)

#pragma pack ()

//------------------------------------------------------------------------
CodecPam::CodecPam  ()
  {
  Initialize ();
  };

//------------------------------------------------------------------------
CodecPam::CodecPam  (const  CodecPam &  codecIn)
  {
  // copy constructor

  *this = codecIn;
  };


//------------------------------------------------------------------------
CodecPam::~CodecPam ()
  {
  };


//------------------------------------------------------------------------
BitmapCodec *  CodecPam::Clone  (VOID)
  {

  return (new CodecPam (*this));
  };


//------------------------------------------------------------------------
const CodecPam &  CodecPam::operator = (const CodecPam & codecIn)
  {

  uMaxVal          = codecIn.uMaxVal;
  iNumBytesPerChan = codecIn.iNumBytesPerChan;
  
  iBitmapWidth     = codecIn.iBitmapWidth ;
  iBitmapHeight    = codecIn.iBitmapHeight;
  iBitmapDepth     = codecIn.iBitmapDepth ; 
  iNumLayers       = codecIn.iNumLayers;

  strCodecName     = codecIn.strCodecName;

  return *this;
  };

//------------------------------------------------------------------------
VOID CodecPam::Initialize ()
  {
  strCodecName = "PAM Codec";

  iNumBytesPerChan = 1;
  };


//------------------------------------------------------------------------
RStrArray  CodecPam::GetFileExtensions  (VOID)
  {
  RStrArray  listReturn;

  listReturn.Append (RStr ("PAM"));
  listReturn.Append (RStr ("PBM"));
  listReturn.Append (RStr ("PGM"));
  listReturn.Append (RStr ("PPM"));
  listReturn.Append (RStr ("PNM"));

  return (listReturn);
  };


//------------------------------------------------------------------------
RStr  CodecPam::GetLayerName  (INT  iLayerNumberIn)
  {
  // only one layer
  return (RStr ("RGBA"));
  };


//------------------------------------------------------------------------
EStatus CodecPam::VerifyBuffer (RStrParser &  parserIn)
  {
  // initialize default values
  iBitmapWidth     = 1;
  iBitmapHeight    = 1;
  iBitmapDepth     = 1;
  iNumLayers       = 1;
  iNumChannels     = 0;
  iNumBytesPerChan = 1;
  uMaxVal          = 1;

  parserBuffer = &parserIn; 
  
  // prepare the parser buffer to read the header
  parserBuffer->ResetCursor ();

  // Make sure the buffer is of a valid size (minimum header size... will probably be longer)
  if (parserBuffer->GetLength () <= 44)
    {
    return (EStatus (EStatus::kFailure, "PAM Codec: Header is too small"));
    };

  // read the header
  parserBuffer->SkipWhitespace ();
  RStr  strKey = parserBuffer->GetWord ();

  if (strKey == "P7")
    {
    // parse the header
    if (ParseP7Header (*parserBuffer) == EStatus::kFailure)
      {
      return (EStatus (EStatus::kFailure, "PAM Codec: Failed to parse P7 header."));
      };
    }
  else if (strKey == "P4")
    {
    // parse the header
    if (ParseP4Header (*parserBuffer) == EStatus::kFailure)
      {
      return (EStatus (EStatus::kFailure, "PAM Codec: Failed to parse P4 header."));
      };
    }
  else if (strKey == "P5")
    {
    // parse the header
    if (ParseP5Header (*parserBuffer) == EStatus::kFailure)
      {
      return (EStatus (EStatus::kFailure, "PAM Codec: Failed to parse P5 header."));
      };
    }
  else if (strKey == "P6")
    {
    // parse the header
    if (ParseP6Header (*parserBuffer) == EStatus::kFailure)
      {
      return (EStatus (EStatus::kFailure, "PAM Codec: Failed to parse P6 header."));
      };
    }
  else
    {
    if ((strKey == "P1") || (strKey == "P2") || (strKey == "P3"))
      {
      return (EStatus (EStatus::kFailure, "PAM Codec: Only binary formats, P4, P5, P6, and P7 are supported."));
      }
    
    // no magic number.  Not a PAM file.
    return (EStatus (EStatus::kFailure, "PAM Codec: No magic number; not a PAM file."));
    };

  // this is a valid buffer
  return (EStatus::kSuccess);
  };


//------------------------------------------------------------------------
EStatus  CodecPam::ParseP4Header (RStrParser &  parserIn)
  {
  // Ignore comments in the header
  parserIn.SetSkipComments (RStrParser::kShellStyle);
  parserIn.FindLineEnd ();
  parserIn.SkipWhitespace ();
  parserIn.SetSkipComments (RStrParser::kNone);
  
  iBitmapWidth  = parserIn.GetInt();
  iBitmapHeight = parserIn.GetInt();

  iNumChannels       = 1;
  strChannelContents = "R";
  iNumBytesPerChan   = -1;
  iNumLayers         = 1;

  return (EStatus::kSuccess);
  };
  
  
//------------------------------------------------------------------------
EStatus  CodecPam::ParseP5Header (RStrParser &  parserIn)
  {
  // Ignore comments in the header
  parserIn.SetSkipComments (RStrParser::kShellStyle);
  parserIn.FindLineEnd ();
  parserIn.SkipWhitespace ();
  parserIn.SetSkipComments (RStrParser::kNone);
  
  iBitmapWidth  = parserIn.GetInt();
  iBitmapHeight = parserIn.GetInt();
  if (SetMaxVal ((UINT32) parserIn.GetInt()) == EStatus::kFailure)
    {
    return (EStatus::kFailure);
    };

  iNumChannels       = 1;
  strChannelContents = "R";
  iNumLayers         = 1;

  return (EStatus::kSuccess);
  };

  
//------------------------------------------------------------------------
EStatus  CodecPam::ParseP6Header (RStrParser &  parserIn)
  {
  // Ignore comments in the header
  parserIn.SetSkipComments (RStrParser::kShellStyle);
  parserIn.FindLineEnd ();
  parserIn.SkipWhitespace ();
  parserIn.SetSkipComments (RStrParser::kNone);
  
  iBitmapWidth  = parserIn.GetInt();
  iBitmapHeight = parserIn.GetInt();
  if (SetMaxVal ((UINT32) parserIn.GetInt()) == EStatus::kFailure)
    {
    return (EStatus::kFailure);
    };

  iNumChannels         = 3;
  strChannelContents = "RGB";
  iNumLayers           = 1;

  return (EStatus::kSuccess);
  };
  
  
//------------------------------------------------------------------------
EStatus  CodecPam::ParseP7Header (RStrParser &  parserIn)
  {
  RStr  strKey = parserIn.GetWord ();
 
  while (strKey != "ENDHDR")
    {
    if (strKey.GetAt (0) == UINT32 ('#'))
      {
      // skip comments
      parserIn.GotoNextLine ();
      }
    else if (strKey == "HEIGHT")
      {
      iBitmapHeight = parserIn.GetInt();
      }
    else if (strKey == "WIDTH")
      {
      iBitmapWidth = parserIn.GetInt();
      }
    else if (strKey == "DEPTH")
      {
      iNumChannels = parserIn.GetInt();
      iNumLayers           = 1;
      
      switch (iNumChannels)
        {
        case 1:  strChannelContents = "R"; break;
        case 2:  strChannelContents = "RG"; break;
        case 3:  strChannelContents = "RGB"; break;
        case 4:  strChannelContents = "RGBA"; break;
        default: break;
        };
      }
    else if (strKey == "MAXVAL")
      {
      if (SetMaxVal ((UINT32) parserIn.GetInt()) == EStatus::kFailure)
        {
        return (EStatus::kFailure);
        };
      }
    else if (strKey == "TUPLTYPE")
      {
      // possible tupple types:
      //   BLACKANDWHITE - bitmap image where values are either 0 or 1, maxval of 1
      //   GRAYSCALE     - depth of 1
      //   RGB           - depth of 3

      //   BLACKANDWHITE_ALPHA - BLACKANDWHITE with an additional alpha channel (alpha/maxval)
      //   GRAYSCALE_ALPHA     - depth of 2, with the second being alpha (alpha/maxval)
      //   RGB_ALPHA           - depth of 4, with the fourth being alpha (alpha/maxval)

      RStr  strTuppleType = parserIn.GetWord ();
      if (strTuppleType == "BLACKANDWHITE")
        {
        iNumChannels       = 1;
        strChannelContents = "R";
        iNumLayers         = 1;
        }
      if (strTuppleType == "BLACKANDWHITE_ALPHA")
        {
        iNumChannels       = 2;
        strChannelContents = "RA";
        iNumLayers         = 1;
        }
      if (strTuppleType == "GRAYSCALE")
        {
        iNumChannels       = 1;
        strChannelContents = "R";
        iNumLayers         = 1;
        }
      if (strTuppleType == "GRAYSCALE_ALPHA")
        {
        iNumChannels       = 2;
        strChannelContents = "RA";
        iNumLayers         = 1;
        }
      if (strTuppleType == "RGB")
        {
        iNumChannels       = 3;
        strChannelContents = "RGB";
        iNumLayers         = 1;
        }
      if (strTuppleType == "RGB_ALPHA")
        {
        iNumChannels       = 4;
        strChannelContents = "RGBA";
        iNumLayers         = 1;
        };
      };
    };
  // we found the ENDHDR tag.  
  parserIn.GotoNextLine ();
  return (EStatus::kSuccess);
  };


//------------------------------------------------------------------------
VOID CodecPam::WriteP7Header (RStrParser &  parserIn)
  {
  // PAM format
  parserIn.AppendFormat ("P7\n");
  parserIn.AppendFormat ("WIDTH %d\n", iBitmapWidth);
  parserIn.AppendFormat ("HEIGHT %d\n", iBitmapHeight);
  parserIn.AppendFormat ("DEPTH %d\n", iNumChannels);
  parserIn.AppendFormat ("MAXVAL 255\n");
  parserIn.AppendFormat ("TUPLTYPE RGB_ALPHA\n");
  parserIn.AppendFormat ("ENDHDR\n");
  };


//------------------------------------------------------------------------
VOID CodecPam::WriteP6Header (RStrParser &  parserIn)
  {
  // PPM
  parserIn.AppendFormat ("P6\n");
  parserIn.AppendFormat ("%d\n", iBitmapWidth);
  parserIn.AppendFormat ("%d\n", iBitmapHeight);
  parserIn.AppendFormat ("255\n");
  };


//------------------------------------------------------------------------
VOID CodecPam::WriteP5Header (RStrParser &  parserIn)
  {
  // PGM
  parserIn.AppendFormat ("P5\n");
  parserIn.AppendFormat ("%d\n", iBitmapWidth);
  parserIn.AppendFormat ("%d\n", iBitmapHeight);
  parserIn.AppendFormat ("MAXVAL 255\n");
  };

  
//------------------------------------------------------------------------
EStatus  CodecPam::SetMaxVal (UINT32  uMaxValIn)
  {

  uMaxVal = uMaxValIn;
  
  iNumBytesPerChan     = -1;
  
  // calculate the number of bytes required to hold uMaxVal
  if (uMaxVal <= 0x000000ff)
    {
    iNumBytesPerChan     = 1;
    }
  else if (uMaxVal <= 0x0000ffff)
    {
    iNumBytesPerChan     = 2;
    }
  else if (uMaxVal <= 0x00ffffff)
    {
    iNumBytesPerChan     = 3;
    DBG_WARNING ("Only 1, 2, or 4 byte per channel values are supported");
    return (EStatus::kFailure);
    }
  else 
    {
    iNumBytesPerChan     = 4;
    };
  return (EStatus::kSuccess);
  };


//------------------------------------------------------------------------
BOOL CodecPam::DecodeLines  (ImageBoxIterator &  itrImageBox,
                             INT                 iFirstLineIn,
                             INT                 iLastLineIn)
  {
  INT  iIndexY;
  INT  iIncrement = 1;
  INT  iStart     = iFirstLineIn;
  INT  iEnd       = iLastLineIn + 1;

  itrImageBox.SetBoxPosition (0, iStart);

  // step through each line (top down)
  for (iIndexY = iStart; iIndexY != iEnd; iIndexY += iIncrement)
    {
    // debugging
    //DBG_INFO ("Reading line %d,   %d bytes left\n", iIndexY, parserIn.GetLength () - parserIn.GetCursorStart ());
    
    itrImageBox.SetBoxPosition (0, iIndexY);
    ReadPamLineUnclipped (*parserBuffer, itrImageBox);
    };
    
  return (false);
  };


//------------------------------------------------------------------------
BOOL CodecPam::DecodeRect  (ImageBoxIterator &  itrImageBox,
                            INT                 iStartX,
                            INT                 iStartY,
                            INT                 iEndX,        // exclusive end
                            INT                 iEndY)        // exclusive end
  {
  // TODO: Not yet supported
  return (false);
  };


//------------------------------------------------------------------------
EStatus  CodecPam::EncodeLines  (RStrParser &        parserIn,
                                 ImageBoxIterator &  itrImageBox,
                                 INT                 iFirstLineIn,
                                 INT                 iLastLineIn)
  {
  return (EncodeRect (parserIn, 
                      itrImageBox,
                      itrImageBox.GetStartPixelX (),
                      iFirstLineIn,
                      itrImageBox.GetEndPixelX (),
                      iLastLineIn));
  };


//------------------------------------------------------------------------
EStatus  CodecPam::EncodeRect  (RStrParser &        parserIn,
                                ImageBoxIterator &  itrImageBox,
                                INT                 iStartX,
                                INT                 iStartY,
                                INT                 iEndX,        // exclusive end
                                INT                 iEndY)        // exclusive end
  {
  INT              iIndexY;
  FLOAT            fLastPercent;
  FLOAT            fCurrPercent;


  iBitmapWidth  = iEndX - iStartX;
  iBitmapHeight = iEndY - iStartY;
  iBitmapDepth  = 0;
  iNumChannels    = itrImageBox.GetChanCount ();

  // Note:  For now, I'm just forcing a generic 8 bit RGBA output image.  Eventually this
  //         should be expanded to allow for exporting a wider variety of data that better
  //         matches the actual image.

  // Write the header
  parserIn.Reset ();
  
  if (iNumChannels == 4)
    {
    WriteP7Header (parserIn);
    }
  else if (iNumChannels == 3)
    {
    WriteP6Header (parserIn);
    }
  else if (iNumChannels == 1)
    {
    WriteP5Header (parserIn);
    }
  else
    {
    WriteP7Header (parserIn);
    };

  // add the image data

  itrImageBox.SetBoxPosition (iStartX, iStartY);

  fLastPercent = -10.0;
  for (iIndexY = iStartY; iIndexY != iEndY; ++iIndexY)
    {
    // show progress in 5% increments
    fCurrPercent = FLOAT (iIndexY) / FLOAT (iEndY) * 100;
    if (fCurrPercent >= fLastPercent + 5.0)
      {
      DBG_INFO ("Encoding %3.0f%% Complete\n", fCurrPercent);
      fLastPercent = fCurrPercent;
      };
    itrImageBox.SetBoxPosition (iStartX, iIndexY);
    WritePamLineUnclipped (parserIn, itrImageBox);
    };
  DBG_INFO ("Encoding 100%% Complete\n");

  return (EStatus::kSuccess);
  };


//------------------------------------------------------------------------
VOID CodecPam::ReadPamLineUnclipped (RStrParser &        parserIn,
                                     ImageBoxIterator &  itrImageBox)
  {
  UINT                uRunLength;
  UINT                uWidth = iBitmapWidth;
  UINT                uCurrX = 0;
  UINT                uRed;
  UINT                uGreen;
  UINT                uBlue;
  UINT                uAlpha = uMaxVal;
  UINT                uTemp = 0;

  // uncompressed line

  uRunLength = uWidth;
    
  do
    {
    if (ReadPixel (parserIn, uRed, uGreen, uBlue, uAlpha, uCurrX, uTemp) == EStatus::kFailure)
      {
      // read past end of line.
      return;
      };
    //DBG_INFO ("%d R %d G %d B %d A %d", uRunLength, uRed, uGreen, uBlue, uAlpha);
    itrImageBox.Set (uRed, uGreen, uBlue, uAlpha);

    --uRunLength;
    ++uCurrX;
    } while (uRunLength);
  };


//------------------------------------------------------------------------
EStatus CodecPam::ReadPixel (RStrParser &  parserIn,
                             UINT &        uRedOut,
                             UINT &        uGreenOut,
                             UINT &        uBlueOut,
                             UINT &        uAlphaOut,
                             UINT          uCurrX,
                             UINT &        uTemp)
  {
  uAlphaOut    = uMaxVal;

  if (parserIn.IsEOF ())
    {
    return (EStatus::kFailure);
    };
  
  // read RGBA (ordered by anticipated frequency)
  
  // 1 byte samples
  if (iNumBytesPerChan     == 1)
    {
    uRedOut   = parserIn.GetU1_LEnd ();
    
    if (iNumChannels < 3)
      {
      uGreenOut = uBlueOut = uRedOut;
      }
    else
      {
      uGreenOut   = parserIn.GetU1_LEnd ();
      uBlueOut    = parserIn.GetU1_LEnd ();
      };

    if ((iNumChannels == 2) || (iNumChannels == 4))
      {
      uAlphaOut    = parserIn.GetU1_LEnd ();
      };
    }
  // 4 byte samples
  else if (iNumBytesPerChan     == 4)
    {
    uRedOut   = parserIn.GetU4_LEnd ();
    
    if (iNumChannels < 3)
      {
      uGreenOut = uBlueOut = uRedOut;
      }
    else
      {
      uGreenOut   = parserIn.GetU4_LEnd ();
      uBlueOut    = parserIn.GetU4_LEnd ();
      };

    if ((iNumChannels == 2) || (iNumChannels == 4))
      {
      uAlphaOut    = parserIn.GetU4_LEnd ();
      };
    }
  // 2 byte samples
  else if (iNumBytesPerChan     == 2)
    {
    uRedOut   = parserIn.GetU2_LEnd ();
    
    if (iNumChannels < 3)
      {
      uGreenOut = uBlueOut = uRedOut;
      }
    else
      {
      uGreenOut   = parserIn.GetU2_LEnd ();
      uBlueOut    = parserIn.GetU2_LEnd ();
      };

    if ((iNumChannels == 2) || (iNumChannels == 4))
      {
      uAlphaOut    = parserIn.GetU2_LEnd ();
      };
    }
  // 8 bits per 1 byte samples (packed)
  else if (iNumBytesPerChan     == -1)
    {
    UINT   uMaskShift = uCurrX & 0x00000007;
    
    if (! uMaskShift)
      {
      // first bit in the byte
      uTemp = parserIn.GetU1_LEnd ();
      };
          
    if (0x00000080 >> uMaskShift)
      {
      // black pixel
      uRedOut = 0x00;
      }
    else
      {
      // white pixel
      uRedOut = 0xff;
      }
    }

  return (EStatus::kSuccess);
  };




//------------------------------------------------------------------------
VOID CodecPam::WritePamLineUnclipped (RStrParser &  parserIn,
                                      ImageBoxIterator &  itrImageBox)
  {
  UINT                uWidth = iBitmapWidth;
  UINT                uSourceIndex;
  Color8U             colorCurr;
  UINT                uNumChan = itrImageBox.GetChanCount ();

  // writes out one line of pixels to the output buffer (parserIn)

  uSourceIndex = 0;
  do
    {
    // read the next pixel
    colorCurr = itrImageBox.GetPixel8U ();
    
    // write the pixel

    if (uNumChan >= 1)
      {
      parserIn.SetU1_LEnd (colorCurr.R ());
      if (uNumChan >= 2)
        {
        parserIn.SetU1_LEnd (colorCurr.G ());
        if (uNumChan >= 3)
          {
          parserIn.SetU1_LEnd (colorCurr.B ());
          if (uNumChan >= 4)
            {
            parserIn.SetU1_LEnd (colorCurr.A ());
            };
          };
        };
      };

    ++uSourceIndex;
    } while (uSourceIndex < uWidth);
  };

//------------------------------------------------------------------------
VOID CodecPam::Debug (const char *  szFilename, 
                      INT           iLineNum)
  {
  };


