/* -----------------------------------------------------------------
                             Bitmap Codec

     This module implements the base class from which bitmap codecs
   inherit.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "RStr.hpp"
#include "BitmapLoader.hpp"
#include <string.h>
#include "CodecPng.hpp"

//========================================================================
//  BitmapLoader
//========================================================================

BaseList  BitmapLoader::listCodecs;


//------------------------------------------------------------------------
BitmapLoader::BitmapLoader  ()
  {
  pCodec = NULL;
  };


//------------------------------------------------------------------------
BitmapLoader::~BitmapLoader  ()
  {
  if (pCodec != NULL) delete pCodec;
  };

//------------------------------------------------------------------------
BitmapLoader::BitmapLoader  (ImageBoxIterator &  itrIn)
  {
  pCodec = NULL;
  SetIterator (itrIn);
  };


//------------------------------------------------------------------------
EStatus BitmapLoader::LoadBufferFromFile  (const char *  szFilenameIn)
  {
  EStatus    statusReturn;
  
  if ((statusReturn = parserBuffer.ReadFromFile (szFilenameIn)) != EStatus::kSuccess)
    {
    return (statusReturn);
    };

  // Note:  VerifyBuffer is called during the course of SetCodecByFilename/Extension
  if ((statusReturn = SetCodecByFilename (szFilenameIn)) != EStatus::kSuccess)
    {
    return (statusReturn);
    };

  if (pCodec == NULL)
    {
    return (EStatus (EStatus::kFailure, "Codec not found"));
    }
  
  return (EStatus::kSuccess);
  };

//------------------------------------------------------------------------
EStatus  BitmapLoader::SetBuffer (const char *  szBufferIn,
                                  const char *  szCodecTypeIn)
  {
  EStatus    statusReturn;

  parserBuffer.Set (szBufferIn);
  
  // step through all the codecs and find the one that matches this file.
  //  Use the filename extension as a hint, before trying each one.
  if ((statusReturn = SetCodecByExtension (szCodecTypeIn)) != EStatus::kSuccess)
    {
    return (statusReturn);
    };

  if (pCodec == NULL)
    {
    return (EStatus (EStatus::kFailure, "Codec not found"));
    }    
    
  if ((statusReturn = pCodec->VerifyBuffer (parserBuffer)) != EStatus::kSuccess)
    {
    return (statusReturn);
    };
  return (EStatus::kSuccess);
  };

//------------------------------------------------------------------------
EStatus  BitmapLoader::SetCodecByFilename (const char *  szFilenameIn)
  {
  // get filename extension to hint at which codec to use
  RStr   strFilename = szFilenameIn;
  RStr   strExtension ("");
  INT32  iSearch = strFilename.ReverseFindChar ('.');
  if (iSearch != -1)
    {
    strFilename.GetRight (strFilename.GetLength () - iSearch - 1, strExtension);
    };

  return (SetCodecByExtension (strExtension.AsChar ()));
  };

  
//------------------------------------------------------------------------
EStatus  BitmapLoader::SetCodecByExtension (const char *  szExtensionIn)
  {
  /// Note:  This function calls VerifyBuffer before returning kSuccess if the codec was found.
    
  // First try to find the codec by exact extension match
  if ((szExtensionIn != NULL) && (szExtensionIn[0] != '\0')) {
    for (BaseListItr  itrCurr = listCodecs.First ();
          itrCurr.IsValid ();
          ++itrCurr)
      {
      BitmapCodec *  pCodecSearch = (BitmapCodec *) itrCurr.GetDataPtr ();
      BitmapCodec *  pCodecSolve = pCodecSearch->Clone ();
      
      if (pCodecSolve->IsValidExtension (szExtensionIn))
        {
        // found a codec!
        
        if (pCodec != NULL) delete pCodec;
        
        pCodec = pCodecSolve;
        return (pCodec->VerifyBuffer (parserBuffer));
        };
      delete pCodecSolve;
      };
    };
    
  // if we didn't find a codec by searching for the extension, just try to
  //  validate the buffer for each codec in the list until you find one that
  //  works.

  for (BaseListItr  itrCurr = listCodecs.First ();
       itrCurr.IsValid ();
       ++itrCurr)
    {
    BitmapCodec *  pCodecSearch = (BitmapCodec *) itrCurr.GetDataPtr ();
    BitmapCodec *  pCodecSolve = pCodecSearch->Clone ();
    
    parserBuffer.ResetCursor ();
    if (pCodecSolve->VerifyBuffer (parserBuffer) == EStatus::kSuccess)
      {
      // found a codec!
      if (pCodec != NULL) delete pCodec;
      pCodec = pCodecSolve;
      return (EStatus::kSuccess);
      };
    delete pCodecSolve;
    };    
    
  RStr   strOut;
  strOut.Format ("Unable to find codec for extension: %s", szExtensionIn);
  return (EStatus (EStatus::kFailure, strOut));
  };
  
  
//------------------------------------------------------------------------
VOID BitmapLoader::SetIterator  (ImageBoxIterator &  itrIn)
  {
  itrImageBox = itrIn;
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::Load  (VOID)
  {
  
  if (SupportsLineDecode () && SupportsDecode ())
    {
    return (DecodeLines (0, GetHeight () - 1));
    };

  return (FALSE);
  };


//------------------------------------------------------------------------
EStatus  BitmapLoader::Save  (VOID)
  {
  if (SupportsEncode ())
    {
    if (SupportsLineEncode ())
      {
      return (EncodeLines  (0, itrImageBox.GetHeight ()));
      }
    else
      {
      return (EStatus (EStatus::kFailure, "Codec does not support line encoding"));
      };
    };
  return (EStatus (EStatus::kFailure, "Codec does not support encoding"));
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::IsValidFile  (VOID)
  {
  return (FALSE);
  };


//------------------------------------------------------------------------
INT BitmapLoader::GetNumLayers  (VOID)
  {
  if (pCodec == NULL) return (0);

  return (pCodec->GetNumLayers ());
  };


//------------------------------------------------------------------------
RStr BitmapLoader::GetLayerName  (INT  iLayerNumberIn)
  {
  if (pCodec == NULL) return (RStr (""));

  return (pCodec->GetLayerName (iLayerNumberIn));
  };


//------------------------------------------------------------------------
INT BitmapLoader::GetWidth  (VOID)
  {
  if (pCodec == NULL) return (0);

  return (pCodec->GetWidth ());
  };


//------------------------------------------------------------------------
INT BitmapLoader::GetHeight  (VOID)
  {
  if (pCodec == NULL) return (0);

  return (pCodec->GetHeight ());
  };


//------------------------------------------------------------------------
INT BitmapLoader::GetDepth  (VOID)
  {
  if (pCodec == NULL) return (0);

  return (pCodec->GetDepth ());
  };


//------------------------------------------------------------------------
INT BitmapLoader::GetNumChannels  (VOID)
  {
  if (pCodec == NULL) return (0);

  return (pCodec->GetNumChannels ());
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::SupportsRectDecode  (VOID)
  {
  if (pCodec == NULL) return (FALSE);
  return (pCodec->SupportsRectDecode ());
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::SupportsLineDecode  (VOID)
  {
  if (pCodec == NULL) return (FALSE);
  return (pCodec->SupportsLineDecode ());
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::SupportsRectEncode  (VOID)
  {
  if (pCodec == NULL) return (FALSE);
  return (pCodec->SupportsRectEncode ());
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::SupportsLineEncode  (VOID)
  {
  if (pCodec == NULL) return (FALSE);
  return (pCodec->SupportsLineEncode ());
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::SupportsDecode  (VOID)
  {
  if (pCodec == NULL) return (FALSE);
  return (pCodec->SupportsDecode ());
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::SupportsEncode  (VOID)
  {
  if (pCodec == NULL) return (FALSE);
  return (pCodec->SupportsEncode ());
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::DecodeLines  (INT  iFirstLineIn,
                                 INT  iLastLineIn)
  {
  if (pCodec == NULL) return (FALSE);

  return (pCodec->DecodeLines (itrImageBox, iFirstLineIn, iLastLineIn));
  };


//------------------------------------------------------------------------
EStatus BitmapLoader::EncodeLines  (INT           iFirstLineIn,
                                    INT           iLastLineIn)
  {
  if (pCodec == NULL) return (EStatus (EStatus::kFailure, "No codec defined."));

  return (pCodec->EncodeLines (parserBuffer, itrImageBox, iFirstLineIn, iLastLineIn));
  };


//------------------------------------------------------------------------
BOOL BitmapLoader::DecodeRect  (INT  iStartX,
                                INT  iStartY,
                                INT  iEndX,    // exclusive end
                                INT  iEndY)    // exclusive end
  {
  if (pCodec == NULL) return (FALSE);

  return (pCodec->DecodeRect (itrImageBox, iStartX, iStartY, iEndX, iEndY));
  };


//------------------------------------------------------------------------
VOID BitmapLoader::RegisterCodec  (BitmapCodec *  pCodecIn)
  {

  // scan to see if this codec has already been registered or not.


  for (BaseListItr  itrCurr = listCodecs.First ();
       itrCurr.IsValid ();
       ++itrCurr)
    {
    BitmapCodec *  pCodec = (BitmapCodec *) itrCurr.GetDataPtr ();

    if (pCodec->GetName () == pCodecIn->GetName ())
      {
      DBG_WARNING ("Codec already registered: " + pCodecIn->GetName ());
      return;
      };
    };

  listCodecs.PushBackPtr ((void *) pCodecIn->Clone ());
  };

//------------------------------------------------------------------------
VOID BitmapLoader::UnregisterAllCodecs  (VOID)
  {

  for (BaseListItr  itrCurr = listCodecs.First ();
       itrCurr.IsValid ();
       ++itrCurr)
    {
    BitmapCodec *  pCodec = (BitmapCodec *) itrCurr.GetDataPtr ();

    delete pCodec;
    };

  listCodecs.Clear ();
  };

//------------------------------------------------------------------------
EStatus  BitmapLoader::LoadIntoBuffer (const char *    szFilenameIn,
                                       IntArray *      paiBufferOut,
                                       INT &           iWidthOut,
                                       INT &           iHeightOut,
                                       INT &           iDepthOut,
                                       INT &           iChannelsOut,
                                       BOOL            bTopDownIn) // whether the bitmap should be flipped vertically for 3D blitting purposes
  {
  EStatus   status;
  
  if (paiBufferOut == NULL)
    {
    return (EStatus (EStatus::kFailure, "Invalid source data."));
    };

  if ((szFilenameIn == NULL) || (szFilenameIn[0] == '\0'))
    {
    return (EStatus (EStatus::kFailure, "No filename given."));
    };


  // load the file into the bitmap loader
  DBG_INFO ("Reading bitmap %s", szFilenameIn);
  
  if ((status = LoadBufferFromFile (szFilenameIn)) == EStatus::kSuccess)
    {
    // query the dimensions
    iWidthOut    = GetWidth ();
    iHeightOut   = GetHeight ();
    iDepthOut    = GetDepth ();
    //iChannelsOut = GetNumChannels ();
    
    // force the channel size to always be 4 (RGBA).  This simplifies things
    //  for the GPU
    iChannelsOut = 4;

    // assumes channel depth of 8 bits    
    INT  iMapSize = iWidthOut * iHeightOut * iDepthOut * iChannelsOut;
    
    // allocate room for the data
    paiBufferOut->SetLength (iMapSize);

    // set up an iterator for the data
    
    ImageBoxIterator  itrImage (paiBufferOut->GetRawBuffer (),
                                iWidthOut,
                                iHeightOut,
                                iDepthOut,
                                iChannelsOut,
                                0,0,0,
                                iWidthOut,
                                iHeightOut,
                                iDepthOut);
    itrImage.SetTopDown (bTopDownIn);
    SetIterator (itrImage);
    Load ();
    
    DBG_INFO ("BitmapLoader::LoadIntoBuffer - Loaded file (%dx%d) : %s ", iWidthOut, iHeightOut, szFilenameIn);
    }
  else
    {
    DBG_INFO ("BitmapLoader::LoadIntoBuffer - Unable to load bitmap : %s", szFilenameIn);
    DBG_ERROR (status.GetDescription ());
    RStr  strOut;
    strOut.Format ("Unable to load bitmap : %s", szFilenameIn);
    return (EStatus (EStatus::kFailure, strOut.AsChar()));
    };

  return (EStatus::kSuccess);
  };


