/* -----------------------------------------------------------------
                          Image Box Iterator

     This class is used to superimpose a 2D array of pixels onto
   a one dimensional memory buffer.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
// //
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "image/ImageBoxIterator.hpp"
#include "base/Color8U.hpp"

#define RED_INDEX   0
#define GREEN_INDEX 1
#define BLUE_INDEX  2
#define ALPHA_INDEX 3


//-----------------------------------------------------------------------------
//  ImageBoxIterator
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------


//************************************************************************
//  ImageBoxIterator
//************************************************************************

//------------------------------------------------------------------------
ImageBoxIterator::ImageBoxIterator ()
  {
  InitVars ();
  };


//------------------------------------------------------------------------
ImageBoxIterator::ImageBoxIterator (PVOID               pvBufferIn,    
                                    INT                 iBufferWidthIn,    
                                    INT                 iBufferHeightIn,   
                                    INT                 iBufferDepthIn,    
                                    INT                 iBufferChanCountIn,

                                    INT                 iBoxStartXIn,
                                    INT                 iBoxStartYIn,
                                    INT                 iBoxStartZIn,
                                    INT                 iWidthIn,
                                    INT                 iHeightIn,
                                    INT                 iDepthIn)
  {
  // Be sure to bounds check the input against the actual layer size, and clip where needed.

  InitVars ();
  //SetLayer (itrLayerIn);

  SetBuffer (pvBufferIn,
             iBufferWidthIn,
             iBufferHeightIn,
             iBufferDepthIn,
             iBufferChanCountIn);
  pvCurrData = NULL;

  DefineActiveRegion (iBoxStartXIn, iBoxStartYIn, iBoxStartZIn, iWidthIn, iHeightIn, iDepthIn);
  };


//------------------------------------------------------------------------
ImageBoxIterator::~ImageBoxIterator ()
  {
  };

//------------------------------------------------------------------------
VOID  ImageBoxIterator::InitVars  (VOID)
  {
  iBoxStartX = iCurrPixelX = 0;
  iBoxStartY = iCurrPixelY = 0;
  iBoxStartZ = iCurrPixelZ = 0;

  iBoxWidth  = 0;
  iBoxHeight = 0;
  iBoxDepth  = 0;

  iBoxEndX   = 0;
  iBoxEndY   = 0;
  iBoxEndZ   = 0;
  
  pvCurrData = NULL;
  bTopDown   = TRUE;
  
  iBytesPerChannel = 1; // Color8U
  iBufferChanCount = 4; // Color8U
  };

  
//------------------------------------------------------------------------
VOID  ImageBoxIterator::SetBuffer (PVOID               pvBufferIn,
                                   INT                 iBufferWidthIn,
                                   INT                 iBufferHeightIn,
                                   INT                 iBufferDepthIn,
                                   INT                 iBufferChanCountIn)
  {
  pvBuffer         = pvBufferIn;      
  iBufferWidth     = iBufferWidthIn;     
  iBufferHeight    = iBufferHeightIn;     
  iBufferDepth     = iBufferDepthIn;     
  iBufferChanCount = iBufferChanCountIn;  

  iBufferWidthStride  = iBufferChanCount * iBytesPerChannel;
  iBufferHeightStride = iBufferWidth * iBufferWidthStride;
  iBufferDepthStride  = iBufferHeight * iBufferHeightStride;
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::operator++ ()    // prefix   This iterates through width, height, and depth
  {

  // the fastest and most common case is that of moving to the next pixel.  Adjust the pointer directly instead
  //  of recalculating it from scratch.
  ++iCurrPixelX;
  // detect whether or not we have a valid pointer
  if (pvCurrData == NULL)
    {
    // pointer isn't valid.  Now that we've incremented iCurrPixelX, try again.
    CalcNewDataPtr ();
    }
  else
    {
    // move to next pixel
    pvCurrData = PVOID (PUINT8 (pvCurrData) + (iBufferWidthStride));
    };

  // check for row overflow
  if (iCurrPixelX >= iBoxEndX)
    {
    iCurrPixelX = iBoxStartX;
    ++iCurrPixelY;
    if (iCurrPixelY >= iBoxEndY)
      {
      iCurrPixelY = iBoxStartY;

      ++iCurrPixelZ;
      if (iCurrPixelZ >= iBoxEndZ)
        {
        iCurrPixelZ = iBoxStartZ;

        // no more pixels 

        return (FALSE);
        };
      };
    // calculate a new pointer to the pixel data.
    CalcNewDataPtr ();
    };
  return (TRUE);
  };



//------------------------------------------------------------------------
BOOL  ImageBoxIterator::operator++ (int) // postfix  This iterates through width, height, and depth
  {
  return (++(*this));
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::operator-- ()    // prefix   This iterates backwards through width, height, and depth
  {
  // to be implemented
  DBG_ERROR ("To Be Implemented");
  return (FALSE);
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::operator-- (int) // postfix  This iterates through width, height, and depth
  {
  return (--(*this));
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::IncWidth      (UINT  uIncAmountIn)
  {
  BOOL         bBoundaryPassed = FALSE;

  // the IncWidth routine does not provide wrap-around.

  iCurrPixelX += uIncAmountIn;

  // detect whether or not we have a valid pointer
  if (pvCurrData == NULL)
    {
    // pointer isn't valid.  Now that we've incremented iCurrPixelX, try again.
    CalcNewDataPtr ();
    }
  else
    {
    //printf ("Adding %d to curr data\n", iBufferChanCount * uIncAmountIn);
    pvCurrData = PVOID (PUINT8 (pvCurrData) + (uIncAmountIn * iBufferWidthStride));
    };

  if (iCurrPixelX >= iBoxEndX)
    {
    bBoundaryPassed = TRUE;

    // update the pointer
    CalcNewDataPtr ();
    };
  return (bBoundaryPassed);
  };



//------------------------------------------------------------------------
BOOL  ImageBoxIterator::DecWidth      (UINT  uDecAmountIn)
  {
  BOOL         bBoundaryPassed = FALSE;

  // the DecWidth routine does not provide wrap-around.

  iCurrPixelX -= uDecAmountIn;
  // detect whether or not we have a valid pointer
  if (pvCurrData == NULL)
    {
    // pointer isn't valid.  Now that we've decremented iCurrPixelX, try again.
    CalcNewDataPtr ();
    }
  else
    {
    pvCurrData = PVOID (PUINT8 (pvCurrData) - (uDecAmountIn * iBufferWidthStride));
    };

  if (iCurrPixelX < iBoxStartX)
    {
    bBoundaryPassed = TRUE;

    // update the pointer
    CalcNewDataPtr ();
    };
  return (bBoundaryPassed);
  };

  
//------------------------------------------------------------------------
BOOL  ImageBoxIterator::ChangeHeight     (INT  iAmountIn)
  {
  BOOL         bBoundaryPassed = FALSE;
  printf ("ImageBoxIterator::ChangeHeight (%d)\n", iAmountIn);

  // the ChangeHeight routine does not provide wrap-around.
  if (! bTopDown) {iAmountIn *= -1;};
  iCurrPixelY += iAmountIn;

  if ((iCurrPixelY >= iBoxEndY) || (iCurrPixelY < iBoxStartY))
    {
    bBoundaryPassed = TRUE;
    };
    
  // update the pointer
  CalcNewDataPtr ();
  return (bBoundaryPassed);
  };
  
  
//------------------------------------------------------------------------
BOOL  ImageBoxIterator::IncHeight     (UINT  uIncAmountIn)
  {
  BOOL         bBoundaryPassed = FALSE;
  printf ("ImageBoxIterator::IncHeight ()\n");
  // the IncHeight routine does not provide wrap-around.
  iCurrPixelY += uIncAmountIn;

  if (iCurrPixelY >= iBoxEndY)
    {
    bBoundaryPassed = TRUE;
    };
  // update the pointer
  CalcNewDataPtr ();
  return (bBoundaryPassed);
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::DecHeight     (UINT  uDecAmountIn)
  {
  BOOL         bBoundaryPassed = FALSE;
  printf ("ImageBoxIterator::DecHeight ()\n");

  // the DecHeight routine does not provide wrap-around.
  iCurrPixelY -= uDecAmountIn;

  if (iCurrPixelY < iBoxStartY)
    {
    bBoundaryPassed = TRUE;
    };
  // update the pointer
  CalcNewDataPtr ();
  return (bBoundaryPassed);
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::IncDepth      (UINT  uIncAmountIn)
  {
  BOOL         bBoundaryPassed = FALSE;

  // the IncDepth routine does not provide wrap-around.
  iCurrPixelZ += uIncAmountIn;

  if (iCurrPixelZ >= iBoxEndZ)
    {
    bBoundaryPassed = TRUE;
    };
  // update the pointer
  CalcNewDataPtr ();
  return (bBoundaryPassed);
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::DecDepth      (UINT  uDecAmountIn)
  {
  BOOL         bBoundaryPassed = FALSE;

  // the DecDepth routine does not provide wrap-around.
  iCurrPixelZ -= uDecAmountIn;

  if (iCurrPixelZ < iBoxStartZ)
    {
    bBoundaryPassed = TRUE;
    };
  // update the pointer
  CalcNewDataPtr ();
  return (bBoundaryPassed);
  };


//------------------------------------------------------------------------
VOID  ImageBoxIterator::CalcNewDataPtr (VOID)
  {
  // bounds check
//printf ("====Calc New Data Ptr Begin %x  curr (%d, %d, %d)  start (%d, %d, %d)  %d %d \n", pvCurrData, iCurrPixelX, iCurrPixelY, iCurrPixelZ, iBoxStartX, iBoxStartY, iBoxStartZ, iBufferHeightStride, iBufferDepthStride);
  iCurrPixelX = RMin (RMax (iCurrPixelX, iBoxStartX), iBoxEndX - 1);
  iCurrPixelY = RMin (RMax (iCurrPixelY, iBoxStartY), iBoxEndY - 1);
  iCurrPixelZ = RMin (RMax (iCurrPixelZ, iBoxStartZ), iBoxEndZ - 1);
  
  pvCurrData = PVOID ( PUINT8 (pvBuffer) +
                       (iCurrPixelX * iBufferWidthStride) +
                       (iCurrPixelY * iBufferHeightStride) + 
                       (iCurrPixelZ * iBufferDepthStride));

//  printf ("====Calc New Data Ptr End %x  curr (%d, %d, %d)  start (%d, %d, %d)  %d %d \n", pvCurrData, iCurrPixelX, iCurrPixelY, iCurrPixelZ, iBoxStartX, iBoxStartY, iBoxStartZ, iBufferHeightStride, iBufferDepthStride);
  };


// auto increments
//------------------------------------------------------------------------
BOOL  ImageBoxIterator::Set  (FLOAT  fRedIn,
                              FLOAT  fGreenIn,
                              FLOAT  fBlueIn,
                              FLOAT  fAlphaIn)
  {
  // bounds check
  if (IsValid ())
    {
    // assuming source and destination data are both a bit depth of 8U for now.
    if (iBytesPerChannel == 1)
      {
      PUINT8 puCurrData = PUINT8 (pvCurrData);
      
      switch (iBufferChanCount)
        {
        default:
        case 4:  *(PUINT32)puCurrData = MakeRGBAf(fRedIn, fGreenIn, fBlueIn, fAlphaIn); break;
        case 3:  puCurrData[BLUE_INDEX]  = (UINT)(fBlueIn * 255.0f);
        case 2:  puCurrData[GREEN_INDEX] = (UINT)(fGreenIn * 255.0f);
        case 1:  puCurrData[RED_INDEX]   = (UINT)(fRedIn * 255.0f);
        case 0:  break;
        };
      };
    };

  // increment without wrap
  return (IncWidth ());
  };


// auto increments
//------------------------------------------------------------------------
BOOL  ImageBoxIterator::Set  (UINT   uRedIn,
                              UINT   uGreenIn,
                              UINT   uBlueIn ,
                              UINT   uAlphaIn)
  {
  // bounds check
  if (IsValid ())
    {
    // assuming source and destination data are both a bit depth of 8U for now.
    if (iBytesPerChannel == 1)
      {
      PUINT8 puCurrData = PUINT8 (pvCurrData);
      
      switch (iBufferChanCount)
        {
        default:
        case 4:  *(PUINT32)puCurrData = MakeRGBA(uRedIn, uGreenIn, uBlueIn, uAlphaIn); break;
        case 3:  puCurrData[BLUE_INDEX]  = uBlueIn;
        case 2:  puCurrData[GREEN_INDEX] = uGreenIn;
        case 1:  puCurrData[RED_INDEX]   = uRedIn;
        case 0:  break;
        };
      };
    };

  // increment without wrap
  return (IncWidth ());
  };


// auto increments
//------------------------------------------------------------------------
BOOL  ImageBoxIterator::SetRange  (INT    iPixelCountIn,
                                   FLOAT  fRedIn,
                                   FLOAT  fGreenIn,
                                   FLOAT  fBlueIn ,
                                   FLOAT  fAlphaIn)
  {
  UINT32  uRedOut   = UINT32 (fRedIn * 255.0f);
  UINT32  uGreenOut = UINT32 (fGreenIn * 255.0f);
  UINT32  uBlueOut  = UINT32 (fBlueIn * 255.0f);
  UINT32  uAlphaOut = UINT32 (fAlphaIn * 255.0f);

  // assuming source and destination data are both a bit depth of 8U for now.
    
  INT     iRunLength;
  INT     iRemainingInBlock;

  iRemainingInBlock = iBoxEndX - iCurrPixelX;
  iRunLength        = RMin(iPixelCountIn, iRemainingInBlock);

  if (iBytesPerChannel == 1)
    {
    while (iRunLength > 0)
      {
      PUINT8  puCurrData = PUINT8 (pvCurrData);

      switch (iBufferChanCount)
        {
        default:
        case 4:  *(PUINT32)puCurrData = MakeRGBA(uRedOut, uGreenOut, uBlueOut, uAlphaOut); break;
        case 3:  puCurrData[BLUE_INDEX]  = uBlueOut;
        case 2:  puCurrData[GREEN_INDEX] = uGreenOut;
        case 1:  puCurrData[RED_INDEX]   = uRedOut;        
        case 0:  break;
        };
      
      // increment after each pixel
      IncWidth (1);
      --iRunLength;
      };
    };
  return (IsValid ());
  };


// auto increments
//------------------------------------------------------------------------
BOOL  ImageBoxIterator::SetRange  (INT    iPixelCountIn,
                                   UINT   uRedIn,
                                   UINT   uGreenIn,
                                   UINT   uBlueIn ,
                                   UINT   uAlphaIn)
  {
  UINT32  uRedOut   = uRedIn;
  UINT32  uGreenOut = uGreenIn;
  UINT32  uBlueOut  = uBlueIn;
  UINT32  uAlphaOut = uAlphaIn;
  
  // assuming source and destination data are both a bit depth of 8U for now.
    
  INT     iRunLength;
  INT     iRemainingInBlock;

  iRemainingInBlock = iBoxEndX - iCurrPixelX;
  iRunLength        = RMin(iPixelCountIn, iRemainingInBlock);

  if (iBytesPerChannel == 1)
    {
    while (iRunLength > 0)
      {
      PUINT8  puCurrData = PUINT8 (pvCurrData);

      switch (iBufferChanCount)
        {
        default:
        case 4:  *(PUINT32)puCurrData = MakeRGBA(uRedOut, uGreenOut, uBlueOut, uAlphaOut); break;
        case 3:  puCurrData[BLUE_INDEX]  = uBlueOut;
        case 2:  puCurrData[GREEN_INDEX] = uGreenOut;
        case 1:  puCurrData[RED_INDEX]   = uRedOut;        
        case 0:  break;
        };
      
      // increment after each pixel
      IncWidth (1);
      --iRunLength;
      };
    };
  return (IsValid ());
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::SetImagePosition  (INT    iXIn,
                                           INT    iYIn,
                                           INT    iZIn)
  {

  // Note that we CAN set an invalid (out of bounds) position.
  iCurrPixelX = iXIn;
  iCurrPixelY = iYIn;
  iCurrPixelZ = iZIn;

  if (! bTopDown) {iCurrPixelY = iBoxEndY - iYIn - 1;};
  //printf ("ImageBoxIterator::SetPosition topdown: %d in: %d %d %d  set: %d %d %d\n", 
  //        bTopDown, iXIn, iYIn, iZIn, 
  //        iCurrPixelX, iCurrPixelY, iCurrPixelZ);

  CalcNewDataPtr ();
  //printf ("ImageBoxIterator::SetPosition Mark 2   %d %d %d\n", iCurrPixelX, iCurrPixelY, iCurrPixelZ);

  return (pvCurrData != NULL);
  };


//------------------------------------------------------------------------
BOOL  ImageBoxIterator::IsValid            (VOID)
  {
  if ((iCurrPixelX <  iBoxStartX) ||
      (iCurrPixelY <  iBoxStartY) ||
      (iCurrPixelZ <  iBoxStartZ) ||
      (iCurrPixelX >= iBoxEndX) ||
      (iCurrPixelY >= iBoxEndY) ||
      (iCurrPixelZ >= iBoxEndZ))
    {
    // out of bounds.
    return (FALSE);
    };
  return (TRUE);
  };


//------------------------------------------------------------------------
VOID  ImageBoxIterator::DefineActiveRegion  (INT   iBoxStartXIn,
                                             INT   iBoxStartYIn,
                                             INT   iBoxStartZIn,
                                             INT   iWidthIn,
                                             INT   iHeightIn,
                                             INT   iDepthIn)
  {
  // start the pixel pointer in the same corner as the start of the box.
  iBoxStartX = iCurrPixelX = RMax (iBoxStartXIn, 0);
  iBoxStartY = iCurrPixelY = RMax (iBoxStartYIn, 0);
  iBoxStartZ = iCurrPixelZ = RMax (iBoxStartZIn, 0);

  iBoxEndX = RMin((iBoxStartXIn + iWidthIn),  iBufferWidth);
  iBoxEndY = RMin((iBoxStartYIn + iHeightIn), iBufferHeight);
  iBoxEndZ = RMin((iBoxStartZIn + iDepthIn),  iBufferDepth);

  iBoxWidth  = iBoxEndX - iBoxStartX;
  iBoxHeight = iBoxEndY - iBoxStartY;
  iBoxDepth  = iBoxEndZ - iBoxStartZ;
  };


// auto increments
//------------------------------------------------------------------------
Color8U  ImageBoxIterator::GetPixel8U  (VOID)
  {
  Color8U   colorOut;
  // assuming source and destination data are both a bit depth of 8U for now.

  // bounds check
  if (IsValid ())
    {
    if (iBytesPerChannel == 1)
      {
      PUINT8 puCurrData = PUINT8 (pvCurrData);

      switch (iBufferChanCount)
        {
        default:
        case 4:  colorOut.SetA (puCurrData [ALPHA_INDEX]);  // fall through
        case 3:  colorOut.SetB (puCurrData [BLUE_INDEX]);   // fall through
        case 2:  colorOut.SetG (puCurrData [GREEN_INDEX]);  // fall through
        case 1:  colorOut.SetR (puCurrData [RED_INDEX]);    // fall through
        case 0:  break;
        };
      };
    }
  else
    {
    // invalid pointer.  Return black.
    colorOut.Set (0xffu, 0xffu, 0xffu, 0xffu);
    };
  IncWidth ();
  return (colorOut);
  };

//------------------------------------------------------------------------
ImageBoxIterator &  ImageBoxIterator::operator= (const ImageBoxIterator & itrIn)
  {

  pvBuffer             = itrIn.pvBuffer;
  iBufferWidth         = itrIn.iBufferWidth;
  iBufferHeight        = itrIn.iBufferHeight;
  iBufferDepth         = itrIn.iBufferDepth;
  iBufferChanCount     = itrIn.iBufferChanCount;

  iBufferWidthStride   = itrIn.iBufferWidthStride;
  iBufferHeightStride  = itrIn.iBufferHeightStride;
  iBufferDepthStride   = itrIn.iBufferDepthStride;
  iBytesPerChannel     = itrIn.iBytesPerChannel;

  iCurrPixelX          = itrIn.iCurrPixelX;
  iCurrPixelY          = itrIn.iCurrPixelY;
  iCurrPixelZ          = itrIn.iCurrPixelZ;

  iBoxWidth            = itrIn.iBoxWidth;
  iBoxHeight           = itrIn.iBoxHeight;
  iBoxDepth            = itrIn.iBoxDepth;

  iBoxStartX           = itrIn.iBoxStartX;
  iBoxStartY           = itrIn.iBoxStartY;
  iBoxStartZ           = itrIn.iBoxStartZ;

  iBoxEndX             = itrIn.iBoxEndX;
  iBoxEndY             = itrIn.iBoxEndY;
  iBoxEndZ             = itrIn.iBoxEndZ;

  pvCurrData           = itrIn.pvCurrData;

  bTopDown             = itrIn.bTopDown;
  
  return *this;
  };


//------------------------------------------------------------------------
VOID ImageBoxIterator::DebugPrint (VOID)
  {                                   
  printf ("ImageBoxIterator Debug Info:\n");
  //printf ("  Buffer buffer: %x\n", (unsigned int) pvBuffer);
  printf ("  %d x %d x %d (%d chan)\n", iBufferWidth, iBufferHeight, iBufferDepth, iBufferChanCount);
  printf ("  stride w: %d h:%d d:%d\n", iBufferWidthStride, iBufferHeightStride, iBufferDepthStride);
  
  printf ("  Curr Pixel %d, %d, %d\n", iCurrPixelX, iCurrPixelY, iCurrPixelZ);
  printf ("  Box Size %d %d %d\n", iBoxWidth, iBoxHeight, iBoxDepth);
  printf ("  Start Pixel %d, %d, %d\n", iBoxStartX, iBoxStartY, iBoxStartZ);
  printf ("  End Pixel %d, %d, %d\n", iBoxEndX, iBoxEndY, iBoxEndZ);
  
  //printf ("  Curr Data : %x\n", (unsigned int) pvCurrData);
  };


