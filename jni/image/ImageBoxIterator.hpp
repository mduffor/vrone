/* -----------------------------------------------------------------
                          Image Box Iterator

     This class is used to superimpose a 2D (or 3D) array of pixels 
   onto a one dimensional memory buffer.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef IMAGEBOXITERATOR_HPP
#define IMAGEBOXITERATOR_HPP

#include "Color8U.hpp"

//------------------------------------------------------------------------
class ImageBoxIterator
  {
  private:

    PVOID               pvBuffer;          ///< target one-dimensional buffer, which is interpreted as an image
    
    INT                 iBufferWidth;      ///< width of total image at target data
    INT                 iBufferHeight;     ///< height of total image at target data
    INT                 iBufferDepth;      ///< depth of total image at target data
    INT                 iBufferChanCount;  ///< number of channels in target data

    INT                 iBufferWidthStride;
    INT                 iBufferHeightStride;
    INT                 iBufferDepthStride;
    INT                 iBytesPerChannel;
    BOOL                bTopDown;

    INT                 iCurrPixelX;       ///< in pixel units.  Current X position within the total image 
    INT                 iCurrPixelY;       ///< in pixel units.  Current Y position within the total image 
    INT                 iCurrPixelZ;       ///< in pixel units.  Current Z position within the total image 

    INT                 iBoxWidth;         ///< in pixel units.  Total width (X dimension) of iterated box
    INT                 iBoxHeight;        ///< in pixel units.  Total height (Y dimension) of iterated box
    INT                 iBoxDepth;         ///< in pixel units.  Total depth (Z dimension) of iterated box

    INT                 iBoxStartX;        ///< in pixel units.  X start of iterated box in total image coordinates
    INT                 iBoxStartY;        ///< in pixel units.  Y start of iterated box in total image coordinates
    INT                 iBoxStartZ;        ///< in pixel units.  Z start of iterated box in total image coordinates

    INT                 iBoxEndX;          ///< in pixel units.  exclusive end of iterated box in X 
    INT                 iBoxEndY;          ///< in pixel units.  exclusive end of iterated box in Y
    INT                 iBoxEndZ;          ///< in pixel units.  exclusive end of iterated box in Z

    PVOID               pvCurrData;        ///< current pointer to target data

  protected:


  public:

                  ImageBoxIterator  ();

                  ImageBoxIterator  (PVOID               pvBufferIn,
                                     INT                 iBufferWidthIn,
                                     INT                 iBufferHeightIn,
                                     INT                 iBufferDepthIn,
                                     INT                 iBufferChanCountIn,
                 
                                     INT                 iBoxStartXIn,
                                     INT                 iBoxStartYIn,
                                     INT                 iBoxStartZIn,
                                     INT                 iWidthIn,
                                     INT                 iHeightIn,
                                     INT                 iDepthIn);


                  ~ImageBoxIterator ();

    VOID          InitVars           (VOID);

    VOID          SetBuffer          (PVOID               pvBufferIn,
                                      INT                 iBufferWidthIn,
                                      INT                 iBufferHeightIn,
                                      INT                 iBufferDepthIn,
                                      INT                 iBufferChanCountIn);

    VOID          CalcEndPixel       (VOID);
    VOID          CalcEndPixelX      (VOID);
    VOID          CalcEndPixelY      (VOID);
    VOID          CalcEndPixelZ      (VOID);


    BOOL          operator++         ();    ///< prefix increment   This iterates through width, height, and depth
    BOOL          operator++         (int); ///< postfix increment  This iterates through width, height, and depth

    BOOL          operator--         ();    ///< prefix decrement  This iterates through width, height, and depth
    BOOL          operator--         (int); ///< postfix decrement  This iterates through width, height, and depth

    ImageBoxIterator & operator=          (const ImageBoxIterator & itrIn);

    INT           GetWidth           (VOID)             {return (iBufferWidth);};
    INT           GetHeight          (VOID)             {return (iBufferHeight);};
    INT           GetDepth           (VOID)             {return (iBufferDepth);};
    INT           GetChanCount       (VOID)             {return (iBufferChanCount);};

    VOID          SetTopDown         (BOOL  bStatusIn)  {bTopDown = bStatusIn;};

    BOOL          IncWidth           (UINT  uIncAmountIn = 1);
    BOOL          DecWidth           (UINT  uDecAmountIn = 1);

    BOOL          IncHeight          (UINT  uIncAmountIn = 1);
    BOOL          DecHeight          (UINT  uDecAmountIn = 1);
    BOOL          ChangeHeight       (INT   iAmountIn    = 1);

    BOOL          IncDepth           (UINT  uIncAmountIn = 1);
    BOOL          DecDepth           (UINT  uDecAmountIn = 1);

    VOID          CalcNewDataPtr     (VOID);
    PVOID         GetDataPtr         (VOID)        {return pvCurrData;};

    // auto increments
    BOOL          Set                (FLOAT  fRedIn,
                                      FLOAT  fGreenIn = 0.0,
                                      FLOAT  fBlueIn  = 0.0,
                                      FLOAT  fAlphaIn = 0.0);

    // auto increments
    BOOL          Set                (UINT   uRedIn,          ///< RGB values given in the range of 0-255
                                      UINT   uGreenIn = 0,
                                      UINT   uBlueIn  = 0,
                                      UINT   uAlphaIn = 0);

    // auto increments
    BOOL          SetRange           (INT    iPixelCountIn,
                                      FLOAT  fRedIn,
                                      FLOAT  fGreenIn = 0.0,
                                      FLOAT  fBlueIn  = 0.0,
                                      FLOAT  fAlphaIn = 0.0);

    // auto increments
    BOOL          SetRange           (INT    iPixelCountIn,
                                      UINT   uRedIn,          ///< RGB values given in the range of 0-255
                                      UINT   uGreenIn = 0,
                                      UINT   uBlueIn  = 0,
                                      UINT   uAlphaIn = 0);

    // returns true if the position is valid
    BOOL          SetImagePosition   (INT    iXIn,
                                      INT    iYIn,
                                      INT    iZIn = 0);

    BOOL          SetBoxPosition     (INT    iXIn,
                                      INT    iYIn,
                                      INT    iZIn = 0)      {return SetImagePosition (iXIn + iBoxStartX, iYIn + iBoxStartY, iZIn + iBoxStartZ);};
                                      
    VOID          ResetPosition      (VOID)   {iCurrPixelX = iBoxStartX; iCurrPixelY = iBoxStartY; iCurrPixelZ = iBoxStartZ; };

    BOOL          IsValid            (VOID);

    // pixel coordinates within the iterated area
    INT           GetBoxPixelX       (VOID)                 {return (iCurrPixelX - iBoxStartX);};
    INT           GetBoxPixelY       (VOID)                 {return (iCurrPixelY - iBoxStartY);};
    INT           GetBoxPixelZ       (VOID)                 {return (iCurrPixelZ - iBoxStartZ);};

    // pixel coordinates within the entire image
    INT           GetPixelX          (VOID)                 {return (iCurrPixelX);};;
    INT           GetPixelY          (VOID)                 {return (iCurrPixelY);};
    INT           GetPixelZ          (VOID)                 {return (iCurrPixelZ);};

    // first pixel coordinates 
    INT           GetStartPixelX     (VOID)                 {return (iBoxStartX);};;
    INT           GetStartPixelY     (VOID)                 {return (iBoxStartY);};
    INT           GetStartPixelZ     (VOID)                 {return (iBoxStartZ);};

    // last pixel coordinates 
    INT           GetEndPixelX       (VOID)                 {return (iBoxEndX);};;
    INT           GetEndPixelY       (VOID)                 {return (iBoxEndY);};
    INT           GetEndPixelZ       (VOID)                 {return (iBoxEndZ);};

    // reposition the area covered by the iterator
    VOID          DefineActiveRegion (INT   iBoxStartXIn,
                                      INT   iBoxStartYIn,
                                      INT   iBoxStartZIn,
                                      INT   iWidthIn,
                                      INT   iHeightIn,
                                      INT   iDepthIn);

    // auto increments
    Color8U       GetPixel8U         (VOID);

    VOID          DebugPrint         (VOID);

  };

#endif // IMAGEBOXITERATOR_HPP
