/* -----------------------------------------------------------------
                             Bitmap Loader

     This module handles registration of bitmap codecs and loading
   bitmaps into memory via these codecs.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 


#ifndef BITMAPLOADER_HPP
#define BITMAPLOADER_HPP

#include "Types.hpp"
#include "RStr.hpp"
#include "RStrParser.hpp"
#include "BaseList.hpp"
#include "IntArray.hpp"
#include "image/ImageBoxIterator.hpp"
#include "image/BitmapCodec.hpp"


/**
  */

//------------------------------------------------------------------------
class BitmapLoader
  {
  public:

    // these variables are left public so that the bitmap codec can more
    //   easily get to them.

    
    RStr           strFilename;    ///< Name of the file on disk to load.
    RStrParser     parserBuffer;   ///< Buffer containing the compressed bitmap data (raw file read from disk)
    BitmapCodec *  pCodec;         ///< Current active codec to decode the mem buffer

    /// Pixel iterator for the image.  This is what the codec writes decoded pixels to.
    ImageBoxIterator    itrImageBox;

    /// list of prototypes for codec creation.
    static BaseList  listCodecs;

  public:


                  BitmapLoader        ();     ///< constructor

                  ~BitmapLoader       ();    ///< destructor

                  BitmapLoader        (ImageBoxIterator &  itrIn);  

                  /** @brief Loads the file into a memory buffer for parsing
                      @param strFilenameIn Path and name of file from where bitmap will be loaded
                      @return error status
                  */
    EStatus       LoadBufferFromFile  (const char *  szFilenameIn);

    EStatus       SetBuffer           (const char *  szBufferIn,
                                       const char *  szCodecTypeIn);

    EStatus       SetCodecByFilename  (const char *  szFilenameIn);
                                       
    EStatus       SetCodecByExtension (const char *  szFilenameIn);

                  /** @brief Sets the iterator that the bitmap will be written to (loading) or read from (saving)
                      @param itrIn The iterator to use for image input and output
                      @return None
                  */
    VOID          SetIterator         (ImageBoxIterator &  itrIn);

                  /** @brief Loads the entire image into the iterator
                      @return True if the operation was successful, false if not.
                  */
    BOOL          Load                (VOID);

                  /** @brief Save the entire image from the iterator into the set filename.
                      @return True if the operation was successful, false if not.
                  */
    EStatus       Save                (VOID);

                  /** @brief Returns whether a valid codec was found for the set file.
                      @return True if a codec was found, false if not.
                  */
    BOOL          IsValidFile         (VOID);

    INT           GetNumLayers        (VOID);  ///< returns the number of layers in the image

                  /** @brief Returns the name of the requested layer. 
                      @param iLayerNumberIn The index for the desired layer.  
                      @return Name of the layer. (ex. "RGBA")
                  */
    RStr          GetLayerName        (INT  iLayerNumberIn);

    INT           GetWidth            (VOID);   ///< returns the width of the image (x dimension)

    INT           GetHeight           (VOID);   ///< returns the height of the image (y dimension)

    INT           GetDepth            (VOID);   ///< returns the depth of the image (z dimension).  This will be "1" for bitmaps, higher for voxel maps.

    INT           GetNumChannels      (VOID);   ///< returns the number of channels in a pixel.
                  
    BOOL          SupportsRectDecode  (VOID);   ///< returns whether or not the codec supports decoding of an arbitrary rectangle

    BOOL          SupportsLineDecode  (VOID);   ///< returns whether or not the codec supports decoding of line by line data

    BOOL          SupportsRectEncode  (VOID);   ///< returns whether or not the codec supports encoding of an arbitrary rectangle

    BOOL          SupportsLineEncode  (VOID);   ///< returns whether or not the codec supports encoding of line by line data

    BOOL          SupportsDecode      (VOID);   ///< returns whether or not the codec supports decoding images.

    BOOL          SupportsEncode      (VOID);   ///< returns whether or not the codec supports encoding images.


                  /** @brief Returns the name of the requested layer. 
                      @param iLayerNumberIn The index for the desired layer.  
                      @return True on success, false on failure
                  */
    BOOL          DecodeLines         (INT  iFirstLineIn,
                                       INT  iLastLineIn);


    EStatus       EncodeLines         (INT  iFirstLineIn,
                                       INT  iLastLineIn);  // exclusive end

    BOOL          DecodeRect          (INT  iStartX,
                                       INT  iStartY,
                                       INT  iEndX,        // exclusive end
                                       INT  iEndY);       // exclusive end


                  /** @brief Store an instance of the given codec in an internal global list.  When a file is encoded or decoded, this list will be searched for an appropriate codec.
                      @param pCodecIn An instance of a codec derived from BitmapCodec that implements a codec
                      @return None
                  */
    static VOID   RegisterCodec        (BitmapCodec *  pCodecIn);

    static VOID   UnregisterAllCodecs  (VOID);

    EStatus       LoadIntoBuffer       (const char *  szFilenameIn,
                                        IntArray *    paiBufferOut,
                                        INT &         iWidthOut,
                                        INT &         iHeightOut,
                                        INT &         iDepthOut,
                                        INT &         iChannelsOut,
                                        BOOL          bTopDownIn = false);
  };

#endif // BITMAPLOADER_HPP

