/* -----------------------------------------------------------------
                             Bitmap Codec

     This module implements the base class from which bitmap codecs
   inherit.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef BITMAPCODEC_HPP
#define BITMAPCODEC_HPP

#include "Types.hpp"
#include "RStr.hpp"
#include "RStrArray.hpp"
#include "RStrParser.hpp"
#include "ImageBoxIterator.hpp"

/**
  The BitmapCodec class is the base class from which the actual bitmap codecs
    inherit.
  */


//-----------------------------------------------------------------------------
class BitmapCodec
  {
  protected:
    INT32           iBitmapWidth;       /// size of the image in pixels across
    INT32           iBitmapHeight;      /// size of the image in pixels high
    INT32           iBitmapDepth;       /// size of the image in pixels deep
    INT32           iNumLayers;         /// number of layers of pixel data
    INT32           iNumBytesPerChan;   /// size of each channel, in bytes
    INT32           iNumChannels;       /// number of channels in a pixel
    RStr            strChannelContents; /// short description of the contents ("RGBA", "RGB", "RA", etc.)
    
    RStr            strCodecName;       /// A description that identifies the codec.  Whitespace allowed.
    RStrParser *    parserBuffer;

  public:

                           BitmapCodec  ();

    virtual                ~BitmapCodec ();

                           /// copy constructor
                           BitmapCodec  (const BitmapCodec & codecIn);

    VOID                   Initialize         (VOID);

    RStr                   GetName            (VOID)     {return strCodecName;};

    INT32                  GetWidth           (VOID)     {return iBitmapWidth;};

    INT32                  GetHeight          (VOID)     {return iBitmapHeight;};

    INT32                  GetDepth           (VOID)     {return iBitmapDepth;};

    INT32                  GetNumLayers       (VOID)     {return iNumLayers;};
    
    INT32                  GetNumChannels     (VOID)     {return iNumChannels;};

    virtual RStr           GetLayerName       (INT  iLayerNumberIn) = 0;
    
    virtual BitmapCodec *  Clone              (VOID) = 0;

    virtual RStrArray      GetFileExtensions  (VOID) = 0;

    virtual BOOL           IsValidExtension   (const char *  szExtensionIn);

    virtual EStatus        VerifyBuffer       (RStrParser &  parserIn) = 0;
    
                           /// returns whether or not the codec supports decoding of an arbitrary rectangle
    virtual BOOL           SupportsRectDecode   (VOID) = 0;

                           /// returns whether or not the codec supports decoding of line by line data
    virtual BOOL           SupportsLineDecode   (VOID) = 0;

                           /// returns whether or not the codec supports encoding of an arbitrary rectangle
    virtual BOOL           SupportsRectEncode   (VOID) = 0;

                           /// returns whether or not the codec supports encoding of line by line data
    virtual BOOL           SupportsLineEncode   (VOID) = 0;

    virtual BOOL           SupportsDecode     (VOID) = 0; ///< returns whether or not the codec supports decoding (loading)

    virtual BOOL           SupportsEncode     (VOID) = 0; ///< returns whether or not the codec supports encoding (saving)

    virtual BOOL           DecodeLines        (ImageBoxIterator &  itrImageBox,
                                               INT                 iFirstLineIn,
                                               INT                 iLastLineIn);

    virtual BOOL           DecodeRect         (ImageBoxIterator &  itrImageBox,
                                               INT                 iStartX,
                                               INT                 iStartY,
                                               INT                 iEndX,        // exclusive end
                                               INT                 iEndY);       // exclusive end

    virtual EStatus        EncodeLines        (RStrParser &        parserIn,
                                               ImageBoxIterator &  itrImageBox,
                                               INT                 iFirstLineIn,
                                               INT                 iLastLineIn);

    virtual EStatus        EncodeRect         (RStrParser &        parserIn,
                                               ImageBoxIterator &  itrImageBox,
                                               INT                 iStartX,
                                               INT                 iStartY,
                                               INT                 iEndX,        // exclusive end
                                               INT                 iEndY);       // exclusive end

    virtual VOID           Debug              (const char *  szFilename, 
                                               INT           iLineNum) = 0;



  };





  
#endif // BITMAPCODEC_HPP