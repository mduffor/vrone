/* -----------------------------------------------------------------
                             Bounding Cylinder

     This class implements a cylinder used for collision detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef BCYLINDER_HPP
#define BCYLINDER_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "BVolume.hpp"
#include "Ray.hpp"
#include "CPlane.hpp"
#include "CPoint.hpp"
#include "CTriangle.hpp"

//------------------------------------------------------------------------
class BCylinder : public BVolume
  {
  public:
    RVec3           vecCenter;
    RVec3           vecAxisNorm;
    RVec3           vecAxisXNorm;
    RVec3           vecAxisYNorm;

    RVec3           vecWorldCenter;

    FLOAT          fHalfHeight;
    FLOAT          fHalfHeightSquared;
    FLOAT          fRadius;
    FLOAT          fRadiusSquared;

  public:
             BCylinder  ()      {Clear ();};
    virtual  ~BCylinder ()      {};
    VOID     Clear      ()      {vecCenter.Zero ();
                                 vecAxisNorm.Zero ();
                                 vecAxisXNorm.Zero ();
                                 vecAxisYNorm.Zero ();
                                 vecWorldCenter.Zero ();
                                 fHalfHeight = 1;
                                 fHalfHeightSquared = 1;
                                 fRadius = 1;
                                 fRadiusSquared = 1;};

    VOID     ToSphere        (RVec3 &      vecCenterOut,
                              FLOAT &      fRadiusOut,
                              FLOAT &      fRadiusSquaredOut)  {vecCenterOut = vecCenter; 
                                                                fRadiusOut = fRadius + fHalfHeight; 
                                                                fRadiusSquaredOut = fRadiusOut * fRadiusOut;};
                              
    BOOL     PointIntersect  (RVec3&       vecPointIn);

    BOOL     RayIntersect    (Ray &        rayIn,
                              CPoint *     ppntCollisionOut);

                              
    BOOL     PlaneIntersect     (CPlane&     v4PlaneIn,
                                 PFLOAT      pfPlanePushOut,
                                 RVec3&      vecPushPoint);
                                 
    BOOL     SphereIntersect    (RVec3 &      vecSphereWorldPosIn,
                                 FLOAT        fSphereRadiusIn);
    
    
    BOOL     TriIntersect       (CTriangle &  vecTriangleIn,
                                 FLOAT        fRisePoint);
                                 
    BOOL     CylinderIntersect  (BCylinder&  cylCylinder2In);
                            


                            
    BOOL     PlaneIntersect  (CPlane&      planeIn);

    BOOL     TriIntersect    (CTriangle &  triangleIn);                            
                            
                            
                            
                            
  };
typedef BCylinder *  PBCylinder;




#endif // BCYLINDER_HPP
