/* -----------------------------------------------------------------
                           Bounds

     This module implements 3D collision detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef BOUNDS_HPP
#define BOUNDS_HPP


#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include <math.h>
#include "CPlane.hpp"


class   RBounds;
typedef RBounds *  PRBounds;
class   RFrustum;
typedef RFrustum *  PRFrustum;



#define BV_VERSION_1_0     0x00010000

// flags for RBounds::AddPoint

#define BM_SPHERE            0x00000001
#define BM_BOX               0x00000002







//------------------------------------------------------------------------
class RBounds
  {
  public:

    enum  EType       {kUnknown, kAxisBox, kObjBox, kCylinder, kPoint, kRay, kSphere};

  private:

    // flags to determine type of second bounding volume, plus other info

    union
      {
      struct
        {
        UINT           fInitialized      : 1;
        UINT           fRootNode         : 1;
        UINT           fNoChildSearch    : 1;
        UINT           fMatrixChanged    : 1;
        UINT           fPreTransformTris : 1;
        };
      UINT             uFlags;
      };

    EType              tType;


    PRBounds   pbvChildren;
    PVOID              pvParent;


    // Bounding sphere.  Applies to all volumes.
    RVec3              vecSphereCenter;
    FLOAT              fSphereRadius;
    FLOAT              fSphereRadiusSqr;

    RVec3              vecWorldSphereCenter;


    // variables used to build a new bounding volume.
    RVec3              vecHighLimit;
    RVec3              vecLowLimit;


    // position and orientation in relation to parent
    RMatrix            m4Matrix;                   // root matrix
    RMatrix            m4CompositeMatrix;          // root plus parent

    RMatrix            m4WorldToLocal;

    // Secondary Volume
    // Note:  This was a union, but unions are invalid for classes with copy constructors.
    //         Re-structure with polymorphism as soon as you get the chance.

      RBoundingBox     bbAxisBox;                  // all parents must be axis boxes too
      RBoundingBox     bbObjectBox;
      RCylinder        cylCylinder;
      RPoint           pntPoint;
      RRay             rayRay;



  public:

                     RBounds          ();

                     ~RBounds         ();

    //PRBounds  NextInList              (VOID)   {return ((PRBounds) pllNext);};

    VOID             Clear                   (VOID);

    VOID             InvalidateMatrix        (VOID)                                    {fMatrixChanged = TRUE;};

    BOOL             IsInvalidMatrix         (VOID)                                    {return (fMatrixChanged ? TRUE : FALSE);};

    VOID             ClearInvalidMatrix      (VOID)                                    {fMatrixChanged = FALSE;};

    RMatrix&          GetMatrix               (VOID)                                    {return m4Matrix;};

    VOID             SetMatrix               (RMatrix&  m4MatIn)                        {m4Matrix = m4MatIn; m4CompositeMatrix = m4MatIn; InvalidateMatrix ();};

    RMatrix&          GetCompMatrix           (VOID)                                    {return m4CompositeMatrix;};

    VOID             SetCompMatrix           (RMatrix&  m4MatIn)                        {m4CompositeMatrix = m4MatIn; InvalidateMatrix ();};

    VOID             AddParentMatrix         (RMatrix&  m4ParentMatIn)                  {m4CompositeMatrix = m4ParentMatIn * m4Matrix; InvalidateMatrix ();};

    BOOL             IsRoot                  (VOID)                                    {return fRootNode;};

    VOID             SetIsRoot               (BOOL  bStatusIn)                         {fRootNode = bStatusIn;};

    BOOL             IsChildSearch           (VOID)                                    {return (fNoChildSearch ? FALSE : TRUE);};

    VOID             SetIsChildSearch        (BOOL  bStatusIn)                         {fNoChildSearch = bStatusIn;};

    BOOL             IsPreTransformTris      (VOID)                                    {return fPreTransformTris;};

    PVOID            GetParent               (VOID)                                    {return pvParent;};

    VOID             SetParent               (PVOID  pvParentIn)                       {pvParent = pvParentIn;};

    VOID             SetType                 (EType  tTypeIn);

    EType            GetType                 (VOID)                                    {return tType;};

    VOID             FirstPoint              (RVec3&  vecPointIn,
                                              UINT    ulFlagsIn = BM_SPHERE | BM_BOX);

    VOID             FirstPointBB2D          (RVec3&  v2PointIn);

    VOID             AddPoint                (RVec3&  vecPointIn,
                                              UINT    ulFlagsIn = BM_SPHERE | BM_BOX);

    VOID             AddPoint                (FLOAT   fXIn,
                                              FLOAT   fYIn,
                                              FLOAT   fZIn,
                                              UINT    ulFlagsIn = BM_SPHERE | BM_BOX);

    VOID             AddPointBB2D            (RVec3&  v2PointIn);

    VOID             AddSphere               (RVec3&  vecCenterIn,
                                              FLOAT   fRadiusIn);

    VOID             AddSphereToBBox         (RVec3&  vecCenterIn,
                                              FLOAT   fRadiusIn);

    VOID             AddBoundingBox          (RBoundingBox&     bbBoxIn);

    VOID             AddCylinder             (RCylinder&        cylCylinderIn);

    VOID             AddRBounds      (RBounds&  bvDataIn);

//    VOID             AddBoundingData         (RBounds&  bdDataIn);

    RVec3&           GetWorldSphereCenter    (VOID)                                    {return vecWorldSphereCenter;};

    VOID             SetWorldSphereCenter    (RVec3&  vecIn)                           {vecWorldSphereCenter = vecIn;};

    RVec3&           GetSphereCenter         (VOID)                                    {return vecSphereCenter;};

    VOID             SetSphereCenter         (RVec3&  vecCenterIn)                     {vecSphereCenter = vecCenterIn;};

    VOID             SetSphereCenter         (RVec4   v4CenterIn)                     {vecSphereCenter.Set (v4CenterIn.fX, v4CenterIn.fY, v4CenterIn.fZ);};

    FLOAT            GetSphereRadius         (VOID)                                    {return fSphereRadius;};

    VOID             SetSphereRadius         (FLOAT  fRadiusIn)                      {fSphereRadius = fRadiusIn; SetSphereRadiusSqr (fSphereRadius * fSphereRadius);};

    FLOAT            GetSphereRadiusSqr      (VOID)                                    {return fSphereRadiusSqr;};

    VOID             SetSphereRadiusSqr      (FLOAT  fRadiusSqrIn)                   {fSphereRadiusSqr = fRadiusSqrIn;};

    RMatrix&         GetWorldToLocalMat      (VOID)                                    {return m4WorldToLocal;};

    VOID             QueryBoundingBox        (RVec3&   vecHighOut,
                                              RVec3&   vecLowOut);

    VOID             QueryBoundingSphere     (RVec3&   vecCenterOut,
                                              FLOAT&  fRadiusOut);

    VOID             QueryBoundingBox2D      (RVec3&   v2HighOut,
                                              RVec3&   v2LowOut);

//    VOID             SetBoundingData         (RBounds&  bdDataIn);
//
//    VOID             ClearBoundingData       (RBounds&  bdDataIn);
//
//    VOID             CopyBoundingData        (RBounds&  bdDataOut,
//                                              RBounds&  bdDataIn);

    BOOL             RaySphereIntersect      (RBounds&  bvRayIn,
                                              RBounds&  bvSphereIn,
                                              PRPoint         ppntCollisionOut = NULL);

    VOID             CalcSphere              (VOID);

    VOID             CalcOBB                 (VOID);

    VOID             CalcAABB                (VOID);

    VOID             PositionAABB            (VOID);

    VOID             PositionOBB             (VOID);

    VOID             PositionCylinder        (VOID);

    BOOL             RayBBIntersect          (RBounds&  bvRayIn,
                                              RBoundingBox&   obbObjectIn,
                                              PRPoint         ppntCollisionOut = NULL);

    BOOL             PointInSphere           (RVec3&          vecPointIn,
                                              RBounds&  bvSphereIn);

    BOOL             PointInBB               (RVec3&          vecPointIn,
                                              RBoundingBox&   obbObjectIn);

    BOOL             RayTriIntersect         (RBounds&  bvRayIn,
                                              RTriangle&      vecTriangleIn,
                                              PRPoint         ppntCollisionOut);

    BOOL             PlaneAABBIntersect      (RPlane&        v4PlaneIn,
                                              RBoundingBox&   bbObjectIn);

    BOOL             PlaneOBBIntersect       (RPlane&        v4PlaneIn,
                                              RBoundingBox&   bbObjectIn);

    BOOL             SphereAABBIntersect     (RBounds&  bvSphereIn,
                                              RBoundingBox&   bbObjectIn);

    BOOL             SphereOBBIntersect      (RBounds&  bvSphereIn,
                                              RBoundingBox&   bbObjectIn);

    BOOL             AABBIntersect           (RBoundingBox&   bbBox1In,
                                              RBoundingBox&   bbBox2In);

    BOOL             OBBIntersect            (RBoundingBox&   bbBox1In,
                                              RBoundingBox&   bbBox2In);

    BOOL             SpherePlaneIntersect    (RBounds&  bvSphereIn,
                                              RPlane&        v4PlaneIn);

    BOOL             CylinderPlaneIntersect  (RCylinder&  cylCylinderIn,
                                              RPlane&    v4PlaneIn,
                                              PFLOAT     pfPlanePushOut,
                                              RVec3&      vecPushPoint);

    BOOL             CylinderRayIntersect    (RCylinder&      cylCylinderIn,
                                              RBounds&  bvRayIn);

    BOOL             CylinderSphereIntersect (RCylinder&      cylCylinderIn,
                                              RBounds&  bvSphereIn);

    BOOL             CylinderTriIntersect    (RCylinder&      cylCylinderIn,
                                              RTriangle&      vecTriangleIn,
                                              FLOAT          fRisePoint);

    BOOL             CylinderCylinderIntersect (RCylinder&      cylCylinder1In,
                                                RCylinder&      cylCylinder2In);

    BOOL             BoxTriIntersect         (RBoundingBox&   bbBoxIn,
                                              RTriangle&      vecTriangleIn);

    BOOL             SphereTriIntersect      (RBounds&    bvSphereIn,
                                              RTriangle&        vecTriangleIn);


    BOOL             CollisionCheck          (PRBounds  pbvVolumeIn);

    BOOL             TriCollisionCheck       (RTriangle&      triTriangleIn);

    RStatus          GetAxisBB               (RVec3&  vecHighCornerOut,
                                              RVec3&  vecLowCornerOut);

    VOID             UpdatePosition          (VOID);

    VOID             CylinderToBox           (RBoundingBox&  bbBox);

    BOOL             PlaneVolumeIntersect    (RPlane&  v4PlaneIn);

    RVec3&          GetHighLimit            (VOID)                                    {return vecHighLimit;};

    RVec3&          GetLowLimit             (VOID)                                    {return vecLowLimit;};

    //VOID             DrawVolume2D            (PRCanvas  pcnvCanvasIn,
    //                                          UINT    ulColorIn,
    //                                          FLOAT  fScreenCornerX,
    //                                          FLOAT  fScreenCornerY);
    /*
    RStatus          ToBlock                 (PBYTE *  ppbyBlockOut,
                                              PUINT    pulBlockSizeOut);

    RStatus          FromBlock               (PBYTE    pbyBlockIn,
                                              UINT     ulBlockSizeIn,
                                              PUINT    pulBytesRead);

    VOID             ReleaseBlock            (PBYTE *  ppbyBlockIn);
    */
    VOID             Copy                    (PRBounds  pbvOriginalIn);


    PRBoundingBox   GetAxisBoxData          (VOID)                                    {return &bbAxisBox;};

    PRBoundingBox   GetObjectBoxData        (VOID)                                    {return &bbObjectBox;};

    PRCylinder      GetCylinderData         (VOID)                                    {return &cylCylinder;};

    PRPoint         GetPointData            (VOID)                                    {return &pntPoint;};

    PRRay           GetRayData              (VOID)                                    {return &rayRay;};

    
    
  };





// portal clipper
//------------------------------------------------------------------------
typedef struct
  {
  UINT            ulIndex;
  UINT            ulFlags;
//------------------------------------------------------------------------
  } BMIndexFlags;
//------------------------------------------------------------------------
typedef BMIndexFlags *  PBMIndexFlags;


//------------------------------------------------------------------------
typedef struct _RPortalInfo
  {

  RVec3           v2High;
  RVec3           v2Low;

  _RPortalInfo *  ppiNext;
//------------------------------------------------------------------------
  } RPortalInfo;
//------------------------------------------------------------------------
typedef RPortalInfo *  PRPortalInfo;



typedef VOID (*SHClipCallback) (RVec4&  v4Point1,
                                RVec4&  v4Point2,
                                RVec4&  v4Intersection);

// portal clipper
VOID BM_NearClip     (RVec4&  v4Point1, RVec4&  v4Point2, RVec4&  v4Intersection);
VOID BM_FarClip      (RVec4&  v4Point1, RVec4&  v4Point2, RVec4&  v4Intersection);
VOID BM_TopClip      (RVec4&  v4Point1, RVec4&  v4Point2, RVec4&  v4Intersection);
VOID BM_BottomClip   (RVec4&  v4Point1, RVec4&  v4Point2, RVec4&  v4Intersection);
VOID BM_LeftClip     (RVec4&  v4Point1, RVec4&  v4Point2, RVec4&  v4Intersection);
VOID BM_RightClip    (RVec4&  v4Point1, RVec4&  v4Point2, RVec4&  v4Intersection);

inline VOID    SetPlane      (RVec4&  v4Plane,
                              RVec3&  vecNormal,
                              RVec3&  vecPoint)   {v4Plane = vecNormal; v4Plane.fW = - (vecPoint * vecNormal);};


inline VOID    SetLine       (RVec3& ln, 
                              RVec3& v2Norm, 
                              RVec3& v2Point)    {ln.fX = v2Norm.fY; ln.fY = v2Norm.fX; ln.fW = - (v2Point * ln);};


                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                 
#endif // RBOUNDS_HPP

