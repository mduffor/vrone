/* -----------------------------------------------------------------
                             Bounding Sphere

     This class implements bounding spheres used for collision 
     detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef BSPHERE_HPP
#define BSPHERE_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "CPoint.hpp"
#include "Ray.hpp"
#include "CPlane.hpp"
#include "CTriangle.hpp"
#include "BVolume.hpp"

//------------------------------------------------------------------------
class BSphere : public BVolume
  {
  public:
    RVec3           vecCenter;
    FLOAT           fRadius;
    FLOAT           fRadiusSqr;
    BOOL            bInitialized;

    RVec3           vecWorldCenter;
  public:
             BSphere         ()                      {Clear ();};
    virtual  ~BSphere        ()                      {};
    VOID     Clear           (VOID)                  {vecCenter.Zero ();
                                                      bInitialized = FALSE;
                                                      fRadius = fRadiusSqr = 1.0f;
                                                      vecWorldCenter.Zero ();};
                                 
    VOID     FirstPoint      (RVec3&       vecPointIn);

    VOID     AddPoint        (RVec3&       vecPointIn);

    VOID     AddSphere       (RVec3 &      vecCenterIn,
                              FLOAT        fRadiusIn);


    VOID     ToSphere        (RVec3 &      vecCenterOut,
                              FLOAT &      fRadiusOut,
                              FLOAT &      fRadiusSquaredOut)   {vecCenterOut = vecCenter; fRadiusOut = fRadius; fRadiusSquaredOut = fRadiusSqr;};
                              
    BOOL     PointIntersect  (RVec3&       vecPointIn);
    
    BOOL     SphereIntersect (RVec3 &      vecSphereWorldPosIn,
                              FLOAT        fSphereRadiusIn);

    BOOL     RayIntersect    (Ray &        rayIn,
                              CPoint *     ppntCollisionOut);
                            
    BOOL     PlaneIntersect  (CPlane&      planeIn);

    BOOL     TriIntersect    (CTriangle &  triangleIn);
    
  };
typedef BSphere *  PBSphere;


#endif // BSPHERE_HPP