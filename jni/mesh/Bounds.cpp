/* -----------------------------------------------------------------
                           Bounds

     This module implements 3D collision detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 


// ***********************************************************************
//  Compile Directives
// ***********************************************************************

// #pragma pack (1);

// ***********************************************************************
//  Header Files
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include "Types.hpp"

#include "RDebug.hpp"
ASSERTFILE (__FILE__)

//#include "memman.hpp"
#include "RBounds.hpp"
//#include "canvas.hpp"


  
  
  
  
  
//------------------------------------------------------------------------
//                             BOUNDS MAKER
//------------------------------------------------------------------------


//------------------------------------------------------------------------
RBounds::RBounds  ()
  {
  Clear ();
  };


//------------------------------------------------------------------------
RBounds::~RBounds  ()
  {
  };


//------------------------------------------------------------------------
VOID RBounds::Clear  (VOID)
  {
  uFlags     = 0;
  tType       = kUnknown;
  pbvChildren = NULL;
  pvParent    = NULL;


  vecSphereCenter.Zero ();
  vecWorldSphereCenter.Zero ();
  fSphereRadius    = 0.0f;
  fSphereRadiusSqr = 0.0f;

  vecHighLimit.Zero ();
  vecLowLimit.Zero ();

  m4Matrix.Identity ();
  m4CompositeMatrix.Identity ();
  InvalidateMatrix ();

  // clear secondary volumes
  bbAxisBox.Clear ();   
  bbObjectBox.Clear ();
  cylCylinder.Clear ();
  pntPoint.Clear ();
  rayRay.Clear ();
  };


//------------------------------------------------------------------------
VOID RBounds::SetType  (EType            tTypeIn)
  {
  // store the type
  tType = tTypeIn;

  // perform any type specific default initialization.

  switch (tType)
    {
    case kCylinder:
        fPreTransformTris = TRUE;
        break;


    case kUnknown:
    case kAxisBox:
    case kObjBox:
    case kPoint:
    case kRay:
    case kSphere:
        break;

    };
  };



//------------------------------------------------------------------------
VOID RBounds::FirstPoint  (RVec3&  vecPointIn,
                           UINT    uFlagsIn)
  {
  if (uFlagsIn & BM_BOX)
    {
    vecHighLimit = vecPointIn;
    vecLowLimit  = vecPointIn;
    };
  if (uFlagsIn & BM_SPHERE)
    {
    vecSphereCenter = vecPointIn;
    };
  fInitialized = TRUE;
  };


//------------------------------------------------------------------------
VOID RBounds::FirstPointBB2D  (RVec3&  v2PointIn)
  {


  vecHighLimit.fX = vecLowLimit.fX = v2PointIn.fX;
  vecHighLimit.fY = vecLowLimit.fY = v2PointIn.fY;
  vecHighLimit.fZ = vecLowLimit.fZ = 0.0f;
  fInitialized = TRUE;
  };


//------------------------------------------------------------------------
VOID RBounds::AddPoint  (FLOAT  fXIn,
                                 FLOAT  fYIn,
                                 FLOAT  fZIn,
                                 UINT   uFlagsIn)
  {
  RVec3             vecPointOut;


  vecPointOut.Set (fXIn, fYIn, fZIn);
  AddPoint (vecPointOut, uFlagsIn);
  };


//------------------------------------------------------------------------
VOID RBounds::AddPoint  (RVec3&          vecPointIn,
                                 UINT            uFlagsIn)
  {
  FLOAT            fCurrDistance;
  FLOAT            fScaledRadius;
  RVec3            vecDirection;
  RVec3            vecOppositePoint;


  if (!fInitialized)
    {
    FirstPoint (vecPointIn, uFlagsIn);
    }
  else
    {
    if (uFlagsIn & BM_BOX)
      {
      // calculate the bounding box volume

      if (vecPointIn.fX < vecLowLimit.fX)  {vecLowLimit.fX = vecPointIn.fX;};
      if (vecPointIn.fY < vecLowLimit.fY)  {vecLowLimit.fY = vecPointIn.fY;};
      if (vecPointIn.fZ < vecLowLimit.fZ)  {vecLowLimit.fZ = vecPointIn.fZ;};

      if (vecPointIn.fX > vecHighLimit.fX)  {vecHighLimit.fX = vecPointIn.fX;};
      if (vecPointIn.fY > vecHighLimit.fY)  {vecHighLimit.fY = vecPointIn.fY;};
      if (vecPointIn.fZ > vecHighLimit.fZ)  {vecHighLimit.fZ = vecPointIn.fZ;};
      };


    if (uFlagsIn & BM_SPHERE)
      {
      // see if this point is in the bounding sphere, and if not recalculate sphere
      fCurrDistance = DistanceSquared (vecPointIn, vecSphereCenter);

      if (fCurrDistance > fSphereRadiusSqr)
        {
        // calculate a new radius and center

        vecDirection = vecSphereCenter - vecPointIn;

        fScaledRadius = (FLOAT) sqrt (fSphereRadiusSqr / vecDirection.LengthSquared ());

        vecOppositePoint = (vecDirection * fScaledRadius) + vecSphereCenter;

        vecSphereCenter = Midpoint (vecOppositePoint, vecPointIn);

        fSphereRadiusSqr = DistanceSquared (vecSphereCenter, vecPointIn);
        fSphereRadius    = sqrtf  (fSphereRadiusSqr);
        };
      };
    };
  };


//------------------------------------------------------------------------
VOID RBounds::AddPointBB2D  (RVec3&  v2PointIn)
  {


  if (!fInitialized)
    {
    FirstPointBB2D (v2PointIn);
    }
  else
    {
    // calculate the bounding box volume

    if (v2PointIn.fX < vecLowLimit.fX)  {vecLowLimit.fX = v2PointIn.fX;};
    if (v2PointIn.fY < vecLowLimit.fY)  {vecLowLimit.fY = v2PointIn.fY;};

    if (v2PointIn.fX > vecHighLimit.fX)  {vecHighLimit.fX = v2PointIn.fX;};
    if (v2PointIn.fY > vecHighLimit.fY)  {vecHighLimit.fY = v2PointIn.fY;};
    };
  };


//------------------------------------------------------------------------
VOID RBounds::AddSphere  (RVec3&         vecCenterIn,
                          FLOAT          fRadiusIn)
  {
  RVec3            vecPoint1;
  RVec3            vecPoint2;
  RVec3            vecRadiusVec;
  FLOAT            fOrigRadius;
  FLOAT            fLength;


  // calc vector from current sphere to new sphere
  vecRadiusVec = vecCenterIn - vecSphereCenter;

  fLength = vecRadiusVec.Length ();

  if (fabs (fLength) < R_EPSILON)
    {
    // the centers are the same.
    fSphereRadius    = RMax (fSphereRadius, fRadiusIn);
    fSphereRadiusSqr = fSphereRadius * fSphereRadius;
    return;
    };

  // normalize the vector
  vecRadiusVec /= fLength;


  fOrigRadius = sqrtf (fSphereRadiusSqr);

  vecPoint1 = (vecRadiusVec * fRadiusIn) + vecCenterIn;

  vecPoint2 = (vecRadiusVec * -fOrigRadius) + vecSphereCenter;

  vecSphereCenter = Midpoint (vecPoint1, vecPoint2);

  fSphereRadiusSqr =  DistanceSquared (vecSphereCenter, vecPoint2);
  fSphereRadius    =  sqrtf (fSphereRadiusSqr);
  };


//------------------------------------------------------------------------
VOID RBounds::AddSphereToBBox  (RVec3&  vecCenterIn,
                                FLOAT   fRadiusIn)
  {
  RVec3             vecPointOut;


  vecPointOut.Set (vecCenterIn.fX + fRadiusIn, vecCenterIn.fY, vecCenterIn.fZ);
  AddPoint (vecPointOut, BM_BOX);

  vecPointOut.Set (vecCenterIn.fX - fRadiusIn, vecCenterIn.fY, vecCenterIn.fZ);
  AddPoint (vecPointOut, BM_BOX);

  vecPointOut.Set (vecCenterIn.fX, vecCenterIn.fY + fRadiusIn, vecCenterIn.fZ);
  AddPoint (vecPointOut, BM_BOX);

  vecPointOut.Set (vecCenterIn.fX, vecCenterIn.fY - fRadiusIn, vecCenterIn.fZ);
  AddPoint (vecPointOut, BM_BOX);

  vecPointOut.Set (vecCenterIn.fX, vecCenterIn.fY, vecCenterIn.fZ + fRadiusIn);
  AddPoint (vecPointOut, BM_BOX);

  vecPointOut.Set (vecCenterIn.fX, vecCenterIn.fY, vecCenterIn.fZ - fRadiusIn);
  AddPoint (vecPointOut, BM_BOX);
  };


//------------------------------------------------------------------------
VOID RBounds::AddBoundingBox  (RBoundingBox&   bbBoxIn )
  {
  RVec3             vecCurr;


  // The center needs to be transformed before being passed, and
  //  vectors need to be transformed according to the parent that
  //  the bounding volume is part of.

  // Note:  You could probably speed this routine by pre-multiplying
  //          and re-using some of the terms.

  vecCurr = bbBoxIn.vecWorldCenter + bbBoxIn.vecHalfX + bbBoxIn.vecHalfY + bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = bbBoxIn.vecWorldCenter + bbBoxIn.vecHalfX + bbBoxIn.vecHalfY - bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = bbBoxIn.vecWorldCenter + bbBoxIn.vecHalfX - bbBoxIn.vecHalfY + bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = bbBoxIn.vecWorldCenter + bbBoxIn.vecHalfX - bbBoxIn.vecHalfY - bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = bbBoxIn.vecWorldCenter - bbBoxIn.vecHalfX + bbBoxIn.vecHalfY + bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = bbBoxIn.vecWorldCenter - bbBoxIn.vecHalfX + bbBoxIn.vecHalfY - bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = bbBoxIn.vecWorldCenter - bbBoxIn.vecHalfX - bbBoxIn.vecHalfY + bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = bbBoxIn.vecWorldCenter - bbBoxIn.vecHalfX - bbBoxIn.vecHalfY - bbBoxIn.vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);
  };


//------------------------------------------------------------------------
VOID RBounds::AddCylinder  (RCylinder&      cylCylinderIn)
  {
  RVec3             vecCurr;
  RVec3             vecHalfX;
  RVec3             vecHalfY;
  RVec3             vecHalfZ;


  // The center needs to be transformed before being passed, and
  //  vectors need to be transformed according to the parent that
  //  the bounding volume is part of.

  // Note:  You could probably speed this routine by pre-multiplying
  //          and re-using some of the terms.

  vecHalfX = cylCylinderIn.vecAxisXNorm * cylCylinderIn.fRadius;
  vecHalfY = cylCylinderIn.vecAxisYNorm * cylCylinderIn.fRadius;
  vecHalfZ = cylCylinderIn.vecAxisNorm  * cylCylinderIn.fHalfHeight;


  vecCurr = cylCylinderIn.vecWorldCenter + vecHalfX + vecHalfY + vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = cylCylinderIn.vecWorldCenter + vecHalfX + vecHalfY - vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = cylCylinderIn.vecWorldCenter + vecHalfX - vecHalfY + vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = cylCylinderIn.vecWorldCenter + vecHalfX - vecHalfY - vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = cylCylinderIn.vecWorldCenter - vecHalfX + vecHalfY + vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = cylCylinderIn.vecWorldCenter - vecHalfX + vecHalfY - vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = cylCylinderIn.vecWorldCenter - vecHalfX - vecHalfY + vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);

  vecCurr = cylCylinderIn.vecWorldCenter - vecHalfX - vecHalfY - vecHalfZ;
  AddPoint  (vecCurr, BM_BOX);
  };


//------------------------------------------------------------------------
VOID RBounds::AddRBounds  (RBounds&  bvDataIn)
  {
  RVec3             vecPointOut;


  // add the bounding sphere
  AddSphere (bvDataIn.vecWorldSphereCenter, bvDataIn.fSphereRadius);

  // now add the secondary volume based on what type it is.

  switch (bvDataIn.tType)
    {
    case kAxisBox:
         {
         // add highest and lowest corner
         vecPointOut = bvDataIn.bbAxisBox.vecWorldCenter + bvDataIn.bbAxisBox.vecHalfLengths;
         AddPoint (vecPointOut, BM_BOX);

         vecPointOut = bvDataIn.bbAxisBox.vecWorldCenter - bvDataIn.bbAxisBox.vecHalfLengths;
         AddPoint (vecPointOut, BM_BOX);
         }
         break;

    case kObjBox:
         {
         // add each of the eight corners to the volume.
        
         this->AddBoundingBox (bvDataIn.bbObjectBox);
         };
         break;

    case kCylinder:
         {
         // just treat the cylinder like a OBB and add eight corners.

         this->AddCylinder (bvDataIn.cylCylinder);
         };
         break;

    case kPoint:
         {
         AddPoint (bvDataIn.pntPoint.vecWorldPoint, BM_BOX);
         };
         break;

    case kRay:
         {
         AddPoint (bvDataIn.rayRay.vecWorldStart, BM_BOX);
         AddPoint (bvDataIn.rayRay.vecWorldEnd, BM_BOX);
         };
         break;

    case kSphere:
         {
         // although the bounding sphere was added to the root bounding sphere
         //  above, since this secondary volume is a sphere we are also going
         //  to add it to the bounding box.

         AddSphereToBBox (bvDataIn.vecWorldSphereCenter, bvDataIn.fSphereRadius);
         };
         break;
         
    case kUnknown:
         break;
    };
  };



////........................................................................
//VOID RBounds::AddBoundingData
////........................................................................
//  (
//  RBounds&  bvDataIn
//  )
//{
//
//
//// this routine assumes that the bounding box data is aligned to the
////  RBounds bounding volume.
//
//AddPoint (bvDataIn.vecHighLimit, BM_BOX);
//AddPoint (bvDataIn.vecLowLimit,  BM_BOX);
//
//AddSphere (bvDataIn.vecSphereCenter, bvDataIn.fSphereRadius);
//};


//------------------------------------------------------------------------
VOID RBounds::QueryBoundingBox  (RVec3&  vecHighOut,
                                         RVec3&  vecLowOut)
  {
  vecHighOut = vecHighLimit;
  vecLowOut  = vecLowLimit;
  };


//------------------------------------------------------------------------
VOID RBounds::QueryBoundingSphere  (RVec3&  vecCenterOut,
                                            FLOAT&  fRadiusOut)
  {


  vecCenterOut = vecSphereCenter;
  fRadiusOut = sqrtf (fSphereRadiusSqr);
  };


//------------------------------------------------------------------------
VOID RBounds::QueryBoundingBox2D  (RVec3&  v2HighOut,
                                           RVec3&  v2LowOut)
  {


  v2HighOut.Set (vecHighLimit.fX, vecHighLimit.fY, 0);
  v2LowOut.Set  (vecLowLimit.fX,  vecLowLimit.fY,  0);
  };


////........................................................................
//VOID RBounds::SetBoundingData
////........................................................................
//  (
//  RBounds&  bvDataIn
//  )
//{
//
//
//bvDataIn.vecBBHigh        = vecHighLimit;
//bvDataIn.vecBBLow         = vecLowLimit;
//bvDataIn.vecBSphereCenter = vecCenter;
//
//bvDataIn.fBSphereRadius    = sqrtf (fSphereRadiusSqr);
//bvDataIn.fBSphereRadiusSqr = fSphereRadiusSqr;
//};


////........................................................................
//VOID RBounds::ClearBoundingData
////........................................................................
//  (
//  BMBoundingData&  bvDataIn
//  )
//{
//
//
//bvDataIn.vecBBHigh.Zero ();
//bvDataIn.vecBBLow.Zero ();
//bvDataIn.vecBSphereCenter.Zero ();
//
//bvDataIn.fBSphereRadius    = 0.0f;
//bvDataIn.fBSphereRadiusSqr = 0.0f;
//};


////........................................................................
//VOID RBounds::CopyBoundingData
////........................................................................
//  (
//  BMBoundingData&  bdDataOut,
//  BMBoundingData&  bvDataIn
//  )
//{
//
//bdDataOut.vecBBHigh        = bvDataIn.vecBBHigh;
//bdDataOut.vecBBLow         = bvDataIn.vecBBLow;
//bdDataOut.vecBSphereCenter = bvDataIn.vecBSphereCenter;
//
//bdDataOut.fBSphereRadius    = bvDataIn.fBSphereRadius;
//bdDataOut.fBSphereRadiusSqr = bvDataIn.fBSphereRadiusSqr;
//};



//------------------------------------------------------------------------
VOID RBounds::CalcSphere  (VOID)
  {
  RVec3             vecSize;

  // the position of the sphere should already be set from when
  //  the points were added.  Just set the type.

  SetType (kSphere);

  // get the center of the AxisBB

  bbObjectBox.vecCenter = Midpoint (vecHighLimit, vecLowLimit);
  vecSize = (vecHighLimit - vecLowLimit) * 0.5f;

  bbObjectBox.vecHalfLengths    = vecSize;
  bbObjectBox.vecSqrHalfLengths = vecSize.Squared ();
  };


//------------------------------------------------------------------------
VOID RBounds::CalcOBB  (VOID)
  {
  SetType (kObjBox);
  bbObjectBox.CalcOBB (vecLowLimit, vecHighLimit);
  };


//------------------------------------------------------------------------
VOID RBounds::CalcAABB  (VOID)
  {
  SetType (kAxisBox);
  bbObjectBox.CalcAABB (vecLowLimit, vecHighLimit);
  };


//------------------------------------------------------------------------
VOID RBounds::PositionAABB  (VOID)
  {

  if (tType == kAxisBox)
    {
    bbAxisBox.PositionAABB (m4CompositeMatrix);
    }
  else
    {
    ASSERT (0);
    };
  };


//------------------------------------------------------------------------
VOID RBounds::PositionOBB  (VOID)
  {

  if (tType == kObjBox)
    {
    bbObjectBox.PositionOBB (m4CompositeMatrix);
    }
  else
    {
    ASSERT (0);
    };
  };


//------------------------------------------------------------------------
VOID RBounds::PositionCylinder  (VOID)
  {
  RVec3             vecCylinderOrigin;
  RVec3             vecScaledAxis;


  if (tType == kCylinder)
    {
    cylCylinder.vecWorldCenter = m4CompositeMatrix.MultTrans (cylCylinder.vecCenter);

     m4CompositeMatrix.GetInvRotateX (cylCylinder.vecAxisXNorm);
     m4CompositeMatrix.GetInvRotateY (cylCylinder.vecAxisYNorm);
     m4CompositeMatrix.GetInvRotateZ (cylCylinder.vecAxisNorm);


    // find the base of the cylinder that will be placed at the origin.
    vecScaledAxis     = cylCylinder.vecAxisNorm * cylCylinder.fHalfHeight;
    vecCylinderOrigin = cylCylinder.vecWorldCenter - vecScaledAxis;

    // calculate a matrix that will move vertices from world space to cylinder space.

    MatrixChangeCoordSystem (m4WorldToLocal,
                             cylCylinder.vecAxisXNorm,
                             cylCylinder.vecAxisYNorm,
                             cylCylinder.vecAxisNorm,
                             vecCylinderOrigin);
    }
  else
    {
    ASSERT (0);
    };
  };


//------------------------------------------------------------------------
BOOL RBounds::RayBBIntersect  (RBounds &       bvRayIn,
                               RBoundingBox &  bbObjectIn,
                               PRPoint         ppntCollisionOut)
  {
  return (bbObjectIn.RayIntersect (bvRayIn.rayRay.vecWorldStart,
                                   bvRayIn.rayRay.vecWorldEnd,
                                   ppntCollisionOut));
  };





//------------------------------------------------------------------------
BOOL RBounds::PointInBB  (RVec3&          vecPointIn,
                          RBoundingBox&   bbObjectIn)
  {
  
  return (bbObjectIn.PointIntersect (vecPointIn));
  };


















//------------------------------------------------------------------------
BOOL RBounds::CollisionCheck  (PRBounds  pbvVolumeIn)
  {
  //RVec3             vecSphereCenter1;
  //RVec3             vecSphereCenter2;
  FLOAT            fDistance;
  PRBounds    pbvVolume1;
  PRBounds    pbvVolume2;
  RBoundingBox      bbBoxOut;


  // this routine performs a collision check between this object and
  //  the passed object, and returns a TRUE if they intersect and a
  //  FALSE otherwise.

  // first check the bounding spheres against each other.

  // translate both sphere centers to world space

  //vecSphereCenter1 = this->m4CompositeMatrix        * this->vecSphereCenter;
  //vecSphereCenter2 = pbvVolumeIn->m4CompositeMatrix * pbvVolumeIn->vecSphereCenter;


  // just for safety, make sure you never try to test a collision
  //  against yourself

  if (this == pbvVolumeIn) return (FALSE);

  fDistance = Distance (this->vecWorldSphereCenter, pbvVolumeIn->vecWorldSphereCenter);

  if (fDistance <= (this->fSphereRadius + pbvVolumeIn->fSphereRadius))
    {
    // the spheres intersect.  Now try the secondary volumes.

    // first sort the volumes according to their volume type.  This way
    //  you only have to check each combination once.
    if (this->tType <= pbvVolumeIn->tType)
      {
      pbvVolume1 = this;
      pbvVolume2 = pbvVolumeIn;
      }
    else
      {
      pbvVolume2 = this;
      pbvVolume1 = pbvVolumeIn;
      };

    switch (pbvVolume1->tType)
      {
      case kAxisBox:
          switch (pbvVolume2->tType)
            {
            case kAxisBox:
                  return (AABBIntersect (pbvVolume1->bbAxisBox, pbvVolume2->bbAxisBox));
            case kObjBox:
                  return (OBBIntersect (pbvVolume1->bbAxisBox, pbvVolume2->bbObjectBox));
            case kCylinder:
                  // not supported directly
                  pbvVolume2->CylinderToBox (bbBoxOut);
                  return (OBBIntersect (pbvVolume1->bbAxisBox, bbBoxOut));
            case kPoint:
                  return (PointInBB (pbvVolume2->pntPoint.vecWorldPoint, pbvVolume1->bbAxisBox));
            case kRay:
                  return (RayBBIntersect (*pbvVolume2, pbvVolume1->bbAxisBox));
            case kSphere:
                  return (SphereAABBIntersect (*pbvVolume2, pbvVolume1->bbAxisBox));
            default:
                  break;
            };
      case kObjBox:
          switch (pbvVolume2->tType)
            {
            case kObjBox:
                  return (OBBIntersect (pbvVolume1->bbObjectBox, pbvVolume2->bbObjectBox));
            case kCylinder:
                  // not supported directly
                  pbvVolume2->CylinderToBox (bbBoxOut);
                  return (OBBIntersect (pbvVolume1->bbObjectBox, bbBoxOut));
            case kPoint:
                  return (PointInBB (pbvVolume2->pntPoint.vecWorldPoint, pbvVolume1->bbObjectBox));
            case kRay:
                  return (RayBBIntersect (*pbvVolume2, pbvVolume1->bbObjectBox));
            case kSphere:
                  return (SphereAABBIntersect (*pbvVolume2, pbvVolume1->bbObjectBox));
            default:
                  break;
            };
      case kCylinder:
          switch (pbvVolume2->tType)
            {
            case kCylinder:
                  return (CylinderCylinderIntersect(pbvVolume1->cylCylinder, pbvVolume2->cylCylinder));
            case kPoint:
                  // not supported
                  ASSERT (0);
                  return (FALSE);
            case kRay:
                  return (CylinderRayIntersect (pbvVolume1->cylCylinder, *pbvVolume2));
            case kSphere:
                  return (CylinderSphereIntersect (pbvVolume1->cylCylinder, *pbvVolume2));
            default:
                  break;
            };
      case kPoint:
          switch (pbvVolume2->tType)
            {
            case kPoint:
                  if (pbvVolume1->pntPoint.vecWorldPoint == pbvVolume2->pntPoint.vecWorldPoint) return TRUE; else return FALSE;
            case kRay:
                  // not supported
                  ASSERT (0);
                  return (FALSE);
                  break;
            case kSphere:
                  return (PointInSphere (pbvVolume1->pntPoint.vecWorldPoint, *pbvVolume2));
            default:
                  break;
            };
      case kRay:
          switch (pbvVolume2->tType)
            {
            case kRay:
                  // not supported
                  ASSERT (0);
                  return (FALSE);
                  break;
            case kSphere:
                  return (RaySphereIntersect (*pbvVolume1, *pbvVolume2));
            default:
                  break;
            };
      case kSphere:
          switch (pbvVolume2->tType)
            {
            case kSphere:
                  // we've already established that the bounding
                  //  spheres intersect.
                  return (TRUE);
            default:
                  break;
            };
            
      default:
          break;
      };
    };

  return (FALSE);
  };


//------------------------------------------------------------------------
BOOL RBounds::TriCollisionCheck  (RTriangle&  triTriangleIn)
  {
  RPoint            pntCollisionOut;


  // this routine performs a collision check between this object and
  //  the passed triangle, and returns a TRUE if they intersect and a
  //  FALSE otherwise.

  switch (this->tType)
    {
    case kAxisBox:
        return (BoxTriIntersect (bbAxisBox, triTriangleIn));

    case kObjBox:
        return (BoxTriIntersect (bbAxisBox, triTriangleIn));

    case kCylinder:
        return (CylinderTriIntersect (cylCylinder, triTriangleIn, 0.0f));

    case kPoint:
        // not supported
        ASSERT (0);
        break;

    case kRay:
        return (RayTriIntersect (*this, triTriangleIn, &pntCollisionOut));

    case kSphere:
        return (SphereTriIntersect (*this, triTriangleIn));
        break;
        
    default:
        break;
    };

  return (FALSE);
  };


//------------------------------------------------------------------------
RStatus RBounds::GetAxisBB  (RVec3&          vecHighCornerOut,
                                     RVec3&          vecLowCornerOut)
  {


  if (this->uFlags == kAxisBox)
    {
    // calc highest and lowest corners
    vecHighCornerOut = bbAxisBox.vecWorldCenter + bbAxisBox.vecHalfLengths;
    vecLowCornerOut  = bbAxisBox.vecWorldCenter - bbAxisBox.vecHalfLengths;
    return (RStatus::kSuccess);
    };

  // not a bounding box
  vecHighCornerOut.Zero ();
  vecLowCornerOut.Zero ();

  return (RStatus::kFailure);
  };


//------------------------------------------------------------------------
VOID RBounds::UpdatePosition  (VOID)
  {


  // update the bounding volume positions (center points and orientations)
  //  if the matrix has changed.

  if (IsInvalidMatrix ())
    {
    // first update the bounding sphere

    vecWorldSphereCenter = m4CompositeMatrix * vecSphereCenter;

    // then update the secondary volume

    switch (tType)
      {
      case kAxisBox:
          // with axis aligned boxes you don't have to change their vectors
          PositionAABB ();
          break;
      case kObjBox:
          PositionOBB ();
          break;
      case kCylinder:
          PositionCylinder ();
          break;
      case kPoint:
          pntPoint.vecWorldPoint = m4CompositeMatrix.MultTrans (pntPoint.vecPoint);
          break;
      case kRay:
          rayRay.vecWorldStart = m4CompositeMatrix.MultTrans (rayRay.vecStart);
          rayRay.vecWorldEnd   = m4CompositeMatrix.MultTrans (rayRay.vecEnd);
          break;
      case kSphere:
          // already updated
          break;
      default:
          break;
      };
    };

  // now that you ahve a new position, clear the invalid matrix flag
  ClearInvalidMatrix ();
  };


//------------------------------------------------------------------------
VOID RBounds::CylinderToBox  (RBoundingBox&   bbBox)
  {

  // build a bounding box around the cylinder so you can use bounding
  //  box collision detection for cylinders.  This is temporarily
  //  needed until all the cylinder collision detection routines can be
  //  written.

  bbBox.vecCenter      = cylCylinder.vecCenter;
  bbBox.vecNormX       = cylCylinder.vecAxisXNorm;
  bbBox.vecNormY       = cylCylinder.vecAxisYNorm;
  bbBox.vecNormZ       = cylCylinder.vecAxisNorm;
  bbBox.vecWorldCenter = cylCylinder.vecWorldCenter;

  bbBox.vecHalfLengths   .fX = cylCylinder.fRadius;
  bbBox.vecSqrHalfLengths.fX = cylCylinder.fRadiusSquared;
  bbBox.vecHalfLengths   .fY = cylCylinder.fRadius;
  bbBox.vecSqrHalfLengths.fY = cylCylinder.fRadiusSquared;
  bbBox.vecHalfLengths   .fZ = cylCylinder.fHalfHeight;
  bbBox.vecSqrHalfLengths.fZ = cylCylinder.fHalfHeightSquared;

  bbBox.vecHalfX = bbBox.vecNormX * bbBox.vecHalfLengths.fX;
  bbBox.vecHalfY = bbBox.vecNormY * bbBox.vecHalfLengths.fY;
  bbBox.vecHalfZ = bbBox.vecNormZ * bbBox.vecHalfLengths.fZ;
  };


//------------------------------------------------------------------------
BOOL RBounds::PlaneVolumeIntersect  (RPlane&        v4PlaneIn)
  {
  //RVec3             vecSphereCenter1;
  //RVec3             vecSphereCenter2;
  RVec3             vecDummy;


  // this routine performs a collision check between this object and
  //  the passed plane, and returns a TRUE if they intersect and a
  //  FALSE otherwise.

  // A check vs the bounding sphere is not made.

  // The passed plane is assumed to be given in world coordinates.


  switch (tType)
    {
    case kAxisBox:
        return (PlaneAABBIntersect (v4PlaneIn, bbAxisBox));

    case kObjBox:
        return (PlaneOBBIntersect (v4PlaneIn, bbAxisBox));

    case kCylinder:
        return (CylinderPlaneIntersect (cylCylinder, v4PlaneIn, NULL, vecDummy));

    case kPoint:
        // not sure if a point/plane intersect is useful
        ASSERT (0);
        return (FALSE);

    case kRay:
        // not implemented yet.
        ASSERT (0);
        return (FALSE);

    case kSphere:
        return (SpherePlaneIntersect (*this, v4PlaneIn));
        
    default:
        break;

    };

  return (FALSE);
  };

/*
//------------------------------------------------------------------------
VOID RBounds::DrawVolume2D  (PCanvas  pcnvCanvasIn,
                                     UINT     ulColorIn,
                                     FLOAT    fScreenCornerX,
                                     FLOAT    fScreenCornerY)
  {
  RVec3             vecCenterPix;
  RVec3             vecCorner1;
  RVec3             vecCorner2;
  RVec3             vecCorner3;
  RVec3             vecCorner4;
  RVec3             vecScreenCorner;
  RVec3             vecHalfLengths;



  // Draw the volume to the canvas for testing purposes so you can see
  //  the size and orientation of the volume.  Not for 3D use.

  vecScreenCorner.Set (fScreenCornerX, fScreenCornerY, 0.0f);

  pcnvCanvasIn->SetDrawColor32 (ulColorIn);

  switch (tType)
    {
    case AxisBox:

        vecCorner1 = bbAxisBox.vecWorldCenter + bbAxisBox.vecHalfLengths - vecScreenCorner;
        vecCorner2 = bbAxisBox.vecWorldCenter - bbAxisBox.vecHalfLengths - vecScreenCorner;

        pcnvCanvasIn->Line ((SHORT) vecCorner1.fX, (SHORT) vecCorner1.fY,
                            (SHORT) vecCorner2.fX, (SHORT) vecCorner1.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner2.fX, (SHORT) vecCorner1.fY,
                            (SHORT) vecCorner2.fX, (SHORT) vecCorner2.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner2.fX, (SHORT) vecCorner2.fY,
                            (SHORT) vecCorner1.fX, (SHORT) vecCorner2.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner1.fX, (SHORT) vecCorner2.fY,
                            (SHORT) vecCorner1.fX, (SHORT) vecCorner1.fY);

        vecCenterPix = bbAxisBox.vecWorldCenter - vecScreenCorner;
        pcnvCanvasIn->PutPixel ((SHORT) vecCenterPix.fX, (SHORT) vecCenterPix.fY);

        break;

    case ObjBox:

        vecCorner1 = bbObjectBox.vecWorldCenter + bbObjectBox.vecHalfX + bbObjectBox.vecHalfY + bbObjectBox.vecHalfZ - vecScreenCorner;
        vecCorner2 = bbObjectBox.vecWorldCenter + bbObjectBox.vecHalfX - bbObjectBox.vecHalfY + bbObjectBox.vecHalfZ - vecScreenCorner;
        vecCorner3 = bbObjectBox.vecWorldCenter - bbObjectBox.vecHalfX - bbObjectBox.vecHalfY - bbObjectBox.vecHalfZ - vecScreenCorner;
        vecCorner4 = bbObjectBox.vecWorldCenter - bbObjectBox.vecHalfX + bbObjectBox.vecHalfY - bbObjectBox.vecHalfZ - vecScreenCorner;

        pcnvCanvasIn->Line ((SHORT) vecCorner1.fX, (SHORT) vecCorner1.fY,
                            (SHORT) vecCorner2.fX, (SHORT) vecCorner2.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner2.fX, (SHORT) vecCorner2.fY,
                            (SHORT) vecCorner3.fX, (SHORT) vecCorner3.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner3.fX, (SHORT) vecCorner3.fY,
                            (SHORT) vecCorner4.fX, (SHORT) vecCorner4.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner4.fX, (SHORT) vecCorner4.fY,
                            (SHORT) vecCorner1.fX, (SHORT) vecCorner1.fY);


        vecCenterPix = bbObjectBox.vecWorldCenter - vecScreenCorner;
        pcnvCanvasIn->PutPixel ((SHORT) vecCenterPix.fX, (SHORT) vecCenterPix.fY);

        break;

    case Cylinder:
        break;

    case Point:
        break;

    case Ray:
        break;

    case Sphere:

        // draw an axis box instead of a sphere for now, until we have a chance
        //  to write a fast circle drawing routine.

        vecHalfLengths.Set (fSphereRadius, fSphereRadius, fSphereRadius);

        vecCorner1 = vecWorldSphereCenter + vecHalfLengths - vecScreenCorner;
        vecCorner2 = vecWorldSphereCenter - vecHalfLengths - vecScreenCorner;

        pcnvCanvasIn->Line ((SHORT) vecCorner1.fX, (SHORT) vecCorner1.fY,
                            (SHORT) vecCorner2.fX, (SHORT) vecCorner1.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner2.fX, (SHORT) vecCorner1.fY,
                            (SHORT) vecCorner2.fX, (SHORT) vecCorner2.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner2.fX, (SHORT) vecCorner2.fY,
                            (SHORT) vecCorner1.fX, (SHORT) vecCorner2.fY);

        pcnvCanvasIn->Line ((SHORT) vecCorner1.fX, (SHORT) vecCorner2.fY,
                            (SHORT) vecCorner1.fX, (SHORT) vecCorner1.fY);

        vecCenterPix = vecWorldSphereCenter - vecScreenCorner;
        pcnvCanvasIn->PutPixel ((SHORT) vecCenterPix.fX, (SHORT) vecCenterPix.fY);


        break;

    };

  };
*/
/*
//------------------------------------------------------------------------
RStatus RBounds::ToBlock  (PBYTE *  ppbyBlockOut,
                                   PUINT    pulBlockSizeOut)
  {
  MemBuffer          mb (&mmBoundsMemory);
  UINT              ulVersion;
  FLOAT            afMatrixOut [12];




  mb.AddElement (&uFlags,    sizeof (UINT));
  mb.AddElement (&tType,      sizeof (UINT));


  // Note:  In the future you need to figure out the best way to save lists and heirarchies
  // to be implemented
  //    PRBounds     pbvChildren;
  //    PVOID               pvParent;


  // save bounding sphere
  mb.AddElement (&vecSphereCenter,      sizeof (RVec3));
  mb.AddElement (&fSphereRadius,       sizeof (FLOAT));
  mb.AddElement (&fSphereRadiusSqr,    sizeof (FLOAT));
  mb.AddElement (&vecWorldSphereCenter, sizeof (RVec3));

  // save general bounding volume
  mb.AddElement (&vecHighLimit, sizeof (RVec3));
  mb.AddElement (&vecLowLimit,  sizeof (RVec3));

  // save root orientation matrix.  Parent will have to be rebuilt after loading.
  m4Matrix.ToArray3x4 (afMatrixOut);
  mb.AddElement (afMatrixOut, sizeof (FLOAT) * 12);

  // save secondary volume

  switch (tType)
    {
    case AxisBox:
        mb.AddElement (&bbAxisBox,   sizeof (RBoundingBox));
        break;

    case ObjBox:
        mb.AddElement (&bbObjectBox, sizeof (RBoundingBox));
        break;

    case Cylinder:
        mb.AddElement (&cylCylinder, sizeof (RCylinder));
        break;

    case Point:
        mb.AddElement (&pntPoint,    sizeof (RPoint));
        break;

    case Ray:
        mb.AddElement (&rayRay,      sizeof (RRay));
        break;

    case Sphere:
        break;
    };


  // make sure all the above AddElements suceeded
  if (mb.QueryLastErrorCode () != ERR_GEN_NONE)
    {
    return (RStatus::kFailure);
    };

  mb.QueryBuffer (ppbyBlockOut, pulBlockSizeOut);
  mb.DetachBuffer ();
  return (RStatus::kSuccess);
  };
*/
/*
//------------------------------------------------------------------------
RStatus  RBounds::FromBlock  (PBYTE  pbyBlockIn,
                                      UINT   ulBlockSizeIn,
                                      PUINT  pulBytesRead)
  {
  MemBuffer          mb (&mmBoundsMemory);
  UINT              ulVersion;
  FLOAT            afMatrixIn [12];


  mb.SetBuffer (pbyBlockIn, ulBlockSizeIn);

  mb.GetElement (&ulVersion, sizeof (UINT));
  if (ulVersion != BV_VERSION_1_0)
    {
    DebugError ("Unknown Bounding Volume version");
    return (ES_ERROR);
    };

  mb.GetElement (&uFlags,    sizeof (UINT));
  mb.GetElement (&tType,      sizeof (UINT));

  // Note:  In the future you need to figure out the best way to load lists and heirarchies
  // to be implemented
  //    PRBounds     pbvChildren;
  //    PVOID               pvParent;


  // load bounding sphere
  mb.GetElement (&vecSphereCenter,      sizeof (RVec3));
  mb.GetElement (&fSphereRadius,       sizeof (FLOAT));
  mb.GetElement (&fSphereRadiusSqr,    sizeof (FLOAT));
  mb.GetElement (&vecWorldSphereCenter, sizeof (RVec3));

  // load general bounding volume
  mb.GetElement (&vecHighLimit, sizeof (RVec3));
  mb.GetElement (&vecLowLimit,  sizeof (RVec3));

  // load root orientation matrix.  Parent will have to be rebuilt after loading.
  mb.GetElement (&afMatrixIn, sizeof (FLOAT) * 12);
  m4Matrix.Set3x4 (afMatrixIn);
  m4CompositeMatrix = m4Matrix;
  InvalidateMatrix ();

  // load secondary volume

  switch (tType)
    {
    case AxisBox:
        mb.GetElement (&bbAxisBox,   sizeof (RBoundingBox));
        break;

    case ObjBox:
        mb.GetElement (&bbObjectBox, sizeof (RBoundingBox));
        break;

    case Cylinder:
        mb.GetElement (&cylCylinder, sizeof (RCylinder));
        break;

    case Point:
        mb.GetElement (&pntPoint,    sizeof (RPoint));
        break;

    case Ray:
        mb.GetElement (&rayRay,      sizeof (RRay));
        break;

    case Sphere:
        break;
    };

  *pulBytesRead = mb.QueryBytesRead ();

  mb.DetachBuffer ();
  return (ES_NO_ERROR);
  };
*/
/*
//------------------------------------------------------------------------
VOID RBounds::ReleaseBlock  (PBYTE *          ppbyBlockIn)
  {

  mmBoundsMemory.Free ((PVOID *) ppbyBlockIn);
  };
*/

//------------------------------------------------------------------------
VOID RBounds::Copy  (PRBounds  pbvOriginalIn)
  {


  Clear ();

  uFlags = pbvOriginalIn->uFlags;
  tType   = pbvOriginalIn->tType;

  // copying of children not yet implemented.
  // to be implemented
  //    PRBounds     pbvChildren;

  // don't change parent
  //    PVOID               pvParent;


  fSphereRadius    = pbvOriginalIn->fSphereRadius;
  fSphereRadiusSqr = pbvOriginalIn->fSphereRadiusSqr;


  vecSphereCenter      = pbvOriginalIn->vecSphereCenter;
  vecWorldSphereCenter = pbvOriginalIn->vecWorldSphereCenter;
  vecHighLimit         = pbvOriginalIn->vecHighLimit;
  vecLowLimit          = pbvOriginalIn->vecLowLimit;
  m4Matrix            = pbvOriginalIn->m4Matrix;
  m4CompositeMatrix   = pbvOriginalIn->m4CompositeMatrix;
  InvalidateMatrix ();

  // copy Secondary Volume

  switch (tType)
    {
    case kAxisBox:
        bbAxisBox = pbvOriginalIn->bbAxisBox;
        break;

    case kObjBox:
        bbObjectBox = pbvOriginalIn->bbObjectBox;
        break;

    case kCylinder:
        cylCylinder = pbvOriginalIn->cylCylinder;
        break;

    case kPoint:
        pntPoint = pbvOriginalIn->pntPoint;
        break;

    case kRay:
        rayRay = pbvOriginalIn->rayRay;
        break;

    case kSphere:
        break;
        
    default:
        break;
    };

  };



// Note: The below BM_* functions are used in RPortalClipper.cpp


//------------------------------------------------------------------------
VOID BM_NearClip  (RVec4&          v4Point0,
                   RVec4&          v4Point1,
                   RVec4&          v4Intersection)
  {
  FLOAT            fT;

  // Plane: Z = -W

  // Since Z' = Z0 + t (Z1 - Z0)  and  W' = W0 + t (W1 - W0), for the Z=-W plane
  //  we substitute in the equations and solve for t, which gives us
  //
  //         (Z0 + W0)
  //   ---------------------
  //    (W0 - W1 + Z0 - Z1)

  fT = (v4Point0 .fZ + v4Point0 .fW) / (v4Point0 .fW - v4Point1 .fW + v4Point0 .fZ - v4Point1 .fZ);

  v4Intersection .fX = v4Point0 .fX + (fT * (v4Point1 .fX - v4Point0 .fX));
  v4Intersection .fY = v4Point0 .fY + (fT * (v4Point1 .fY - v4Point0 .fY));
  v4Intersection .fZ = v4Point0 .fZ + (fT * (v4Point1 .fZ - v4Point0 .fZ));
  v4Intersection .fW = v4Point0 .fW + (fT * (v4Point1 .fW - v4Point0 .fW));
  };


//------------------------------------------------------------------------
VOID BM_FarClip  (RVec4&          v4Point0,
                  RVec4&          v4Point1,
                  RVec4&          v4Intersection)
  {
  FLOAT            fT;


  // Plane: Z = W


  fT = (v4Point0 .fZ - v4Point0 .fW) / (-v4Point0 .fW + v4Point1 .fW + v4Point0 .fZ - v4Point1 .fZ);

  v4Intersection .fX = v4Point0 .fX + (fT * (v4Point1 .fX - v4Point0 .fX));
  v4Intersection .fY = v4Point0 .fY + (fT * (v4Point1 .fY - v4Point0 .fY));
  v4Intersection .fZ = v4Point0 .fZ + (fT * (v4Point1 .fZ - v4Point0 .fZ));
  v4Intersection .fW = v4Point0 .fW + (fT * (v4Point1 .fW - v4Point0 .fW));
  };


//------------------------------------------------------------------------
VOID BM_TopClip  (RVec4&          v4Point0,
                  RVec4&          v4Point1,
                  RVec4&          v4Intersection)
  {
  FLOAT            fT;


  // Plane: Y = W

  fT = (v4Point0 .fY - v4Point0 .fW) / (-v4Point0 .fW + v4Point1 .fW + v4Point0 .fY - v4Point1 .fY);

  v4Intersection .fX = v4Point0 .fX + (fT * (v4Point1 .fX - v4Point0 .fX));
  v4Intersection .fY = v4Point0 .fY + (fT * (v4Point1 .fY - v4Point0 .fY));
  v4Intersection .fZ = v4Point0 .fZ + (fT * (v4Point1 .fZ - v4Point0 .fZ));
  v4Intersection .fW = v4Point0 .fW + (fT * (v4Point1 .fW - v4Point0 .fW));
  };


//------------------------------------------------------------------------
VOID BM_BottomClip  (RVec4&          v4Point0,
                     RVec4&          v4Point1,
                     RVec4&          v4Intersection)
  {
  FLOAT            fT;


  // Plane: Y = -W

  fT = (v4Point0 .fY + v4Point0 .fW) / (v4Point0 .fW - v4Point1 .fW + v4Point0 .fY - v4Point1 .fY);

  v4Intersection .fX = v4Point0 .fX + (fT * (v4Point1 .fX - v4Point0 .fX));
  v4Intersection .fY = v4Point0 .fY + (fT * (v4Point1 .fY - v4Point0 .fY));
  v4Intersection .fZ = v4Point0 .fZ + (fT * (v4Point1 .fZ - v4Point0 .fZ));
  v4Intersection .fW = v4Point0 .fW + (fT * (v4Point1 .fW - v4Point0 .fW));
  };


//------------------------------------------------------------------------
VOID BM_RightClip  (RVec4&          v4Point0,
                    RVec4&          v4Point1,
                    RVec4&          v4Intersection)
  {
  FLOAT            fT;


  // Plane: X = W

  fT = (v4Point0 .fX - v4Point0 .fW) / (-v4Point0 .fW + v4Point1 .fW + v4Point0 .fX - v4Point1 .fX);

  v4Intersection .fX = v4Point0 .fX + (fT * (v4Point1 .fX - v4Point0 .fX));
  v4Intersection .fY = v4Point0 .fY + (fT * (v4Point1 .fY - v4Point0 .fY));
  v4Intersection .fZ = v4Point0 .fZ + (fT * (v4Point1 .fZ - v4Point0 .fZ));
  v4Intersection .fW = v4Point0 .fW + (fT * (v4Point1 .fW - v4Point0 .fW));
  };


//------------------------------------------------------------------------
VOID BM_LeftClip  (RVec4&          v4Point0,
                   RVec4&          v4Point1,
                   RVec4&          v4Intersection)
  {
  FLOAT            fT;


  // Plane: X = -W

  fT = (v4Point0 .fX + v4Point0 .fW) / (v4Point0 .fW - v4Point1 .fW + v4Point0 .fX - v4Point1 .fX);

  v4Intersection .fX = v4Point0 .fX + (fT * (v4Point1 .fX - v4Point0 .fX));
  v4Intersection .fY = v4Point0 .fY + (fT * (v4Point1 .fY - v4Point0 .fY));
  v4Intersection .fZ = v4Point0 .fZ + (fT * (v4Point1 .fZ - v4Point0 .fZ));
  v4Intersection .fW = v4Point0 .fW + (fT * (v4Point1 .fW - v4Point0 .fW));
  };

