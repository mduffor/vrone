/* -----------------------------------------------------------------
                            OBJ Codec

     This class handles loading of the Wavefront OBJ format

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

// Note: You can probably speed performance by moving pointers to the 
//        various face, vertex, and group/shader arrays into the base
//        class (RBaseGeomCodec).  This would eliminate a lot of the 
//        parameters that are passed down through the call stack, so 
//        less time would be spent pushing and popping pointers.


#include "Types.hpp"
#include "ObjCodec.hpp"
#include "EditMesh.hpp"
//#include "Functions.hpp"
#include "Material.hpp"


/// Define a fourCC code for a little endian system
#define OBJB_MAKE_FOURCC(a,b,c,d) ((__int32_t)((__int32_t)(a)|((__int32_t)(b)<<8)|((__int32_t)(c)<<16)|((__int32_t)(d)<<24)))
typedef __uint32_t   objbFourCC;

const objbFourCC  ccHeader       = OBJB_MAKE_FOURCC ('O','B','J','B');

const objbFourCC  ccVert3        = OBJB_MAKE_FOURCC ('v','3','_','_');
const objbFourCC  ccVert4        = OBJB_MAKE_FOURCC ('v','4','_','_');
const objbFourCC  ccCoordNormal  = OBJB_MAKE_FOURCC ('v','n','_','_');
const objbFourCC  ccVertTex1     = OBJB_MAKE_FOURCC ('v','t','1','_');
const objbFourCC  ccVertTex2     = OBJB_MAKE_FOURCC ('v','t','2','_');
const objbFourCC  ccVertTex3     = OBJB_MAKE_FOURCC ('v','t','3','_');
const objbFourCC  ccFace3        = OBJB_MAKE_FOURCC ('f','3','_','_');
const objbFourCC  ccFace4        = OBJB_MAKE_FOURCC ('f','4','_','_');
const objbFourCC  ccFace3Tex     = OBJB_MAKE_FOURCC ('f','3','t','_');
const objbFourCC  ccFace4Tex     = OBJB_MAKE_FOURCC ('f','4','t','_');
const objbFourCC  ccFace3TexNorm = OBJB_MAKE_FOURCC ('f','3','t','n');
const objbFourCC  ccFace4TexNorm = OBJB_MAKE_FOURCC ('f','4','t','n');
const objbFourCC  ccFace3Norm    = OBJB_MAKE_FOURCC ('f','3','n','_');
const objbFourCC  ccFace4Norm    = OBJB_MAKE_FOURCC ('f','4','n','_');

const objbFourCC  ccFace5        = OBJB_MAKE_FOURCC ('f','5','_','_');
const objbFourCC  ccFace5Tex     = OBJB_MAKE_FOURCC ('f','5','t','_');
const objbFourCC  ccFace5TexNorm = OBJB_MAKE_FOURCC ('f','5','t','n');
const objbFourCC  ccFace5Norm    = OBJB_MAKE_FOURCC ('f','5','n','_');

const objbFourCC  ccPoint        = OBJB_MAKE_FOURCC ('p','_','_','_');
const objbFourCC  ccLine         = OBJB_MAKE_FOURCC ('l','_','_','_');
const objbFourCC  ccLineTex      = OBJB_MAKE_FOURCC ('l','t','_','_');
const objbFourCC  ccCurve        = OBJB_MAKE_FOURCC ('c','u','r','v');
const objbFourCC  ccGroup        = OBJB_MAKE_FOURCC ('g','_','_','_');
const objbFourCC  ccObject       = OBJB_MAKE_FOURCC ('o','_','_','_');
const objbFourCC  ccMtlUse       = OBJB_MAKE_FOURCC ('u','m','t','l');
const objbFourCC  ccMtlLibrary   = OBJB_MAKE_FOURCC ('l','m','t','l');


//------------------------------------------------------------------------
//  ObjCodec
//------------------------------------------------------------------------



//------------------------------------------------------------------------
ObjCodec::ObjCodec ()
  {
  };


//------------------------------------------------------------------------
ObjCodec::~ObjCodec ()
  {
  };


//------------------------------------------------------------------------
VOID  ObjCodec::CalcNumElements (RStrParser &  parserIn,
                                 INT32 &       iCoordTextureCount,
                                 INT32 &       iCoordNormalCount,
                                 INT32 &       iCoordPointCount,
                                 INT32 &       iGroupCount,
                                 INT32 &       iFaceCount,
                                 INT32 &       iCurveCount,
                                 INT32 &       iVertCount)
  {
  // first run through the buffer and calculate the array sizes
  parserIn.ResetCursor ();
  while (!parserIn.IsEOF ())
    {
    if (parserIn.Compare ("vt ", 3, parserIn.GetCursorStart ()) == 0)
      {
      ++iCoordTextureCount;
      }
    else if (parserIn.Compare ("vn ", 3, parserIn.GetCursorStart ()) == 0)
      {
      ++iCoordNormalCount;
      }
    else if (parserIn.Compare ("v ", 2, parserIn.GetCursorStart ()) == 0)
      {
      ++iCoordPointCount;
      }
    else if (parserIn.Compare ("g ", 2, parserIn.GetCursorStart ()) == 0)
      {
      ++iGroupCount;
      }
    else if (parserIn.Compare ("s ", 2, parserIn.GetCursorStart ()) == 0)
      {
      }
    else if (parserIn.Compare ("mtllib ", 7, parserIn.GetCursorStart ()) == 0)
      {
      // multiple filenames possible.
      // to be implemented
      }
    else if (parserIn.Compare ("usemtl ", 7, parserIn.GetCursorStart ()) == 0)
      {
      // catch the case where there is a material, but no groups.  This will
      //  give incorrect material assignment for obj files that are not set up
      //  as expected, but should avoid crashing.
      //++iGroupCount;
      }
    else if (parserIn.Compare ("f ", 2, parserIn.GetCursorStart ()) == 0)
      {
      ++iFaceCount;
      // skip the "f" tag
      parserIn.GetWord (NULL, true);
      // count the number of words in the line.
      while (! parserIn.IsEOL ())
        {
        parserIn.GetWord (NULL, false);
        ++iVertCount;
        }
      }
    else if (parserIn.Compare ("l ", 2, parserIn.GetCursorStart ()) == 0)
      {
      ++iCurveCount;
      // skip the "l" tag
      parserIn.GetWord (NULL, true);
      // count the number of words in the line.
      while (! parserIn.IsEOL ())
        {
        parserIn.GetWord (NULL, false);
        ++iVertCount;
        }
      };
    parserIn.GotoNextLine ();
    };
    
  };

//------------------------------------------------------------------------
EStatus  ObjCodec::Read  (RStrParser &  parserIn,
                          EditMesh &    editMeshOut,
                          BOOL          bAppendIn,
                          BOOL          bBinaryIn)
  {
  printf ("ObjCodec::Read ()\n");
  INT32            iCoordTextureCount = 0;
  INT32            iCoordNormalCount  = 0;
  INT32            iCoordPointCount   = 0;
  INT32            iFaceCount         = 0;
  INT32            iCurveCount        = 0;
  INT32            iGroupCount        = 0;
  INT32            iVertCount         = 0;

  INT32            iCoordTextureOffset = 0;
  INT32            iCoordNormalOffset  = 0;
  INT32            iCoordPointOffset   = 0;
  INT32            iFaceOffset         = 0;
  INT32            iCurveOffset        = 0;
  INT32            iVertOffset         = 0;

  EStatus          status;


  // first run through the buffer and calculate the array sizes
  if (bBinaryIn)
    {
    CalcNumElementsBin (parserIn,
                        iCoordTextureCount,
                        iCoordNormalCount,
                        iCoordPointCount,
                        iGroupCount,                                     
                        iFaceCount,
                        iCurveCount,
                        iVertCount);
    }                
  else
    {
    CalcNumElements (parserIn,
                     iCoordTextureCount,
                     iCoordNormalCount,
                     iCoordPointCount,
                     iGroupCount,                                     
                     iFaceCount,
                     iCurveCount,
                     iVertCount);
    };

  printf ("Loading model.  Faces %d  Groups %d\n", (int) iFaceCount, (int) iGroupCount);   
    
  // pre-allocate the attribute arrays, or grow them if we are appending and they already exist.

  if (iCoordPointCount)
    {
    if (bAppendIn)
      {
      iCoordPointOffset = editMeshOut.avecCoordPoints.Length ();
      iCoordPointCount += iCoordPointOffset;
      };
    editMeshOut.avecCoordPoints.SetLength (iCoordPointCount);
    };
  if (iCoordTextureCount)
    {
    if (bAppendIn)
      {
      iCoordTextureOffset = editMeshOut.avecCoordTexture.Length ();
      iCoordTextureCount += iCoordTextureOffset;
      };
    editMeshOut.avecCoordTexture.SetLength (iCoordTextureCount);
    };
  if (iCoordNormalCount)
    {
    if (bAppendIn)
      {
      iCoordNormalOffset = editMeshOut.avecCoordNormal.Length ();
      iCoordNormalCount += iCoordNormalOffset;
      };
    editMeshOut.avecCoordNormal.SetLength (iCoordNormalCount);
    };
    
  if (iFaceCount)
    {
    if (bAppendIn)
      {
      iFaceOffset = editMeshOut.aiFaces.Length ();
      iFaceCount += iFaceOffset;
      };
    editMeshOut.aiFaces.SetLength (iFaceCount);
    editMeshOut.aiFaceVCount.SetLength (iFaceCount);
    };

  if (iCoordPointCount)
    {
    if (bAppendIn)
      {
      iVertOffset = editMeshOut.aiFacePoints.Length ();
      iVertCount += iVertOffset;
      };
    editMeshOut.aiFacePoints.SetLength (iVertCount);
    if (iCoordNormalCount)
      {
      editMeshOut.aiFaceNorms.SetLength (iVertCount);
      };
    if (iCoordTextureCount)
      {
      editMeshOut.aiFaceTex.SetLength (iVertCount);
      };
    };
    
       
  if (iCurveCount)
    {
    if (bAppendIn)
      {
      iCurveOffset = editMeshOut.aiCurves.Length ();
      iCurveCount += iCurveOffset;
      };
    editMeshOut.aiCurves.SetLength (iCurveCount);
    editMeshOut.aiCurveVCount.SetLength (iCurveCount);
    };

  if (iGroupCount)
    {
    editMeshOut.aiFaceGroups.SetLength (iFaceCount);
    editMeshOut.aiCurveGroups.SetLength (iCurveCount);
  
    // we don't know the number of unique shaders yet, so we can't preallocate the shaders array.
    editMeshOut.astrShaders.Length ();
    editMeshOut.astrGroups.Length ();
    };


  // setup the append offsets
  
  printf ("Loading Geometry:  VertCount %d  FaceCount %d  CurveCount %d  MatCount %d  VertCount  %d\n", 
          INT (iCoordPointCount), INT (iFaceCount), 
          INT (iCurveCount), INT (iGroupCount), INT (iVertCount));
    
  // now read in the values
  
  if (bBinaryIn)
    {
    ParseDataBin (parserIn,
                  iCoordTextureOffset,
                  iCoordNormalOffset,
                  iCoordPointOffset,
                  iFaceOffset,
                  iCurveOffset,
                  iVertOffset,
                  iGroupCount,
                  editMeshOut);
    }         
  else
    {
    ParseData (parserIn,
               iCoordTextureOffset,
               iCoordNormalOffset,
               iCoordPointOffset,
               iFaceOffset,
               iCurveOffset,
               iVertOffset,
               iGroupCount,
               editMeshOut);
    };
  
  return (EStatus::kSuccess);
  };


//------------------------------------------------------------------------
VOID  ObjCodec::ParseData (RStrParser &  parserIn,
                           INT32         iCoordTextureOffset,
                           INT32         iCoordNormalOffset,
                           INT32         iCoordPointOffset,
                           INT32         iFaceOffset,
                           INT32         iCurveOffset,
                           INT32         iFaceVertOffset,
                           INT32         iGroupCount,
                           EditMesh &    editMeshOut)
  {
  
  RStr             strShaderName;
  RStr             strGroupName;

  INT32            iCoordTextureIndex = 0;
  INT32            iCoordNormalIndex  = 0;
  INT32            iCoordPointIndex   = 0;
  INT32            iFaceIndex        = 0;
  INT32            iCurveIndex       = 0;
  //INT32            iMaterialIndex    = 0;
  INT32            iFaceVertIndex    = 0;
  INT32            iFaceShaderIndex  = 0;


  iCoordTextureIndex = iCoordTextureOffset;
  iCoordNormalIndex  = iCoordNormalOffset ;
  iCoordPointIndex   = iCoordPointOffset  ;
  iFaceIndex         = iFaceOffset        ;
  iCurveIndex        = iCurveOffset       ;
  iFaceVertIndex     = iFaceVertOffset    ;

  parserIn.ResetCursor ();

  while (!parserIn.IsEOF ())
    {
    if (parserIn.Compare ("vt ", 3, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fX = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fY = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fZ = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fW = parserIn.GetFloat (false);
      ++iCoordTextureIndex;
      }
    else if (parserIn.Compare ("vn ", 3, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordNormal [iCoordNormalIndex].fX = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordNormal [iCoordNormalIndex].fY = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordNormal [iCoordNormalIndex].fZ = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) {editMeshOut.avecCoordNormal [iCoordNormalIndex].fW = parserIn.GetFloat (false);} else {editMeshOut.avecCoordNormal [iCoordNormalIndex].fW = 0.0f;};
      ++iCoordNormalIndex;
      }
    else if (parserIn.Compare ("v ", 2, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fX = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fY = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fZ = parserIn.GetFloat (false);
      if (!parserIn.IsEOL ()) {editMeshOut.avecCoordPoints [iCoordPointIndex].fW = parserIn.GetFloat (false);} else {editMeshOut.avecCoordPoints [iCoordPointIndex].fW = 1.0f;};
      ++iCoordPointIndex;
      }
    else if (parserIn.Compare ("g ", 2, parserIn.GetCursorStart ()) == 0)
      {
      // to be implemented
      parserIn.GetWord (NULL, false);
      if (!parserIn.IsEOL ())   strGroupName = parserIn.GetWord (false);
      
      iFaceShaderIndex = editMeshOut.astrGroups.Length ();
      editMeshOut.astrGroups.Append (strGroupName);

      //printf ("Group found: %s\n", strGroupName.AsChar ());
      
      editMeshOut.aiGroupPoints.SetMinLength (iFaceShaderIndex + 1);
      editMeshOut.aiGroupPoints [iFaceShaderIndex] = 0;
      }
    else if (parserIn.Compare ("s ", 2, parserIn.GetCursorStart ()) == 0)
      {
      // smoothing groups
      // to be implemented
      }
    else if (parserIn.Compare ("usemtl ", 7, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, false);
      if (!parserIn.IsEOL ())   strShaderName = parserIn.GetWord (false);

      // first see if this material is in our list or not.
      /*
      iFaceShaderIndex = astrShaders->Find (strShaderName);
      
      if (iFaceShaderIndex == -1)
        {
        // not in list.  Add it.
        iFaceShaderIndex = astrShaders->Length ();
        astrShaders->Append (strShaderName);
        };
      */
      if (editMeshOut.astrShaders.Length () < editMeshOut.astrGroups.Length ())
        {
        while (editMeshOut.astrShaders.Length () < (editMeshOut.astrGroups.Length () - 1))
          {
          editMeshOut.astrShaders.Append ("");
          };
        editMeshOut.astrShaders.Append (strShaderName);
        };
      }
    else if (parserIn.Compare ("f ", 2, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, false);

      editMeshOut.aiFaces [iFaceIndex] = iFaceVertIndex;

      while (! parserIn.IsEOL ())
        {
        // geom/tex/norm

        // indicies in an obj file are 1-based, so subtract 1 in order to make it zero based.
        editMeshOut.aiFacePoints [iFaceVertIndex] = parserIn.GetInt (false) - 1 + iCoordPointOffset;
        
        if (iGroupCount > 0)
          {
          editMeshOut.aiGroupPoints [iFaceShaderIndex] = RMin (editMeshOut.aiGroupPoints [iFaceShaderIndex], 
                                                               editMeshOut.aiFacePoints [iFaceVertIndex]);
          };
        
        if (parserIn.PeekChar () == '/') 
          {
          parserIn.SkipChars (1);
          if (parserIn.PeekChar () != '/')
            {
            editMeshOut.aiFaceTex [iFaceVertIndex] = parserIn.GetInt (false) - 1 + iCoordTextureOffset;
            };
          };
        if (parserIn.PeekChar () == '/') 
          {
          parserIn.SkipChars (1);
        
          if (! parserIn.IsWhitespace (parserIn.GetCursorStart ()))
            {
            editMeshOut.aiFaceNorms [iFaceVertIndex] = parserIn.GetInt (false) - 1 + iCoordNormalOffset;
            };
          };
        ++iFaceVertIndex;
        };
        
      // save the number of verts in the polygon
      editMeshOut.aiFaceVCount [iFaceIndex] = iFaceVertIndex - editMeshOut.aiFaces [iFaceIndex];


      if (iGroupCount > 0)
        {
        editMeshOut.aiFaceGroups [iFaceIndex] = iFaceShaderIndex;
        };

      ++iFaceIndex;
      }
    else if (parserIn.Compare ("l ", 2, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, false);

      editMeshOut.aiCurves [iCurveIndex] = iFaceVertIndex;

      while (! parserIn.IsEOL ())
        {
        // geom/tex/norm

        // indicies in an obj file are 1-based, so subtract 1 in order to make it zero based.
        editMeshOut.aiFacePoints [iFaceVertIndex] = parserIn.GetInt (false) - 1 + iCoordPointOffset;
        
        if (iGroupCount > 0)
          {
          editMeshOut.aiGroupPoints [iFaceShaderIndex] = RMin (editMeshOut.aiGroupPoints[iFaceShaderIndex], 
                                                               editMeshOut.aiFacePoints [iFaceVertIndex]);
          };
        
        if (parserIn.PeekChar () == '/') 
          {
          parserIn.SkipChars (1);
          if (parserIn.PeekChar () != '/')
            {
            editMeshOut.aiFaceTex [iFaceVertIndex] = parserIn.GetInt (false) - 1 + iCoordTextureOffset;
            };
          };
        if (parserIn.PeekChar () == '/') 
          {
          parserIn.SkipChars (1);
        
          if (! parserIn.IsWhitespace (parserIn.GetCursorStart ()))
            {
            editMeshOut.aiFaceNorms [iFaceVertIndex] = parserIn.GetInt (false) - 1 + iCoordNormalOffset;
            };
          };
        ++iFaceVertIndex;
        };
        
      // save the number of verts in the line
      editMeshOut.aiCurveVCount [iCurveIndex] = iFaceVertIndex - editMeshOut.aiCurves [iCurveIndex];


      if (iGroupCount > 0)
        {
        editMeshOut.aiCurveGroups [iCurveIndex] = iFaceShaderIndex;
        };

      ++iCurveIndex;
      };
      
    parserIn.GotoNextLine ();
    };
  };  







//------------------------------------------------------------------------
VOID  ObjCodec::CalcNumElementsBin (RStrParser &  parserIn,
                                    INT32 &       iCoordTextureCount,
                                    INT32 &       iCoordNormalCount,
                                    INT32 &       iCoordPointCount,
                                    INT32 &       iGroupCount,
                                    INT32 &       iFaceCount,
                                    INT32 &       iCurveCount,
                                    INT32 &       iVertCount)
  {
  //     Hdr: 4cc OBJB
  //     Bdy: (any combo of following)
  //          4cc(v3__) 4U(NumVerts) array4F (x1 y1 z1 ... xn yn zn)
  //          4cc(v4__) 4U(NumVerts) array4F (x1 y1 z1 w1... xn yn zn wn)
  //          4cc(vn__) 4U(NumNorms) array4F (x1 y1 z1 ... xn yn zn)
  //          4cc(vt1_) 4U(NumVTex) array4F (u1 ... un)
  //          4cc(vt2_) 4U(NumVTex) array4F (u1 v1 ... un vn)
  //          4cc(vt3_) 4U(NumVTex) array4F (u1 v1 w1 ... un vn wn)
  //          4cc(f3__) 4U(NumFaces) array4U (v1 v2 v3 ... v1n v2n v3n)
  //          4cc(f4__) 4U(NumFaces) array4U (v1 v2 v3 v4 ... v1n v2n v3n v4n)
  //          4cc(f3t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 ... 
  //                                          v1n t1n v2n t2n v3n t3n)
  //          4cc(f4t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 v4 t4 ... 
  //                                          v1n t1n v2n t2n v3n t3n v4n t4n)
  //          4cc(f3tn) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 ... 
  //                                          v1n n1n v2n n2n v3n n3n)
  //          4cc(f4tn) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 v4 n4 ... 
  //                                          v1n n1n v2n n2n v3n n3n v4n n4n)
  //
  //          4cc(f3n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 ... 
  //                                          v1n n1n v2n n2n v3n n3n)
  //          4cc(f4n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 v4 n4 ... 
  //                                          v1n n1n v2n n2n v3n n3n v4n n4n)
  //          4cc(p___) 4U(NumPoints) (v1 v2 ... vn)
  //          4cc(l___) 4U(NumPoints) (v1 v2 ... vn)
  //          4cc(lt__) 4U(NumPoints) (v1 t1 v2 t2 ... vn tn)
  //          4cc(curv) 4F(u0) 4F(u1) 4U(NumCVs) (v1 v2 ... vn)
  //          4cc(g___) 4U(strzLen) STRZ(groupName1 groupNameN)
  //          4cc(o___) 4U(strzLen) STRZ(objectName)
  //
  //          4cc(umtl) 4U(strzLen) STRZ(materialName)
  //          4cc(lmtl) 4U(strzLen) STRZ(space separated filenames)
  //

  UINT  uCount = 0;

  // first run through the buffer and calculate the array sizes
  parserIn.ResetCursor ();

  UINT  uHeader = parserIn.GetU4_LEnd ();
  if (uHeader !=  ccHeader) return;
  
  while (!parserIn.IsEOF ())
    {
    UINT  uCode = parserIn.GetU4_LEnd ();
    
    if (uCode == ccVert3)
      {
      // 4cc(v3__) 4U(NumVerts) array4F (x1 y1 z1 ... xn yn zn)
      uCount = parserIn.GetU4_LEnd ();
      iCoordPointCount += uCount;
      parserIn.SkipChars (uCount * sizeof (FLOAT32) * 3);
      }
    else if (uCode == ccVert4)
      {
      // 4cc(v4__) 4U(NumVerts) array4F (x1 y1 z1 w1... xn yn zn wn)
      uCount = parserIn.GetU4_LEnd ();
      iCoordPointCount += INT (uCount);
      parserIn.SkipChars (uCount * sizeof (FLOAT32) * 4);
      }
    else if (uCode == ccCoordNormal)
      {
      // 4cc(vn__) 4U(NumNorms) array4F (x1 y1 z1 ... xn yn zn)
      uCount = parserIn.GetU4_LEnd ();
      iCoordNormalCount += INT (uCount);
      parserIn.SkipChars (uCount * sizeof (FLOAT32) * 3);
      }
    else if (uCode == ccVertTex1)
      {
      // 4cc(vt1_) 4U(NumVTex) array4F (u1 ... un)
      uCount = parserIn.GetU4_LEnd ();
      iCoordTextureCount += INT (uCount);
      parserIn.SkipChars (uCount * sizeof (FLOAT32) * 1);
      }
    else if (uCode == ccVertTex2)
      {
      // 4cc(vt2_) 4U(NumVTex) array4F (u1 v1 ... un vn)
      uCount = parserIn.GetU4_LEnd ();
      iCoordTextureCount += INT (uCount);
      parserIn.SkipChars (uCount * sizeof (FLOAT32) * 2);
      }
    else if (uCode == ccVertTex3)
      {
      // 4cc(vt3_) 4U(NumVTex) array4F (u1 v1 w1 ... un vn wn)
      uCount = parserIn.GetU4_LEnd ();
      iCoordTextureCount += INT (uCount);
      parserIn.SkipChars (uCount * sizeof (FLOAT32) * 3);
      }
    else if (uCode == ccFace3)
      {
      // 4cc(f3__) 4U(NumFaces) array4U (v1 v2 v3 ... v1n v2n v3n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 3;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 3);
      }
    else if (uCode == ccFace4)
      {
      // 4cc(f4__) 4U(NumFaces) array4U (v1 v2 v3 v4 ... v1n v2n v3n v4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 4;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 4);
      }
    else if (uCode == ccFace3Tex)
      {
      // 4cc(f3t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 ... 
      //                                 v1n t1n v2n t2n v3n t3n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 3;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 6);
      }
    else if (uCode == ccFace4Tex)
      {
      // 4cc(f4t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 v4 t4 ... 
      //                                 v1n t1n v2n t2n v3n t3n v4n t4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 4;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 8);
      }
    else if (uCode == ccFace3TexNorm)
      {
      // 4cc(f3tn) 4U(NumFaces) array4U (v1 t1 n1 v2 t2 n2 v3 t3 n3 ... 
      //                                 v1n t1n n1n v2n t2n n2n v3n t3n n3n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 3;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 9);
      }
    else if (uCode == ccFace4TexNorm)
      {
      // 4cc(f4tn) 4U(NumFaces) array4U (v1 t2 n1 v2 t2 n2 v3 t3 n3 v4 t4 n4 ... 
      //                                          v1n t1n n1n v2n t2n n2n v3n t3n n3n v4n tn n4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 4;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 12);
      }
    else if (uCode == ccFace3Norm)
      {
      // 4cc(f3n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 ... 
      //                                 v1n n1n v2n n2n v3n n3n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 3;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 6);
      }
    else if (uCode == ccFace4Norm)
      {
      // 4cc(f4n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 v4 n4 ... 
      //                                 v1n n1n v2n n2n v3n n3n v4n n4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 4;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 8);
      }

    else if (uCode == ccFace5)
      {
      // 4cc(f5__) 4U(NumFaces) array4U (v1 v2 v3 v4 ... v1n v2n v3n v4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 5;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 5);
      }
    else if (uCode == ccFace5Tex)
      {
      // 4cc(f5t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 v4 t4 ... 
      //                                 v1n t1n v2n t2n v3n t3n v4n t4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 5;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 10);
      }
    else if (uCode == ccFace5TexNorm)
      {
      // 4cc(f5tn) 4U(NumFaces) array4U (v1 t2 n1 v2 t2 n2 v3 t3 n3 v4 t4 n4 ... 
      //                                          v1n t1n n1n v2n t2n n2n v3n t3n n3n v4n tn n4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 5;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 15);
      }
    else if (uCode == ccFace5Norm)
      {
      // 4cc(f5n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 v4 n4 ... 
      //                                 v1n n1n v2n n2n v3n n3n v4n n4n)
      uCount = parserIn.GetU4_LEnd ();
      iFaceCount += INT (uCount);
      iVertCount += INT (uCount) * 5;
      parserIn.SkipChars (uCount * sizeof (UINT32) * 10);
      }

    else if (uCode == ccPoint)
      {
      // 4cc(p___) 4U(NumPoints) (v1 v2 ... vn)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32));
      }
    else if (uCode == ccLine)
      {
      // 4cc(l___) 4U(NumPoints) (v1 v2 ... vn)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32));
      }
    else if (uCode == ccLineTex)
      {
      // 4cc(lt__) 4U(NumPoints) (v1 t1 v2 t2 ... vn tn)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32) * 2);
      }
    else if (uCode == ccCurve)
      {
      // 4cc(curv) 4F(u0) 4F(u1) 4U(NumCVs) (v1 v2 ... vn)
      parserIn.SkipChars (sizeof (FLOAT32) + 
                          sizeof (FLOAT32));
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32));
      }
    else if (uCode == ccGroup)
      {
      // 4cc(g___) 4U(strzLen) STRZ(groupName1 groupNameN)
      ++iGroupCount;
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount);
      }
    else if (uCode == ccObject)
      {
      // 4cc(o___) 4U(strzLen) STRZ(objectName)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount);
      }
    else if (uCode == ccMtlUse)
      {
      // 4cc(umtl) 4U(strzLen) STRZ(materialName)
      //++iGroupCount;
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount);
      }
    else if (uCode == ccMtlLibrary)
      {
      // 4cc(lmtl) 4U(strzLen) STRZ(space separated filenames)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount);
      };
    };
  };

  
//------------------------------------------------------------------------
VOID  ObjCodec::ParseVertListBin (RStrParser &  parserIn,
                                  INT           iNumFacesIn,
                                  BOOL          bHasTexIn,
                                  BOOL          bHasNormIn,
                                  
                                  INT &         iFaceIndex,
                                  INT &         iVertIndex,
                                  INT           iShaderIndex,
                                  INT           iCoordPointOffset,
                                  INT           iCoordTextureOffset,
                                  INT           iCoordNormalOffset,
                                  INT           iGroupCount,
                                  EditMesh &    editMeshOut)
  {
  // 4cc(f4tn) 4U(NumFaces) array4U (v1 t2 n1 v2 t2 n2 v3 t3 n3 v4 t4 n4 ... 
  //                                 v1n t1n n1n v2n t2n n2n v3n t3n n3n v4n tn n4n)
  UINT  uCount = parserIn.GetU4_LEnd ();
  for (UINT  uIndex = 0; uIndex < uCount; ++uIndex)
    {
    editMeshOut.aiFaces [iFaceIndex] = iVertIndex;
    INT  iNumFaces = iNumFacesIn;
    while (iNumFaces--)     
      {
      // geom/tex/norm
      // for the binary format, we store with a 0-based index
      editMeshOut.aiFacePoints [iVertIndex] = parserIn.GetU4_LEnd () + iCoordPointOffset;
      editMeshOut.aiFaceTex    [iVertIndex] = bHasTexIn  ? parserIn.GetU4_LEnd () + iCoordTextureOffset : 0;
      editMeshOut.aiFaceNorms  [iVertIndex] = bHasNormIn ? parserIn.GetU4_LEnd () + iCoordNormalOffset : 0;

      if (iGroupCount > 0)
        {
        editMeshOut.aiGroupPoints [iShaderIndex] = RMin (editMeshOut.aiGroupPoints [iShaderIndex],
                                                         editMeshOut.aiFacePoints  [iVertIndex]);
        };

      //if (iVertIndex < 15) printf ("Reading v4 face point %d: %d\n", iVertIndex, editMeshOut.aiFacePoints [iVertIndex] + iCoordPointOffset);
      ++iVertIndex;
      };
    // save the number of verts in the curve
    editMeshOut.aiFaceVCount [iFaceIndex] = iFaceIndex - editMeshOut.aiFaces [iFaceIndex];

    if (iGroupCount > 0)
      {
      editMeshOut.aiFaceGroups [iFaceIndex] = iShaderIndex;
      };

    ++iFaceIndex;
    };
  };
  

//------------------------------------------------------------------------
VOID  ObjCodec::ParseDataBin (RStrParser &  parserIn,
                              INT32         iCoordTextureOffset,
                              INT32         iCoordNormalOffset,
                              INT32         iCoordPointOffset,
                              INT32         iFaceOffset,
                              INT32         iCurveOffset,
                              INT32         iFaceVertOffset,
                              INT32         iGroupCount,
                              EditMesh &    editMeshOut)
  {
  RStr             strShaderName;
  RStr             strGroupName;

  INT              iCoordTextureIndex = 0;
  INT              iCoordNormalIndex  = 0;
  INT              iCoordPointIndex   = 0;
  INT              iFaceIndex        = 0;
  //INT            iMaterialIndex    = 0;
  INT              iFaceVertIndex    = 0;
  INT              iFaceShaderIndex  = 0;
  UINT             uCount            = 0;
  UINT             uIndex;


  iCoordTextureIndex = iCoordTextureOffset;
  iCoordNormalIndex  = iCoordNormalOffset ;
  iCoordPointIndex   = iCoordPointOffset  ;
  iFaceIndex         = iFaceOffset        ;
  iFaceVertIndex     = iFaceVertOffset    ;

  parserIn.ResetCursor ();


  // first run through the buffer and calculate the array sizes
  parserIn.ResetCursor ();

  UINT  uHeader = parserIn.GetU4_LEnd ();
  if (uHeader !=  ccHeader) return;
  
  while (!parserIn.IsEOF ())
    {
    UINT  uCode = parserIn.GetU4_LEnd ();
    
    if (uCode == ccVert3)
      {
      // 4cc(v3__) 4U(NumVerts) array4F (x1 y1 z1 ... xn yn zn)
      uCount = parserIn.GetU4_LEnd ();
      for (uIndex = 0; uIndex < uCount; ++uIndex)
        {
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fX = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fY = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fZ = parserIn.GetF4_LEnd ();
        editMeshOut.avecCoordPoints [iCoordPointIndex].fW = 1.0f;
        ++iCoordPointIndex;
        };
      }
    else if (uCode == ccVert4)
      {
      // 4cc(v4__) 4U(NumVerts) array4F (x1 y1 z1 w1... xn yn zn wn)
      uCount = parserIn.GetU4_LEnd ();
      for (uIndex = 0; uIndex < uCount; ++uIndex)
        {
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fX = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fY = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fZ = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordPoints [iCoordPointIndex].fW = parserIn.GetF4_LEnd ();
        ++iCoordPointIndex;
        };
      }
    else if (uCode == ccCoordNormal)
      {
      // 4cc(vn__) 4U(NumNorms) array4F (x1 y1 z1 ... xn yn zn)
      uCount = parserIn.GetU4_LEnd ();
      for (uIndex = 0; uIndex < uCount; ++uIndex)
        {
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordNormal [iCoordNormalIndex].fX = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordNormal [iCoordNormalIndex].fY = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordNormal [iCoordNormalIndex].fZ = parserIn.GetF4_LEnd ();
        editMeshOut.avecCoordNormal [iCoordNormalIndex].fW = 0.0f;
        ++iCoordNormalIndex;
        };
      }
    else if (uCode == ccVertTex1)
      {
      // 4cc(vt1_) 4U(NumVTex) array4F (u1 ... un)
      uCount = parserIn.GetU4_LEnd ();
      for (uIndex = 0; uIndex < uCount; ++uIndex)
        {
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fX = parserIn.GetF4_LEnd ();
        editMeshOut.avecCoordTexture [iCoordTextureIndex].fY = 0;
        editMeshOut.avecCoordTexture [iCoordTextureIndex].fZ = 0;
        editMeshOut.avecCoordTexture [iCoordTextureIndex].fW = 0;
        ++iCoordTextureIndex;
        };
      }
    else if (uCode == ccVertTex2)
      {
      // 4cc(vt2_) 4U(NumVTex) array4F (u1 v1 ... un vn)
      uCount = parserIn.GetU4_LEnd ();
      for (uIndex = 0; uIndex < uCount; ++uIndex)
        {
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fX = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fY = parserIn.GetF4_LEnd ();
        editMeshOut.avecCoordTexture [iCoordTextureIndex].fZ = 0;
        editMeshOut.avecCoordTexture [iCoordTextureIndex].fW = 0;
        ++iCoordTextureIndex;
        };
      }
    else if (uCode == ccVertTex3)
      {
      // 4cc(vt3_) 4U(NumVTex) array4F (u1 v1 w1 ... un vn wn)
      uCount = parserIn.GetU4_LEnd ();
      for (uIndex = 0; uIndex < uCount; ++uIndex)
        {
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fX = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fY = parserIn.GetF4_LEnd ();
        if (!parserIn.IsEOF ()) editMeshOut.avecCoordTexture [iCoordTextureIndex].fZ = parserIn.GetF4_LEnd ();
        editMeshOut.avecCoordTexture [iCoordTextureIndex].fW = 0;
        ++iCoordTextureIndex;
        };
      }
    else if (uCode == ccFace3)
      {
      // 4cc(f3__) 4U(NumFaces) array4U (v1 v2 v3 ... v1n v2n v3n)
      ParseVertListBin  (parserIn,
                         3, FALSE, FALSE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace4)
      {
      // 4cc(f4__) 4U(NumFaces) array4U (v1 v2 v3 v4 ... v1n v2n v3n v4n)
      ParseVertListBin  (parserIn,
                         4, FALSE, FALSE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace3Tex)
      {
      // 4cc(f3t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 ... 
      //                                 v1n t1n v2n t2n v3n t3n)
      ParseVertListBin  (parserIn,
                         3, TRUE, FALSE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace4Tex)
      {
      // 4cc(f4t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 v4 t4 ... 
      //                                 v1n t1n v2n t2n v3n t3n v4n t4n)
      ParseVertListBin  (parserIn,
                         4, TRUE, FALSE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace3TexNorm)
      {
      // 4cc(f3tn) 4U(NumFaces) array4U (v1 t1 n1 v2 t2 n2 v3 t3 n3 ... 
      //                                 v1n t1n n1n v2n t2n n2n v3n t3n n3n)
      ParseVertListBin  (parserIn,
                         3, TRUE, TRUE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace4TexNorm)
      {
      // 4cc(f4tn) 4U(NumFaces) array4U (v1 t2 n1 v2 t2 n2 v3 t3 n3 v4 t4 n4 ... 
      //                                          v1n t1n n1n v2n t2n n2n v3n t3n n3n v4n tn n4n)
      ParseVertListBin  (parserIn,
                         4, TRUE, TRUE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace3Norm)
      {
      // 4cc(f3n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 ... 
      //                                 v1n n1n v2n n2n v3n n3n)
      ParseVertListBin  (parserIn,
                         3, FALSE, TRUE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace4Norm)
      {
      // 4cc(f4n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 v4 n4 ... 
      //                                 v1n n1n v2n n2n v3n n3n v4n n4n)
      ParseVertListBin  (parserIn,
                         4, FALSE, TRUE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }


    else if (uCode == ccFace5)
      {
      // 4cc(f5__) 4U(NumFaces) array4U (v1 v2 v3 v4 ... v1n v2n v3n v4n)
      ParseVertListBin  (parserIn,
                         5, FALSE, FALSE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace5Tex)
      {
      // 4cc(f5t_) 4U(NumFaces) array4U (v1 t1 v2 t2 v3 t3 v4 t4 ... 
      //                                 v1n t1n v2n t2n v3n t3n v4n t4n)
      ParseVertListBin  (parserIn,
                         5, TRUE, FALSE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace5TexNorm)
      {
      // 4cc(f5tn) 4U(NumFaces) array4U (v1 t2 n1 v2 t2 n2 v3 t3 n3 v4 t4 n4 ... 
      //                                          v1n t1n n1n v2n t2n n2n v3n t3n n3n v4n tn n4n)
      ParseVertListBin  (parserIn,
                         5, TRUE, TRUE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }
    else if (uCode == ccFace5Norm)
      {
      // 4cc(f5n_) 4U(NumFaces) array4U (v1 n1 v2 n2 v3 n3 v4 n4 ... 
      //                                 v1n n1n v2n n2n v3n n3n v4n n4n)
      ParseVertListBin  (parserIn,
                         5, FALSE, TRUE,
                         iFaceIndex, iFaceVertIndex, iFaceShaderIndex,
                         iCoordPointOffset, iCoordTextureOffset, iCoordNormalOffset,
                         iGroupCount, editMeshOut);
      }


 
    else if (uCode == ccPoint)
      {
      // 4cc(p___) 4U(NumPoints) (v1 v2 ... vn)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32));
      // to be implemented
      }
    else if (uCode == ccLine)
      {
      // 4cc(l___) 4U(NumPoints) (v1 v2 ... vn)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32));
      // to be implemented
      }
    else if (uCode == ccLineTex)
      {
      // 4cc(lt__) 4U(NumPoints) (v1 t1 v2 t2 ... vn tn)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32) * 2);
      // to be implemented
      }
    else if (uCode == ccCurve)
      {
      // 4cc(curv) 4F(u0) 4F(u1) 4U(NumCVs) (v1 v2 ... vn)
      parserIn.SkipChars (sizeof (FLOAT32) + 
                          sizeof (FLOAT32));
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount * sizeof (UINT32));
      // to be implemented
      }
    else if (uCode == ccGroup)
      {
      // 4cc(g___) 4U(strzLen) STRZ(groupName1 groupNameN)
      uCount = parserIn.GetU4_LEnd ();
      if (!parserIn.IsEOF ())   
        {
        strGroupName = parserIn.SubString (parserIn.GetCursorStart (), uCount);
        };
      parserIn.SkipChars (uCount);
      // to be implemented

      editMeshOut.astrGroups.Append (strGroupName);
      iFaceShaderIndex = iGroupCount;
      ++iGroupCount;
      }
    else if (uCode == ccObject)
      {
      // 4cc(o___) 4U(strzLen) STRZ(objectName)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount);
      // to be implemented
      }
    else if (uCode == ccMtlUse)
      {
      // 4cc(umtl) 4U(strzLen) STRZ(materialName)
      uCount = parserIn.GetU4_LEnd ();

      if (!parserIn.IsEOF ())   
        {
        strShaderName = parserIn.SubString (parserIn.GetCursorStart (), uCount);
        };
      parserIn.SkipChars (uCount);

      if (editMeshOut.astrShaders.Length () < editMeshOut.astrGroups.Length ())
        {
        editMeshOut.astrShaders.Append (strShaderName);
        };

      // first see if this material is in our list or not.
      /*
      iFaceShaderIndex = astrShaders->Find (strShaderName);
      
      if (iFaceShaderIndex == -1)
        {
        // not in list.  Add it.
        iFaceShaderIndex = astrShaders->Length ();
        astrShaders->Append (strShaderName);
        };
      */
      }
    else if (uCode == ccMtlLibrary)
      {
      // 4cc(lmtl) 4U(strzLen) STRZ(space separated filenames)
      uCount = parserIn.GetU4_LEnd ();
      parserIn.SkipChars (uCount);
      // to be implemented
      };
    };
  };


//------------------------------------------------------------------------
EStatus  ObjCodec::ReadFromFile  (RStr        strFileNameIn,
                                  EditMesh &  editMeshOut,
                                  BOOL        bAppendIn,
                                  BOOL        bBinaryIn)
  {
  RStrParser   parserFile;
  RStrParser   parserMatLib;

  EStatus  status = parserFile.ReadFromFile (strFileNameIn);

  if (status == EStatus::kSuccess)
    {
    // replace obj extension with mtl, and search for a material library
    RStr  strMatLibFile = strFileNameIn;
    INT   iPeriod = strMatLibFile.ReverseFindChar ('.');
    if (iPeriod != -1)
      {
      strMatLibFile.TruncateRight (iPeriod);
      strMatLibFile += "mtl";
      
      RStrParser  parserPath (strMatLibFile);
      
      parserPath.TruncateRight (parserPath.ReverseFindChar ('/'));
      
      
      
      ReadMtlFromFile (strMatLibFile, RStr (parserPath));
      };

    // Read in the OBJ file
    status = Read (parserFile, editMeshOut, bAppendIn, bBinaryIn);
    };
  return (status);
  };


//------------------------------------------------------------------------
EStatus  ObjCodec::Write  (RStrParser &   parserIn,
                           EditMesh &     editMeshIn,
                           BOOL           bWritePts,
                           BOOL           bWriteUvs,
                           BOOL           bWriteNmls,
                           BOOL           bWriteFaces,
                           BOOL           bWriteShaders)
  {
  RStr             strTemp;
  RStr             strVertTemp;
  INT32            iIndex;
  INT32            iFaceVertIndex;


  INT32  iCoordTextureCount = editMeshIn.avecCoordTexture.Length ();
  INT32  iCoordNormalCount  = editMeshIn.avecCoordNormal. Length ();
  INT32  iCoordPointCount   = editMeshIn.avecCoordPoints. Length ();
  INT32  iFaceCount         = editMeshIn.aiFaces.         Length ();
  INT32  iVertTexCount      = editMeshIn.aiFaceTex.       Length ();
  INT32  iVertNormCount     = editMeshIn.aiFaceNorms.     Length ();
  INT32  iVertPointCount    = editMeshIn.aiFacePoints.    Length ();
  INT32  iFaceGroupsCount   = editMeshIn.aiFaceGroups.    Length ();

  // the face vert count is the max number of verts that can be written out.  The smallest
  //  non-zero array determines the max number of valid face verts we can write out.  Ideally
  //  all three arrays will be the same size.
  INT32  iFaceVertCount = iVertPointCount;
  if (iVertTexCount != 0)   {iFaceVertCount = RMin (iFaceVertCount, iVertTexCount);};
  if (iVertNormCount != 0)  {iFaceVertCount = RMin (iFaceVertCount, iVertNormCount);};

  // make a rough guesstimate as far as how large of a buffer you need.  You won't get it
  //  exactly, but it should greatly cut down on the number of reallocations you need to do.
  // v  = 31
  // vt = 20
  // vn = 32
  // f  = 49

  UINT32  uGuesstimateSize = (iCoordPointCount * 32) + (iCoordTextureCount * 21) +
                             (iCoordNormalCount * 33) + (iFaceCount * 50);

  parserIn.Grow (uGuesstimateSize);

  parserIn += "# File written by the Raven library.\n";

  // write out the point coordinates
  for (iIndex = 0; iIndex < iCoordPointCount; ++iIndex)
    {
    if (bWritePts)
      {
      strTemp.Format ("v %g %g %g\n", editMeshIn.avecCoordPoints [iIndex].fX, editMeshIn.avecCoordPoints [iIndex].fY, editMeshIn.avecCoordPoints [iIndex].fZ);
      parserIn += strTemp;
      };
    };

  // write out the texture coordinates
  for (iIndex = 0; iIndex < iCoordTextureCount; ++iIndex)
    {
    if (bWriteUvs)
      {
      strTemp.Format ("vt %g %g\n", editMeshIn.avecCoordTexture [iIndex].fU, editMeshIn.avecCoordTexture [iIndex].fV);
      parserIn += strTemp;
      };
    };

  // write out the vert normals
  for (iIndex = 0; iIndex < iCoordNormalCount; ++iIndex)
    {
    if (bWriteNmls)
      {
      strTemp.Format ("vn %g %g %g\n", editMeshIn.avecCoordNormal [iIndex].fX, editMeshIn.avecCoordNormal [iIndex].fY, editMeshIn.avecCoordNormal [iIndex].fZ);
      parserIn += strTemp;
      };
    };

  // write out the faces
  iFaceVertIndex = 0;
  INT32  iMaxIndex;
  INT32  iCurrGroup = -1;
  
  // make sure there are enough verts to write out.
  if ((iFaceCount > 0) && (iFaceVertCount >= editMeshIn.aiFaces [iFaceCount - 1]))
    {
    // step through the faces
    for (iIndex = 0; iIndex < iFaceCount; ++iIndex)
      {
      // make sure there is a group listed for this face
      if (iIndex < iFaceGroupsCount)
        {
        // check to see if the group/shader has changed
        if (editMeshIn.aiFaceGroups [iIndex] != iCurrGroup)
          {
          // we have a new material
          iCurrGroup = editMeshIn.aiFaceGroups [iIndex];

          printf ("Now working with group %d\n", INT (iCurrGroup));

          // make sure there is an entry for this shader in the shader name list.
          if (editMeshIn.astrGroups.Length () > iCurrGroup)
            {
            parserIn.AppendFormat ("g %s\n", editMeshIn.astrGroups [iCurrGroup].AsChar ());
            };
          if (bWriteShaders)
            {
            if (editMeshIn.astrShaders.Length () > iCurrGroup)
              {
              parserIn.AppendFormat ("usemtl %s\n", editMeshIn.astrShaders [iCurrGroup].AsChar ());
              };
            };
          };
        };
      
      // The next entry in the faces index list gives the start of the verts for that face.
      //  So that lets us know where the verts for this face stop.  This is an alternative to
      //  storing a face count for each face.  If we are on the last entry, use the
      //  vertex array size to determine how many verts this face can have.
      if (iIndex == iFaceCount - 1)
        {
        iMaxIndex = iFaceVertCount;
        }
      else
        {
        iMaxIndex = editMeshIn.aiFaces [iIndex + 1];
        };

      //f 1/1/1 2/2/2 10/11/3 9/10/4
      strTemp = "f";

      while (iFaceVertIndex < iMaxIndex)
        {
        // Note:  The vertex indices need to have 1 added to them so that they
        //   are saved as 1-based instead of 0 based.
        
        // write the vertex point
        strVertTemp.Format (" %d", editMeshIn.aiFacePoints [iFaceVertIndex] + 1);
        strTemp += strVertTemp;

        // write the vertex texture coordinate
        if (iVertTexCount >= iFaceVertCount)
          {
          strVertTemp.Format ("/%d", editMeshIn.aiFaceTex [iFaceVertIndex] + 1);
          strTemp += strVertTemp;
          }
        else
          {
          if (iVertNormCount >= iFaceVertCount)
            {
            strTemp += "/";
            };
          };

        // write the vertex normal
        if (iVertNormCount >= iFaceVertCount)
          {
          strVertTemp.Format ("/%d", editMeshIn.aiFaceNorms [iFaceVertIndex] + 1);
          strTemp += strVertTemp;
          };

        ++iFaceVertIndex;
        };
      if (bWriteFaces)
        {
        parserIn += strTemp;
        parserIn += "\n";
        };
      };
    };
  return (EStatus::kSuccess);
  };



//------------------------------------------------------------------------
EStatus  ObjCodec::WriteBin  (RStrParser &   parserIn,
                              EditMesh &     editMeshIn,
                              BOOL           bWritePts,
                              BOOL           bWriteUvs,
                              BOOL           bWriteNmls,
                              BOOL           bWriteFaces,
                              BOOL           bWriteShaders)
  {
  RStr             strTemp;
  RStr             strVertTemp;
  INT32            iIndex;
  INT32            iFaceVertIndex;

  INT32  iCoordTextureCount = editMeshIn.avecCoordTexture.Length ();
  INT32  iCoordNormalCount  = editMeshIn.avecCoordNormal. Length ();
  INT32  iVertCount         = editMeshIn.avecCoordPoints. Length ();
  INT32  iFaceCount         = editMeshIn.aiFaces.         Length ();
  INT32  iFaceTexCount      = editMeshIn.aiFaceTex.       Length ();
  INT32  iFaceNormCount     = editMeshIn.aiFaceNorms.     Length ();
  INT32  iFacePointCount    = editMeshIn.aiFacePoints.    Length ();
  INT32  iFaceGroupsCount   = editMeshIn.aiFaceGroups.    Length ();

  // the face vert count is the max number of verts that can be written out.  The smallest
  //  non-zero array determines the max number of valid face verts we can write out.  Ideally
  //  all three arrays will be the same size.
  INT32  iFaceVertCount = iFacePointCount;
  if (iFaceTexCount != 0)   {iFaceVertCount = RMin (iFaceVertCount, iFaceTexCount);};
  if (iFaceNormCount != 0)  {iFaceVertCount = RMin (iFaceVertCount, iFaceNormCount);};

  // make a rough guesstimate as far as how large of a buffer you need.  You won't get it
  //  exactly, but it should greatly cut down on the number of reallocations you need to do.

  UINT32  uGuesstimateSize = (iVertCount * 32) + (iCoordTextureCount * 21) +
                             (iCoordNormalCount * 33) + (iFaceCount * 50);

  parserIn.Grow (uGuesstimateSize);

  parserIn.SetU4_LEnd (ccHeader);

  // write out the verts
  if (bWritePts)
    {
    parserIn.SetU4_LEnd (ccVert3);
    parserIn.SetU4_LEnd (iVertCount);
    for (iIndex = 0; iIndex < iVertCount; ++iIndex)
      {
      parserIn.SetF4_LEnd (editMeshIn.avecCoordPoints [iIndex].fX);
      parserIn.SetF4_LEnd (editMeshIn.avecCoordPoints [iIndex].fY);
      parserIn.SetF4_LEnd (editMeshIn.avecCoordPoints [iIndex].fZ);
      };
    };
    
  // write out the texture coordinates
  if (bWriteUvs)
    {
    parserIn.SetU4_LEnd (ccVertTex2);
    parserIn.SetU4_LEnd (iCoordTextureCount);
    for (iIndex = 0; iIndex < iCoordTextureCount; ++iIndex)
      {
      parserIn.SetF4_LEnd (editMeshIn.avecCoordTexture [iIndex].fU);
      parserIn.SetF4_LEnd (editMeshIn.avecCoordTexture [iIndex].fV);
      };
    };

  // write out the vert normals
  if (bWriteNmls)
    {
    parserIn.SetU4_LEnd (ccCoordNormal);
    parserIn.SetU4_LEnd (iCoordNormalCount);
    for (iIndex = 0; iIndex < iCoordNormalCount; ++iIndex)
      {
      parserIn.SetF4_LEnd (editMeshIn.avecCoordNormal [iIndex].fX);
      parserIn.SetF4_LEnd (editMeshIn.avecCoordNormal [iIndex].fY);
      parserIn.SetF4_LEnd (editMeshIn.avecCoordNormal [iIndex].fZ);
      };
    };

   // write out the faces
  iFaceVertIndex = 0;
  INT32  iCurrShader = -1;
  
  // make sure there are enough verts to write out.
  if ((iFaceCount > 0) && (iFaceVertCount >= editMeshIn.aiFaces [iFaceCount - 1]))
    {
    INT  iHasTex   = (iFaceTexCount  >= iFaceVertCount) ? 1 : 0;
    INT  iHasNorm  = (iFaceNormCount >= iFaceVertCount) ? 1 : 0;
    
    for (iIndex = 0; iIndex < iFaceCount;)
      {
      
      // count how many sides this run of polygons has
      UINT  uRunSize = 0;
      INT   iSearchIndex = 0;
      
      INT  iNumVerts = editMeshIn.aiFaceVCount [iIndex];
      
      // calculate how long this run of faces is
      for (iSearchIndex = iIndex; iSearchIndex < iFaceCount; ++iSearchIndex)
        {
        INT  iCurrVerts = editMeshIn.aiFaceVCount [iSearchIndex];
        
        // end this run if the number of edges or the current material changes.
        if (iNumVerts != iCurrVerts) break;
        if (editMeshIn.aiFaceGroups [iSearchIndex] != iCurrShader) break;
        
        ++uRunSize;
        };
        
      // switch materials as needed
      if (iIndex < iFaceGroupsCount)
        {
        if (editMeshIn.aiFaceGroups [iIndex] != iCurrShader)
          {
          // we have a new material
          iCurrShader = editMeshIn.aiFaceGroups [iIndex];

          if (editMeshIn.astrShaders.Length () > iCurrShader)
            {
            // 4cc(umtl) 4U(strzLen) STRZ(materialName)

            parserIn.SetU4_LEnd (ccGroup);
            UINT  uStrLen = editMeshIn.astrGroups [iCurrShader].GetLength () + 1;
            parserIn.SetU4_LEnd (uStrLen);
            parserIn.AppendChars (editMeshIn.astrGroups [iCurrShader].AsChar (), uStrLen);

            if (bWriteShaders)
              {
              parserIn.SetU4_LEnd (ccMtlUse);
              uStrLen = editMeshIn.astrShaders [iCurrShader].GetLength () + 1;
              parserIn.SetU4_LEnd (uStrLen);
              parserIn.AppendChars (editMeshIn.astrShaders [iCurrShader].AsChar (), uStrLen);
              };
            };
          };
        };

      // now that we are done using iIndex, we can increment it.  
      //  we increment it here before the early-return decision.
      iIndex += INT (uRunSize);

      if (bWriteFaces)
        {
        if (iNumVerts == 3)
          {
          if (iHasTex)
            {
            if (iHasNorm) parserIn.SetU4_LEnd (ccFace3TexNorm);
            else          parserIn.SetU4_LEnd (ccFace3Tex);
            }
          else
            {
            if (iHasNorm) parserIn.SetU4_LEnd (ccFace3Norm);
            else          parserIn.SetU4_LEnd (ccFace3);
            };
          }
        else if (iNumVerts == 4)
          {
          if (iHasTex)
            {
            if (iHasNorm) parserIn.SetU4_LEnd (ccFace4TexNorm);
            else          parserIn.SetU4_LEnd (ccFace4Tex);
            }
          else
            {
            if (iHasNorm) parserIn.SetU4_LEnd (ccFace4Norm);
            else          parserIn.SetU4_LEnd (ccFace4);
            };
          }
        else if (iNumVerts == 5)
          {
          if (iHasTex)
            {
            if (iHasNorm) parserIn.SetU4_LEnd (ccFace5TexNorm);
            else          parserIn.SetU4_LEnd (ccFace5Tex);
            }
          else
            {
            if (iHasNorm) parserIn.SetU4_LEnd (ccFace5Norm);
            else          parserIn.SetU4_LEnd (ccFace5);
            };
          }
        else
          {
          // we don't support this face count.  Only 3 or 4 sided faces
          printf ("Skipping poly with %d verts\n", iNumVerts);
          
          iFaceVertIndex += INT (uRunSize) * iNumVerts;
          continue;
          };

        parserIn.SetU4_LEnd (uRunSize);
        };
        
      while (uRunSize--)  
        {
        INT  iVertIndex = iNumVerts;
        while (iVertIndex--)
          {
          // for the binary format, we store with a 0-based index
          
          if (bWriteFaces)
            {
            // write the vertex point
            parserIn.SetU4_LEnd (editMeshIn.aiFacePoints [iFaceVertIndex]);

            // write the vertex texture coordinate
            if (iHasTex)  parserIn.SetU4_LEnd (editMeshIn.aiFaceTex [iFaceVertIndex]);

            // write the vertex normal
            if (iHasNorm)  parserIn.SetU4_LEnd (editMeshIn.aiFaceNorms [iFaceVertIndex]);
            };
          ++iFaceVertIndex;
          };
        };
      };
      
      
    };
  return (EStatus::kSuccess);
  };



//------------------------------------------------------------------------
EStatus  ObjCodec::WriteToFile  (RStr        strFileNameIn,
                                 EditMesh &  editMeshIn,
                                 BOOL        bBinaryIn,
                                 BOOL        bWritePts,
                                 BOOL        bWriteUvs,
                                 BOOL        bWriteNmls,
                                 BOOL        bWriteFaces,
                                 BOOL        bWriteShaders)
  {
  RStrParser   parserFile;
  EStatus      status;
  if (bBinaryIn)
    {
    printf ("Write bin\n");
    status = WriteBin (parserFile, editMeshIn, 
                       bWritePts, bWriteUvs, bWriteNmls, bWriteFaces, bWriteShaders);
    }
  else
    {
    status = Write (parserFile, editMeshIn,
                    bWritePts, bWriteUvs, bWriteNmls, bWriteFaces, bWriteShaders);
    };

  if (status == EStatus::kSuccess)
    {
    status = parserFile.WriteToFile (strFileNameIn);
    
    if (bWriteShaders)
      {
      // write out a .mtl file as well.
      RStr  strMatLibFile = strFileNameIn;
      INT   iPeriod = strMatLibFile.ReverseFindChar ('.');
      if (iPeriod != -1)
        {
        strMatLibFile.TruncateRight (iPeriod);
        strMatLibFile += "mtl";
        
        WriteMtlToFile (strMatLibFile, editMeshIn);
        };
      };
    };
  return (status);
  };


//------------------------------------------------------------------------
EStatus  ObjCodec::ReadMtlFromFile  (RStr     strFileNameIn,
                                     RStr     strPathIn)
  {
  RStrParser   parserFile;
  
  // debugging
  //printf ("reading from file %s\n", strFileNameIn.AsChar ());      
  
  EStatus  status = parserFile.ReadFromFile (strFileNameIn);

  if (status == EStatus::kSuccess)
    {
    status = ReadMtl (parserFile, strPathIn);
    };
  return (status);
  };



//------------------------------------------------------------------------
EStatus  ObjCodec::WriteMtlToFile  (RStr     strFileNameIn,
                                    EditMesh &  editMeshIn)
  {
  RStrParser   parserFile;

  EStatus  status = WriteMtl (editMeshIn, parserFile);
  if (status == EStatus::kSuccess)
    {
    status = parserFile.WriteToFile (strFileNameIn);
    };

  return (status);
  };



//------------------------------------------------------------------------
EStatus  ObjCodec::ReadMtl  (RStrParser &  parserIn,
                             RStr          strPathIn)
  {
  EStatus          status;
  RStr             strMtlName;
  Material *       pmtlNew = NULL;
  
//  RAttr *          pattrValue;
//  RAttr *          pattrR;
//  RAttr *          pattrG;
//  RAttr *          pattrB;
  RStrParser       parserValue;


  //newmtl phong1SG
  //Kd  0 0.8000000119 0
  //Ka 0 0 0
  //Ks 0.5 0.5 0.5
  //Ns 200
  //illum 1


  //Ns = Phong specular component. Ranges from 0 to 1000. (I've seen various statements about this range (see below))
  //Kd = Diffuse color weighted by the diffuse coefficient.
  //Ka = Ambient color weighted by the ambient coefficient.
  //Ks = Specular color weighted by the specular coefficient.
  //d = Dissolve factor (pseudo-transparency). Values are from 0-1. 0 is completely transparent, 1 is opaque.
  //Ni = Refraction index. Values range from 1 upwards. A value of 1 will cause no refraction. A higher value implies refraction.
  //illum = (0, 1, or 2) 0 to disable lighting, 1 for ambient & diffuse only (specular color set to black), 2 for full lighting (see below)
  //sharpness = ? (see below)
  //map_Kd = Diffuse color texture map.
  //map_Ks = Specular color texture map.
  //map_Ka = Ambient color texture map.
  //map_Bump = Bump texture map.
  //map_d = Opacity texture map.
  //refl = reflection type and filename (?)

  // run through the file and read in the materials
  parserIn.ResetCursor ();

  while (!parserIn.IsEOF ())
    {
printf ("Reading line\n\n %s\n", parserIn.GetBufferPtr (parserIn.GetCursorStart ()));      
    
    
    if (parserIn.Compare ("newmtl ", 7, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);
      if (!parserIn.IsEOL ()) strMtlName = parserIn.GetWord ();
      
printf ("creating material %s\n", strMtlName.AsChar ());      
      // create a material node.
      pmtlNew = Material::FindByName (strMtlName);
      if (pmtlNew == NULL)
        {
        new Material (strMtlName, "ShdUnlitTex");
        pmtlNew = Material::FindByName (strMtlName);
        }
  
      if (pmtlNew == NULL)
        {
        printf ("Error creating material\n");
        };
printf ("create material done\n");      
      }
    else if (parserIn.Compare ("Kd ", 3, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);
      
      if (pmtlNew)
        {
        FLOAT  fR = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        FLOAT  fG = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        FLOAT  fB = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        
        pmtlNew->SetDiffuseColor (fR, fG, fB);
        };
      }
    else if (parserIn.Compare ("map_Kd ", 7, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      parserIn.SetGreedyRead (FALSE);
      parserIn.GetQuoteString (&parserValue);
      
      if (pmtlNew)
        {
        // add the object path if the texture name is relative
        if (parserValue.GetAt (0) != '/')
          {
          parserValue = strPathIn + parserValue;
          };
        if (!parserIn.IsEOL ())  {pmtlNew->SetDiffuseTexture (parserValue);};
        };
      }
    else if (parserIn.Compare ("Ka ", 3, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      if (pmtlNew)
        {
        FLOAT  fR = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        FLOAT  fG = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        FLOAT  fB = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        
        pmtlNew->SetAmbientColor (fR, fG, fB);
        };
      }
    else if (parserIn.Compare ("map_Ka ", 7, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      parserIn.SetGreedyRead (FALSE);
      parserIn.GetQuoteString (&parserValue);
      
      if (pmtlNew)
        {
        // add the object path if the texture name is relative
        if (parserValue.GetAt (0) != '/')
          {
          parserValue = strPathIn + parserValue;
          };
        if (!parserIn.IsEOL ())  {pmtlNew->SetAmbientTexture (parserValue);};
        };
      }
    else if (parserIn.Compare ("Ks ", 3, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      if (pmtlNew)
        {
        FLOAT  fR = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        FLOAT  fG = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        FLOAT  fB = (!parserIn.IsEOL ()) ? parserIn.GetFloat () : 0.0f;
        
        pmtlNew->SetSpecularColor (fR, fG, fB);
        };
      }
    else if (parserIn.Compare ("map_Ks ", 7, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      parserIn.SetGreedyRead (FALSE);
      parserIn.GetQuoteString (&parserValue);
      
      if (pmtlNew)
        {
        // add the object path if the texture name is relative
        if (parserValue.GetAt (0) != '/')
          {
          parserValue = strPathIn + parserValue;
          };
        if (!parserIn.IsEOL ())  {pmtlNew->SetSpecularTexture (parserValue);};
        };
      }
    else if (parserIn.Compare ("Ns ", 3, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      // you should map the specular factor into our desired range here.
      // to be implemented
      if (pmtlNew)
        {
        // TODO: Hook up specular factor
        //if (!parserIn.IsEOL ())  {pmtlNew->SetAttrFloat (parserIn.GetFloat (), "Fs");};
        };
      }
    else if (parserIn.Compare ("map_Bump ", 9, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      parserIn.SetGreedyRead (FALSE);
      parserIn.GetQuoteString (&parserValue);
      
      if (pmtlNew)
        {
        // add the object path if the texture name is relative
        if (parserValue.GetAt (0) != '/')
          {
          parserValue = strPathIn + parserValue;
          };
        // TODO: Hook up bump map
        //pmtlNew->SetAttrString (parserValue, RStr ("bmap"));
        };
      }
    else if (parserIn.Compare ("d ", 2, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);
      
      if (pmtlNew)
        {
        // TODO: Hook up opacity
        //if (!parserIn.IsEOL ())  {pmtlNew->SetAttrFloat (parserIn.GetFloat (), "Opacity");};
        };
      }
    else if (parserIn.Compare ("map_d ", 6, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      parserIn.SetGreedyRead (FALSE);
      parserIn.GetQuoteString (&parserValue);
      
      if (pmtlNew)
        {
        // add the object path if the texture name is relative
        if (parserValue.GetAt (0) != '/')
          {
          parserValue = strPathIn + parserValue;
          };
        // TODO: Hook up opacity map
        //pmtlNew->SetAttrString (parserValue, RStr ("OpacityMap"));
        };
      }
    else if (parserIn.Compare ("Ni ", 3, parserIn.GetCursorStart ()) == 0)
      {
      parserIn.GetWord (NULL, true);

      if (pmtlNew)
        {
        // TODO: Hook up refraction
        //if (!parserIn.IsEOL ())  {pmtlNew->SetAttrFloat (parserIn.GetFloat (), "Refraction");};
        };
      }
      
      
    parserIn.GotoNextLine ();
    };
  return (EStatus::kSuccess);
  };


//------------------------------------------------------------------------
EStatus  ObjCodec::WriteMtl  (EditMesh &    editMeshIn,
                              RStrParser &  parserIn)
  {
  RStrParser       parserValue;
  FLOAT            fR; 
  FLOAT            fG; 
  FLOAT            fB; 
  FLOAT            fValue;
  


  //newmtl phong1SG
  //Kd  0 0.8000000119 0
  //Ka 0 0 0
  //Ks 0.5 0.5 0.5
  //Ns 200
  //illum 1


  //Ns = Phong specular component. Ranges from 0 to 1000. (I've seen various statements about this range (see below))
  //Kd = Diffuse color weighted by the diffuse coefficient.
  //Ka = Ambient color weighted by the ambient coefficient.
  //Ks = Specular color weighted by the specular coefficient.
  //d = Dissolve factor (pseudo-transparency). Values are from 0-1. 0 is completely transparent, 1 is opaque.
  //Ni = Refraction index. Values range from 1 upwards. A value of 1 will cause no refraction. A higher value implies refraction.
  //illum = (0, 1, or 2) 0 to disable lighting, 1 for ambient & diffuse only (specular color set to black), 2 for full lighting (see below)
  //sharpness = ? (see below)
  //map_Kd = Diffuse color texture map.
  //map_Ks = Specular color texture map.
  //map_Ka = Ambient color texture map.
  //map_Bump = Bump texture map.
  //map_d = Opacity texture map.
  //refl = reflection type and filename (?)


  // get a list of all materials to save.

  INT32  iNumShaders = editMeshIn.astrShaders.Length ();
  for (INT32  iShaderIndex = 0; iShaderIndex < iNumShaders; ++iShaderIndex)
    {
    Material *  pmtlToSave = Material::FindByName (editMeshIn.astrShaders [iShaderIndex]);

    if (pmtlToSave != NULL)
      {
      // We have the shader node
      
      parserIn.AppendFormat ("\nnewmtl %s\nillum 1\n", pmtlToSave->GetName ());

      const Color8U &  clrDiffuse = pmtlToSave->GetDiffuse ();
      parserIn.AppendFormat ("Kd %g %g %g\n", clrDiffuse.GammaR (), clrDiffuse.GammaG (), clrDiffuse.GammaB ());

      const char *  szDiffuseTexture = pmtlToSave->GetDiffuseTexture ();
      if ((szDiffuseTexture != NULL) && (!strcmp (szDiffuseTexture, "")))  
        {
        parserIn.AppendFormat ("map_Kd %s\n", szDiffuseTexture);
        };
      
      const Color8U &  clrAmbient = pmtlToSave->GetAmbient ();
      parserIn.AppendFormat ("Ka %g %g %g\n", clrAmbient.GammaR (), clrAmbient.GammaG (), clrAmbient.GammaB ());
      
      const char *  szAmbientTexture = pmtlToSave->GetAmbientTexture ();
      if ((szAmbientTexture != NULL) && (!strcmp (szAmbientTexture, "")))  
        {
        parserIn.AppendFormat ("map_Ka %s\n", szAmbientTexture);
        };

      const Color8U &  clrSpecular = pmtlToSave->GetDiffuse ();
      parserIn.AppendFormat ("Ks %g %g %g\n", clrSpecular.GammaR (), clrSpecular.GammaG (), clrSpecular.GammaB ());
      
      const char *  szSpecularTexture = pmtlToSave->GetSpecularTexture ();
      if ((szSpecularTexture != NULL) && (!strcmp (szSpecularTexture, "")))  
        {
        parserIn.AppendFormat ("map_Ks %s\n", szSpecularTexture);
        };

      // TODO: Hook up specular factor
      //if (pmtlToSave->GetAttr ("Fs") != NULL)
      //  {
      //  fValue = pmtlToSave->GetAttrFloat ("Fs");
      //  parserIn.AppendFormat ("Ns %g\n", fValue);
      //  };
      
      // TODO: Hook up bump map
      //if (pmtlToSave->GetAttr ("bmap") != NULL)
      //  {
      //  parserValue = pmtlToSave->GetAttrString ("bmap");
      //  parserIn.AppendFormat ("map_Bump %s\n", parserValue.AsChar ());
      //  };

      // TODO: Hook up opacity
      //if (pmtlToSave->GetAttr ("Opacity") != NULL)
      //  {
      //  fValue = pmtlToSave->GetAttrFloat ("Opacity");
      //  parserIn.AppendFormat ("d %g\n", fValue);
      //  };

      // TODO: Hook up opacity map
      //if (pmtlToSave->GetAttr ("OpacityMap") != NULL)
      //  {
      //  parserValue = pmtlToSave->GetAttrString ("OpacityMap");
      //  parserIn.AppendFormat ("map_d %s\n", parserValue.AsChar ());
      //  };

      // TODO: Hook up refraction
      //if (pmtlToSave->GetAttr ("Refraction") != NULL)
      //  {
      //  fValue = pmtlToSave->GetAttrFloat ("Refraction");
      //  parserIn.AppendFormat ("Ni %g\n", fValue);
      //  };
      };
    };

  return (EStatus::kSuccess);
  };

