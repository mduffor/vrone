/* -----------------------------------------------------------------
                             Ray

     This class implements a ray in space.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef RAY_HPP
#define RAY_HPP

#include "Types.hpp"
#include "RVec.hpp"

//------------------------------------------------------------------------
class Ray
  {
  public:
    RVec3           vecStart;
    RVec3           vecEnd;

    RVec3           vecWorldStart;
    RVec3           vecWorldEnd;

  public:
             Ray   ()                     {Clear ();};
    virtual  ~Ray  ()                     {};
    VOID     Clear ()                     {vecStart.Zero (); vecEnd.Zero (); vecWorldStart.Zero (); vecWorldEnd.Zero ();};
    VOID     Set   (RVec3 &  vecStartIn,
                    RVec3 &  vecEndIn)    {vecStart = vecStartIn; vecEnd   = vecEndIn;};
  };
typedef Ray *  PRay;


#endif // RAY_HPP