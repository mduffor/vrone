/* -----------------------------------------------------------------
                             Bounding Volume

     This is the base class for bounding volumes used for collision.
     It provides an abstraction for dealing with the various types
     of bounding volumes.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef BVOLUME_HPP
#define BVOLUME_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "CPoint.hpp"
#include "Ray.hpp"
#include "CPlane.hpp"
#include "CTriangle.hpp"

//------------------------------------------------------------------------
class BVolume
  {
  public:
    
  public:
                      BVolume        () {};
    virtual          ~BVolume        () {};
    virtual VOID     Clear           (VOID) = 0;

    virtual VOID     ToSphere        (RVec3 &      vecCenterOut,
                                      FLOAT &      fRadiusOut,
                                      FLOAT &      fRadiusSquaredOut) = 0;
    
    virtual BOOL     PointIntersect  (RVec3 &      vecPointIn) = 0;
    
    virtual BOOL     SphereIntersect (RVec3 &      vecSphereWorldPosIn,
                                      FLOAT        fSphereRadiusIn) = 0;

    virtual BOOL     RayIntersect    (Ray &        rayIn,
                                      CPoint *     ppntCollisionOut) = 0;
                            
    virtual BOOL     PlaneIntersect  (CPlane &     planeIn) = 0;

    virtual BOOL     TriIntersect    (CTriangle &  triangleIn) = 0;
    
  };

#endif // BVOLUME_HPP


