/* -----------------------------------------------------------------
                            OBJ Codec

     This class handles loading of the Wavefront OBJ format

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef OBJCODEC_HPP
#define OBJCODEC_HPP


#include "Types.hpp"
#include "RStrParser.hpp"
#include "BaseGeomCodec.hpp"
#include "RVec.hpp"
#include "RVecArray.hpp"
#include "IntArray.hpp"
#include "EditMesh.hpp"


//------------------------------------------------------------------------
class ObjCodec : public BaseGeomCodec
  {
  private:



  public:

    ObjCodec ();
    ~ObjCodec ();

    VOID     CalcNumElements  (RStrParser &  parserIn,
                               INT32 &       iCoordTextureCount,
                               INT32 &       iCoordNormalCount,
                               INT32 &       iCoordPointCount,
                               INT32 &       iGroupCount,
                               INT32 &       iFaceCount,
                               INT32 &       iCurveCount,
                               INT32 &       iVertCount);

    VOID     ParseData        (RStrParser &  parserIn,
                               INT32         iCoordTextureOffset,
                               INT32         iCoordNormalOffset,
                               INT32         iCoordPointOffset,
                               INT32         iFaceOffset,
                               INT32         iCurveOffset,
                               INT32         iVertOffset,
                               INT32         iGroupCount,
                               EditMesh &    editMeshOut);

    VOID     CalcNumElementsBin (RStrParser &  parserIn,
                                 INT32 &       iCoordTextureCount,
                                 INT32 &       iCoordNormalCount,
                                 INT32 &       iCoordPointCount,
                                 INT32 &       iGroupCount,
                                 INT32 &       iFaceCount,
                                 INT32 &       iCurveCount,
                                 INT32 &       iVertCount);
    /*
    VOID     ParseFaceBin       (RStrParser &  parserIn,
                                 INT           iNumFacesIn,
                                 BOOL          bHasTexIn,
                                 BOOL          bHasNormIn,
                                
                                 INT &         iFaceIndex,
                                 INT &         iFaceVertIndex,
                                 INT           iFaceShaderIndex,
                                 INT           iVertOffset,
                                 INT           iVertTextureOffset,
                                 INT           iVertNormalOffset,
                                 INT           iGroupCount,
                                
                                 IntArray *   aiFaceVTex,
                                 IntArray *   aiFaceVNorms,
                                 IntArray *   aiFacePoints,
                                 IntArray *   aiFaces,
                                 IntArray *   aiFaceVCount,
                                 IntArray *   aiFaceGroups,
                                 IntArray *   aiGroupPoints);
    */
    VOID  ParseVertListBin      (RStrParser &  parserIn,
                                 INT           iNumElemIn,
                                 BOOL          bHasTexIn,
                                 BOOL          bHasNormIn,                                  
                                 INT &         iElemIndex,
                                 INT &         iVertIndex,
                                 INT           iShaderIndex,
                                 INT           iCoordPointOffset,
                                 INT           iCoordTextureOffset,
                                 INT           iCoordNormalOffset,
                                 INT           iGroupCount,       
                                 EditMesh &    editMeshOut);

    VOID     ParseDataBin       (RStrParser &  parserIn,
                                 INT32         iCoordTextureOffset,
                                 INT32         iCoordNormalOffset,
                                 INT32         iCoordPointOffset,
                                 INT32         iFaceOffset,
                                 INT32         iCurveOffset,
                                 INT32         iVertOffset,
                                 INT32         iGroupCount,
                                 EditMesh &    editMeshOut);

                                                                 
    EStatus  Read             (RStrParser &  parserIn,
                               EditMesh &    editMeshOut,
                               BOOL          bAppendIn = FALSE,
                               BOOL          bBinaryIn = FALSE);

    EStatus  ReadFromFile     (RStr          strFileNameIn,
                               EditMesh &    editMeshOut,
                               BOOL          bAppendIn = FALSE,
                               BOOL          bBinaryIn = FALSE);

    EStatus  ReadMtl          (RStrParser &  parserIn,
                               RStr          strPathIn);

    EStatus  WriteMtl         (EditMesh &    editMeshIn,
                               RStrParser &  parserIn);

    EStatus  ReadMtlFromFile  (RStr          strFileNameIn,
                               RStr          strPathIn);

    EStatus  WriteMtlToFile   (RStr          strFileNameIn,
                               EditMesh &    editMeshIn);

    EStatus  Write            (RStrParser &  parserIn,
                               EditMesh &    editMeshIn,
                               BOOL          bWritePts = 1,
                               BOOL          bWriteUvs = 1,
                               BOOL          bWriteNmls = 1,
                               BOOL          bWriteFaces = 1,
                               BOOL          bWriteShaders = 1);

    EStatus  WriteBin         (RStrParser &  parserIn,
                               EditMesh &    editMeshIn,
                               BOOL          bWritePts = 1,
                               BOOL          bWriteUvs = 1,
                               BOOL          bWriteNmls = 1,
                               BOOL          bWriteFaces = 1,
                               BOOL          bWriteShaders = 1);
                               

    EStatus  WriteToFile      (RStr          strFileNameIn,
                               EditMesh &    editMeshIn,
                               BOOL          bBinaryIn = FALSE,
                               BOOL          bWritePts = 1,
                               BOOL          bWriteUvs = 1,
                               BOOL          bWriteNmls = 1,
                               BOOL          bWriteFaces = 1,
                               BOOL          bWriteShaders = 1);
  };


#endif  // OBJCODEC_HPP
