/* -----------------------------------------------------------------
                             Bounding Box

     This class implements bounding boxes used for collision 
     detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef BBOX_HPP
#define BBOX_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "CPoint.hpp"
#include "CPlane.hpp"
#include "BSphere.hpp"
#include "CTriangle.hpp"
#include "BVolume.hpp"

//------------------------------------------------------------------------
class BBox : public BVolume
  {
  public:
    RVec3           vecCenter;

    RVec3           vecHalfX;
    RVec3           vecHalfY;
    RVec3           vecHalfZ;

    RVec3           vecNormX;
    RVec3           vecNormY;
    RVec3           vecNormZ;

    RVec3           vecWorldCenter;

    RVec3           vecHalfLengths;
    RVec3           vecSqrHalfLengths;
    
    BOOL            bIsOBB;
    
  public:
              BBox   () {Clear ();};
    virtual  ~BBox   () {};
    VOID     Clear               () {vecCenter.Zero ();
                                     vecHalfX.Zero ();
                                     vecHalfY.Zero ();
                                     vecHalfZ.Zero ();
                                     vecNormX.Zero ();
                                     vecNormY.Zero ();
                                     vecNormZ.Zero ();
                                     vecWorldCenter.Zero ();
                                     vecHalfLengths.Zero ();
                                     vecSqrHalfLengths.Zero ();
                                     bIsOBB = FALSE;};
                               
    VOID     CalcOBB             (RVec3    vecLowLimitIn,
                                  RVec3    vecHighLimitIn);

    VOID     CalcAABB            (RVec3    vecLowLimitIn,
                                  RVec3    vecHighLimitIn);

    VOID     SetPosition         (RMatrix  matIn);
                                  
    VOID     PositionAABB        (RMatrix  matIn);

    VOID     PositionOBB         (RMatrix  matIn);
    
    VOID     ToSphere            (RVec3 &      vecCenterOut,
                                  FLOAT &      fRadiusOut,
                                  FLOAT &      fRadiusSquaredOut);

    BOOL     PointIntersect      (RVec3 &  vecPointIn);
    
    BOOL     SphereIntersect     (RVec3 &      vecSphereWorldPosIn,
                                  FLOAT        fSphereRadiusIn)     {return (bIsOBB) ? SphereOBBIntersect (vecSphereWorldPosIn, fSphereRadiusIn) : SphereAABBIntersect (vecSphereWorldPosIn, fSphereRadiusIn);};
                                  
    BOOL     SphereIntersect     (BSphere &    sphereIn)            {return (bIsOBB) ? SphereOBBIntersect (sphereIn.vecWorldCenter, sphereIn.fRadius) : SphereAABBIntersect (sphereIn.vecWorldCenter, sphereIn.fRadius);};
                                           
    BOOL     SphereAABBIntersect (RVec3 &      vecSphereWorldPosIn,
                                  FLOAT        fSphereRadiusIn);
   
    BOOL     SphereOBBIntersect  (RVec3 &      vecSphereWorldPosIn,
                                  FLOAT        fSphereRadiusIn);
    
    BOOL     RayIntersect        (Ray &        rayIn,
                                  CPoint *     ppntCollisionOut);

    BOOL     PlaneIntersect      (CPlane &     planeIn)             {return (bIsOBB) ? PlaneOBBIntersect (planeIn) : PlaneAABBIntersect (planeIn);};

    BOOL     PlaneAABBIntersect  (CPlane&      planeIn);
   
    BOOL     PlaneOBBIntersect   (CPlane&      planeIn);
   
    BOOL     AABBIntersect       (BBox &       bbBox2In);
   
    BOOL     OBBIntersect        (BBox &       bbBox2In);
   
    BOOL     TriIntersect        (CTriangle &  triangleIn);
                            
  };
typedef BBox *  PBBox;




#endif // BBOX_HPP
