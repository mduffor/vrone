/* -----------------------------------------------------------------
                             Frustum

     This class implements a camera viewing frustum and clipping 
     against it.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef FRUSTUM_HPP
#define FRUSTUM_HPP

#include "Types.hpp"
#include "CPlane.hpp"
#include "RVec.hpp"
#include "BVolume.hpp"

// bounding planes are defined in the order that should provide the fastest
//   removal of objects from consideration.  Bounding plane order for Frustum.

#define BPLANE_NEAR          0
#define BPLANE_LEFT          1
#define BPLANE_RIGHT         2
#define BPLANE_TOP           3
#define BPLANE_BOTTOM        4
#define BPLANE_FAR           5


// flags used in the Cohen-Sutherland algorithms

#define BM_CLIP_NEAR         0x00000001
#define BM_CLIP_FAR          0x00000002
#define BM_CLIP_LEFT         0x00000004
#define BM_CLIP_RIGHT        0x00000008
#define BM_CLIP_TOP          0x00000010
#define BM_CLIP_BOTTOM       0x00000020


#define RFRUSTUM_NUM_PLANES   6

//------------------------------------------------------------------------
class Frustum
//------------------------------------------------------------------------
  {
  public:
    UINT         uPlaneMask;
    CPlane       v4Planes [RFRUSTUM_NUM_PLANES];

    FLOAT        fNear;
    FLOAT        fFar;
    FLOAT        fXDelta;
    FLOAT        fYDelta;

    FLOAT        fAspect;
    FLOAT        fFOVY;

    RVec3        v2HighCorner;
    RVec3        v2LowCorner;

  public:

             Frustum           ();

             ~Frustum          ();

    VOID     SetNearFar          (FLOAT  fNearIn,
                                  FLOAT  fFarIn);

    VOID     SetCorners          (RVec3&  v2HighIn,
                                  RVec3&  v2LowIn);

    VOID     SetFOV              (FLOAT  fFOVYIn);

    VOID     SetAspect           (FLOAT  fAspectIn);

    FLOAT    GetFOVY             (VOID)                                 {return fFOVY;};

    FLOAT    GetAspect           (VOID)                                 {return fAspect;};

    BOOL     InFrustum           (BVolume&          volumeIn,
                                  RMatrix&          m4ModelMatrixIn);

    BOOL     InFrustum           (BVolume&          volumeIn);

    VOID     CalcBoundingPlanes  (RMatrix&  m4FacingMatrixIn);



  };





#endif // FRUSTUM_HPP
