/* -----------------------------------------------------------------
                             Bounding Cylinder

     This class implements a cylinder used for collision detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 


#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "BCylinder.hpp"
#include "Ray.hpp"

#define CYL_CLIP_TOP          0x00000010
#define CYL_CLIP_BOTTOM       0x00000020


inline VOID    SetLine       (RVec3& ln, 
                              RVec3& v2Norm, 
                              RVec3& v2Point)    {ln.fX = v2Norm.fY; ln.fY = v2Norm.fX; ln.fW = - (v2Point * ln);};
                              
                              
//------------------------------------------------------------------------
BOOL BCylinder::PointIntersect  (RVec3&       vecPointIn)
  {
  // TODO
  return (FALSE);
  };

//------------------------------------------------------------------------
BOOL BCylinder::PlaneIntersect  (CPlane&     planeIn,
                                 PFLOAT      pfPlanePushOut,
                                 RVec3&      vecPushPoint)
  {
  RVec3             vecEnd1;          // end 1
  FLOAT             fDistance1;
  RVec3             vecEnd2;          // end 2
  FLOAT             fDistance2;

  RVec3             vecToPlane;       // normal from the center towards the plane
  RVec3             vecDelta;

  RVec3             vecSide;
  RVec3             vecToEdge;

  RVec3             vecEdge;
  FLOAT             fEdgeDistance;

  RVec3             vecHighest;
  FLOAT             fHighestDistance;
  RVec3             vecLowest;
  FLOAT             fLowestDistance;



  vecDelta = vecAxisNorm * fHalfHeight;

  vecEnd1 = vecWorldCenter + vecDelta;
  vecEnd2 = vecWorldCenter - vecDelta;

  fDistance1 = planeIn.Distance (vecEnd1);
  fDistance2 = planeIn.Distance (vecEnd2);

  vecSide = planeIn.GetNormal () % vecToPlane;
  vecSide.Normalize ();

  if (vecSide.LengthSquared () < R_EPSILON)
    {
    // perpendicular to plane normal.  Use end point as nearest point.

    if (((fDistance1 >= -R_EPSILON) && (fDistance2 >= -R_EPSILON)) ||
        ((fDistance1 <=  R_EPSILON) && (fDistance2 <=  R_EPSILON)))
      {
      // Both ends are on the same side of the plane.  no hit.
      return (FALSE);
      };

    if (pfPlanePushOut != NULL)
      {
      // ends are on opposite sides.  A compare of distance 1 to distance 2 will give
      //   the negative most distance
      if (fDistance1 < fDistance2)
        {
        *pfPlanePushOut = -fDistance1;
        vecPushPoint = vecEnd1;
        }
      else
        {
        *pfPlanePushOut = -fDistance2;
        vecPushPoint = vecEnd2;
        };
      };
    return (TRUE);
    };


  vecToEdge = vecSide % vecAxisNorm;

  // since the plane normal is normalized and ToPlane should be normalized,
  //  we don't have to normalize ToEdge.

  vecDelta = vecToEdge * fRadius;

  // we need to calculate the 4 edge points on the cylinder.  Then we need to find
  //   the greatest and least of these to determine both if the cylinder is
  //   intersected by the plane and what the furthest negative point is.

  fHighestDistance = 0.0f;
  fLowestDistance  = 0.0f;

  // calc first edge point and initialize other variables with it.
  vecHighest = vecEnd1 + vecDelta;
  vecLowest  = vecHighest;

  fHighestDistance = planeIn.Distance (vecHighest);
  fLowestDistance  = fHighestDistance;

  // now check remaining 3 edge points and check for highest/lowest

  vecEdge = vecEnd1 - vecDelta;
  fEdgeDistance = planeIn.Distance (vecEdge);
  if (fEdgeDistance > fHighestDistance)  {vecHighest = vecEdge; fHighestDistance = fEdgeDistance;}
  else if (fEdgeDistance < fLowestDistance)  {vecLowest = vecEdge; fLowestDistance = fEdgeDistance;};

  vecEdge = vecEnd2 + vecDelta;
  fEdgeDistance = planeIn.Distance (vecEdge);
  if (fEdgeDistance > fHighestDistance)  {vecHighest = vecEdge; fHighestDistance = fEdgeDistance;}
  else if (fEdgeDistance < fLowestDistance)  {vecLowest = vecEdge; fLowestDistance = fEdgeDistance;};

  vecEdge = vecEnd2 - vecDelta;
  fEdgeDistance = planeIn.Distance (vecEdge);
  if (fEdgeDistance > fHighestDistance)  {vecHighest = vecEdge; fHighestDistance = fEdgeDistance;}
  else if (fEdgeDistance < fLowestDistance)  {vecLowest = vecEdge; fLowestDistance = fEdgeDistance;};


  // now determine if the highest and lowest points are on the same side or different
  //  sides of the plane


  if ((fLowestDistance >= -R_EPSILON) || (fHighestDistance <=  R_EPSILON))
    {
    // Both ends are on the same side of the plane.  no hit.
    return (FALSE);
    };

  // intersection
  if (pfPlanePushOut != NULL)
    {
    *pfPlanePushOut = -fLowestDistance;
    vecPushPoint = vecLowest;
    };

  return (TRUE);
  };


//------------------------------------------------------------------------
BOOL BCylinder::RayIntersect  (Ray &     rayIn,
                               CPoint *  ppntCollisionOut)
  {
  RVec3            vecRC;
  RVec3            vecRay;
  RVec3            vecPerp;
  FLOAT            fDistance;
  RVec3            vecAxisToRayPerp;
  RVec3            vecDownAxis;
  RVec3            vecO;
  FLOAT            fT;
  FLOAT            fT1;
  FLOAT            fT2;
  FLOAT            fTDelta;
  FLOAT            fPerpLength;
  RVec3            vecPoint1;
  RVec3            vecPoint2;
  FLOAT            fDistance1;
  FLOAT            fDistance2;
  CPlane           v4Plane;



  vecRay = rayIn.vecWorldEnd - rayIn.vecWorldStart;
  vecRay.Normalize ();

  vecRC = rayIn.vecWorldStart - vecWorldCenter;
  vecPerp = vecRay % vecAxisNorm;

  fPerpLength = vecPerp.Length ();

  if (fPerpLength < R_EPSILON)
    {
    // ray is parallel to cylinder

    fDistance = vecRC * vecAxisNorm;
    vecDownAxis = vecAxisNorm * fDistance;

    vecAxisToRayPerp = vecRC - vecDownAxis;

    fDistance = vecAxisToRayPerp.LengthSquared ();

    if (fDistance <= fRadiusSquared)
      {
      // TODO
      // ray collides with one of the end-caps.  Find the sphere cap center
      //  that is closest to the ray's vecWorldStart, and find the intersect
      //  with the cap plane.
      return (TRUE);
      };
    return (FALSE);
    };

  vecPerp.Normalize ();

  fDistance = fabsf (vecRC * vecPerp);  // shortest distance

  if (fDistance > fRadius)
    {
    return (FALSE);
    };

  // we now know we hit the infinite cylinder.
  //  now we have to figure out if we hit the bounded cylinder.

  vecO = vecRC % vecAxisNorm;

  fT = - (vecO * vecPerp) / fPerpLength;

  vecO = vecPerp % vecAxisNorm;
  vecO.Normalize ();

  fTDelta = fabsf (sqrtf (fRadiusSquared - (fDistance * fDistance)) / (vecRay * vecO));

  fT1 = fT - fTDelta;
  fT2 = fT + fTDelta;

  // now we have the time value for the ray.  We need to find
  //  the actual points, and see if they are within the cylinder's
  //  endcaps.

  vecPoint1 = (vecRay * fT1) + rayIn.vecWorldStart;
  vecPoint2 = (vecRay * fT2) + rayIn.vecWorldStart;

  // to miss the cylinder, both entry and exit points must be on
  //  the same side of the bounding planes.  Since the bounding
  //  planes are parallel, we only need one plane, and we make this
  //  plane pass through the center of the cylinder

  v4Plane.Set (vecAxisNorm, vecWorldCenter);

  fDistance1 = v4Plane.Distance (vecPoint1);
  fDistance2 = v4Plane.Distance (vecPoint2);

  if (((fDistance1 <= -fRadius) && (fDistance2 <= -fRadius)) ||
      ((fDistance1 >=  fRadius) && (fDistance2 >=  fRadius)))
    {
    // both points are on one side of the plane.
    return (FALSE);
    };
    
  // TODO
  // If both points are on the positive side of both planes, use the closest
  // distance to find the collision point.  If the collision points straddle
  // a plane, collide the ray with the plane to get the intersect.
    
  return (TRUE);
  };


//------------------------------------------------------------------------
BOOL BCylinder::SphereIntersect  (RVec3 &      vecSphereWorldPosIn,
                                  FLOAT        fSphereRadiusIn)
  {
  FLOAT            fT;
  FLOAT            fAbsT;
  RVec3            vecRayToPoint;
  RVec3            vecClosestPoint;
  FLOAT            fSquaredDist;
  RVec3            vecRayToCenterPerp;
  FLOAT            fRadiusSumSquared;
  FLOAT            fCenterToPlane;
  FLOAT            fIntersectRadius;
  FLOAT            fIntersectRadiusSqr;
  FLOAT            fSphereRadiusSquared = fSphereRadiusIn * fSphereRadiusIn;


  // first project the center of the sphere onto the cylinder axis


  vecRayToPoint = vecSphereWorldPosIn - vecWorldCenter;

  // calc time
  fT = vecRayToPoint * vecAxisNorm;

  // calc closest point
  vecClosestPoint = (vecAxisNorm * fT) + vecWorldCenter;

  // calc distance between axis ray and sphere center

  vecRayToCenterPerp = vecWorldCenter - vecClosestPoint;
  fSquaredDist = vecRayToCenterPerp.LengthSquared ();

  fAbsT = fabsf (fT);

  if (fAbsT <= fHalfHeight)
    {
    // sphere is within the main part of the cylinder

    // we want to check if Distance <= Radius1 + Radius2.  Squaring both sides gives us
    // SquaredDistance <= Radius1Squared + Radius2Squared + 2*Radius1*Radius2

    fRadiusSumSquared = fRadiusSquared + fSphereRadiusSquared +
                        (2.0f * fRadius + fSphereRadiusIn);

    if (fSquaredDist <= fRadiusSumSquared) return (TRUE);
    return (FALSE);
    };

  if (fAbsT > (fHalfHeight + fSphereRadiusIn))
    {
    // sphere is completely beyond end cap plane.  No chance of hitting.
    return (FALSE);
    };

  // We now know that the sphere intersects one or both of the endcap
  //  planes.  We also know that the center is outside of the main part
  //  of the cylinder, so we only have to check the closest plane.  If we
  //  simply use the absolute value of T, we only need to calculate against
  //  the positive plane

  fCenterToPlane = fAbsT - fHalfHeight;

  // the vector from the sphere center to the plane (A), the vector from that intersection
  //  with the plane to the edge where the plane and sphere intersect (B), and then the vector
  //  from that point back to the sphere center (C) forms a right triangle.
  //  We just calculated the distance of A and the distance of C is the sphere radius.  So now
  //  we can solve for B.  A^2 + B^2 = C^2  so  B^2 = C^2 - A^2

  fIntersectRadiusSqr = fSphereRadiusSquared - (fCenterToPlane * fCenterToPlane);
  fIntersectRadius    = sqrtf (fIntersectRadiusSqr);


  fRadiusSumSquared = fRadiusSquared + fIntersectRadiusSqr +
                      (2.0f * fRadius + fIntersectRadius);

  if (fSquaredDist <= fRadiusSumSquared) return (TRUE);
  return (FALSE);
  };



//------------------------------------------------------------------------
BOOL BCylinder::TriIntersect  (CTriangle &  triangleIn,
                               FLOAT        fRisePoint)
  {
  RVec3             vecSourceEdge [3];
  RVec3             vecSourceList [4];
  UINT              uFlags      [4];
  RVec3             vecVertexList [7];
  RVec3             v2EdgeVec    [7];
  FLOAT            fPerpT       [7];
  UINT              ulCurrVert;
  UINT              ulIndex;
  //RMatrix             m4WorldToCylinder;
  RVec3             vecCylinderOrigin;
  FLOAT            fCylinderHeight;
  FLOAT            fT;
  FLOAT            fVX;
  FLOAT            fVY;
  FLOAT            fVZ;
  RVec3             v2Point;
  UINT              ulTotalFlags;
  BOOL               bHit;
  FLOAT            fCrossX;
  UINT              ulCrossings;
  RVec3             vecTriNormal;
  RVec3             v2RevNormal;
  RVec3             v2PerpVec;
  RVec3             v2NearestPoint;
  RVec3             v2TangentPoint;
  RVec3             vecTangentLine;   // note:  I'm leaving lines as RVec3 for now.
  RVec3             vecVertexLine;
  RVec3             v2OldPoint0;
  RVec3             v2NewPoint0;
  FLOAT            fDenom;
  FLOAT            fPushback = 0.0f;
  FLOAT            fDist;
  RVec3             vecQuad;
  RVec3             v2Point1;
  RVec3             v2Point2;
  RVec3             v2Vert1;
  RVec3             v2Vert2;
  FLOAT            fPart;
  RVec3             vecPlaneNorm;
  RVec3             vecCylEdgeVec;
  FLOAT            fHighestZ;
  FLOAT            fB;
  FLOAT            fC;
  FLOAT            fZ;
  RVec3             v2Edge;
  RVec3             vecUp (0.0f, 0.0f, 1.0f);
  Ray               ray;
  CPoint            pntCollision;
  CTriangle         triangleOut;
  RVec3             vecScaledAxis;
  RVec3             vecHighestPoint;
  RVec3             vecLowestPoint;


  // initialize the source array with the tri vertices which have been
  //   pre-transformed to cylinder object space.

  vecSourceList [0] = triangleIn.vec0;
  vecSourceList [1] = triangleIn.vec1;
  vecSourceList [2] = triangleIn.vec2;
  vecSourceList [3] = vecSourceList [0];


  // calculate an AABB for the triangle, and do a quick AABB check first
  //  before going into the more involved cylinder vs tri check.

  // Note:  There should be a way to reduce the number of calcs performed below by
  //         using the compares for calculating the higest point to also calc
  //         the lowest point.

  // Note:  I'm not sure exactly how much of a speedup performing the AABB check
  //         really is.  It SEEMS like it provides a little speedup, but one that
  //         is hardly detectable on the framerate.  I'm going to leave it for now,
  //         but I'll have to wait till I get a good profiler up and running again (VTune)
  //         before making a final determination whether this check stays in or not.

  vecHighestPoint.fX = RMax (triangleIn.vec0.fX, RMax (triangleIn.vec1.fX, triangleIn.vec2.fX));
  vecHighestPoint.fY = RMax (triangleIn.vec0.fY, RMax (triangleIn.vec1.fY, triangleIn.vec2.fY));
  vecHighestPoint.fZ = RMax (triangleIn.vec0.fZ, RMax (triangleIn.vec1.fZ, triangleIn.vec2.fZ));

  vecLowestPoint.fX = RMin (triangleIn.vec0.fX, RMin (triangleIn.vec1.fX, triangleIn.vec2.fX));
  vecLowestPoint.fY = RMin (triangleIn.vec0.fY, RMin (triangleIn.vec1.fY, triangleIn.vec2.fY));
  vecLowestPoint.fZ = RMin (triangleIn.vec0.fZ, RMin (triangleIn.vec1.fZ, triangleIn.vec2.fZ));


  // perform the AABB test

  if ((vecHighestPoint.fX < -fRadius)     || (vecLowestPoint.fX > fRadius) ||
      (vecHighestPoint.fY < -fRadius)     || (vecLowestPoint.fY > fRadius) ||
      (vecHighestPoint.fZ < -fHalfHeight) || (vecLowestPoint.fZ > fHalfHeight))
    {
    // bounding boxes don't collide.  No collision
    return (FALSE);
    };


  // we go ahead and use the CYL_CLIP_TOP and CYL_CLIP_BOTTOM flags here because
  //  they are named what we want and we only need the two of them.

  // first do a quick check to see whether vertices are above, below, or within cap planes.
  ulTotalFlags = 0xffffffff;
  fCylinderHeight = (fHalfHeight * 2.0f);
  for (ulIndex = 0; ulIndex < 3; ++ulIndex)
    {
    // each vertex can be above, below, or in the cylinder
    //  planes, but only one at a time.

    if ((vecSourceList [ulIndex]).fZ < -R_EPSILON)
      {
      uFlags [ulIndex] = CYL_CLIP_BOTTOM;
      }
    else if ((vecSourceList [ulIndex]).fZ > fCylinderHeight)
      {
      uFlags [ulIndex] = CYL_CLIP_TOP;
      }
    else
      {
      uFlags [ulIndex] = 0;
      };
    ulTotalFlags &= uFlags [ulIndex];
    };
  uFlags [3] = uFlags [0];


  // check if tri is completely above or below cylinder.
  if (ulTotalFlags != 0) return (FALSE);

  // step through vertex tris and clip to cap planes.  Store resulting poly in vertex list.
  ulCurrVert = 0;
  for (ulIndex = 0; ulIndex < 3; ++ulIndex)
    {
    if (uFlags [ulIndex] == 0)
      {
      // add self to list
      vecVertexList [ulCurrVert] = vecSourceList [ulIndex];
      ++ulCurrVert;

      if (uFlags [ulIndex + 1] == CYL_CLIP_BOTTOM)
        {
        // clip to plane Z = 0
        fT = - vecSourceList [ulIndex].fZ / (vecSourceList [ulIndex + 1].fZ - vecSourceList [ulIndex].fZ);

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * (vecSourceList [ulIndex + 1].fX - vecSourceList [ulIndex].fX);
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * (vecSourceList [ulIndex + 1].fY - vecSourceList [ulIndex].fY);
        ++ulCurrVert;
        }
      else if (uFlags [ulIndex + 1] == CYL_CLIP_TOP)
        {
        // clip to plane Z = fCylinderHeight
        fT = (fCylinderHeight - vecSourceList [ulIndex].fZ) / (vecSourceList [ulIndex + 1].fZ - vecSourceList [ulIndex].fZ);

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * (vecSourceList [ulIndex + 1].fX - vecSourceList [ulIndex].fX);
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * (vecSourceList [ulIndex + 1].fY - vecSourceList [ulIndex].fY);
        ++ulCurrVert;
        };
      }
    else if (uFlags [ulIndex] == CYL_CLIP_BOTTOM)
      {
      // starting off below cylinder

      if (uFlags [ulIndex + 1] == 0)
        {
        // clip to plane Z = 0
        fT = - vecSourceList [ulIndex].fZ / (vecSourceList [ulIndex + 1].fZ - vecSourceList [ulIndex].fZ);

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * (vecSourceList [ulIndex + 1].fX - vecSourceList [ulIndex].fX);
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * (vecSourceList [ulIndex + 1].fY - vecSourceList [ulIndex].fY);
        ++ulCurrVert;
        }
      else if (uFlags [ulIndex + 1] == CYL_CLIP_TOP)
        {
        // large move from below cylinder to above.  Clip against both planes.

        fVX = (vecSourceList [ulIndex + 1].fX - vecSourceList [ulIndex].fX);
        fVY = (vecSourceList [ulIndex + 1].fY - vecSourceList [ulIndex].fY);
        fVZ = (vecSourceList [ulIndex + 1].fZ - vecSourceList [ulIndex].fZ);

        // clip to plane Z = 0
        fT = - vecSourceList [ulIndex].fZ / fVZ;

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * fVX;
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * fVY;
        ++ulCurrVert;

        // clip to plane Z = fCylinderHeight
        fT = (fCylinderHeight - vecSourceList [ulIndex].fZ) / fVZ;

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * fVX;
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * fVY;
        ++ulCurrVert;
        };
      }
    else if (uFlags [ulIndex] == CYL_CLIP_TOP)
      {
      // starting off above cylinder

      if (uFlags [ulIndex + 1] == 0)
        {
        // clip to plane Z = fCylinderHeight
        fT = (fCylinderHeight - vecSourceList [ulIndex].fZ) / (vecSourceList [ulIndex + 1].fZ - vecSourceList [ulIndex].fZ);

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * (vecSourceList [ulIndex + 1].fX - vecSourceList [ulIndex].fX);
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * (vecSourceList [ulIndex + 1].fY - vecSourceList [ulIndex].fY);
        ++ulCurrVert;
        }
      else if (uFlags [ulIndex + 1] == CYL_CLIP_BOTTOM)
        {
        // large move from below cylinder to above.  Clip against both planes.

        fVX = (vecSourceList [ulIndex + 1].fX - vecSourceList [ulIndex].fX);
        fVY = (vecSourceList [ulIndex + 1].fY - vecSourceList [ulIndex].fY);
        fVZ = (vecSourceList [ulIndex + 1].fZ - vecSourceList [ulIndex].fZ);

        // clip to plane Z = fCylinderHeight
        fT = (fCylinderHeight - vecSourceList [ulIndex].fZ) / fVZ;

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * fVX;
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * fVY;
        ++ulCurrVert;

        // clip to plane Z = 0
        fT = - vecSourceList [ulIndex].fZ / fVZ;

        vecVertexList [ulCurrVert].fX = vecSourceList [ulIndex].fX + fT * fVX;
        vecVertexList [ulCurrVert].fY = vecSourceList [ulIndex].fY + fT * fVY;
        ++ulCurrVert;
        };
      };
    };

  // duplicate the first vertex into the last vertex to close the object
  vecVertexList [ulCurrVert] = vecVertexList [0];
  ++ulCurrVert;

  // At least some of the triangle is between the two endcap clipping planes.
  //   we must now see if the cylinder intersects the resulting clipped poly.

  // first check the distance from each line segment to the cylinder

  bHit = FALSE;

  for (ulIndex = 0; ulIndex < ulCurrVert - 1; ++ulIndex)
    {
    // create an un-normalized vector for the edge.
    v2EdgeVec [ulIndex].fX = vecVertexList [ulIndex + 1].fX - vecVertexList [ulIndex].fX;
    v2EdgeVec [ulIndex].fY = vecVertexList [ulIndex + 1].fY - vecVertexList [ulIndex].fY;

    v2Vert1.fX = vecVertexList [ulIndex].fX;
    v2Vert1.fY = vecVertexList [ulIndex].fY;

    v2Vert2.fX = vecVertexList [ulIndex + 1].fX;
    v2Vert2.fY = vecVertexList [ulIndex + 1].fY;

    // find the t value for the edge vector to see if the point on the line perpendicular
    //  to the cylinder center lies within the line segment of the edge or not.

    // given point R and line P(t) = P0 + tv, t = ((R-P0) dot v) / (v dot v)
    // since R is the origin, you have (-P0 dot v) / (v dot v), or -(P0 dot v) / (v dot v)

    fPerpT [ulIndex] = - (v2EdgeVec [ulIndex] * v2Vert1) / (v2EdgeVec [ulIndex] * v2EdgeVec [ulIndex]);

    if (fPerpT [ulIndex] < 0.0f)
      {
      // distance check the first point of the line

      if (v2Vert1.LengthSquared () < fRadiusSquared)
        {
        bHit = TRUE;
        };
      }
    else if (fPerpT [ulIndex] > 1.0f)
      {
      // distance check the second point of the line
      if (v2Vert2.LengthSquared () < fRadiusSquared)
        {
        bHit = TRUE;
        };
      }
    else
      {
      // distance check the center of the line

      v2Point = v2EdgeVec [ulIndex] * fPerpT [ulIndex];
      v2Point += v2Vert1;

      if (v2Point.LengthSquared () < fRadiusSquared)
        {
        bHit = TRUE;
        };
      };
    };


  if (bHit == FALSE)
    {
    // none of the line segments intersect the circle.  Do a point in poly check to see if
    //  the circle might be completely within the poly.

    // we will do our point in poly check by casting a ray down the X axis from the origin,
    //   and count how many edges we cross.

    ulCrossings = 0;
    for (ulIndex = 0; ulIndex < ulCurrVert - 1; ++ulIndex)
      {
      if (vecVertexList [ulIndex].fY < 0.0f)
        {
        if (vecVertexList [ulIndex + 1].fY > 0.0f)
          {
          // possible cross

          fT = - vecVertexList [ulIndex].fY / (vecVertexList [ulIndex + 1].fY - vecVertexList [ulIndex].fY);

          fCrossX = vecVertexList [ulIndex].fX + fT * (vecVertexList [ulIndex + 1].fX - vecVertexList [ulIndex].fX);

          if (fCrossX > 0.0) {++ulCrossings;};
          };
        }
      else if (vecVertexList [ulIndex].fY > 0.0f)
        {
        if (vecVertexList [ulIndex + 1].fY < 0.0f)
          {
          // possible cross

          fT = - vecVertexList [ulIndex].fY / (vecVertexList [ulIndex + 1].fY - vecVertexList [ulIndex].fY);

          fCrossX = vecVertexList [ulIndex].fX + fT * (vecVertexList [ulIndex + 1].fX - vecVertexList [ulIndex].fX);

          if (fCrossX > 0.0) {++ulCrossings;};
          };
        }
      else
        {
        // exactly on the axis

        if (vecVertexList [ulIndex].fX > 0.0) {++ulCrossings;};
        };
      };
    if (ulCrossings & 0x00000001)
      {
      bHit = TRUE;
      };
    };


  if (bHit == FALSE) return (FALSE);

  // we now know that we had an intersection.  Unfortunately we can't stop there.  We must
  //   calculate the pushback distance so that the cylinder is no longer intersecting the tri.

  // calculate the normalized 3D edge vectors for each edge.  We will use these below.

  for (ulIndex = 0; ulIndex < 3; ++ulIndex)
    {
    vecSourceEdge [ulIndex] = vecSourceList [ulIndex + 1] - vecSourceList [ulIndex];
    vecSourceEdge [ulIndex].Normalize ();
    };

  // first calculate the height of the axis push.  Compare that to the RisePoint to see if we can move up

  // calculate the normal of the triangle.
  // normal is AB cross BC

  vecTriNormal = vecSourceEdge [0] % vecSourceEdge [1];
  vecTriNormal.Normalize ();

  // There are three possible constructs that define the max axis push distance for a cylinder.  They are:
  //   1) original vertices within the cylinder radius
  //   2) original edges crossing the cylinder radius
  //   3) rays on the perpendicular edges of the cylinder intersecting the original tri.

  fHighestZ = 0.0f;

  // first check original vertices.

  for (ulIndex = 0; ulIndex < 3; ++ulIndex)
    {
    if (vecSourceList [ulIndex].LengthSquared () <= fRadiusSquared)
      {
      if (vecSourceList [ulIndex].fZ > fHighestZ)
        {
        fHighestZ = vecSourceList [ulIndex].fZ;
        };
      };
    };

  // Next check original edges crossing infinite cylinder radius

  for (ulIndex = 0; ulIndex < 3; ++ulIndex)
    {
    // calculate the edge vector

    v2Edge.fX = vecSourceEdge [ulIndex].fX;
    v2Edge.fY = vecSourceEdge [ulIndex].fY;
    fDist = v2Edge.Length ();
    v2Edge /= fDist;

    // check to make sure you aren't mostly perpendicular to the axis

    if (fabsf (vecUp * vecSourceEdge [ulIndex]) > 0.99)
      {
      // mostly parallel.  Ignore.
      }
    else
      {
      // although I use Vec3 structures here, I'm doing Vec2 calculations so I'm working in 2D.

      v2Vert1.fX = vecSourceList [ulIndex].fX;
      v2Vert1.fY = vecSourceList [ulIndex].fY;

      fC = (v2Vert1 * v2Vert1) - fRadiusSquared;
      fB = v2Edge * v2Vert1;

      fPart = (fB * fB) - fC;
      if (fPart >= 0.0)
        {
        // intersection with infinite line
        fPart = sqrtf (fPart);
        fT = - fB + fPart;
        if ((fT >= 0.0) && (fT <= fDist))
          {
          // intersection with segment.  Calc z coordinate of 3d collision.
          fZ = vecSourceList [ulIndex].fZ + (vecSourceEdge [ulIndex].fZ * fT);
          if (fZ > fHighestZ)
            {
            fHighestZ = fZ;
            };
          };
        };

      fPart = (fB * fB) + fC;
      if (fPart >= 0.0)
        {
        // intersection with infinite line
        fPart = sqrtf (fPart);
        fT = - fB + fPart;
        if ((fT >= 0.0) && (fT <= fDist))
          {
          // intersection with segment.  Calc z coordinate of 3d collision.
          fZ = vecSourceList [ulIndex].fZ + (vecSourceEdge [ulIndex].fZ * fT);
          if (fZ > fHighestZ)
            {
            fHighestZ = fZ;
            };
          };
        };
      };
    };


  // Finally, check the rays on the perpendicular edge of the cylinder for collision with original tri

  if (fabsf (vecTriNormal * vecUp) < R_EPSILON)
    {
    // triangle and cylinder are parallel for all practical purposes.  Their collision would have been handled above.
    }
  else
    {
    // calculate the plane normal for the plane perp to the tri and containing the cylinder axis
    vecPlaneNorm = vecTriNormal % vecUp;
    vecPlaneNorm.Normalize ();

    // get the vector in the above plane pointing out from the cylinder.
    vecCylEdgeVec = vecPlaneNorm % vecUp;
    // since the plane normal and axis normal are unit length, you don't have to normalize the edge vec

    vecCylEdgeVec *= fRadius;

    // since the cylinder is centered at the origin and its bottom plane is at the origin, the
    //  perp vector and its negative give us the two base points of the cylinder.

    triangleOut.vec0 = vecSourceList [0];
    triangleOut.vec1 = vecSourceList [1];
    triangleOut.vec2 = vecSourceList [2];

    ray.vecWorldStart = vecCylEdgeVec;
    ray.vecWorldEnd   = ray.vecWorldStart + vecUp;

    if (triangleOut.RayIntersect (ray.vecWorldStart, ray.vecWorldEnd, &pntCollision) == TRUE)
      {
      // since the ray starts at z=0 and the ray vector was unit length, the time value will
      //  give us the z value at the intersection

      if (pntCollision.fTime > fHighestZ)
        {
        fHighestZ = pntCollision.fTime;
        };
      };

    ray.vecWorldStart = -vecCylEdgeVec;
    ray.vecWorldEnd   = ray.vecWorldStart + vecUp;

    if (triangleOut.RayIntersect (ray.vecWorldStart, ray.vecWorldEnd, &pntCollision) == TRUE)
      {
      // since the ray starts at z=0 and the ray vector was unit length, the time value will
      //  give us the z value at the intersection

      if (pntCollision.fTime > fHighestZ)
        {
        fHighestZ = pntCollision.fTime;
        };
      };
    };


  if (fHighestZ <= fRisePoint)
    {
    // push up is the valid course of action.


    }
  else
    {
    // if the axis push fails (too high) calculate the push along the flat projection of the triangle normal.

    // make sure the tri normal and the cylinder axis aren't too parallel.

    // calculate the reversed projected flat 2D normal.

    v2RevNormal.fX = -vecTriNormal.fX;
    v2RevNormal.fY = -vecTriNormal.fY;

    v2RevNormal.Normalize ();

    // step through each edge.  Calculate the perpendicular vector.  Find the edge point on the circle.

    for (ulIndex = 0; ulIndex < ulCurrVert - 1; ++ulIndex)
      {
      // calc nearest point
      v2NearestPoint = v2EdgeVec [ulIndex] * fPerpT [ulIndex];
      v2NearestPoint.fX += vecVertexList [ulIndex].fX;
      v2NearestPoint.fY += vecVertexList [ulIndex].fY;

      // calc perp vector
      v2PerpVec = v2NearestPoint;
      v2PerpVec.Normalize ();

      // point perpendicular vector in the direction of the push normal
      if ((v2PerpVec * v2RevNormal) < 0.0)
        {
        v2PerpVec.Reverse ();
        };

      // calc point at which perp vector is tangent to radius.
      v2TangentPoint = v2PerpVec * fRadius;

      // take the line formed by the first edge point and the push vector, and the line from the
      //  tangent point and the edge vector.  Find where they cross.  That's your new Point 0.

      SetLine (vecTangentLine, v2EdgeVec [ulIndex], v2TangentPoint);

      v2OldPoint0.Set (vecVertexList [ulIndex].fX, vecVertexList [ulIndex].fY, 0);
      SetLine (vecVertexLine, v2RevNormal, v2OldPoint0);

      fDenom = (vecVertexLine.fX * vecTangentLine.fY) - (vecTangentLine.fX * vecVertexLine.fY);

      if (fDenom < R_EPSILON)
        {
        // zero denominator.  No solution.  Lines are parallel.  Ignore this line.
        continue;
        }
      else
        {
        v2NewPoint0.fX = ((vecTangentLine.fZ * vecVertexLine.fY) - (vecVertexLine.fZ * vecTangentLine.fY)) / fDenom;
        v2NewPoint0.fY = - (vecTangentLine.fZ + (vecTangentLine.fX * v2NewPoint0.fX)) / vecTangentLine.fY;
        };

      // you now have a new Point0 and the vector for the line.  Find out where the tangent point falls
      //   in terms of T.

      fT = (v2TangentPoint.fX - v2NewPoint0.fX) / v2EdgeVec [ulIndex].fX;

      if ((fT >= 0.0) && (fT <= 1.0))
        {
        // the tangent point is within the edge.  So this edge is a possible pushback amount.

        fDist = Distance (v2OldPoint0, v2NewPoint0);

        if (fDist > fPushback)
          {
          fPushback = fDist;
          };
        };
      };
    if (fPushback == 0.0)
      {
      // none of the edges worked for a pushback.  Calculate the pushback for the vertices
      for (ulIndex = 0; ulIndex < ulCurrVert - 1; ++ulIndex)
        {
        v2OldPoint0.Set (vecVertexList [ulIndex].fX, vecVertexList [ulIndex].fY, 0.0f);
        SetLine (vecVertexLine, v2RevNormal, v2OldPoint0);

        // calculate the two intersection points of the line from the old point along the
        //  normal vector, and the circle. vecQuad holds the A B and C values for solving
        //  the quadratic formula. (solving for y)
        //  A = a^2 - b^2    B = 2cb    c = c^2 - a^2r^2

        vecQuad.fX = (vecVertexLine.fX * vecVertexLine.fX) - (vecVertexLine.fY * vecVertexLine.fY);
        vecQuad.fY = 2.0f * vecVertexLine.fZ * vecVertexLine.fY;
        vecQuad.fZ = (vecVertexLine.fZ * vecVertexLine.fZ) - ((vecVertexLine.fX * vecVertexLine.fX) * fRadiusSquared);

        // b^2 - 4ac
        fPart = (vecQuad.fY * vecQuad.fY) - (4.0f * vecQuad.fX * vecQuad.fZ);

        if (fPart < 0.0f)
          {
          // no solution, so there's no collision
          }
        else
          {
          fPart = sqrtf (fPart);
          v2Point1.fY = (- vecQuad.fZ + fPart) / (2.0f * vecQuad.fX);
          v2Point2.fY = (- vecQuad.fZ - fPart) / (2.0f * vecQuad.fX);

          v2Point1.fX = - (vecQuad.fZ + (vecQuad.fY * v2Point1.fY)) / vecQuad.fX;
          v2Point2.fX = - (vecQuad.fZ + (vecQuad.fY * v2Point2.fY)) / vecQuad.fX;

          fDist = Distance (v2OldPoint0, v2Point1);

          if (fDist > fPushback)
            {
            fPushback = fDist;
            };

          fDist = Distance (v2OldPoint0, v2Point2);

          if (fDist > fPushback)
            {
            fPushback = fDist;
            };
          };
        };
      };
    };

  return (TRUE);
  };


//------------------------------------------------------------------------
BOOL BCylinder::CylinderIntersect  (BCylinder&  cylCylinder2In)
  {
  RVec3            vecAxisToRayPerp;
  RVec3            vecDownAxis;
  RVec3            vecCylAxis1;
  RVec3            vecCylAxis2;
  RVec3            vecPerp;
  FLOAT            fPerpLength;
  FLOAT            fPerpLengthSquared;
  RVec3            vecCenterToCenter;
  RVec3            vecOOD;
  FLOAT            fS;
  FLOAT            fT;
  RVec3            vecPoint1;
  RVec3            vecPoint2;
  FLOAT            fDistance;
  FLOAT            fC2End1;
  FLOAT            fC2End2;
  FLOAT            fDifference;

  
  // Note: for now I'm just going to imlement capsule vs capsule collision detection
  //         because it is faster to implement, faster to run, and good enough for
  //         what I'll generally be using cylinders for.

  // You detect capsule collisions by finding the nearest points of the two
  //   axis line segments and comparing the distance to the sum of the radiuses.
  //   You find the nearest points by detecting the nearest points on rays, and
  //   and then clipping the scalar value in the line equation to the range
  //   of -1 to 1.

  // The basic algorithm for line vs line collision detection is found on page
  //   338 of Real Time Rendering.

  // Line 1 is defined as o1 + sd1 and line 2 as o2 + td2 where o1 and o2 are
  //   points on the line (cylinder centers) and d1 and d2 are the line vectors
  //   (axis normal scaled by half length).

  // Given the above:
  //
  //      ((o2 - o1) x d2) dot (d1 x d2)          ((o2 - o1) x d1) dot (d1 x d2)
  //  s = ------------------------------      t = ------------------------------
  //            || d1 x d2 || ^2                        || d1 x d2 || ^2



  // calc d1 and d2
  vecCylAxis1 = vecAxisNorm * fHalfHeight;
  vecCylAxis2 = cylCylinder2In.vecAxisNorm * cylCylinder2In.fHalfHeight;

  //FLOAT  fAxis1Length = vecAxisNorm.Length ();
  //FLOAT  fAxis2Length = cylCylinder2In.vecAxisNorm.Length ();


  // calc (o2 - o1)
  vecCenterToCenter = cylCylinder2In.vecWorldCenter - vecWorldCenter;

  // calc d1 x d2
  vecPerp = vecAxisNorm % cylCylinder2In.vecAxisNorm;

  // if || d1 x d2 || is 0, the lines are parallel
  fPerpLength = vecPerp.Length ();
  /*
  FLOAT fTemper = R_EPSILON;

  if (fPerpLength < 0.001)
    {
    ASSERT (0);
    };

  if (fPerpLength < fTemper)
    {
    ASSERT (0);
    };
  */


  if (fPerpLength < R_EPSILON)
    {
    // cylinder 1 is parallel to cylinder 2

    // project the center to center vector onto cyl 1 axis to get the point
    //   directly across from cyl 2 center.
    fDistance = vecCenterToCenter * vecAxisNorm;
    vecDownAxis = vecAxisNorm * fDistance;

    // calc a vector to the point directly across from cyl 1 center.
    vecAxisToRayPerp = vecCenterToCenter - vecDownAxis;

    // since the cylinders are parallel and you now know where the center
    //  of cylinder 2 projects onto the axis of cylinder 1, you can treat the
    //  cylinder 1 axis like a number line to see if the two cylinder intervals
    //  overlap.  You have three cases to check for 1) cyl 1 above cyl2,
    //  2) cyl2 above cyl1, and 3) overlap.


    fC2End1 = fDistance - cylCylinder2In.fHalfHeight;
    fC2End2 = fDistance + cylCylinder2In.fHalfHeight;

    if (fC2End1 > fHalfHeight)
      {
      // cylinder2 above cylinder1.  compare

      fDifference = fC2End1 - fHalfHeight;
      // calculate a new vector down the axis the size of the difference
      vecDownAxis = vecAxisNorm * fDifference;

      vecAxisToRayPerp += vecDownAxis;

      fDistance = vecAxisToRayPerp.Length ();
      }
    else if (fC2End2 < -fHalfHeight)
      {
      // cylinder2 below cylinder1

      fDifference = fC2End2 + fHalfHeight;
      // calculate a new vector down the axis the size of the difference
      vecDownAxis = vecAxisNorm * fDifference;

      vecAxisToRayPerp += vecDownAxis;

      fDistance = vecAxisToRayPerp.Length ();
      }
    else
      {
      // overlap - check distance between axis rays

      // the length of that vector is the shortest point.
      fDistance = vecAxisToRayPerp.Length ();
      };


    if (fDistance < (fRadius + cylCylinder2In.fRadius))
      {
      // infinite cylinders intersect
      return (TRUE);
      };

    }
  else
    {
    // calc || d1 x d2 || ^2
    fPerpLengthSquared = fPerpLength * fPerpLength;

    // calc ((o2 - o1) x d2)
    vecOOD = vecCenterToCenter % vecCylAxis2;

    // calc ((o2 - o1) x d2) dot (d1 x d2)
    fS = vecOOD * vecPerp;

    // finish off the equation
    fS /= fPerpLengthSquared;

    // now calculate T

    // calc ((o2 - o1) x d1)
    vecOOD = vecCenterToCenter % vecCylAxis1;

    // calc ((o2 - o1) x d1) dot (d1 x d2)
    fT = vecOOD * vecPerp;

    // finish off the equation
    fT /= fPerpLengthSquared;


    // clip S and T to the line segment

    fS = RMin (RMax (fS, -1.0f), 1.0f);
    fT = RMin (RMax (fT, -1.0f), 1.0f);

    // calc the closest points

    vecPoint1 = vecWorldCenter + (vecCylAxis1 * fS);
    vecPoint2 = cylCylinder2In.vecWorldCenter + (vecCylAxis2 * fT);

    fDistance = Distance (vecPoint1, vecPoint2);

    if (fDistance <= (fRadius + cylCylinder2In.fRadius))
      {
      return (TRUE);
      };
    };
  return (FALSE);
  };

