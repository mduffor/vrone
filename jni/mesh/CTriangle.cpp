

/* -----------------------------------------------------------------
                             Collision Triangle

     This class implements a triangle as used in collision 
     calculations.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "CTriangle.hpp"

//------------------------------------------------------------------------
// Triangle
//------------------------------------------------------------------------

  
//------------------------------------------------------------------------
BOOL CTriangle::RayIntersect  (RVec3 &    vecRayWorldStart,
                               RVec3 &    vecRayWorldEnd,
                               CPoint *   ppntCollisionOut)
  {
  RVec3            vecE1;
  RVec3            vecE2;
  RVec3            vecD;
  RVec3            vecQ;
  RVec3            vecP;
  RVec3            vecS;
  FLOAT            fA;
  FLOAT            fF;
  FLOAT            fU;
  FLOAT            fV;


  // This is the implementation of the algorithm found on page 303 of Real Time Rendering


  vecD = vecRayWorldEnd - vecRayWorldStart;

  // Note:  I don't believe you need to normalize the ray vector, since you need
  //         it un-normalized in order to get an accurate "time" value.
  //vecD.Normalize ();

  vecE1 = vec1 - vec0;
  vecE2 = vec2 - vec0;

  vecP = vecD % vecE2;

  fA = vecE1 * vecP;

  if ((fA > -R_EPSILON) && (fA < R_EPSILON)) return (FALSE);

  fF = 1 / fA;

  vecS = vecRayWorldStart - vec0;

  fU = fF * (vecS * vecP);

  if ((fU < 0.0f) || (fU > 1.0f)) return (FALSE);

  vecQ = vecS % vecE1;

  fV = fF * (vecD * vecQ);

  if ((fV < 0.0f) || ((fU + fV) > 1.0f)) return (FALSE);

  if (ppntCollisionOut != NULL)
    {
    ppntCollisionOut->fTime = fF * (vecE2 * vecQ);

    ppntCollisionOut->vecBaryCoord.fX = fU;
    ppntCollisionOut->vecBaryCoord.fY = fV;
    ppntCollisionOut->vecBaryCoord.fZ = 0.0f;
    
    ppntCollisionOut->vecPoint = vecRayWorldStart + (vecD * ppntCollisionOut->fTime);
    
    };
  return (TRUE);
  };
  
  
  
  
  
