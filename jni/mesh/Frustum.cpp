/* -----------------------------------------------------------------
                             Frustum

     This class implements a camera viewing frustum and clipping 
     against it.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "Frustum.hpp"
#include "BSphere.hpp"

//------------------------------------------------------------------------
//                               FRUSTUM
//------------------------------------------------------------------------



//------------------------------------------------------------------------
Frustum::Frustum  ()
  {
  UINT              ulIndex;


  uPlaneMask = 0;


  fNear   = 0;
  fFar    = 1;
  fXDelta = 1.0f;
  fYDelta = 1.0f;

  fAspect = 1.0f;
  fFOVY   = 1.0f;

  v2HighCorner.Set (1.0f,  1.0f, 0);
  v2LowCorner.Set  (-1.0f, -1.0f, 0);

  for (ulIndex = 0; ulIndex < RFRUSTUM_NUM_PLANES; ++ulIndex)
    {
    v4Planes [ulIndex].Zero ();
    };
  };


//------------------------------------------------------------------------
Frustum::~Frustum  ()
  {
  };


//------------------------------------------------------------------------
VOID Frustum::SetNearFar  (FLOAT          fNearIn,
                            FLOAT          fFarIn)
  {


  fNear = fNearIn;
  fFar  = fFarIn;
  };


//------------------------------------------------------------------------
VOID Frustum::SetCorners  (RVec3&          v2HighIn,
                            RVec3&          v2LowIn)
  {


  v2HighCorner = v2HighIn;
  v2LowCorner  = v2LowIn;
  };


//------------------------------------------------------------------------
VOID Frustum::SetFOV  (FLOAT  fFOVYIn)
  {


  fFOVY   = fFOVYIn;

  // now calculate the size of the u and v vectors

  fYDelta = tanf (fFOVY / 2);
  fXDelta = fYDelta * fAspect;
  };


//------------------------------------------------------------------------
VOID Frustum::SetAspect  (FLOAT  fAspectIn)
  {


  fAspect = fAspectIn;

  // now calculate the size of the u and v vectors

  fYDelta = tanf (fFOVY / 2);
  fXDelta = fYDelta * fAspect;
  };



//------------------------------------------------------------------------
BOOL Frustum::InFrustum  (BVolume&  volumeIn,
                           RMatrix&          m4ModelMatrixIn)
  {
  UINT              ulIndex;
  FLOAT            fDistance;
  BSphere          sphereWorldSpace;



  // Note: The passed model matrix needs to take objects from local object
  //         coordinates to world space coordinates

  // transform local bounding info to world bounding info.
  
  volumeIn.ToSphere (sphereWorldSpace.vecCenter,
                     sphereWorldSpace.fRadius,
                     sphereWorldSpace.fRadiusSqr);

  sphereWorldSpace.vecWorldCenter = m4ModelMatrixIn * sphereWorldSpace.vecCenter;
  
  // step through each plane and make sure you are on the negative side of each
  //  one.

  for (ulIndex = 0; ulIndex < RFRUSTUM_NUM_PLANES; ++ulIndex)
    {
    fDistance = v4Planes [ulIndex].Distance (sphereWorldSpace.vecWorldCenter);

    if (fDistance > sphereWorldSpace.fRadius)
      {
      // distance is positive, and greater than radius.  Out of this plane.
      return (FALSE);
      };
    };
  return (TRUE);
  };


//------------------------------------------------------------------------
BOOL Frustum::InFrustum  (BVolume&  volumeIn)
  {
  UINT              ulIndex;
  FLOAT            fDistance;
  UINT              ulMask;


  // step through each plane and make sure you are on the negative side of each
  //  one.
  
  BSphere          sphereBounding;
  volumeIn.ToSphere (sphereBounding.vecWorldCenter,
                     sphereBounding.fRadius,
                     sphereBounding.fRadiusSqr);
                     
  for (ulIndex = 0; ulIndex < RFRUSTUM_NUM_PLANES; ++ulIndex)
    {
    ulMask = 0x00000001 << ulIndex;
    if (ulMask & uPlaneMask)
      {
      fDistance = v4Planes [ulIndex].Distance (sphereBounding.vecWorldCenter);

      if (fDistance > sphereBounding.fRadius)
        {
        // distance is positive, and greater than radius.  Out of this plane.
        return (FALSE);
        };

      if ((fabsf (fDistance) <= sphereBounding.fRadius) &&
          (fDistance > 0.0f))
        {
        // the sphere intersects the bounding plane, but the secondary volume
        //  might not.  The Sphere center is also on the outside of the frustum.
        //  So if the secondary volume intersects the plane we consider it in,
        //  and if it doesn't then we consider it out.

        if (volumeIn.PlaneIntersect (v4Planes [ulIndex]) == FALSE)
          {
          // doesn't collide and is on the outside of the plane, so it is out
          //  of the frustum
          return (FALSE);
          };
        };
      };

    };
  return (TRUE);
  };


//------------------------------------------------------------------------
VOID Frustum::CalcBoundingPlanes  (RMatrix&      m4FacingMatrixIn)
  {
  RVec3             vecWorking;
  RVec3             vecNormal;
  RVec3             vecPoint;



  // Note:  The following calculations could be optimized if you work out how each
  //          camera space vector would multiply out against the facing matrix.
  //          Since one vector value is always 0, and the other is always 1 or -1 before
  //          normalization, you could eliminate several operations by performing and
  //          optimizing the tranformation before normalization.  It would complicate
  //          the code a bit, and I'm not sure if any noticeable gains would be detected,
  //          but if you wind up using this code for other reasons (light volume culling, etc)
  //          then you should consider making the optimizations.


  // vectors of bounding planes face outward.  Objects inside the negative half space
  //  of all the bounding planes are considered to be inside the view frustrum.

  // all 6 planes are in use
  uPlaneMask = 0x0000003f;

  // build the normal vector in camera space, normalize it, then rotate it into world space.
  //  Once the normal is calculated, build the plane using the matrix origin as the point on the plane.

  vecNormal = m4FacingMatrixIn.MultRotate ((RVec3 ( 1.0f,  0.0f,   fXDelta * v2HighCorner.fX)).Normalize ());
  v4Planes [BPLANE_RIGHT] .Set (vecNormal, m4FacingMatrixIn.GetTrans ());

  vecNormal = m4FacingMatrixIn.MultRotate ((RVec3 (-1.0f,  0.0f, - fXDelta * v2LowCorner.fX)).Normalize ());
  v4Planes [BPLANE_LEFT]  .Set (vecNormal, m4FacingMatrixIn.GetTrans ());

  vecNormal = m4FacingMatrixIn.MultRotate ((RVec3 ( 0.0f,  1.0f,   fYDelta * v2HighCorner.fY)).Normalize ());
  v4Planes [BPLANE_TOP]   .Set (vecNormal, m4FacingMatrixIn.GetTrans ());

  vecNormal = m4FacingMatrixIn.MultRotate ((RVec3 ( 0.0f, -1.0f, - fYDelta * v2LowCorner.fY)).Normalize ());
  v4Planes [BPLANE_BOTTOM].Set (vecNormal, m4FacingMatrixIn.GetTrans ());

  vecNormal = m4FacingMatrixIn.MultRotate (RVec3 (0.0f, 0.0f,  1.0f));
  vecPoint  = m4FacingMatrixIn.MultHomogenous (RVec3 (0.0f, 0.0f, -fNear));
  v4Planes [BPLANE_NEAR].Set (vecNormal, vecPoint);

  vecNormal = m4FacingMatrixIn.MultRotate (RVec3 (0.0f, 0.0f, -1.0f));
  vecPoint  = m4FacingMatrixIn.MultHomogenous (RVec3 (0.0f, 0.0f, -fFar));
  v4Planes [BPLANE_FAR].Set (vecNormal, vecPoint);
  };



