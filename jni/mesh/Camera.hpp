/* -----------------------------------------------------------------
                               Camera

     This class stores information about cameras.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"

//------------------------------------------------------------------------
class Camera
  {
  public:
    FLOAT    fFocalLength;
    FLOAT    fNearClip;
    FLOAT    fFarClip;
    FLOAT    fApertureX;
    FLOAT    fApertureY;
    FLOAT    fCenterOfInterest;
    BOOL     bIsOrthographic;
    BOOL     bDirty;
    
    RMatrix  matProjection;
    
    
  public:
             Camera    () {};
             
    virtual  ~Camera   ()                       {};
    
             Camera    (const Camera &  cameraIn) : fFocalLength (cameraIn.fFocalLength),
                                                    fNearClip (cameraIn.fNearClip),
                                                    fFarClip (cameraIn.fFarClip),
                                                    fApertureX (cameraIn.fApertureX),
                                                    fApertureY (cameraIn.fApertureY),
                                                    fCenterOfInterest (cameraIn.fCenterOfInterest),
                                                    bIsOrthograpic (cameraIn.bIsOrthographic),
                                                    bDirty (TRUE) { };
                                              
    VOID     operator= (const Camera &  cameraIn)  {fFocalLength = cameraIn.fFocalLength;
                                                    fNearClip = cameraIn.fNearClip;
                                                    fFarClip = cameraIn.fFarClip;
                                                    fApertureX = cameraIn.fApertureX;
                                                    fApertureY = cameraIn.fApertureY;
                                                    fCenterOfInterest = cameraIn.fCenterOfInterest;
                                                    bIsOrthograpic = cameraIn.bIsOrthographic;
                                                    bDirty = TRUE;};

    VOID     SetFocalLength      (FLOAT  fIn)       {fFocalLength = fIn; bDirty = TRUE;};                                                
    VOID     SetNearClip         (FLOAT  fClipIn)   {fNearClip = fClipIn; bDirty = TRUE;};                                                
    VOID     SetFarClip          (FLOAT  fClipIn)   {fFarClip = fClipIn; bDirty = TRUE;};                                                

    VOID     SetAperture         (FLOAT  fXIn,
                                  FLOAT  fYIn)      {fApertureX = fXIn; fApertureY = fYIn; bDirty = TRUE;};

    VOID     SetCenterOfInterest (FLOAT  fIn)       {fCenterOfInterest = fIn;  bDirty = TRUE;};                         
                             
    VOID     SetOrthographic     (BOOL   bStatusIn) {bIsOrthographic = bStatusIn; bDirty = TRUE;};

    VOID     CalcMatrix          (VOID);
                           
  };
typedef Camera *  PCamera;

#endif // CAMERA_HPP
