/* -----------------------------------------------------------------
                             Bounding Sphere

     This class implements bounding spheres used for collision 
     detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 


#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "BSphere.hpp"

//------------------------------------------------------------------------
VOID BSphere::FirstPoint  (RVec3&  vecPointIn)
  {
  vecCenter = vecPointIn;
  bInitialized = TRUE;
  };

//------------------------------------------------------------------------
VOID BSphere::AddPoint  (RVec3&          vecPointIn)
  {
  FLOAT            fCurrDistance;
  FLOAT            fScaledRadius;
  RVec3            vecDirection;
  RVec3            vecOppositePoint;


  if (!bInitialized)
    {
    FirstPoint (vecPointIn);
    }
  else
    {
    // see if this point is in the bounding sphere, and if not recalculate sphere
    fCurrDistance = DistanceSquared (vecPointIn, vecCenter);

    if (fCurrDistance > fRadiusSqr)
      {
      // calculate a new radius and center

      vecDirection = vecCenter - vecPointIn;

      fScaledRadius = (FLOAT) sqrt (fRadiusSqr / vecDirection.LengthSquared ());

      vecOppositePoint = (vecDirection * fScaledRadius) + vecCenter;

      vecCenter = Midpoint (vecOppositePoint, vecPointIn);

      fRadiusSqr = DistanceSquared (vecCenter, vecPointIn);
      fRadius    = sqrtf  (fRadiusSqr);
      };
    };
  };
  
//------------------------------------------------------------------------
VOID BSphere::AddSphere  (RVec3 &        vecCenterIn,
                          FLOAT          fRadiusIn)
  {
  RVec3            vecPoint1;
  RVec3            vecPoint2;
  RVec3            vecRadiusVec;
  FLOAT            fOrigRadius;
  FLOAT            fLength;


  // calc vector from current sphere to new sphere
  vecRadiusVec = vecCenterIn - vecCenter;

  fLength = vecRadiusVec.Length ();

  if (fabs (fLength) < R_EPSILON)
    {
    // the centers are the same.
    fRadius    = RMax (fRadius, fRadiusIn);
    fRadiusSqr = fRadius * fRadius;
    return;
    };

  // normalize the vector
  vecRadiusVec /= fLength;


  fOrigRadius = sqrtf (fRadiusSqr);

  vecPoint1 = (vecRadiusVec * fRadiusIn) + vecCenterIn;

  vecPoint2 = (vecRadiusVec * -fOrigRadius) + vecCenter;

  vecCenter = Midpoint (vecPoint1, vecPoint2);

  fRadiusSqr =  DistanceSquared (vecCenter, vecPoint2);
  fRadius    =  sqrtf (fRadiusSqr);
  };

//------------------------------------------------------------------------
BOOL BSphere::RayIntersect  (Ray &      rayIn,
                             CPoint *   ppntCollisionOut)
  {
  RVec3            vecL;
  FLOAT            fLLengthSqr;
  RVec3            vecD;
  FLOAT            fDProjLength;
  FLOAT            fMSqr;
  FLOAT            fQ;


  // returns TRUE if the ray and sphere intersect, FALSE if not.

  // The details of this algorithm can be found in the book Real-Time Rendering,
  //  page 299

  vecL = vecWorldCenter - rayIn.vecWorldStart;
  vecD = rayIn.vecWorldEnd - rayIn.vecWorldStart;

  fLLengthSqr  = vecL.LengthSquared ();
  fDProjLength = vecL * vecD;

  if ((fDProjLength < 0.0f) && (fLLengthSqr > fRadiusSqr)) return (FALSE);

  fMSqr = fLLengthSqr - (fDProjLength * fDProjLength);

  if (fMSqr > fRadiusSqr) return (FALSE);

  // we have a collision, find out where.

  if (ppntCollisionOut != NULL)
    {
    fQ = sqrtf (fRadiusSqr - fMSqr);

    if (fLLengthSqr > fRadiusSqr)
      {
      ppntCollisionOut->fTime = fDProjLength - fQ;
      }
    else
      {
      ppntCollisionOut->fTime = fDProjLength + fQ;
      };
    ppntCollisionOut->vecPoint = (vecD * ppntCollisionOut->fTime) + rayIn.vecWorldStart;
    };

  return (TRUE);
  };

  
//------------------------------------------------------------------------
BOOL BSphere::PointIntersect  (RVec3&    vecPointIn)
  {
  RVec3             vecD;
  // returns true if the point is in the sphere

  vecD = vecPointIn - vecWorldCenter;

  if (vecD.LengthSquared () > fRadiusSqr)
    {
    return (FALSE);
    };
  return (TRUE);
  };  

  
//------------------------------------------------------------------------
BOOL BSphere::SphereIntersect (RVec3 &      vecSphereWorldPosIn,
                               FLOAT        fSphereRadiusIn)
  {
  return (Distance (vecWorldCenter, vecSphereWorldPosIn) <= (fRadius + fSphereRadiusIn));
  };
  
  
//------------------------------------------------------------------------
BOOL BSphere::PlaneIntersect  (CPlane&           planeIn)
  {
  FLOAT            fDistance;


  fDistance = planeIn.Distance (vecWorldCenter);

  if (fabsf (fDistance) <= (fRadius + R_EPSILON))
    {
    // hit
    return (TRUE);
    };
  return (FALSE);
  };

  

//------------------------------------------------------------------------
BOOL BSphere::TriIntersect  (CTriangle &      triangleIn)
  {

  // The sphere vs tri intersection check works by finding the distance
  //   from the sphere's center to the triangle, and comparing that against
  //   the sphere's radius.  The distance from the center to the triangle
  //   is either the shortest distance to the triangle's plane, the shortest
  //   distance to a tri edge, or the shortest distance to a tri vertex.
  //   You decide which of these to use by calculating the barycentric coordinates
  //   of the nearest point on the plane.  If the point is inside the triangle,
  //   you use the distance to the plane.  Otherwise you use the sign of the (r,s,t)
  //   barycentric coordinates to determine which of the six sextants it is in (with
  //   space divided up by the rays formed by the triangle edges.)  The sextant
  //   determines which edge or vertex to compare against.

  // There are a few speedups that could be used here.  First if you check the
  //   distance from the sphere center to the plane, you can measure it against
  //   the squared sphere radius to see if the plane and sphere even intersect.
  //   This requires a square root per triangle to calculate the triangle normal.
  //
  // Another possible speedup is to calculate the distance from each of the
  //   vertexes to the sphere center, and if any are in the sphere, you have a
  //   collision.  This is only faster if you expect to collide with triangle
  //   vertices a fair number of times, which is possible if the sphere tends
  //   to be larger than the triangles, or deep penetration of the triangle's
  //   plane is expected.
  //

  RVec3            vecE1;       // V0V1
  RVec3            vecE2;       // V0V2
  RVec3            vecE3;       // V1V2

  RVec3            vecCenter;
  RVec3            vecNearestPoint;
  RVec3            vecNormal;
  FLOAT            fNormalLength;
  FLOAT            fPseudoDistance;

  FLOAT            fR;
  FLOAT            fS;
  FLOAT            fT;

  FLOAT            fDistSquared;
  RVec3            vecMinDist;
  RVec3            vecEndToCenter;
  RVec3            vecEdgePoint;


  // translate the triangle and sphere such that vertex 0 is at the origin to simplify calculations.


  vecE1 = triangleIn.vec1 - triangleIn.vec0;
  vecE2 = triangleIn.vec2 - triangleIn.vec0;
  vecE3 = triangleIn.vec2 - triangleIn.vec1;

  vecCenter = vecWorldCenter - triangleIn.vec0;

  // calculate the plane normal (D is 0 since we are at the origin)
  // We don't need to normalize this because we don't need the exact distance
  //  to the plane yet, we only need to know where the nearest point on the plane is.

  vecNormal      = vecE1 % vecE2;


  fNormalLength = vecNormal.Length ();
  vecNormal     /= fNormalLength;       // normalize

  // project the sphere center onto the normal vector, then use that distance to
  //   scale the normal vector.  This subtracted from the sphere center should give
  //   you the nearest point in the plane of the triangle.

  fPseudoDistance = vecCenter * vecNormal;
  vecNearestPoint  = vecCenter - (vecNormal * fPseudoDistance);

  // because vertex 0 is at the origin, vecNearestPoint is also the vector from V0 to that point.

  fS = ((vecNearestPoint % vecE2) * vecNormal) / fNormalLength;
  fT = ((vecE1 % vecNearestPoint) * vecNormal) / fNormalLength;
  fR = 1 - (fS + fT);

  // now figure out which sextant you are in
  
  //    \ 4 /      |
  // T   \ /   S   |
  //  5   2   3    |
  //     / \       |
  // ---0---1----  |
  // 0 /  1  \ 2   |
  //  /   R   \    |


  if (fR < 0.0f)
    {
    // sextants 0, 1, or 2
    if (fS < 0.0f)
      {
      // sextant 2
      // distance to V1

      vecMinDist = vecWorldCenter - triangleIn.vec1;

      fDistSquared = vecMinDist.LengthSquared ();
      }
    else
      {
      // sextant 0 or 1
      if (fT < 0.0f)
        {
        // sextant 0
        // distance to V0

        fDistSquared = vecCenter.LengthSquared ();
        }
      else
        {
        // sextant 1
        // distance to edge 1

        vecEdgePoint = (vecE1 * (vecCenter * vecE1));

        vecMinDist = vecEdgePoint - vecCenter;
        fDistSquared = vecMinDist.LengthSquared ();
        };
      };
    }
  else
    {
    // sextants 3, 4, 5, or center
    if (fS < 0.0f)
      {
      // sextant 3 or 4
      if (fT < 0.0f)
        {
        // sextant 4
        // distance to V2

        vecMinDist = vecWorldCenter - triangleIn.vec2;

        fDistSquared = vecMinDist.LengthSquared ();
        }
      else
        {
        // sextant 3
        // distance to edge 3

        vecEndToCenter = vecCenter - vecE1;
        vecEdgePoint = (vecE3  * (vecEndToCenter * vecE3)) + vecE1;

        vecMinDist = vecEdgePoint - vecCenter;
        fDistSquared = vecMinDist.LengthSquared ();
        };
      }
    else
      {
      // sextant 5 or center
      if (fT < 0.0f)
        {
        // sextant  5
        // distance to edge 2

        vecEdgePoint = (vecE2 * (vecCenter * vecE2));

        vecMinDist = vecEdgePoint - vecCenter;
        fDistSquared = vecMinDist.LengthSquared ();
        }
      else
        {
        // center
        vecMinDist = vecCenter - vecNearestPoint;
        fDistSquared = vecMinDist.LengthSquared ();
        };
      };
    };

  if (fDistSquared < fRadiusSqr) return (TRUE);

  return (FALSE);
  };
  
  