/* -----------------------------------------------------------------
                         Bitmap Font

     This module implements bitmapped fonts created with the 
   BMFont Windows utility.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2013-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef BITMAPFONT_HPP
#define BITMAPFONT_HPP


#include "Types.hpp"
#include "RStr.hpp"
#include "RVec.hpp"
#include "RVecArray.hpp"
#include "IntArray.hpp"
#include "LinkedList.hpp"
#include "DisplayMesh.hpp"

//------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------


//------------------------------------------------------------------------
// Forward Declarations
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Class Definitions
//------------------------------------------------------------------------




#define BMF_NUM_ARRAY_CHARS 256

//=============================================================================
class BMFCharKerning
  {
  public:
    INT  iAdjacentChar;
    INT  iAmount;
  };

//=============================================================================
class BMFCharDesc
  {
  public:
    INT          iX;
    INT          iY;
    INT          iWidth;
    INT          iHeight;
    INT          iOffsetX;
    INT          iOffsetY;
    INT          iStrideX;
    INT          iPage;
    INT          iChannel;
    LinkedList   listKernings;
  };


//=============================================================================
class BitmapFont
  {
  public:
    INT   iLineHeight;
    INT   iBaseline;
    INT   iFontAtlasWidth;  // Pixel dimensions of the atlas that holds the characters of the font.
    INT   iFontAtlasHeight;  // 
    INT   iPages;
    INT   iPacked;
    INT   iAlphaChan;
    INT   iRedChan;
    INT   iGreenChan;
    INT   iBlueChan;

    RStr  strTypeface;
    INT   iSize;
    INT   iBold;
    INT   iItalic;
    RStr  strCharset;
    INT   iUnicode;
    INT   iStretchH;
    INT   iSmooth;
    INT   iAA;
    INT   iPaddingL;
    INT   iPaddingR;
    INT   iPaddingU;
    INT   iPaddingD;
    INT   iSpacingX;
    INT   iSpacingY;
    INT   iOutline;

    INT   iAtlasXPos;   // pixel position on a parent atlas for where the font atlas is stored.
    INT   iAtlasYPos;
    INT   iAtlasWidth;  // size in pixels of the parent atlas containing the font atlas
    INT   iAtlasHeight;
    
    BMFCharDesc   arrayChars [BMF_NUM_ARRAY_CHARS];
    RStr          strFontDef;
    RStr          strName;

    INT   iId;
    RStr  strFile;

    
    enum AlignHoriz {Left,
                     Center,
                     Right}; 
    
    BitmapFont *  pMasterNext;
    
    
  public:
  
                         BitmapFont ();
         
                         ~BitmapFont ();
         
    const char *         GetName         (VOID)   {return strTypeface;};
    
    const char *         GetTexturePath  (VOID)   {return strFile.AsChar();};

    const char *         GetMaterialName (VOID)   {return strTypeface;};
    
    VOID                 Register        (VOID);

    VOID                 Unregister      (VOID);
    
    static BitmapFont *  FindByName      (const char *  szNameIn);
         
    VOID                 ParseFont       (const char *  szFontDefIn);
    
    INT                  CalcLineWidth   (const char *  szTextIn);

    INT                  GetNumVerts     (const char *  szTextIn);

    INT                  GetNumTriangles (const char *  szTextIn);

    VOID                 AddTriangles    (RStr &        strTextIn,
                                          AlignHoriz    alignIn,
                                          INT           iPixelsPerUnit,
                                            
                                          RVec4Array *  avecCoordPoints,    // array to hold point coordinates
                                          RVec4Array *  avecCoordTexture,   // array to hold texture coordinates
                                          RVec4Array *  avecCoordNormal,    // array to hold normal coordinates

                                          INT &         iCoordPointOffset,   // first empty index in coordPoints
                                          INT &         iCoordTextureOffset, // first empty index in coordTexture
                                          INT &         iCoordNormalOffset,  // first empty index in coordNormal

                                          IntArray *   aiFacePoints,        // indexes into avecCoordPoints for the face's verts.  Unrolled face verts.
                                          IntArray *   aiFaceTex,           // indexes into iCoordTextureOffset for the face's verts.  Unrolled face verts.
                                          IntArray *   aiFaceNorms,         // indexes into iCoordNormalOffset for the face's verts.  Unrolled face verts.

                                          IntArray *   aiFaces,             // index into aiFacePoints/aiFaceTex/aiFaceNorms giving first index of the face's verts.
                                          IntArray *   aiFaceVCount,        // number of verts in the face.  should always be 3.
                                            
                                          IntArray *   aiFaceGroups,        // index of the group to which this face belogs.  Always set to 0.
                                          IntArray *   aiGroupPoints,
                                            
                                          INT           iVertexIndex,          // first free index in the aiFacePoints, aiFaceTex, and aiFaceNorms arrays
                                          INT           iFaceIndex);           // first free index in the aiFaces, aiFaceVCount, and aiFaceGroups arrays.
                                
    VOID                 AddTriangleStrip (const char *   szTextIn,
                                           AlignHoriz     alignIn,
                                           INT            iPixelsPerUnit, // Is this valid in 3D space, or is it just a scale factor?
                                           DisplayMesh *  pDisplayMesh);
  };

  
//extern BitmapFont * pBMFontHead;
  
#endif // BITMAPFONT_HPP