/* -----------------------------------------------------------------
                             Bounding Box

     This class implements bounding boxes used for collision 
     detection.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "BBox.hpp"

//------------------------------------------------------------------------
// Bounding Box
//------------------------------------------------------------------------


//------------------------------------------------------------------------
VOID BBox::CalcOBB  (RVec3  vecLowLimitIn,
                     RVec3  vecHighLimitIn)
  {
  RVec3             vecSize;


  // get the center of the AxisBB

  vecCenter = Midpoint (vecHighLimitIn, vecLowLimitIn);
  vecSize = (vecHighLimitIn - vecLowLimitIn) * 0.5f;

  // don't allow any dimension of the bounding box to be zero.
  if (fabs (vecSize.fX) < R_EPSILON) vecSize.fX = 1.0f;
  if (fabs (vecSize.fY) < R_EPSILON) vecSize.fY = 1.0f;
  if (fabs (vecSize.fZ) < R_EPSILON) vecSize.fZ = 1.0f;

  vecHalfLengths    = vecSize;
  vecSqrHalfLengths = vecSize.Squared ();
  bIsOBB = TRUE;
  };


//------------------------------------------------------------------------
VOID BBox::CalcAABB  (RVec3  vecLowLimitIn,
                      RVec3  vecHighLimitIn)
  {
  RVec3             vecSize;


  // get the center of the AxisBB

  vecCenter = Midpoint (vecHighLimitIn, vecLowLimitIn);
  vecSize = (vecHighLimitIn - vecLowLimitIn) * 0.5f;

  // don't allow any dimension of the bounding box to be zero.
  if (fabs (vecSize.fX) < R_EPSILON) vecSize.fX = 1.0f;
  if (fabs (vecSize.fY) < R_EPSILON) vecSize.fY = 1.0f;
  if (fabs (vecSize.fZ) < R_EPSILON) vecSize.fZ = 1.0f;

  // since the vectors don't change, we calculate them once here.

  vecHalfX.Set (vecSize.fX,       0.0f,       0.0f);
  vecHalfY.Set (      0.0f, vecSize.fY,       0.0f);
  vecHalfZ.Set (      0.0f,       0.0f, vecSize.fZ);

  vecNormX.Set (      1.0f,       0.0f,       0.0f);
  vecNormY.Set (      0.0f,       1.0f,       0.0f);
  vecNormZ.Set (      0.0f,       0.0f,       1.0f);

  vecHalfLengths    = vecSize;
  vecSqrHalfLengths = vecSize.Squared ();
  bIsOBB = FALSE;
  };


//------------------------------------------------------------------------
VOID BBox::SetPosition (RMatrix  matIn) // m4CompositeMatrix
  {
  if (bIsOBB)
    {
    PositionOBB (matIn);
    }
  else
    {
    PositionAABB (matIn);
    }
  };
  
  
//------------------------------------------------------------------------
VOID BBox::PositionAABB  (RMatrix  matIn) // m4CompositeMatrix
  {

  vecWorldCenter = matIn.MultTrans (vecCenter);
  };


//------------------------------------------------------------------------
VOID BBox::PositionOBB  (RMatrix  matIn) // m4CompositeMatrix
  {
  //RMatrix4          m4Trans;

  // create an RT matrix

  //vecWorldCenter = matIn.MultTrans (vecCenter);
  vecWorldCenter = matIn * vecCenter;

  matIn.GetInvRotateX (vecNormX);
  vecHalfX = vecNormX * vecHalfLengths.fX;

  matIn.GetInvRotateY (vecNormY);
  vecHalfY = vecNormY * vecHalfLengths.fY;

  matIn.GetInvRotateZ (vecNormZ);
  vecHalfZ = vecNormZ * vecHalfLengths.fZ;
  };

  
//------------------------------------------------------------------------
VOID BBox::ToSphere  (RVec3 &      vecCenterOut,
                      FLOAT &      fRadiusOut,
                      FLOAT &      fRadiusSquaredOut)
  {
  vecCenterOut = vecCenter;
  fRadiusSquaredOut = vecSqrHalfLengths.fX + vecSqrHalfLengths.fY + vecSqrHalfLengths.fZ;
  fRadiusOut = sqrtf (fRadiusSquaredOut);
  };

  
//------------------------------------------------------------------------
BOOL BBox::RayIntersect  (Ray &      rayIn,
                          CPoint *   ppntCollisionOut)
  {
  FLOAT            fTMin;
  FLOAT            fTMax;
  FLOAT            fT1;
  FLOAT            fT2;

  RVec3            vecP;
  RVec3            vecD;
  RVec3            vecDNorm;
  FLOAT            fE;
  FLOAT            fF;
  FLOAT            fOneOverF;

  
  // The bounding box may not be centered in local space.  Adjust
  //  the collision ray and output collision point accordingly
  RVec3  vecRayWorldStart = rayIn.vecWorldStart - vecCenter;
  RVec3  vecRayWorldEnd   = rayIn.vecWorldEnd   - vecCenter;
  
  // returns TRUE if the ray and bounding box intersect, FALSE if not.

  fTMin = -FLT_MAX;
  fTMax = FLT_MAX;

  // vecD is the direction vector of the ray
  vecD = vecRayWorldEnd - vecRayWorldStart;
  // vecP is a vector from the ray start to the center of the box
  vecP = vecWorldCenter - vecRayWorldStart;

  // Note:  I'm not completely certain whether this ray vector needs to be normalized
  //   or not.  I'll have to study the way this algorithm works better in the
  //   future and see if this can be removed
  vecDNorm = vecD;
  //vecDNorm.Normalize ();

  // X Slab

  // E is the dot product between the normal vector and the ray-to-boxCenter vector
  //   It gives the projection of the ray's origin onto the x axis, telling how
  //   far along the x axis it starts.
  fE = vecNormX * vecP;
  // F is the dot product between the normal vector and the direction vector.
  //    it will approach 0 if the two are near parallel
  fF = vecNormX * vecDNorm;

  
  //vecDNorm.DebugPrint ("  vecDNorm");
 
  //printf ("  fE %f\n  fF %f\n",fE, fF);
  
  // check against a small value to make sure planes aren't parallel to slab (perpendicular to vector)
  if (fabsf (fF) >= R_EPSILON)
    {
    fOneOverF  = 1.0f / fF;

    fT1 = (fE + vecHalfLengths.fX) * fOneOverF;
    fT2 = (fE - vecHalfLengths.fX) * fOneOverF;

    //printf ("  fT1 %f\n  fT2  %f\n  fTMin %f\n  fTMax  %f\n", fT1, fT2, fTMin, fTMax);
    
    if (fT1 > fT2)
      {
      if (fT2 > fTMin) fTMin = fT2;
      if (fT1 < fTMax) fTMax = fT1;
      }
    else
      {
      if (fT1 > fTMin) fTMin = fT1;
      if (fT2 < fTMax) fTMax = fT2;
      };
    //printf ("RayIntersect %d\n", __LINE__);  
    if (fTMin > fTMax) return (FALSE);
    //printf ("RayIntersect %d\n", __LINE__);  
    if (fTMax < 0.0f)  return (FALSE);
    //printf ("RayIntersect %d\n", __LINE__);  
    }
  else
    {
    if ((-fE - vecHalfLengths.fX > 0.0f) ||
        (-fE + vecHalfLengths.fX < 0.0f))
      {
      //printf ("RayIntersect %d\n", __LINE__);  
      return (FALSE);
      };
    };


  // Y Slab

  fE = vecNormY * vecP;
  fF = vecNormY * vecDNorm;

  // check against a small value to make sure planes aren't parallel to slab (perpendicular to vector)
  if (fabsf (fF) >= R_EPSILON)
    {
    fOneOverF  = 1.0f / fF;

    fT1 = (fE + vecHalfLengths.fY) * fOneOverF;
    fT2 = (fE - vecHalfLengths.fY) * fOneOverF;

    if (fT1 > fT2)
      {
      if (fT2 > fTMin) fTMin = fT2;
      if (fT1 < fTMax) fTMax = fT1;
      }
    else
      {
      if (fT1 > fTMin) fTMin = fT1;
      if (fT2 < fTMax) fTMax = fT2;
      };
    //printf ("RayIntersect %d\n", __LINE__);  
    if (fTMin > fTMax) return (FALSE);
    //printf ("RayIntersect %d\n", __LINE__);  
    if (fTMax < 0.0f)  return (FALSE);
    //printf ("RayIntersect %d\n", __LINE__);  
    }
  else
    {
    if ((-fE - vecHalfLengths.fY > 0.0f) ||
        (-fE + vecHalfLengths.fY < 0.0f))
      {
    //printf ("RayIntersect %d\n", __LINE__);  
      return (FALSE);
      };
    };


  // Z Slab

  fE = vecNormZ * vecP;
  fF = vecNormZ * vecDNorm;

  // check against a small value to make sure planes aren't parallel to slab (perpendicular to vector)
  if (fabsf (fF) >= R_EPSILON)
    {
    fOneOverF  = 1.0f / fF;

    fT1 = (fE + vecHalfLengths.fZ) * fOneOverF;
    fT2 = (fE - vecHalfLengths.fZ) * fOneOverF;

    if (fT1 > fT2)
      {
      if (fT2 > fTMin) fTMin = fT2;
      if (fT1 < fTMax) fTMax = fT1;
      }
    else
      {
      if (fT1 > fTMin) fTMin = fT1;
      if (fT2 < fTMax) fTMax = fT2;
      };
    //printf ("RayIntersect %d\n", __LINE__);  
    if (fTMin > fTMax) return (FALSE);
    //printf ("RayIntersect %d\n", __LINE__);  
    if (fTMax < 0.0f)  return (FALSE);
    //printf ("RayIntersect %d\n", __LINE__);  
    }
  else
    {
    if ((-fE - vecHalfLengths.fZ > 0.0f) ||
        (-fE + vecHalfLengths.fZ < 0.0f))
      {
    //printf ("RayIntersect %d\n", __LINE__);  
      return (FALSE);
      };
    };


  if (ppntCollisionOut != NULL)
    {
    if (fTMin > 0.0f)
      {
      ppntCollisionOut->fTime = fTMin;
      }
    else
      {
      ppntCollisionOut->fTime = fTMax;
      };
    ppntCollisionOut->vecPoint = (vecD * ppntCollisionOut->fTime) + vecRayWorldStart;
    ppntCollisionOut->vecPoint += vecCenter;
    };

    //printf ("RayIntersect %d\n", __LINE__);  
  return (TRUE);
  };

//------------------------------------------------------------------------
BOOL BBox::PointIntersect (RVec3 &  vecPointIn)
  {
  RVec3            vecD;
  FLOAT            fDistance;


  // project the vector from the center of the BB to the point on each of the three
  //   half-vectors.  If it lies within [-1,1] then it should be in the box.  By
  //   comparing against the vectors one at a time, we avoid unneeded multiplies
  //   that we would get if we used a matrix.


  vecD = vecPointIn - vecWorldCenter;

  fDistance = vecHalfX * vecD;
  if ((fDistance < -vecSqrHalfLengths.fX) ||
      (fDistance >  vecSqrHalfLengths.fX)) return (FALSE);

  fDistance = vecHalfY * vecD;
  if ((fDistance < -vecSqrHalfLengths.fY) ||
      (fDistance >  vecSqrHalfLengths.fY)) return (FALSE);

  fDistance = vecHalfZ * vecD;
  if ((fDistance < -vecSqrHalfLengths.fZ) ||
      (fDistance >  vecSqrHalfLengths.fZ)) return (FALSE);

  return (TRUE);
  };


//------------------------------------------------------------------------
BOOL BBox::PlaneAABBIntersect  (CPlane&        planeIn)
  {
  RVec3             vecMin;
  RVec3             vecMax;
  RVec3             vecNormal;


  // if we are axis aligned, we can assume that the half axis values are all
  //   positive and the half length values are all positive

  if (planeIn.vecNormal.fX >= 0.0f)
    {
    vecMin.fX = vecWorldCenter.fX - vecHalfLengths.fX;
    vecMax.fX = vecWorldCenter.fX + vecHalfLengths.fX;
    }
  else
    {
    vecMin.fX = vecWorldCenter.fX + vecHalfLengths.fX;
    vecMax.fX = vecWorldCenter.fX - vecHalfLengths.fX;
    };
  if (planeIn.vecNormal.fY >= 0.0f)
    {
    vecMin.fY = vecWorldCenter.fY - vecHalfLengths.fY;
    vecMax.fY = vecWorldCenter.fY + vecHalfLengths.fY;
    }
  else
    {
    vecMin.fY = vecWorldCenter.fY + vecHalfLengths.fY;
    vecMax.fY = vecWorldCenter.fY - vecHalfLengths.fY;
    };
  if (planeIn.vecNormal.fZ >= 0.0f)
    {
    vecMin.fZ = vecWorldCenter.fZ - vecHalfLengths.fZ;
    vecMax.fZ = vecWorldCenter.fZ + vecHalfLengths.fZ;
    }
  else
    {
    vecMin.fZ = vecWorldCenter.fZ + vecHalfLengths.fZ;
    vecMax.fZ = vecWorldCenter.fZ - vecHalfLengths.fZ;
    };

  vecNormal = planeIn.GetNormal ();

  if (((vecNormal * vecMin) + planeIn.fD) >  0.0f) return (FALSE);
  if (((vecNormal * vecMax) + planeIn.fD) >= 0.0f) return (TRUE);
  return (FALSE);
  };


//------------------------------------------------------------------------
BOOL BBox::PlaneOBBIntersect  (CPlane&        planeIn)
  {
  FLOAT            fRadius;
  FLOAT            fDistance;
  RVec3             vecNormal;

  // TODO : Double check this logic.  It doesn't look very exact.
  
  vecNormal = planeIn.GetNormal ();

  fRadius = fabsf ((vecNormal * vecNormX) * vecHalfLengths.fX) +
            fabsf ((vecNormal * vecNormY) * vecHalfLengths.fY) +
            fabsf ((vecNormal * vecNormZ) * vecHalfLengths.fZ);

  fDistance = planeIn.Distance (vecWorldCenter);

  if (fDistance > fRadius) return (FALSE);
  return (TRUE);
  };



//------------------------------------------------------------------------
BOOL BBox::SphereAABBIntersect  (RVec3 &      vecSphereWorldPosIn,
                                 FLOAT        fSphereRadiusIn)
  {
  FLOAT            fDistance;
  FLOAT            fDelta;
  RVec3            vecMin;
  RVec3            vecMax;


  fDistance = 0.0f;

  vecMin = vecWorldCenter - vecHalfLengths;
  vecMax = vecWorldCenter + vecHalfLengths;

  if (vecSphereWorldPosIn.fX < vecMin.fX)
    {
    fDelta = vecSphereWorldPosIn.fX - vecMin.fX;
    fDistance += fDelta * fDelta;
    }
  else if (vecSphereWorldPosIn.fX > vecMax.fX)
    {
    fDelta = (vecSphereWorldPosIn.fX - vecMax.fX);
    fDistance += fDelta * fDelta;
    };

  if (vecSphereWorldPosIn.fY < vecMin.fY)
    {
    fDelta = vecSphereWorldPosIn.fY - vecMin.fY;
    fDistance += fDelta * fDelta;
    }
  else if (vecSphereWorldPosIn.fY > vecMax.fY)
    {
    fDelta = (vecSphereWorldPosIn.fY - vecMax.fY);
    fDistance += fDelta * fDelta;
    };

  if (vecSphereWorldPosIn.fZ < vecMin.fZ)
    {
    fDelta = vecSphereWorldPosIn.fZ - vecMin.fZ;
    fDistance += fDelta * fDelta;
    }
  else if (vecSphereWorldPosIn.fZ > vecMax.fZ)
    {
    fDelta = (vecSphereWorldPosIn.fZ - vecMax.fZ);
    fDistance += fDelta * fDelta;
    };

  if (fDistance > (fSphereRadiusIn * fSphereRadiusIn)) return (FALSE);
  return (TRUE);
  };


//------------------------------------------------------------------------
BOOL BBox::SphereOBBIntersect  (RVec3 &      vecSphereWorldPosIn,
                                FLOAT        fSphereRadiusIn)
  {
  RMatrix           m4TransMatrix;
  
  // transform the sphere's center into the coordinate space of the bounding box,
  //  then use the axis aligned bounding box routine to determine intersection.

  MatrixChangeCoordSystem (m4TransMatrix,
                           vecNormX,
                           vecNormY,
                           vecNormZ,
                           vecWorldCenter);

  RVec3  vecTransformedCenter = m4TransMatrix * vecSphereWorldPosIn;

  return (SphereAABBIntersect (vecTransformedCenter, fSphereRadiusIn));
  };


//------------------------------------------------------------------------
BOOL BBox::AABBIntersect  (BBox &   bbBox2In)
  {
  RVec3             vecMin1;
  RVec3             vecMax1;
  RVec3             vecMin2;
  RVec3             vecMax2;


  vecMin1 = vecWorldCenter - vecHalfLengths;
  vecMax1 = vecWorldCenter + vecHalfLengths;

  vecMin2 = bbBox2In.vecWorldCenter - bbBox2In.vecHalfLengths;
  vecMax2 = bbBox2In.vecWorldCenter + bbBox2In.vecHalfLengths;


  if ((vecMin1.fX > vecMax2.fX) || (vecMin2.fX > vecMax1.fX))
    {
    return (FALSE);
    };
  if ((vecMin1.fY > vecMax2.fY) || (vecMin2.fY > vecMax1.fY))
    {
    return (FALSE);
    };
  if ((vecMin1.fZ > vecMax2.fZ) || (vecMin2.fZ > vecMax1.fZ))
    {
    return (FALSE);
    };
  return (TRUE);
  };


//------------------------------------------------------------------------
BOOL BBox::OBBIntersect  (BBox &   bbBox2In)
  {
  RVec3             vecAxisXB;
  RVec3             vecAxisYB;
  RVec3             vecAxisZB;
  RVec3             vecCenterB;
  FLOAT             fRadiusA;
  FLOAT             fRadiusB;
  FLOAT             fDistance;
  RVec3             vecAbsAxisUB;
  RVec3             vecAbsAxisVB;
  RVec3             vecAbsAxisWB;
  RVec3             vecCenterBMinusA;




  // returns TRUE if the boxes intersect, FALSE if not.

  // first we must transform bounding box 2 into the object space of box 1

  // multiply the normal vectors of B by the inverse of the rotation matrix of A.

  vecAxisXB.fX = (vecNormX.fX * bbBox2In.vecNormX.fX) + (vecNormY.fX * bbBox2In.vecNormY.fX) + (vecNormZ.fX * bbBox2In.vecNormZ.fX);
  vecAxisXB.fY = (vecNormX.fX * bbBox2In.vecNormX.fY) + (vecNormY.fX * bbBox2In.vecNormY.fY) + (vecNormZ.fX * bbBox2In.vecNormZ.fY);
  vecAxisXB.fZ = (vecNormX.fX * bbBox2In.vecNormX.fZ) + (vecNormY.fX * bbBox2In.vecNormY.fZ) + (vecNormZ.fX * bbBox2In.vecNormZ.fZ);

  vecAxisYB.fX = (vecNormX.fY * bbBox2In.vecNormX.fX) + (vecNormY.fY * bbBox2In.vecNormY.fX) + (vecNormZ.fY * bbBox2In.vecNormZ.fX);
  vecAxisYB.fY = (vecNormX.fY * bbBox2In.vecNormX.fY) + (vecNormY.fY * bbBox2In.vecNormY.fY) + (vecNormZ.fY * bbBox2In.vecNormZ.fY);
  vecAxisYB.fZ = (vecNormX.fY * bbBox2In.vecNormX.fZ) + (vecNormY.fY * bbBox2In.vecNormY.fZ) + (vecNormZ.fY * bbBox2In.vecNormZ.fZ);

  vecAxisZB.fX = (vecNormX.fZ * bbBox2In.vecNormX.fX) + (vecNormY.fZ * bbBox2In.vecNormY.fX) + (vecNormZ.fZ * bbBox2In.vecNormZ.fX);
  vecAxisZB.fY = (vecNormX.fZ * bbBox2In.vecNormX.fY) + (vecNormY.fZ * bbBox2In.vecNormY.fY) + (vecNormZ.fZ * bbBox2In.vecNormZ.fY);
  vecAxisZB.fZ = (vecNormX.fZ * bbBox2In.vecNormX.fZ) + (vecNormY.fZ * bbBox2In.vecNormY.fZ) + (vecNormZ.fZ * bbBox2In.vecNormZ.fZ);


  vecCenterBMinusA = bbBox2In.vecWorldCenter - vecWorldCenter;

  //vecCenterB.fX = vecNormX * vecCenterBMinusA;
  //vecCenterB.fY = vecNormY * vecCenterBMinusA;
  //vecCenterB.fZ = vecNormZ * vecCenterBMinusA;

  vecCenterB.fX = (vecNormX.fX * vecCenterBMinusA.fX) + (vecNormX.fY * vecCenterBMinusA.fY) + (vecNormX.fZ * vecCenterBMinusA.fZ);
  vecCenterB.fY = (vecNormY.fX * vecCenterBMinusA.fX) + (vecNormY.fY * vecCenterBMinusA.fY) + (vecNormY.fZ * vecCenterBMinusA.fZ);
  vecCenterB.fZ = (vecNormZ.fX * vecCenterBMinusA.fX) + (vecNormZ.fY * vecCenterBMinusA.fY) + (vecNormZ.fZ * vecCenterBMinusA.fZ);

  // calc the values we will reuse.

  vecAbsAxisUB.fX = fabsf (vecAxisXB.fX);  vecAbsAxisUB.fY = fabsf (vecAxisXB.fY);  vecAbsAxisUB.fZ = fabsf (vecAxisXB.fZ);
  vecAbsAxisVB.fX = fabsf (vecAxisYB.fX);  vecAbsAxisVB.fY = fabsf (vecAxisYB.fY);  vecAbsAxisVB.fZ = fabsf (vecAxisYB.fZ);
  vecAbsAxisWB.fX = fabsf (vecAxisZB.fX);  vecAbsAxisWB.fY = fabsf (vecAxisZB.fY);  vecAbsAxisWB.fZ = fabsf (vecAxisZB.fZ);

  //sprintf (szDbgBuffer, "Vol #1  %f %f, Vol #2  %f %f, Center B %f %f\n",
  //         vecWorldCenter.fX, vecWorldCenter.fY,
  //         bbBox2In.vecWorldCenter.fX, bbBox2In.vecWorldCenter.fY,
  //         vecCenterB.fX, vecCenterB.fY);
  //OutputDebugString (szDbgBuffer);

  // now we must step through each of the 15 possible overlap cases and
  //  check for overlap. If any are found to not overlap, we have found
  //  our axis of separation and can return FALSE.

  // Unfortunately we must code each of the 15 cases individually because
  //  they each reduce differently.

  // Case 1: l = Au

  fDistance = fabsf (vecCenterB.fX);
  fRadiusA  = vecHalfLengths.fX;
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisUB.fX) + (bbBox2In.vecHalfLengths.fY * vecAbsAxisVB.fX) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisWB.fX);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 2: l = Av

  fDistance = fabsf (vecCenterB.fY);
  fRadiusA  = vecHalfLengths.fY;
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisUB.fY) + (bbBox2In.vecHalfLengths.fY * vecAbsAxisVB.fY) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisWB.fY);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 3: l = Aw

  fDistance = fabsf (vecCenterB.fZ);
  fRadiusA  = vecHalfLengths.fZ;
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisUB.fZ) + (bbBox2In.vecHalfLengths.fY * vecAbsAxisVB.fZ) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisWB.fZ);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 4: l = Bu

  fDistance = fabsf (vecCenterB * vecAxisXB);
  fRadiusA  = vecHalfLengths * vecAbsAxisUB;
  fRadiusB  = bbBox2In.vecHalfLengths.fX;

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 5: l = Bv

  fDistance = fabsf (vecCenterB * vecAxisYB);
  fRadiusA  = vecHalfLengths * vecAbsAxisVB;
  fRadiusB  = bbBox2In.vecHalfLengths.fY;

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 6: l = Bw

  fDistance = fabsf (vecCenterB * vecAxisZB);
  fRadiusA  = vecHalfLengths * vecAbsAxisWB;
  fRadiusB  = bbBox2In.vecHalfLengths.fZ;

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case  7: l = Au x Bu

  fDistance = fabsf ( -(vecCenterB.fY * vecAxisXB.fZ) + (vecCenterB.fZ * vecAxisXB.fY));
  fRadiusA  = (vecHalfLengths.fY * vecAbsAxisUB.fZ) + (vecHalfLengths.fZ * vecAbsAxisUB.fY);
  fRadiusB  = (bbBox2In.vecHalfLengths.fY * vecAbsAxisWB.fX) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisVB.fX);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case  8: l = Au x Bv

  fDistance = fabsf ( -(vecCenterB.fY * vecAxisYB.fZ) + (vecCenterB.fZ * vecAxisYB.fY));
  fRadiusA  = (vecHalfLengths.fY * vecAbsAxisVB.fZ) + (vecHalfLengths.fZ * vecAbsAxisVB.fY);
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisWB.fX) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisUB.fX);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case  9: l = Au x Bw

  fDistance = fabsf ( -(vecCenterB.fY * vecAxisZB.fZ) + (vecCenterB.fZ * vecAxisZB.fY));
  fRadiusA  = (vecHalfLengths.fY * vecAbsAxisWB.fZ) + (vecHalfLengths.fZ * vecAbsAxisWB.fY);
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisVB.fX) + (bbBox2In.vecHalfLengths.fY * vecAbsAxisUB.fX);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 10: l = Av x Bu

  fDistance = fabsf ( (vecCenterB.fX * vecAxisXB.fZ) - (vecCenterB.fZ * vecAxisXB.fX));
  fRadiusA  = (vecHalfLengths.fX * vecAbsAxisUB.fZ) + (vecHalfLengths.fZ * vecAbsAxisUB.fX);
  fRadiusB  = (bbBox2In.vecHalfLengths.fY * vecAbsAxisWB.fY) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisVB.fY);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 11: l = Av x Bv

  fDistance = fabsf ( (vecCenterB.fX * vecAxisYB.fZ) - (vecCenterB.fZ * vecAxisYB.fX));
  fRadiusA  = (vecHalfLengths.fX * vecAbsAxisVB.fZ) + (vecHalfLengths.fZ * vecAbsAxisVB.fX);
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisWB.fY) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisUB.fY);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 12: l = Av x Bw

  fDistance = fabsf ( (vecCenterB.fX * vecAxisZB.fZ) - (vecCenterB.fZ * vecAxisZB.fX));
  fRadiusA  = (vecHalfLengths.fX * vecAbsAxisWB.fZ) + (vecHalfLengths.fZ * vecAbsAxisWB.fX);
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisVB.fY) + (bbBox2In.vecHalfLengths.fY * vecAbsAxisUB.fY);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 13: l = Aw x Bu

  fDistance = fabsf ( - (vecCenterB.fX * vecAxisXB.fY) + (vecCenterB.fY * vecAxisXB.fX));
  fRadiusA  = (vecHalfLengths.fX * vecAbsAxisUB.fY) + (vecHalfLengths.fY * vecAbsAxisUB.fX);
  fRadiusB  = (bbBox2In.vecHalfLengths.fY * vecAbsAxisWB.fZ) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisVB.fZ);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 14: l = Aw x Bv

  fDistance = fabsf ( - (vecCenterB.fX * vecAxisYB.fY) + (vecCenterB.fY * vecAxisYB.fX));
  fRadiusA  = (vecHalfLengths.fX * vecAbsAxisVB.fY) + (vecHalfLengths.fY * vecAbsAxisVB.fX);
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisWB.fZ) + (bbBox2In.vecHalfLengths.fZ * vecAbsAxisUB.fZ);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  // Case 15: l = Aw x Bw

  fDistance = fabsf ( - (vecCenterB.fX * vecAxisZB.fY) + (vecCenterB.fY * vecAxisZB.fX));
  fRadiusA  = (vecHalfLengths.fX * vecAbsAxisWB.fY) + (vecHalfLengths.fY * vecAbsAxisWB.fX);
  fRadiusB  = (bbBox2In.vecHalfLengths.fX * vecAbsAxisVB.fZ) + (bbBox2In.vecHalfLengths.fY * vecAbsAxisUB.fZ);

  if ((fRadiusA + fRadiusB) < fDistance) return (FALSE);

  return (TRUE);
  };

//------------------------------------------------------------------------
BOOL BBox::TriIntersect  (CTriangle &      triangleIn)
  {


  // The algorithm to check if a poly and a box intersect has three steps.
  //  1) First check the three triangle vertices against the planes of the
  //      box faces.  If all three verts are on the positive side of the plane,
  //      the triangle is trivially rejected.  If one point is on the negative
  //      side of all the planes, the triangle is trivially accepted.
  //  2) A swept volume is created for each edge by sweeping the box volume along
  //      it.  Then the box center is checked for inclusion in this volume.  If
  //      the point is in the volume, you have an intersection.
  //  3) Find the box diagonal that is most parallel to the triangle normal.  If
  //      this diagonal segment intersects the triangle, you have an intersection.
  //      Otherwise there is no intersection.

  // to be implemented



  return (FALSE);
  };  
  
  
