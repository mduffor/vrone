/* -----------------------------------------------------------------
                             Display Mesh

     This module implements a mesh object which is stored in a 
     format that is friendly to being displayed by the graphics card.
     
                            Michael T. Duffy
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef DISPLAYMESH_HPP
#define DISPLAYMESH_HPP


#include "Types.hpp"
#include "RVec.hpp"
#include "GLUtil.hpp"
#include "EditMesh.hpp"

#define MAX_BUFFER_SIZE 65534

// REFACTOR: Need to look at whether Normals need to be switched to half-floats.

// Note: If any of the below types change, be sure to update the FLOAT_TO_*_TYPE macros as well.
#define POSITION_TYPE FLOAT
#define NORMAL_TYPE FLOAT
#define UV_TYPE UINT16

#define INDEX_TYPE UINT16

#define POSITION_GL_TYPE GL_FLOAT
#define NORMAL_GL_TYPE GL_FLOAT
#define UV_GL_TYPE GL_UNSIGNED_SHORT

#define INDEX_GL_TYPE GL_UNSIGNED_SHORT

// Reference: http://www.informit.com/articles/article.aspx?p=2033340&seqNum=3

#define FLOAT_TO_POSITION_TYPE(value) (value)
#define FLOAT_TO_NORMAL_TYPE(value) (value)
//#define FLOAT_TO_UV_TYPE(value) ((UV_TYPE) ((((value)*FLOAT(UINT32(UINT16_MAX))) - 1.0f) / 2.0f ))
#define FLOAT_TO_UV_TYPE(value) ((UV_TYPE) ((((value)*FLOAT(UINT32(UINT16_MAX))) - 1.0f) ))

#define INT_TO_INDEX_TYPE(value) ((INDEX_TYPE)(value))


//------------------------------------------------------------------------------
class DisplayMesh
//------------------------------------------------------------------------------
  {
  private:
    UINT             uVertexBO;
    UINT             uIndexBO;
    
    INT              iPositionElementCount;
    INT              iNormalElementCount;
    INT              iUVElementCount;
    INT              iStride;
    
    BOOL             bVertsChanged;
    INT              iCurrVert;
    INT              iMaxVert;
    unsigned char *  pInterlacedBuffer;
    
    BOOL             bIndexesChanged;
    INT              iCurrIndex;
    INT              iMaxIndex;
    INDEX_TYPE *     pIndexBuffer;
    
    BOOL             bCulled;

  public:
    DisplayMesh *    pNextInMaterial;
    DisplayMesh *    pMasterNext;      /// Master list of DisplayMeshes loaded into memory
    
  public:
         DisplayMesh  ();
         
         ~DisplayMesh ();

    VOID Register         (VOID);

    VOID Unregister       (VOID);
         
    VOID SetElementCounts (INT  iNumPositions,
                           INT  iNumNormals,
                           INT  iNumUVs);

    VOID FreeBuffers      ();

    VOID Reset            ()  {bVertsChanged = TRUE; iCurrVert = 0; bIndexesChanged = TRUE; iCurrIndex = 0;};
    
    VOID InitStride      ();

    VOID GrowVertArray    (INT  iCountIn);
    
    VOID GrowIndexArray   (INT  iCountIn);
    
    INT  AddVerts         (INT            iCount,
                           const FLOAT *  pPositions,
                           const FLOAT *  pNormals,
                           const FLOAT *  pUVs);
    
    INT  AddTriangleStrip (INT            iCount,
                           INT            iOffset,
                           const INT *    pIndexes);
                           
    VOID Draw             (INT iPositionAttrib, 
                           INT iNormalAttrib,
                           INT iUVAttrib);
    
    BOOL IsCulled         (VOID)                     {return bCulled;};

    static VOID OnContextLost  (VOID);
    
    
    
    
    
    
  };



#endif // DISPLAY_MESH