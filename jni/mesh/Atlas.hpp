/* -----------------------------------------------------------------
                            Atlas

     This module implements sprite atlases.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef ATLAS_HPP
#define ATLAS_HPP


#include "Types.hpp"
#include "RStr.hpp"
#include "RVec.hpp"
#include "RVecArray.hpp"
#include "IntArray.hpp"
#include "LinkedList.hpp"
#include "DisplayMesh.hpp"
#include "Material.hpp"

//------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------


//------------------------------------------------------------------------
// Forward Declarations
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Class Definitions
//------------------------------------------------------------------------

//=============================================================================
class AtlasImage
  {
  public:
    RStr         strId;
    INT          iX;
    INT          iY;
    INT          iWidth;
    INT          iHeight;
    INT          iOffsetX;
    INT          iOffsetY;
    INT          iOrigWidth;
    INT          iOrigHeight;
    INT          iPage;
    AtlasImage * pNext;
  };


//=============================================================================
class Atlas
  {
  public:
    enum AlignHoriz {Left,
                     Center,
                     Right}; 
    
    INT           iAtlasWidth;   // Pixel dimensions of the atlas
    INT           iAtlasHeight;  // 

    RStr          strAtlasName;

    AtlasImage *  plistImages;
    RStr          strAtlasDef;

    INT           iId;
    RStr          strFile; ///< Texture file

    Material *    pmtlAtlas;
    
    Atlas *       pMasterNext;
    
    
  public:
  
                         Atlas            ();
         
                         ~Atlas           ();
         
    const char *         GetName          (VOID)   {return (strAtlasName.AsChar ());};

    VOID                 Register         (VOID);

    VOID                 Unregister       (VOID);
    
    static Atlas *       FindByName       (const char *  szNameIn);
         
    VOID                 ParseAtlas       (const char *  szAtlasDefIn);
    
    VOID                 AddTriangleStrip (const char *   szImageNameIn,
                                           AlignHoriz     alignIn,
                                           INT            iPixelsPerUnit, // Is this valid in 3D space, or is it just a scale factor?
                                           RMatrix &      matIn,
                                           DisplayMesh *  pDisplayMesh);
                                           
    VOID                 CreateMaterial   (VOID);
    
    const char *         GetMaterialName  (VOID);
    
    const char *         GetTexturePath   (VOID)     {return (strFile.AsChar ());};
                                           
  };

  
//extern Atlas * pBMFontHead;
  
#endif // ATLAS_HPP