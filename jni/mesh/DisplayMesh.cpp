/* -----------------------------------------------------------------
                             File Path

     This module implements cross-platform routines for dealing with
     file and directory paths

                            Michael T. Duffy
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include <stdlib.h>
#include <string.h>

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "DisplayMesh.hpp"

static DisplayMesh *  pmeshMasterList = NULL;

//------------------------------------------------------------------------------
DisplayMesh::DisplayMesh ()
  {
  iPositionElementCount = 0;
  iNormalElementCount   = 0;
  iUVElementCount       = 0;
  
  uVertexBO         = 0;
  bVertsChanged     = TRUE;
  iCurrVert         = 0;
  iMaxVert          = 0;
  pInterlacedBuffer = NULL;
  
  uIndexBO        = 0;
  bIndexesChanged = TRUE;
  iCurrIndex      = 0;
  iMaxIndex       = 0;
  pIndexBuffer    = NULL;  

  pNextInMaterial = NULL;
  pMasterNext     = NULL;
  bCulled         = FALSE;
  
  Register ();
  };

//------------------------------------------------------------------------------
DisplayMesh::~DisplayMesh ()
  {
  FreeBuffers ();
  };

//-----------------------------------------------------------------------------
VOID DisplayMesh::Register (VOID)
  {
  /// Store in the master list of materials.
  this->pMasterNext = pmeshMasterList;
  pmeshMasterList = this;
  };

//-----------------------------------------------------------------------------
VOID DisplayMesh::Unregister (VOID)
  {
  /// Remove from the master list of materials.
  DisplayMesh *  pPrev = NULL;
  DisplayMesh *  pCurr = pmeshMasterList;
  
  while (pCurr != NULL)
    {
    if (pCurr == this)
      {
      if (pPrev == NULL)
        {
        pmeshMasterList = pCurr->pMasterNext;
        }
      else
        {
        pPrev->pMasterNext = pCurr->pMasterNext;
        };
      pMasterNext = NULL;
      return;
      }
    pPrev = pCurr;
    pCurr = pCurr->pMasterNext;
    }
  };  
  
//------------------------------------------------------------------------------
VOID DisplayMesh::SetElementCounts (INT  iNumPositions,
                                    INT  iNumNormals,
                                    INT  iNumUVs)
  {
  iPositionElementCount = iNumPositions;
  iNormalElementCount   = iNumNormals;
  iUVElementCount       = iNumUVs;
  
  FreeBuffers ();
  InitStride ();
  }

//------------------------------------------------------------------------------
VOID DisplayMesh::FreeBuffers ()
  {
  if (pIndexBuffer != NULL)
    {
    free (pIndexBuffer);
    pIndexBuffer = NULL;
    };
  if (pInterlacedBuffer != NULL)
    {
    free (pInterlacedBuffer);
    pInterlacedBuffer = NULL;
    };
  if (uVertexBO != 0)
    {
    glDeleteBuffers (1, &uVertexBO);
    uVertexBO = 0;
    };
  if (uIndexBO != 0)
    {
    glDeleteBuffers (1, &uIndexBO);
    uIndexBO = 0;
    };
    
  iCurrVert   = 0;
  iMaxVert    = 0;
  bVertsChanged = TRUE;
  
  iCurrIndex      = 0;
  iMaxIndex       = 0;
  bIndexesChanged = TRUE;
  };
  
//------------------------------------------------------------------------------
VOID DisplayMesh::InitStride ()
  {
  iStride = iPositionElementCount * sizeof (POSITION_TYPE) + 
            iNormalElementCount * sizeof (NORMAL_TYPE) + 
            iUVElementCount * sizeof (UV_TYPE);
  };
  
//------------------------------------------------------------------------------
VOID DisplayMesh::GrowVertArray (INT  iCountIn)
  {
  if (iCurrVert + iCountIn < iMaxVert)
    {
    // we have enough space already
    return;
    }
  
  INT  iNewMax = iCurrVert + iCountIn;
  unsigned char *  pNewBuffer = (unsigned char *) realloc (pInterlacedBuffer, iStride * iNewMax);
  if (pNewBuffer != NULL)
    {
    pInterlacedBuffer = pNewBuffer;
    iMaxVert = iNewMax;
    };
  };

//------------------------------------------------------------------------------
VOID DisplayMesh::GrowIndexArray (INT  iCountIn)
  {
  if (iCurrIndex + iCountIn < iMaxIndex)
    {
    // we have enough space already
    return;
    }

  INT  iNewMax = iCurrIndex + iCountIn;
  INDEX_TYPE *  pNewBuffer = (INDEX_TYPE *) realloc (pIndexBuffer, sizeof (INDEX_TYPE) * iNewMax);
  if (pNewBuffer != NULL)
    {
    pIndexBuffer = pNewBuffer;
    iMaxIndex = iNewMax;
    };
  };
  
//------------------------------------------------------------------------------
INT DisplayMesh::AddVerts (INT            iCount,
                           const FLOAT *  pPositions,
                           const FLOAT *  pNormals,
                           const FLOAT *  pUVs)
  {
  INT  iStartVert = iCurrVert;
  INT  iIndex;
  
  bVertsChanged = TRUE;
  GrowVertArray (iCount);
  
  unsigned char *  pWritePos = &pInterlacedBuffer[iCurrVert * iStride];
  while (iCount)
    {
    // copy positions
    iIndex = iPositionElementCount;
    while (iIndex)
      {
      *(POSITION_TYPE *)pWritePos = FLOAT_TO_POSITION_TYPE(*pPositions);
      ++pPositions;
      pWritePos += sizeof (POSITION_TYPE);
      --iIndex;
      }
    // copy normals
    iIndex = iNormalElementCount;
    while (iIndex)
      {
      *(NORMAL_TYPE *)pWritePos = FLOAT_TO_NORMAL_TYPE(*pNormals);
      ++pNormals;
      pWritePos += sizeof (NORMAL_TYPE);
      --iIndex;
      }
    
    // copy UVs
    iIndex = iUVElementCount;
    while (iIndex)
      {
      *(UV_TYPE *)pWritePos = FLOAT_TO_UV_TYPE(*pUVs);
      ++pUVs;
      pWritePos += sizeof (UV_TYPE);
      --iIndex;
      }
      
    --iCount;
    ++iCurrVert;
    }
  return (iStartVert);
  };

//------------------------------------------------------------------------------
INT DisplayMesh::AddTriangleStrip (INT          iCount,
                                   INT          iOffset,
                                   const INT *  pIndexes)
  {
  INT  iStartIndex = iCurrIndex;
  INT  iIndex;
  INT  iDegenerativeCount = (iCurrIndex == 0) ? 0 : 2;
  
  bIndexesChanged = TRUE;
  GrowIndexArray (iCount + iDegenerativeCount);
  
  // duplicate last index and first index to create the degenerative triangles 
  //  between the two triangle strips.
  if (iCurrIndex != 0)
    {
    pIndexBuffer [iCurrIndex] = pIndexBuffer [iCurrIndex - 1];
    ++iCurrIndex;
    pIndexBuffer [iCurrIndex] = INT_TO_INDEX_TYPE ((*pIndexes) + iOffset);
    ++iCurrIndex;
    };
  
  // copy indexes
  while (iCount)
    {
    pIndexBuffer [iCurrIndex] = INT_TO_INDEX_TYPE ((*pIndexes) + iOffset);
    ++iCurrIndex;
    ++pIndexes;
    --iCount;
    }
  return (iStartIndex);
  };
  
//------------------------------------------------------------------------------
VOID DisplayMesh::Draw (INT iPositionAttrib, 
                        INT iNormalAttrib,
                        INT iUVAttrib)
  {
  // mesh has changed since last draw.  (Re)bind arrays.
  if (uVertexBO == 0) {glGenBuffers (1, &uVertexBO);  GL_CHECK_ERROR ("glGenBuffers uVertexBO"); };
  if (uIndexBO == 0) {glGenBuffers (1, &uIndexBO);   GL_CHECK_ERROR ("glGenBuffers uIndexBO"); };

  glBindBuffer (GL_ARRAY_BUFFER, uVertexBO);
  GL_CHECK_ERROR ("glBindBuffer ");
  
  if (bVertsChanged)
    {
    glBufferData (GL_ARRAY_BUFFER, iCurrVert * iStride, pInterlacedBuffer, GL_STATIC_DRAW);
    GL_CHECK_ERROR ("glBufferData ");
    bVertsChanged = false;
    }
    
  if (iPositionAttrib != -1) 
    {
    glEnableVertexAttribArray (iPositionAttrib);
    GL_CHECK_ERROR ("glEnableVertexAttribArray ");
    
    glVertexAttribPointer (iPositionAttrib, 
                           iPositionElementCount, 
                           POSITION_GL_TYPE, 
                           GL_FALSE, // normalized
                           iStride, 
                           (void *)0);
    GL_CHECK_ERROR ("glVertexAttribPointer Position");
    }
    
  if (iNormalAttrib != -1) 
    {
    glEnableVertexAttribArray (iNormalAttrib);
    GL_CHECK_ERROR ("glEnableVertexAttribArray ");
    
    glVertexAttribPointer (iNormalAttrib, 
                           iNormalElementCount, 
                           NORMAL_GL_TYPE, 
                           GL_FALSE, // normalized
                           iStride, 
                           (void *)(iPositionElementCount * sizeof (POSITION_TYPE)));
    GL_CHECK_ERROR ("glVertexAttribPointer Normal");
    }
  if (iUVAttrib != -1) 
    {
    glEnableVertexAttribArray (iUVAttrib);
    GL_CHECK_ERROR ("glEnableVertexAttribArray ");
    
    glVertexAttribPointer (iUVAttrib, 
                           iUVElementCount, 
                           UV_GL_TYPE, 
                           GL_TRUE, // normalized
                           iStride, 
                           (void *)(iPositionElementCount * sizeof (POSITION_TYPE) + iNormalElementCount * sizeof (NORMAL_TYPE)));
    GL_CHECK_ERROR ("glVertexAttribPointer UV");
    }
  
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, uIndexBO);
  GL_CHECK_ERROR ("glBindBuffer ");
  if (bIndexesChanged)
    {
    glBufferData (GL_ELEMENT_ARRAY_BUFFER, iCurrIndex * sizeof (INDEX_TYPE), pIndexBuffer, GL_STATIC_DRAW);
    GL_CHECK_ERROR ("glBufferData ");
    bIndexesChanged = false;
    }
  
  glDrawElements(GL_TRIANGLE_STRIP, iCurrIndex, INDEX_GL_TYPE, (void*)0);  
  GL_CHECK_ERROR ("glDrawElements ");
  
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
  GL_CHECK_ERROR ("glBindBuffer ");
  
  glBindBuffer (GL_ARRAY_BUFFER, 0);
  GL_CHECK_ERROR ("glBindBuffer GL_ARRAY_BUFFER 0");

  /*    
    glVertexAttribPointer (iPositionAttrib, 
                           iPositionElementCount, 
                           POSITION_GL_TYPE, 
                           GL_FALSE, 
                           iStride, 
                           (void *)pInterlacedBuffer);  
  
  glDrawArrays    (GL_TRIANGLE_STRIP, 0, 4);
  GLCheckError ("glDrawArrays");    
  */  
  }
  
//-----------------------------------------------------------------------------
VOID  DisplayMesh::OnContextLost (VOID)
  {
  DisplayMesh *  pCurr = pmeshMasterList;
  
  while (pCurr != NULL)
    {
    pCurr->uVertexBO = 0;
    pCurr->uIndexBO  = 0;
    pCurr->bVertsChanged   = TRUE;
    pCurr->bIndexesChanged = TRUE;
    
    pCurr = pCurr->pMasterNext;
    };
  };
  
  
  
/*

  There are two kinds of DisplayMesh, static and dynamic.  
  Static is for non-deforming objects.  All vertex traits in interleaved VBO.  
  Stored in triangle strips with degenerate triangles between strips.  Indexed vertices.  
  Take triangle strips as input.  External mesh-readers will have to worry about stripping tris.  
  Pre-alloc point and index arrays.  Grow point arrays.  
  GL_STATIC_DRAW
  Only fill VBO during init for static objects.
  
  Fill buffer each time for dynamic objects.
  For dynamic objects, tex coords are in a different VBO than point and normal data.
  Fill point/normal VBO for dynamic objects only if the values change.  Otherwise re-use them.
*/

