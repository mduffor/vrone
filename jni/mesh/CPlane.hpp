/* -----------------------------------------------------------------
                          Collision Plane Primitive

     This module implements a geometric plane.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2000-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef CPLANE_HPP
#define CPLANE_HPP


#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"


//------------------------------------------------------------------------
class CPlane
//------------------------------------------------------------------------
  {
  public:


    RVec3        vecNormal;
    FLOAT        fD;

  public:

              CPlane      ()                                 {};

              CPlane      (FLOAT  fAIn,
                           FLOAT  fBIn,
                           FLOAT  fCIn,
                           FLOAT  fDIn)                      {vecNormal.Set (fAIn, fBIn, fCIn); fD = fDIn;};

              CPlane      (const CPlane& planeIn)            {vecNormal.Set (planeIn.vecNormal); fD = planeIn.fD;};

              CPlane      (const RVec3&  vecNormIn,
                           FLOAT         fDist)              {Set (vecNormIn, fDist);};

              CPlane      (const RVec3&  vecNormIn,
                           const RVec3&  vecPoint)           {Set (vecNormIn, vecPoint);};

              CPlane      (const RVec3&  vecPointA,
                           const RVec3&  vecPointB,
                           const RVec3&  vecPointC)          {Set (vecPointA, vecPointB, vecPointC);};
              
              
              ~CPlane     ()                                 {};

    VOID      Set           (FLOAT  fAIn,
                             FLOAT  fBIn,
                             FLOAT  fCIn,
                             FLOAT  fDIn)                    {vecNormal.Set (fAIn, fBIn, fCIn); fD = fDIn;};

    VOID      Set           (const RVec3&  vecNormIn,
                             FLOAT         fDist)            {vecNormal.Set (vecNormIn); fD = fDist;};

    VOID      Set           (const RVec3&  vecNormIn,
                             const RVec3&  vecPoint)         {vecNormal.Set (vecNormIn); fD = - vecPoint * vecNormal;};

    VOID      Set           (const RVec3&  vecPointA,
                             const RVec3&  vecPointB,
                             const RVec3&  vecPointC)        {RVec3  vecBA = vecPointA - vecPointB;
                                                              RVec3  vecBC = vecPointC - vecPointB;
                                                              vecNormal = vecBA % vecBC;
                                                              vecNormal.Normalize ();
                                                              fD = - vecPointB * vecNormal;};
                             
    VOID      Zero          (VOID)                           {vecNormal.Zero (); fD = 0.0f;};

    // assignment
    CPlane&   operator=     (const CPlane& planeIn)          {vecNormal.Set (planeIn.vecNormal); fD = planeIn.fD; return *this;};

    // reverse normal (unary negation)
    CPlane    operator-     (VOID) const                     {return (CPlane (-vecNormal.fX, -vecNormal.fY, -vecNormal.fZ, fD));};

    // equality (each value within SB_EPSOLON (0.001) of each other)
    BOOL      operator==    (const CPlane& planeIn) const    {return ((vecNormal == planeIn.vecNormal) && (fabsf (fD - planeIn.fD) <= R_EPSILON));};
    BOOL      operator!=    (const CPlane& planeIn) const    {return ((vecNormal != planeIn.vecNormal) || (fabsf (fD - planeIn.fD) > R_EPSILON));};

    // distance between the plane and a point
    FLOAT     Distance      (const RVec3&  vecPointIn) const {return (vecNormal * vecPointIn) + fD;};

    RVec3     GetNormal     (VOID) const                     {return vecNormal;};

  };



#endif // CPLANE_HPP
