/* -----------------------------------------------------------------
                            Atlas

     This module implements sprite atlases.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2013-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include <string.h>
#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "Atlas.hpp"
#include "RVec.hpp"
#include "RStr.hpp"
#include "RStrParser.hpp"

// Global list of all bitmap font instances
static Atlas * pMasterAtlasList = NULL;



//------------------------------------------------------------------------
Atlas::Atlas ()
  {
  strAtlasName = "";
  plistImages = NULL;
  pmtlAtlas = NULL;
  pMasterNext = NULL;
  iAtlasWidth = iAtlasHeight = 0;
  iId = 0;
  
  // add self to global list
  Register ();
  };


//------------------------------------------------------------------------
Atlas::~Atlas ()
  {
  // delete image list
  AtlasImage *  pNext = NULL;
  AtlasImage *  pCurr = plistImages;
  while (pCurr != NULL)
    {
    pNext = pCurr->pNext;
    delete pCurr;
    pCurr = pNext;
    }
  
  // remove self from global list
  Unregister ();
  };

  
//-----------------------------------------------------------------------------
VOID Atlas::Register (VOID)
  {
  /// Store in the master list of atlases.
  this->pMasterNext = pMasterAtlasList;
  pMasterAtlasList = this;
  };

//-----------------------------------------------------------------------------
VOID Atlas::Unregister (VOID)
  {
  /// Remove from the master list of atlases.
  
  Atlas *  pPrev = NULL;
  Atlas *  pCurr = pMasterAtlasList;
  
  while (pCurr != NULL)
    {
    if (pCurr == this)
      {
      if (pPrev == NULL)
        {
        // deleting head
        pMasterAtlasList = this->pMasterNext;
        }
      else
        {
        pPrev->pMasterNext = this->pMasterNext;
        };
      break;
      };
    pPrev = pCurr;
    pCurr = pCurr->pMasterNext;
    };  
  };  

//------------------------------------------------------------------------
Atlas *  Atlas::FindByName (const char *  szNameIn) 
  {
  Atlas *  pCurr = pMasterAtlasList;
  while (pCurr != NULL)
    {
    if (streq (pCurr->GetName (), szNameIn))
      {
      return (pCurr);
      };
    pCurr = pCurr->pMasterNext;
    };
  return (NULL);
  };
  
//------------------------------------------------------------------------
VOID Atlas::ParseAtlas (const char *  szAtlasDefIn)
  {
  RStr          strKey;
  RStrParser    parserValue;
  RStr          strLineType;
  //INT           iImageCount;
  AtlasImage *  pImage;
  
  /*      
  info name="ABC" sizeW=64 sizeH=64
  page id=0 file="ABC.png"
  images count=3
  image id="A" x=0 y=0 width=22 height=30 sizeW=32 sizeH=32 xoffset=-6 yoffset=-2 page=0
  image id="B" x=22 y=0 width=19 height=29 sizeW=32 sizeH=32 xoffset=-6 yoffset=-3 page=0
  image id="C" x=41 y=0 width=23 height=28 sizeW=32 sizeH=32 xoffset=-5 yoffset=-2 page=0
  */      
  
  RStrParser  parser = szAtlasDefIn;

  while (! parser.IsEOF ())
    {
    // for each line
    while (!parser.IsEOL ())
      {
      strLineType = parser.GetWord (FALSE);

      //  Info Line 
      if (strLineType == "info")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);

            // process key-value pair
            if      (strKey == "name")     {parserValue.GetQuoteString (&strAtlasName);}
            else if (strKey == "sizeW")    {iAtlasWidth  = parserValue.GetInt ();}
            else if (strKey == "sizeH")    {iAtlasHeight = parserValue.GetInt ();}
            };
          };
        }
        
      // Page Line
      else if (strLineType == "page")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "id")        {iId = parserValue.GetInt ();}
            else if (strKey == "file") {parserValue.GetQuoteString (&strFile);}
            };
          };
        }
      // Images Line
      else if (strLineType == "images")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "count")        {/*iImageCount =*/ parserValue.GetInt ();}
            };
          };
        }
      // Image line
      else if (strLineType == "image")
        {
        RStr  strId;
        
        // Create a new atlas image and put it at the end of the list
        pImage = new AtlasImage;
        pImage->pNext = NULL;
        
        if (plistImages == NULL)
          {
          plistImages = pImage;
          }
        else
          {
          AtlasImage *  pLast = plistImages;
          while (pLast->pNext != NULL)
            {
            pLast = pLast->pNext;
            }
          pLast->pNext = pImage;
          }        
        
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "id")            {parserValue.GetQuoteString (&pImage->strId);}
            else if (strKey == "x")        {pImage->iX          = parserValue.GetInt ();}
            else if (strKey == "y")        {pImage->iY          = parserValue.GetInt ();}
            else if (strKey == "width")    {pImage->iWidth      = parserValue.GetInt ();}
            else if (strKey == "height")   {pImage->iHeight     = parserValue.GetInt ();}
            else if (strKey == "sizeW")    {pImage->iOrigWidth  = parserValue.GetInt ();}
            else if (strKey == "sizeH")    {pImage->iOrigHeight = parserValue.GetInt ();}
            else if (strKey == "xoffset")  {pImage->iOffsetX    = parserValue.GetInt ();}
            else if (strKey == "yoffset")  {pImage->iOffsetY    = parserValue.GetInt ();}
            else if (strKey == "page")     {pImage->iPage       = parserValue.GetInt ();}
            };
          };
        };
      
      } // while not EOL
    parser.GotoNextLine ();
    }; // while not EOF
  }; // ParseAtlas
  
  
//---------------------------------------------------------------------------
VOID Atlas::AddTriangleStrip (const char *   szImageNameIn,
                              AlignHoriz     alignIn,
                              INT            iPixelsPerUnit, // Is this valid in 3D space, or is it just a scale factor?
                              RMatrix &      matIn,
                              DisplayMesh *  pDisplayMesh)
  {  
  INT  iVertOffset = 0;
  INT  horizOffset = 0;
  FLOAT fPositions[12];
  FLOAT fUVs[8];
  
  // Note:  This needs to be sped up somehow, either by caching the pointer to the AtlasImage, or
  //   turning the string into a hashed value for faster searching.
  // find the image
  AtlasImage *  pImage = plistImages;
  while (pImage != NULL)
    {
    if (streq (szImageNameIn, pImage->strId.AsChar ()))
      {
      break;
      }
    pImage = pImage->pNext;
    }
  if (pImage == NULL) return;

  pDisplayMesh->SetElementCounts (3, 0, 2);
  pDisplayMesh->GrowVertArray    (4);
  pDisplayMesh->GrowIndexArray   (5);  
  
  static const INT aIndexes[] = {0, 1, 2, 3};

  FLOAT  X0;
  FLOAT  X1;
  FLOAT  Y0;
  FLOAT  Y1;

  // TODO: Implement horizontal adjustments
  //if (alignIn == Atlas::Center) horizOffset = -lineWidth / 2;
  //if (alignIn == Atlas::Right)  horizOffset = -lineWidth;

  INT  iXPos = horizOffset;
  INT  iYPos = 0;

  // solve for position  
  X0 = (float) (iXPos + pImage->iOffsetX) / (float) iPixelsPerUnit;
  X1 = (float) (iXPos + pImage->iOffsetX + pImage->iWidth) / (float) iPixelsPerUnit;
  
  Y0 = (float) (iYPos + pImage->iOffsetY) / (float) iPixelsPerUnit; 
  Y1 = (float) (iYPos + pImage->iOffsetY - pImage->iHeight) / (float) iPixelsPerUnit; 

  RVec4  vecPos0 = matIn.MultVec (X0, Y0, 0.0f);
  RVec4  vecPos1 = matIn.MultVec (X0, Y1, 0.0f);
  RVec4  vecPos2 = matIn.MultVec (X1, Y0, 0.0f);
  RVec4  vecPos3 = matIn.MultVec (X1, Y1, 0.0f);
  
  fPositions[0] = vecPos0.fX; fPositions[1]  = vecPos0.fY; fPositions[2]  = vecPos0.fZ;
  fPositions[3] = vecPos1.fX; fPositions[4]  = vecPos1.fY; fPositions[5]  = vecPos1.fZ;
  fPositions[6] = vecPos2.fX; fPositions[7]  = vecPos2.fY; fPositions[8]  = vecPos2.fZ;
  fPositions[9] = vecPos3.fX; fPositions[10] = vecPos3.fY; fPositions[11] = vecPos3.fZ;
  
  
  //DBG_INFO ("Curr ASCII char is %c (%d) %f,%f %f,%f", currAsciiChar, currAsciiChar, X0, Y0, X1, Y1);
  //DBG_INFO ("Atlas sprite (%s) %f,%f %f,%f", pImage->strId.AsChar (), X0, Y0, X1, Y1);
  
  // solve for texture atlas position 
  X0 = (pImage->iX)/(float)(iAtlasWidth);
  X1 = (pImage->iX + pImage->iWidth)/(float)(iAtlasWidth) ;
  Y1 = /*1.0f -*/ ((pImage->iY)/(float)(iAtlasHeight));
  Y0 = /*1.0f -*/ ((pImage->iY + pImage->iHeight)/(float)(iAtlasHeight));
                              
  // set the texture coordinates
  fUVs[0] = X0; fUVs[1] = Y1; 
  fUVs[2] = X0; fUVs[3] = Y0; 
  fUVs[4] = X1; fUVs[5] = Y1; 
  fUVs[6] = X1; fUVs[7] = Y0;
  
  //DBG_INFO ("Tex %f,%f %f,%f", X0, Y0, X1, Y1);

  // add the next bit of the triangle strip to the display mesh
  iVertOffset = pDisplayMesh->AddVerts (4, fPositions, NULL, fUVs);
  /*INT iStartIndex = */ pDisplayMesh->AddTriangleStrip (4, iVertOffset, aIndexes);
  
  //DBG_INFO ("Vert offset %d strip start %d", iVertOffset, iStartIndex);
  } 
    
//---------------------------------------------------------------------------
VOID Atlas::CreateMaterial (VOID)
  {
  if (pmtlAtlas != NULL) return;
  
  pmtlAtlas = Material::FindByName (GetName ());
  
  if (pmtlAtlas == NULL)
    {
    new Material (GetName (), "ShdUnlitTex");
    pmtlAtlas = Material::FindByName (GetName ());
    }
  
  if (pmtlAtlas != NULL)
    {
    pmtlAtlas->SetDiffuseTexture (strFile);
    }
  else
    {
    DBG_INFO ("Failed to load material %s", GetName ());
    };
  }
    
//---------------------------------------------------------------------------
const char *  Atlas::GetMaterialName  (VOID)
  {
  return (GetName ());
  };
    