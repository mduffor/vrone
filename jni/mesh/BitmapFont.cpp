/* -----------------------------------------------------------------
                         Bitmap Font

     This module implements bitmapped fonts created with the 
   BMFont Windows utility.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2013-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include <string.h>
#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "BitmapFont.hpp"
#include "RVec.hpp"
#include "RStr.hpp"
#include "RStrParser.hpp"

// Global list of all bitmap font instances
static BitmapFont * pMasterFontList = NULL;



//------------------------------------------------------------------------
BitmapFont::BitmapFont ()
  {
/*
  iLineHeight;
  iBaseline;
  iImageWidth;  // ?
  iImageHeight;  // ?
  iPages;
  iPacked;
  iAlphaChan;
  iRedChan;
  iGreenChan;
  iBlueChan;
  */
  
  strTypeface = "";
  iSize     = 0;
  iBold     = 0;
  iItalic   = 0;
  strCharset  = "";
  iUnicode  = 0;
  iStretchH = 100;
  iSmooth   = 0;
  iAA       = 1;
  iPaddingL = 0;
  iPaddingR = 0;
  iPaddingU = 0;
  iPaddingD = 0;
  iSpacingX = 0;
  iSpacingY = 0;
  iOutline  = 0;
  
  //BMFCharDesc  arrayChars [256];
  //RStr          strFontDef;
  
  // add self to global list
  Register ();

  
  iAtlasXPos = 0;   // pixel position on a parent atlas for where the font atlas is stored.
  iAtlasYPos = 0;
  iAtlasWidth  = 0;  // size in pixels of the parent atlas containing the font atlas
  iAtlasHeight = 0;
  
  };


//------------------------------------------------------------------------
BitmapFont::~BitmapFont ()
  {
  // remove self from global list
  Unregister ();
  };


  
//-----------------------------------------------------------------------------
VOID BitmapFont::Register (VOID)
  {
  /// Store in the master list of fonts.
  this->pMasterNext = pMasterFontList;
  pMasterFontList = this;
  };

//-----------------------------------------------------------------------------
VOID BitmapFont::Unregister (VOID)
  {
  /// Remove from the master list of fonts.
  
  BitmapFont *  pPrev = NULL;
  BitmapFont *  pCurr = pMasterFontList;
  
  while (pCurr != NULL)
    {
    if (pCurr == this)
      {
      if (pPrev == NULL)
        {
        // deleting head
        pMasterFontList = this->pMasterNext;
        }
      else
        {
        pPrev->pMasterNext = this->pMasterNext;
        };
      break;
      };
    pPrev = pCurr;
    pCurr = pCurr->pMasterNext;
    };  
  };  

//------------------------------------------------------------------------
BitmapFont *  BitmapFont::FindByName (const char *  szNameIn) 
  {
  BitmapFont *  pCurr = pMasterFontList;
  while (pCurr != NULL)
    {
    if (pCurr->strTypeface == szNameIn)
      {
      return (pCurr);
      };
    pCurr = pCurr->pMasterNext;
    };
  return (NULL);
  };
  
//------------------------------------------------------------------------
VOID BitmapFont::ParseFont (const char *  szFontDefIn)
  {
  //string      currKey;
  //string      currValue;
  INT         kerningIndex = 0;
  RStr        strKey;
  RStrParser  parserValue;
  RStr        strLineType;
  INT         iCharCount;
  INT         iKerningCount;

  
  /*      
  info face="Trebuchet MS" size=32 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1 outline=0
  common lineHeight=32 base=25 scaleW=256 scaleH=256 pages=1 packed=0 alphaChnl=1 redChnl=0 greenChnl=0 blueChnl=0
  page id=0 file="TrebuchetMS32_0.tga"
  chars count=191
  char id=32   x=251   y=132   width=3     height=1     xoffset=-1    yoffset=31    xadvance=8     page=0  chnl=15      
  ...
  kernings count=96
  kerning first=32  second=65  amount=-2 
  ...
  */      
  
  
  RStrParser  parser = szFontDefIn;

  while (! parser.IsEOF ())
    {
    // for each line
    while (!parser.IsEOL ())
      {
      strLineType = parser.GetWord (FALSE);

      //  Info Line 
      if (strLineType == "info")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);

            // process key-value pair
            if      (strKey == "face")     {parserValue.GetQuoteString (&strTypeface);}
            else if (strKey == "size")     {iSize = parserValue.GetInt ();}
            else if (strKey == "bold")     {iBold = parserValue.GetInt ();}
            else if (strKey == "italic")   {iItalic = parserValue.GetInt ();}
            else if (strKey == "charset")  {parserValue.GetQuoteString (&strCharset);}
            else if (strKey == "unicode")  {iUnicode = parserValue.GetInt ();}
            else if (strKey == "stretchH") {iStretchH = parserValue.GetInt ();}
            else if (strKey == "smooth")   {iSmooth = parserValue.GetInt ();}
            else if (strKey == "aa")       {iAA = parserValue.GetInt ();}
            else if (strKey == "padding")
              {
              iPaddingL = parserValue.GetInt ();
              if (parserValue.PeekChar () == ',') {parserValue.SkipChars(1); parserValue.SkipWhitespace ();};
              iPaddingR = parserValue.GetInt ();
              if (parserValue.PeekChar () == ',') {parserValue.SkipChars(1); parserValue.SkipWhitespace ();};
              iPaddingU = parserValue.GetInt ();
              if (parserValue.PeekChar () == ',') {parserValue.SkipChars(1); parserValue.SkipWhitespace ();};
              iPaddingD = parserValue.GetInt ();
              }
            else if (strKey == "spacing")
              {
              iSpacingX = parserValue.GetInt ();
              if (parserValue.PeekChar () == ',') {parserValue.SkipChars(1); parserValue.SkipWhitespace ();};
              iSpacingY = parserValue.GetInt ();
              }
            else if (strKey == "outline")  {iOutline = parserValue.GetInt ();}
            };
          };
        }
        
      // Common Line
      else if (strLineType == "common")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "lineHeight")      {iLineHeight  = parserValue.GetInt ();}
            else if (strKey == "base")       {iBaseline    = parserValue.GetInt ();}
            else if (strKey == "scaleW")     {iFontAtlasWidth  = parserValue.GetInt ();}
            else if (strKey == "scaleH")     {iFontAtlasHeight = parserValue.GetInt ();}
            else if (strKey == "pages")      {iPages       = parserValue.GetInt ();}
            else if (strKey == "packed")     {iPacked      = parserValue.GetInt ();}
            else if (strKey == "alphaChnl")  {iAlphaChan   = parserValue.GetInt ();}
            else if (strKey == "redChnl")    {iRedChan     = parserValue.GetInt ();}
            else if (strKey == "greenChnl")  {iGreenChan   = parserValue.GetInt ();}
            else if (strKey == "blueChnl")   {iBlueChan    = parserValue.GetInt ();}
            };
          };
        }

      // Page Line
      else if (strLineType == "page")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "id")        {iId = parserValue.GetInt ();}
            else if (strKey == "file") {parserValue.GetQuoteString (&strFile);}
            };
          };
        }
      // Chars Line
      else if (strLineType == "chars")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "count")        {iCharCount = parserValue.GetInt ();}
            };
          };
        }
      // char line
      else if (strLineType == "char")
        {
        INT  iId = 0;
        
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "id")        
              {
              iId = parserValue.GetInt ();
              if (iId >= BMF_NUM_ARRAY_CHARS) {break;}
              }
            else if (strKey == "x")        {arrayChars[iId].iX       = parserValue.GetInt ();}
            else if (strKey == "y")        {arrayChars[iId].iY       = parserValue.GetInt ();}
            else if (strKey == "width")    {arrayChars[iId].iWidth   = parserValue.GetInt ();}
            else if (strKey == "height")   {arrayChars[iId].iHeight  = parserValue.GetInt ();}
            else if (strKey == "xoffset")  {arrayChars[iId].iOffsetX = parserValue.GetInt ();}
            else if (strKey == "yoffset")  {arrayChars[iId].iOffsetY = parserValue.GetInt ();}
            else if (strKey == "xadvance") {arrayChars[iId].iStrideX = parserValue.GetInt ();}
            else if (strKey == "page")     {arrayChars[iId].iPage    = parserValue.GetInt ();}
            else if (strKey == "chnl")     {arrayChars[iId].iChannel = parserValue.GetInt ();}            
            };
          };
        }

      // Kernings Line
      else if (strLineType == "kernings")
        {
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "count")        {iKerningCount = parserValue.GetInt ();}
            };
          };
        }      

      // Kerning line
      else if (strLineType == "kerning")
        {
        INT  iId = 0;
        BMFCharKerning *  pKerning = new BMFCharKerning;
        
        while (!parser.IsEOL ())
          {
          strKey = parser.GetWord (FALSE, "=");
          if (parser.PeekChar () == '=') 
            {
            parser.SkipChars (1);
            parser.SkipWhitespace ();
            parserValue = parser.GetWord (FALSE);
            
            if (strKey == "first")        
              {
              INT  iLeftChar = parserValue.GetInt ();
              if (iLeftChar < BMF_NUM_ARRAY_CHARS)
                {
                arrayChars[iLeftChar].listKernings.PushBack (pKerning);
                };
              }
            else if (strKey == "second")
              {
              pKerning->iAdjacentChar = parserValue.GetInt ();
              }
            else if (strKey == "amount")
              {
              pKerning->iAmount = parserValue.GetInt ();
              };
            };
          };
        } // kerning line
      
      } // while not EOL
    parser.GotoNextLine ();
    }; // while not EOF
    
  if (iAtlasWidth == 0) {iAtlasWidth = iFontAtlasWidth;};
  if (iAtlasHeight == 0) {iAtlasHeight = iFontAtlasHeight;};
  }; // ParseFont
  
  
//------------------------------------------------------------------------
INT BitmapFont::CalcLineWidth (const char *  szTextIn)
  {
  int  iWidthOut = 0;
  int  iNumChars = strlen (szTextIn);
  // Note: The below needs to take into account kerning adjustments.
  // To be implemented
  for (int  iIndex = 0; iIndex < iNumChars; ++iIndex)
    {
    iWidthOut += arrayChars [szTextIn[iIndex]].iStrideX; 
    }
  return (iWidthOut);
  };

  
//------------------------------------------------------------------------
INT BitmapFont::GetNumVerts (const char *  szTextIn)
  {
  return (strlen (szTextIn) * 6);
  };  

  
//------------------------------------------------------------------------
INT BitmapFont::GetNumTriangles (const char *  szTextIn)
  {
  return (strlen (szTextIn) * 2);
  };
  
//---------------------------------------------------------------------------
VOID BitmapFont::AddTriangleStrip (const char *   szTextIn,
                                   AlignHoriz     alignIn,
                                   INT            iPixelsPerUnit, // Is this valid in 3D space, or is it just a scale factor?
                                   DisplayMesh *  pDisplayMesh)
  {  
  INT  iNumVerts = GetNumVerts     (szTextIn);
  INT  iNumTris  = GetNumTriangles (szTextIn);  
  INT  iNumChars = strlen (szTextIn);
  INT  iVertOffset = 0;
  INT  lineWidth = CalcLineWidth (szTextIn);
  INT  horizOffset = 0;
  FLOAT fPositions[12];
  FLOAT fUVs[8];
  
  
  
  pDisplayMesh->SetElementCounts (3, 0, 2);
  pDisplayMesh->GrowVertArray    (iNumChars * 4);
  pDisplayMesh->GrowIndexArray   ((iNumChars * 4) + iNumChars);  
  
  static const INT aIndexes[] = {0, 1, 2, 3};

  float  X0;
  float  X1;
  float  Y0;
  float  Y1;

  // Note:  Do the offsets need to be adjusted by iPixelsPerUnit?
  if (alignIn == BitmapFont::Center) horizOffset = -lineWidth / 2;
  if (alignIn == BitmapFont::Right)  horizOffset = -lineWidth;

  INT  iXPos = horizOffset;
  INT  iYPos = 0;

  INT prevAsciiChar;
  INT currAsciiChar = 0;
  for (INT  iIndex = 0; iIndex < iNumChars; ++iIndex)
    {
    prevAsciiChar = currAsciiChar;
    currAsciiChar = szTextIn[iIndex];
    
    BMFCharDesc &  currChar = arrayChars [currAsciiChar];
    //DBG_INFO ("curr char x:%d y:%d w:%d h:%d", currChar.iX, currChar.iY, currChar.iWidth, currChar.iHeight);
    
    
    // kerning
    for (LinkedListIterator  itrCurrKern = currChar.listKernings.GetFirst ();
         itrCurrKern.IsValid ();
         ++itrCurrKern)
      {
      if (((BMFCharKerning*)(itrCurrKern.GetValue()))->iAdjacentChar == prevAsciiChar)
        {
        iXPos += ((BMFCharKerning*)(itrCurrKern.GetValue()))->iAmount;
        break;
        }
      }
    
    // solve for position  
    X0 = (float) (iXPos + currChar.iOffsetX) / (float) iPixelsPerUnit;
    X1 = (float) (iXPos + currChar.iOffsetX + currChar.iWidth) / (float) iPixelsPerUnit;
    
    Y0 = (float) (iYPos + iBaseline - currChar.iOffsetY) / (float) iPixelsPerUnit; 
    Y1 = (float) (iYPos + iBaseline - currChar.iOffsetY - currChar.iHeight) / (float) iPixelsPerUnit; 

    fPositions[0] = X0; fPositions[1]  = Y0; fPositions[2]  = 0.0f;
    fPositions[3] = X0; fPositions[4]  = Y1; fPositions[5]  = 0.0f;
    fPositions[6] = X1; fPositions[7]  = Y0; fPositions[8]  = 0.0f;
    fPositions[9] = X1; fPositions[10] = Y1; fPositions[11] = 0.0f;
    
    
    //DBG_INFO ("Curr ASCII char is %c (%d) %f,%f %f,%f", currAsciiChar, currAsciiChar, X0, Y0, X1, Y1);
    
    // solve for texture atlas position 
    X0 = iAtlasXPos + (currChar.iX)/(float)(iAtlasWidth - 1);
    X1 = iAtlasXPos + (currChar.iX + currChar.iWidth)/(float)(iAtlasWidth - 1) ;
    Y1 = /*1.0f -*/ (iAtlasYPos + (currChar.iY)/(float)(iAtlasHeight - 1));
    Y0 = /*1.0f -*/ (iAtlasYPos + (currChar.iY + currChar.iHeight)/(float)(iAtlasHeight - 1));
                                
    // set the texture coordinates
    fUVs[0] = X0; fUVs[1] = Y1; 
    fUVs[2] = X0; fUVs[3] = Y0; 
    fUVs[4] = X1; fUVs[5] = Y1; 
    fUVs[6] = X1; fUVs[7] = Y0;
    
    //DBG_INFO ("Tex %f,%f %f,%f", X0, Y0, X1, Y1);
  
    // add the next bit of the triangle strip to the display mesh
    iVertOffset = pDisplayMesh->AddVerts (4, fPositions, NULL, fUVs);
    INT iStartIndex = pDisplayMesh->AddTriangleStrip (4, iVertOffset, aIndexes);
    
    //DBG_INFO ("Vert offset %d strip start %d", iVertOffset, iStartIndex);
    
    iXPos += arrayChars [currAsciiChar].iStrideX; 
    }
  } 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
  /*
  float                         atlasXPos = 0;         
  float                         atlasYPos = 0;
  float                         atlasWidth = 256;
  float                         atlasHeight = 256;
  
  
 #   faces      - list of starting indexes into the face* index arrays
  #   facevcount - number of verts/sides in this face
  #   facegroup  - Index of the group this face belongs in.  
  #   facepoints - Index into points for each vert in this face.  "faces" gives the starting index and
  #                "facesvcount" gives the number of entries in this array.  Sized to the sum of all facevcount entries.
  #   facenorms  - Index into normals for each vert in this face.  Setup like facepoints.
  #   faceuvs    - Index into uvs for each vert in this face.  Setup like facepoints.
  #
  #   materials    - name of the material instance used to display faces in this group
  #   materialobjs - links to the material definitions so you don't have to search for them over and over again.
  #   groups       - Gives a name for each group, separate from the name of the material used to display it.
  #                  Meant to break a single model up into logical parts (upper arm, lower arm, head, etc.)
  #   grouppoints  - Index into the vert* arrays for where each group starts.  
  #  
*/  

//---------------------------------------------------------------------------
VOID BitmapFont::AddTriangles (RStr &        strTextIn,
                            AlignHoriz    alignIn,
                            INT           iPixelsPerUnit,
                            
                            RVec4Array *  avecCoordPoints,    // array to hold point coordinates
                            RVec4Array *  avecCoordTexture,   // array to hold texture coordinates
                            RVec4Array *  avecCoordNormal,    // array to hold normal coordinates

                            INT &         iCoordPointOffset,   // first empty index in coordPoints
                            INT &         iCoordTextureOffset, // first empty index in coordTexture
                            INT &         iCoordNormalOffset,  // first empty index in coordNormal

                            IntArray *   aiFacePoints,        // indexes into avecCoordPoints for the face's verts.  Unrolled face verts.
                            IntArray *   aiFaceTex,           // indexes into iCoordTextureOffset for the face's verts.  Unrolled face verts.
                            IntArray *   aiFaceNorms,         // indexes into iCoordNormalOffset for the face's verts.  Unrolled face verts.

                            IntArray *   aiFaces,             // index into aiFacePoints/aiFaceTex/aiFaceNorms giving first index of the face's verts.
                            IntArray *   aiFaceVCount,        // number of verts in the face.  should always be 3.
                            
                            IntArray *   aiFaceGroups,        // index of the group to which this face belogs.  Always set to 0.
                            IntArray *   aiGroupPoints,
                            
                            INT           iVertexIndex,          // first free index in the aiFacePoints, aiFaceTex, and aiFaceNorms arrays
                            INT           iFaceIndex             // first free index in the aiFaces, aiFaceVCount, and aiFaceGroups arrays.
                            )
  {
  INT  lineWidth = CalcLineWidth (strTextIn);
  INT  horizOffset = 0;
  //Color32 white = new Color32 (255, 255, 255, 255);
  float  X0;
  float  X1;
  float  Y0;
  float  Y1;

  // we only use a single normal
  INT    iNormalOffset  = iCoordNormalOffset;
  (*avecCoordNormal)[iNormalOffset].Set (0.0f, 0.0f, 1.0f);
  iCoordNormalOffset += 1;
  
  if (alignIn == BitmapFont::Center) horizOffset = -lineWidth / 2;
  if (alignIn == BitmapFont::Right)  horizOffset = -lineWidth;

  INT  iXPos = horizOffset;
  INT  iYPos = 0;

  INT prevAsciiChar;
  INT currAsciiChar = 0;
  for (INT  iIndex = 0; iIndex < strTextIn.GetLength(); ++iIndex)
    {
    prevAsciiChar = currAsciiChar;
    currAsciiChar = strTextIn.GetAt(iIndex);
    BMFCharDesc &  currChar = arrayChars [currAsciiChar];

    // kerning
    for (LinkedListIterator  itrCurrKern = currChar.listKernings.GetFirst ();
         itrCurrKern.IsValid ();
         ++itrCurrKern)
      {
      if (((BMFCharKerning*)(itrCurrKern.GetValue()))->iAdjacentChar == prevAsciiChar)
        {
        iXPos += ((BMFCharKerning*)(itrCurrKern.GetValue()))->iAmount;
        break;
        }
      }

    // solve for position  
    X0 = (float) (iXPos + currChar.iOffsetX) / (float) iPixelsPerUnit;
    X1 = (float) (iXPos + currChar.iOffsetX + currChar.iWidth) / (float) iPixelsPerUnit;
    
    Y0 = (float) (iYPos + iBaseline - currChar.iOffsetY) / (float) iPixelsPerUnit; // + baseline 
    Y1 = (float) (iYPos + iBaseline - currChar.iOffsetY - currChar.iHeight) / (float) iPixelsPerUnit; // + baseline 

    // set the point coordinates
    (*avecCoordPoints)[iCoordPointOffset+0].Set (X0, Y1, 0.0f);
    (*avecCoordPoints)[iCoordPointOffset+1].Set (X0, Y0, 0.0f);
    (*avecCoordPoints)[iCoordPointOffset+2].Set (X1, Y1, 0.0f);
  
    (*avecCoordPoints)[iCoordPointOffset+3].Set (X1, Y0, 0.0f);
    (*avecCoordPoints)[iCoordPointOffset+4].Set (X1, Y1, 0.0f);
    (*avecCoordPoints)[iCoordPointOffset+5].Set (X0, Y0, 0.0f);

    // solve for texture atlas position 
    X0 = iAtlasXPos + (currChar.iX)/(float)iAtlasWidth;
    X1 = iAtlasXPos + (currChar.iX + currChar.iWidth)/(float)iAtlasWidth;
    Y0 = 1.0f - (iAtlasYPos + (currChar.iY)/(float)iAtlasHeight);
    Y1 = 1.0f - (iAtlasYPos + (currChar.iY + currChar.iHeight)/(float)iAtlasHeight);
                                
    // set the texture coordinates
    (*avecCoordTexture)[iCoordTextureOffset+0].Set (X0, Y1, 0.0f);
    (*avecCoordTexture)[iCoordTextureOffset+1].Set (X0, Y0, 0.0f);
    (*avecCoordTexture)[iCoordTextureOffset+2].Set (X1, Y1, 0.0f);
    
    (*avecCoordTexture)[iCoordTextureOffset+3].Set (X1, Y0, 0.0f);
    (*avecCoordTexture)[iCoordTextureOffset+4].Set (X1, Y1, 0.0f);
    (*avecCoordTexture)[iCoordTextureOffset+5].Set (X0, Y0, 0.0f);

    // build triangles with the newly created coordinates
    
    aiFaces      [iFaceIndex]   = iVertexIndex;
    aiFaces      [iFaceIndex+1] = iVertexIndex+3;
    aiFaceVCount [iFaceIndex]   = 3;
    aiFaceVCount [iFaceIndex+1] = 3;
    aiFaceGroups [iFaceIndex]   = 0;
    aiFaceGroups [iFaceIndex+1] = 0;
    
    aiFacePoints [iVertexIndex+0] = iCoordPointOffset+0;
    aiFacePoints [iVertexIndex+1] = iCoordPointOffset+1;
    aiFacePoints [iVertexIndex+2] = iCoordPointOffset+2;

    aiFacePoints [iVertexIndex+3] = iCoordPointOffset+3;
    aiFacePoints [iVertexIndex+4] = iCoordPointOffset+2;
    aiFacePoints [iVertexIndex+5] = iCoordPointOffset+1;

    aiFaceTex [iVertexIndex+0] = iCoordTextureOffset+0;
    aiFaceTex [iVertexIndex+1] = iCoordTextureOffset+1;
    aiFaceTex [iVertexIndex+2] = iCoordTextureOffset+2;

    aiFaceTex [iVertexIndex+3] = iCoordTextureOffset+3;
    aiFaceTex [iVertexIndex+4] = iCoordTextureOffset+2;
    aiFaceTex [iVertexIndex+5] = iCoordTextureOffset+1;

    aiFaceNorms [iVertexIndex+0] = iNormalOffset;
    aiFaceNorms [iVertexIndex+1] = iNormalOffset;
    aiFaceNorms [iVertexIndex+2] = iNormalOffset;

    aiFaceNorms [iVertexIndex+3] = iNormalOffset;
    aiFaceNorms [iVertexIndex+4] = iNormalOffset;
    aiFaceNorms [iVertexIndex+5] = iNormalOffset;

    //                        IntArray *   aiGroupPoints,

    iVertexIndex += 6;
    iFaceIndex += 2;
    
    iXPos += arrayChars [strTextIn.GetAt(iIndex)].iStrideX; 
    }
  } 

  
  
    