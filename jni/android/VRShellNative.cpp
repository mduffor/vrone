
#include <jni.h>
#include <sys/time.h>
#include <time.h>
#include <android/log.h>
#include <stdint.h>

#include "Types.hpp"

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "game/GameShell.hpp"
#include "FilePath.hpp"

static GameShell *  pshellGame = NULL;
static long lPrevTimeMs = 0;

static char DEBUG_TAG[] = "VRShell";

extern "C" {
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeInit   (JNIEnv*  env, jobject obj, jobject assetManager);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeUninit (JNIEnv*  env);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeUpdate (JNIEnv*  env);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnKeyDown     (JNIEnv*  env, 
                                                                             jobject  obj, 
                                                                             jint     iVKeyIn);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnKeyUp       (JNIEnv*  env, 
                                                                             jobject  obj, 
                                                                             jint     iVKeyIn);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnPointerMove (JNIEnv*  env, 
                                                                             jobject  obj, 
                                                                             jint     iVKeyIn,
                                                                             jfloat   fXIn,
                                                                             jfloat   fYIn,
                                                                             jfloat   fPressureIn);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnPointerDown (JNIEnv*  env, 
                                                                             jobject  obj, 
                                                                             jint     iVKeyIn,
                                                                             jfloat   fXIn,
                                                                             jfloat   fYIn,
                                                                             jfloat   fPressureIn);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnPointerUp  (JNIEnv*  env, 
                                                                             jobject  obj, 
                                                                             jint     iVKeyIn,
                                                                             jfloat   fXIn,
                                                                             jfloat   fYIn,
                                                                             jfloat   fPressureIn);
  JNIEXPORT jboolean JNICALL Java_com_mduffor_vrone_VRShell_nativeAppIsClosing (JNIEnv*  env);
};


//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeInit (JNIEnv*  env, jobject obj, jobject assetManager)
  {
  DebugMessagesFactory::Initialize ();
  filePath_assetManager = AAssetManager_fromJava(env, assetManager);
  
  pshellGame = new GameShell;
  __android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, "VRShell_nativeInit Done");
  }
  
  
//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeUninit (JNIEnv*  env)
  {
  delete pshellGame;
  pshellGame = NULL;
  }

//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeUpdate (JNIEnv*  env)
  {
  if (pshellGame != NULL) 
    {
    // calculate the elapsed time since last update
    struct timeval  now;

    gettimeofday(&now, NULL);
    long lCurrTimeMs = now.tv_sec*1000 + now.tv_usec/1000;
    if (lPrevTimeMs == 0)
      {
      // initialize prev time
      lPrevTimeMs = lCurrTimeMs;
      }
    long lDeltaTimeMs = lCurrTimeMs - lPrevTimeMs;
    lPrevTimeMs = lCurrTimeMs;
    
    // increment shell time
    pshellGame->Update ((int) lDeltaTimeMs);
    }
  }
  
//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnKeyDown  (JNIEnv*  env, 
                                                                        jobject  obj, 
                                                                        jint     iVKeyIn)
  {
  Shell::OnKeyDown (iVKeyIn);
  };
  
//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnKeyUp  (JNIEnv*  env, 
                                                                      jobject  obj, 
                                                                      jint     iVKeyIn)
  {
  Shell::OnKeyUp (iVKeyIn);
  };

//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnPointerMove (JNIEnv*  env, 
                                                                           jobject  obj, 
                                                                           jint     iVKeyIn,
                                                                           jfloat   fXIn,
                                                                           jfloat   fYIn,
                                                                           jfloat   fPressureIn)
  {
  Shell::OnPointerMove (iVKeyIn, fXIn, fYIn, fPressureIn);
  };

//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnPointerDown (JNIEnv*  env, 
                                                                           jobject  obj, 
                                                                           jint     iVKeyIn,
                                                                           jfloat   fXIn,
                                                                           jfloat   fYIn,
                                                                           jfloat   fPressureIn)
  {
  Shell::OnPointerDown (iVKeyIn, fXIn, fYIn, fPressureIn);
  };

//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRShell_nativeOnPointerUp  (JNIEnv*  env, 
                                                                          jobject  obj, 
                                                                          jint     iVKeyIn,
                                                                          jfloat   fXIn,
                                                                          jfloat   fYIn,
                                                                          jfloat   fPressureIn)
  {
  Shell::OnPointerUp (iVKeyIn, fXIn, fYIn, fPressureIn);
  };
  
//-----------------------------------------------------------------------------
JNIEXPORT jboolean JNICALL Java_com_mduffor_vrone_VRShell_nativeAppIsClosing (JNIEnv*  env)
  {
  return (jboolean(Shell::AppIsClosing()));
  }  
  
