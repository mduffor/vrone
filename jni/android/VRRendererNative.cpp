
#include <jni.h>
#include <sys/time.h>
#include <time.h>
#include <android/log.h>
#include <stdint.h>

#include "RStr.hpp"
#include "Shell.hpp"
#include "game/GameShell.hpp"

int   gAppAlive   = 1;

static int  sWindowWidth  = 800;
static int  sWindowHeight = 480;
static int  sPirateStopped  = 0;
static long sTimeOffset   = 0;
static int  sTimeOffsetInit = 0;
static long sTimeStopped  = 0;

static char DEBUG_TAG[] = "VRRenderer";


extern "C" {
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeInit   (JNIEnv*  env);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeDone   (JNIEnv*  env);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeResize (JNIEnv*  env, 
                                                                         jobject  obj, 
                                                                         jint     w, 
                                                                         jint     h);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeRender (JNIEnv*  env);
};


//-----------------------------------------------------------------------------
long _getTime (void)
  {
  struct timeval  now;

  gettimeofday(&now, NULL);
  return (long)(now.tv_sec*1000 + now.tv_usec/1000);
  }

//-----------------------------------------------------------------------------
// Call to initialize the graphics state 
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeInit (JNIEnv*  env)
  {
  Shell::InitOpenGL ();
  //appInit();
  gAppAlive    = 1;
  sPirateStopped = 0;
  sTimeOffsetInit = 0;
  }

//-----------------------------------------------------------------------------
// Call to finalize the graphics state 
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeDone (JNIEnv*  env)
  {
  //appDeinit();
  }

//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeResize (JNIEnv*  env, 
                                                                       jobject  obj, 
                                                                       jint     w, 
                                                                       jint     h)
  {
  sWindowWidth  = w;
  sWindowHeight = h;
  __android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, "resize w=%d h=%d", w, h);
  glViewport(0, 0, sWindowWidth, sWindowHeight);
  //setupGraphics(sWindowWidth, sWindowHeight);
  }

//-----------------------------------------------------------------------------
// Call to render the next GL frame 
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRRenderer_nativeRender (JNIEnv*  env)
  {
  long   curTime;

  /* NOTE: if sPirateStopped is TRUE, then we re-render the same frame
    *       on each iteration.
    */
  if (sPirateStopped) 
    {
    curTime = sTimeStopped + sTimeOffset;
    } 
  else 
    {
    curTime = _getTime() + sTimeOffset;
    if (sTimeOffsetInit == 0) 
      {
      sTimeOffsetInit = 1;
      sTimeOffset     = -curTime;
      curTime         = 0;
      }
    }

  //VRShell.Update();
    
  //__android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, "curTime=%ld", curTime);
  //renderFrame();
  //appRender(curTime, sWindowWidth, sWindowHeight);
  }
