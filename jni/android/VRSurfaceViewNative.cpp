
#include <jni.h>
#include <sys/time.h>
#include <time.h>
#include <android/log.h>
#include <stdint.h>

#include "base/Shader.hpp"
#include "mesh/DisplayMesh.hpp"

static int  sAppPaused  = 0;
static long sTimeOffset   = 0;
static int  sTimeOffsetInit = 0;
static long sTimeStopped  = 0;

static char DEBUG_TAG[] = "VRSurfaceView";

extern long _getTime (void);


extern "C" {
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRSurfaceView_nativeTogglePause (JNIEnv*  env);
  JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRSurfaceView_nativeOnContextLost (JNIEnv*  env);
};



/* This is called to indicate to the render loop that it should
 * stop as soon as possible.
 */
//-----------------------------------------------------------------------------
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRSurfaceView_nativeTogglePause (JNIEnv*  env)
  {
  __android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, "touch event read");
  sAppPaused = !sAppPaused;
  if (sAppPaused) 
    {
    /* we paused the animation, so store the current
      * time in sTimeStopped for future nativeRender calls */
    sTimeStopped = _getTime();
    } 
  else 
    {
    /* we resumed the animation, so adjust the time offset
      * to take care of the pause interval. */
    sTimeOffset -= _getTime() - sTimeStopped;
    }
  }
  
//-----------------------------------------------------------------------------
// This is called whenever the OpenGL Context is lost, so all resources need to be recreated.
JNIEXPORT void JNICALL Java_com_mduffor_vrone_VRSurfaceView_nativeOnContextLost (JNIEnv*  env)
  {
  __android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, "OpenGL context was lost");
  
  Shader::OnContextLost();
  DisplayMesh::OnContextLost();
  
  }


