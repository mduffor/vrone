LOCAL_PATH := $(call my-dir)

# declare static library : libpng16 (1.6.1)

include $(CLEAR_VARS)
LOCAL_MODULE    := libpng16

LOCAL_SRC_FILES := libpng16.a
LOCAL_EXPORT_C_INCLUDES := ../include
include $(PREBUILT_STATIC_LIBRARY)


# build our shared library

include $(CLEAR_VARS)

LOCAL_MODULE := vrone

LOCAL_CFLAGS := -DANDROID_NDK \
                -DDISABLE_IMPORTGL \
                -fexceptions \
                -I./base

LOCAL_SRC_FILES := \
    base/Types.cpp \
    base/Debug.cpp \
    base/RStr.cpp \
    base/Timer.cpp \
    base/Shell.cpp \
    base/GLUtil.cpp \
    game/ScreenMode.cpp \
    game/GameShell.cpp \
    android/VRSurfaceViewNative.cpp \
    android/VRRendererNative.cpp \
    android/VRShellNative.cpp \
    game/TestAScreenMode.cpp \
    base/BaseArray.cpp \
    base/RVec.cpp \
    base/RMatrix.cpp \
    base/RVecArray.cpp \
    base/RMatrixArray.cpp \
    base/IntArray.cpp \
    base/RStrArray.cpp \
    base/Euler.cpp \
    base/Transform.cpp \
    base/RegEx.cpp \
    base/FilePath.cpp \
    mesh/DisplayMesh.cpp \
    base/Camera.cpp \
    base/Shader.cpp \
    base/Material.cpp \
    base/Renderer.cpp \
    base/Color8U.cpp \
    base/RStrParser.cpp \
    image/ColorOps.cpp \
    image/BitmapCodec.cpp \
    base/BaseList.cpp \
    image/ImageBoxIterator.cpp \
    image/BitmapLoader.cpp \
    image/CodecPam.cpp \
    base/Texture.cpp \
    image/CodecPng.cpp \
    base/LinkedList.cpp \
    mesh/BitmapFont.cpp \
    base/Attr.cpp \
    base/AttrFloat.cpp \
    base/AttrInt.cpp \
    base/AttrString.cpp \
    base/Component.cpp \
    base/Node.cpp \
    base/Resource.cpp \
    base/World.cpp \
    base/NodeDelegate.cpp \
    base/SceneLoader.cpp \
    mesh/Ray.cpp \
    mesh/CPoint.cpp \
    mesh/BSphere.cpp \
    mesh/BBox.cpp \
    mesh/CTriangle.cpp \
    mesh/Frustum.cpp \
    base/FloatArray.cpp \
    base/InputManager.cpp \
    mesh/BaseGeomCodec.cpp \
    mesh/EditMesh.cpp \
    mesh/ObjCodec.cpp \
    base/PtrArray.cpp \
    mesh/Atlas.cpp \
    component/ComponentInit.cpp \
    component/TransformComponent.cpp \
    component/MeshComponent.cpp \
    component/SpriteComponent.cpp \
    component/MeshCollectorComponent.cpp \
    component/AtlasComponent.cpp \
    component/CameraComponent.cpp \
    component/FontComponent.cpp \
    component/TextComponent.cpp \
    base/AttrStringArray.cpp \
    component/AnimatedSpriteComponent.cpp \
    component/SpriteClipComponent.cpp \
    
    
LOCAL_LDFLAGS := -L$(LOCAL_PATH)/
LOCAL_LDLIBS := -lGLESv2 -ldl -llog -landroid -lz
LOCAL_STATIC_LIBRARIES += libpng16

include $(BUILD_SHARED_LIBRARY)

