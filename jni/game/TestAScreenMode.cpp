#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Types.hpp"

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "Shell.hpp"
#include "GLUtil.hpp"
#include "ScreenMode.hpp"
#include "mesh/DisplayMesh.hpp"
#include "TestAScreenMode.hpp"
#include "RStrParser.hpp"
#include "Renderer.hpp"
#include "image/BitmapLoader.hpp"
#include "mesh/BitmapFont.hpp"
#include "component/TransformComponent.hpp"
#include "component/CameraComponent.hpp"

#include "Attr.hpp"
#include "AttrFloat.hpp"
#include "Node.hpp"
#include "Component.hpp"
#include "Resource.hpp"
#include "World.hpp"
#include "SceneLoader.hpp"
#include "InputManager.hpp"

/* This file is for testing shaders and working out GL-level issues directly
 * before they are wrapped up in the framework itself
 */

GLuint g_ShaderProgram;
GLuint gvPositionHandle;

  
static const char gVertexShader[] = 
    "attribute vec4 a_position;\n"
    "void main() {\n"
    "  gl_Position = a_position;\n"
    "}\n";

static const char gFragmentShader[] = 
    "precision mediump float;\n"
    "void main() {\n"
    "  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n" // R G B A
    "}\n";

DisplayMesh meshDisplay;    
DisplayMesh meshTextTest;    
Renderer    renderer;




//------------------------------------------------------------------------------
TestAScreenMode::TestAScreenMode (World *  pWorldIn)
  {
  pwldTheWorld = pWorldIn;
  pnodeCamera = NULL;
  };

//------------------------------------------------------------------------------
TestAScreenMode::~TestAScreenMode ()
  {
  };

//------------------------------------------------------------------------------
void TestAScreenMode::Init ()
  {
  //TestWorld();
  DirectInit();
  DisplayMeshInit();
  MaterialInit();
  
  pnodeCamera = pwldTheWorld->FindNodeByPath ("|MainCamera");
  ASSERT (pnodeCamera != NULL);
  pwldTheWorld->AddCamera (pnodeCamera);
  
  pcmpCameraTransform = NULL;
  pcmpCamera          = NULL;
  
  if (pnodeCamera != NULL)
    {
    Component *  pcmpSearch = NULL;
    
    pcmpSearch = pnodeCamera->FindComponent (TransformComponent::Identifier ());
    pcmpCameraTransform  = (pcmpSearch == NULL) ? NULL : (TransformComponent *) pcmpSearch;
    
    pcmpSearch = pnodeCamera->FindComponent (CameraComponent::Identifier ());
    pcmpCamera           = (pcmpSearch == NULL) ? NULL : (CameraComponent *) pcmpSearch;
                                  
    }
  };

//------------------------------------------------------------------------------
void TestAScreenMode::DirectInit ()
  {
  g_ShaderProgram = GLCreateProgram (gVertexShader, gFragmentShader);
  if (!g_ShaderProgram) 
    {
    DBG_ERROR("Could not create program.");
    return;
    }
  glUseProgram(g_ShaderProgram);

  gvPositionHandle = glGetAttribLocation(g_ShaderProgram, "a_position");
  GLCheckError (__FILE__, "glGetAttribLocation");
  DBG_INFO ("glGetAttribLocation(\"a_position\") = %d\n", gvPositionHandle);
  };
  
//------------------------------------------------------------------------------
void TestAScreenMode::Uninit ()
  {
  DBG_INFO ("TestAScreenMode::Uninit");
  };

//------------------------------------------------------------------------------
void TestAScreenMode::Draw ()
  {
  DBG_INFO ("TestAScreenMode::Draw");
  RenderFrameDirectGLCalls();
  };

//------------------------------------------------------------------------------
void TestAScreenMode::Update ()
  {
  DBG_INFO ("TestAScreenMode::Update");
  
  // react to input and update game state
  INT   iVKey    = VKEY_INVALID;
  INT   iVKeyMod = 0;
    
  while (Shell::GetNextKey (iVKey, iVKeyMod))
    {
    if (iVKey == VKEY_ESC)
      {
      Shell::CloseApplication ();
      };
    };
  
  
  };
  
//------------------------------------------------------------------------------
void TestAScreenMode::RenderFrameDirectGLCalls ()
  {
  DBG_INFO ("RenderFrame");
  
  //DirectDrawCall (); // draw the triangle
  //DisplayMeshDraw ();
  
  // draw materials for each camera.
  
  if ((pcmpCamera == NULL) || (pcmpCameraTransform == NULL)) return;
  
  RMatrix  matProj;
  pcmpCamera->GetCamera().GetMatrix (matProj);
  
  RMatrix  matCam;
  pcmpCameraTransform->GetTransform().GetWorldMatrix (matCam);
  matCam.RigidInvert (); // Cameras should not be scaled, especially not non-uniformly.

  RMatrix  matCamProj = matProj * matCam;

  DisplayViaMaterialDraw (&matCamProj);
  }

//------------------------------------------------------------------------------
void TestAScreenMode::DirectDrawCall ()
  {
  static const GLfloat gTriangleVertices[] = { 0.0f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f };

  glUseProgram(g_ShaderProgram);
  GLCheckError (__FILE__, "glUseProgram");
  
  glVertexAttribPointer(gvPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, gTriangleVertices);
  GLCheckError (__FILE__, "glVertexAttribPointer");
  glEnableVertexAttribArray(gvPositionHandle);
  GLCheckError (__FILE__, "glEnableVertexAttribArray");
  glDrawArrays(GL_TRIANGLES, 0, 3);
  GLCheckError (__FILE__, "glDrawArrays");
  };  

//------------------------------------------------------------------------------
void TestAScreenMode::DisplayMeshInit ()
  {
  static const FLOAT fPositions[] = {-1.0f, -1.0f, -0.5f, 
                                     -0.9f, -0.5f, -0.5f,
                                     -0.5f, -0.9f, -0.5f,
                                     -0.5f, -0.5f, -0.5f};
  static const FLOAT fUVs[]       = {0.0f, 1.0f, 
                                     0.0f, 0.0f, 
                                     1.0f, 1.0f, 
                                     1.0f, 0.0f};
  static const INT aIndexes[] = {0, 1, 2, 3};
  
  meshDisplay.SetElementCounts (3, 0, 2);
  INT  iVertOffset = meshDisplay.AddVerts (4, fPositions, NULL, fUVs);
  
  meshDisplay.AddTriangleStrip (4, iVertOffset, aIndexes);
  
  }
  
//------------------------------------------------------------------------------
void TestAScreenMode::DisplayMeshDraw ()
  {
  glUseProgram(g_ShaderProgram);
  meshDisplay.Draw (gvPositionHandle, -1, -1); 
  }

//------------------------------------------------------------------------------
void TestAScreenMode::MaterialInit()
  {
  new Material ("MtlBlue", "ShdBlue");
  new Material ("MtlChecker", "ShdUnlitTex");

  Material *  pmtlBlue    = Material::FindByName ("MtlBlue");
  Material *  pmtlChecker = Material::FindByName ("MtlChecker");
  
  if (pmtlChecker != NULL)
    {
    //pmtlChecker->SetDiffuseTexture ("gfx/textures/checker.ppm");
    pmtlChecker->SetDiffuseTexture ("gfx/textures/checker.png");
    };

  //RStr   strFontName = "TrebuchetMS32";
  /*
  RStr   strFontName = "Gashly";
  LoadFont (strFontName.AsChar ());
  TestFont ();
  
  Material *  pmtlFont = Material::FindByName (strFontName.AsChar ());
  if (pmtlFont != NULL)
    {
    pmtlFont->AddMesh (&meshTextTest);
    }
  else
    {
    DBG_INFO ("Unable to find %s font material", strFontName.AsChar ());
    }
  */
  if (pmtlChecker != NULL)
    {
    pmtlChecker->AddMesh (&meshDisplay);
    };
  };

  
//------------------------------------------------------------------------------
VOID TestAScreenMode::LoadFont (const char *  szFontName)
  {
  RStr  strFontDefFile = "gfx/fonts/";
  strFontDefFile += szFontName;
  strFontDefFile += ".fnt";
  
  DBG_INFO ("Parsing font at %s", strFontDefFile.AsChar());
  RStrParser  parserFontDef;  parserFontDef.ReadFromFile (strFontDefFile.AsChar());
  
  BitmapFont * pFont = new BitmapFont ();
  pFont->ParseFont (parserFontDef.AsChar ());
  
  new Material (pFont->GetName (), "ShdMaskedColor"); // ShdUnlitTex
  Material *  pmtlFont = Material::FindByName (pFont->GetName ());
  
  if (pmtlFont != NULL)
    {
    DBG_INFO ("Setting material's diffuse texture %s", pFont->strFile.AsChar ());
    pmtlFont->SetDiffuseTexture (pFont->strFile);
    }
  else
    {
    DBG_INFO ("Failed to load material %s", pFont->GetName ());
    
    };
  }


//------------------------------------------------------------------------------
void TestAScreenMode::TestFont (void)
  {
  //BitmapFont *  pFont = BitmapFont::FindByName ("TrebuchetMS32");
  BitmapFont *  pFont = BitmapFont::FindByName ("Gashly");
  if (pFont == NULL) {return;};
  
  RStr  strOutputText ("The quick brown fox");

  
  pFont->AddTriangleStrip (strOutputText.AsChar (),
                           BitmapFont::Left,
                           320, // pixels per unit
                           &meshTextTest);
  }  
  
  
//------------------------------------------------------------------------------
void TestAScreenMode::DisplayViaMaterialDraw (RMatrix *  pmatCamProj)
  {
  renderer.DrawFrame(pmatCamProj);
  };
  
//------------------------------------------------------------------------------
void TestAScreenMode::TestWorld (void)
  {
  
  World  wldTheWorld;
  
  SceneLoader loader;
  
  loader.ReadFile ("scenes/TestSceneA.scn", &wldTheWorld);
  /*
  wldTheWorld.CreateNode ("|Animal");
  wldTheWorld.CreateNode ("|Animal|Mammal");
  wldTheWorld.CreateNode ("|Animal|Mammal|Rabbit");
  wldTheWorld.CreateNode ("|Animal|Mammal|Fox");
  */
  
  wldTheWorld.DebugPrint("");
  }










