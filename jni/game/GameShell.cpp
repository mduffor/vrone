
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "Timer.hpp"
#include "GLUtil.hpp"
#include "Shell.hpp"

#include "game/GameShell.hpp"
#include "image/BitmapLoader.hpp"
#include "image/CodecPam.hpp"
#include "image/CodecPng.hpp"
#include "component/ComponentInit.hpp"
#include "SceneLoader.hpp"
#include "NodeDelegate.hpp"
#include "World.hpp"
#include "Shader.hpp"

//#include "ScreenMode.hpp"
#include "TestAScreenMode.hpp"

const int SCREEN_WIDTH  = 800;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP    = 32;
const int GAME_FPS      = 60;

ScreenMode *        pScreenMode = NULL;


INT        iCurrScreenMode = 0;    
INT        iMaxScreenMode  = 1;

// OpenGL ES 2.0 test code

GLuint gGameShellProgram;
GLuint gvGameShellPositionHandle;


//-----------------------------------------------------------------------------
class ComponentDefaultLooper : public NodeDelegate
  {
  public:
    enum Callback {Update,
                   PostUpdate,
                   Collision,
                   Click,
                   Enable,
                   Disable,
                   Display};
                   
    Callback  callbackMode;
    BOOL      bWakeUp;
                     
  public:
                  ComponentDefaultLooper          () {};
    virtual       ~ComponentDefaultLooper         () {};

    virtual BOOL  VisitNode             (Node *  pnodeIn)    
                                           {
                                           pnodeIn->Awake ();
                                           
                                           if (callbackMode == Update)
                                             {
                                             pnodeIn->Update ();
                                             }
                                           return TRUE;
                                           };
             
    virtual VOID  VisitComponent        (Node *       pnodeIn,
                                         Component *  pcomponentIn)   
                                           {
                                           switch (callbackMode)
                                             {
                                             case PostUpdate:  pcomponentIn->OnPostUpdate (); break;
                                             case Collision:   pcomponentIn->OnCollision (); break;
                                             case Click:       pcomponentIn->OnClick (); break;
                                             case Enable:      pcomponentIn->OnEnable (); break;
                                             case Disable:     pcomponentIn->OnDisable (); break;
                                             case Display:     pcomponentIn->OnDisplay (); break;
                                             }
                                           };
                                         
    VOID          SetCallbackMode       (Callback  modeIn)  {callbackMode = modeIn;};
  };


//-----------------------------------------------------------------------------
class WakeNodesLooper : public NodeDelegate
  {
  public:
    BOOL      bWakeUp;
                     
  public:
                  WakeNodesLooper          () {};
    virtual       ~WakeNodesLooper         () {};

    virtual BOOL  VisitNode             (Node *  pnodeIn)    
                                           {
                                           bWakeUp = !pnodeIn->IsAwake ();
                                           if (bWakeUp)
                                             {
                                             pnodeIn->SetAwake (TRUE); 
                                             }
                                           return TRUE;
                                           };
             
    virtual VOID  VisitComponent        (Node *       pnodeIn,
                                         Component *  pcomponentIn)   
                                           {
                                           if (bWakeUp)
                                             {
                                             pcomponentIn->OnAwake ();
                                             }
                                           };
  };
  
  
//------------------------------------------------------------------------------
GameShell::GameShell ()
  {
  EStatus  status = EStatus (EStatus::kSuccess);
  
  pshellSingleton = this;
  
  Shell::SetCanvasWidth    (SCREEN_WIDTH);
  Shell::SetCanvasHeight   (SCREEN_HEIGHT);
  Shell::SetCanvasBitDepth (SCREEN_BPP);
  Shell::SetShellFlags     (0);
  Shell::SetTargetFps      (GAME_FPS);
  Shell::SetName           ("VROne");
  Shell::SetTitle          ("VROne");  
  
  // TODO: load templates
  
  ComponentInit ();
  
  BitmapLoader::RegisterCodec (new CodecPam);
  BitmapLoader::RegisterCodec (new CodecPng);

  ShaderInit ();

  LoadWorld (wldTheWorld, "scenes/Main.scn");
  
  pScreenMode = new TestAScreenMode (&wldTheWorld);
  
  Shell::Init ();
  }

//------------------------------------------------------------------------------
GameShell::~GameShell ()  
  {
  }

  
//------------------------------------------------------------------------------
VOID  GameShell::Update (UINT32  uMsDeltaIn)
  {
  if (bActive)
    {
    // check timer
    uCurrMsTime += uMsDeltaIn;
    thTimers.IncTime (uMsDeltaIn);
    uLastMsTime = uCurrMsTime;

    // perform your drawing
    ++iCurrFrameCount;
    
    // TODO: setup projection and model matrix for UI elements

    DBG_INFO ("=========== glClearColor =========");
    glClearColor(.1f, .2f, .3f, 1.0f);
    GLCheckError(__FILE__, "glClearColor");
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GLCheckError(__FILE__, "glClear");
    
    
    GameDraw ();

    // swap buffers as needed
    
    }
  else // call if stopping game from running in background
    {
    // make sure we go to sleep if we have nothing else to do
    //WaitMessage ();
    };
  };
  
  
//------------------------------------------------------------------------------
EStatus  GameShell::GameLoop  (UINT32  uMillisecondDeltaIn)
  {
  /// This function is called from the GameLoopTimer on a regular heartbeat
  
  DBG_INFO ("------====== GameLoop ======------");
  EStatus  status = EStatus (EStatus::kSuccess);

  // First time through the game loop, initialize it
  static BOOL     bInitialized = FALSE;
  if (bInitialized == FALSE)
    {
    status = GameInitialize ();
    if (status == EStatus::kFailure)   {return (status);};
    bInitialized = TRUE;
    };

  // TODO: Call per-frame game logic here.
  Shell::SetTimeDelta (uMillisecondDeltaIn);
  
  // Update, Post Update, Display
  ComponentDefaultLooper looper;
  
  looper.SetCallbackMode (ComponentDefaultLooper::Update);
  looper.RecurseNodeTree (wldTheWorld.RootNode ());
  
  looper.SetCallbackMode (ComponentDefaultLooper::PostUpdate);
  looper.RecurseNodeTree (wldTheWorld.RootNode ());
  
  
  if (pScreenMode != NULL)
    {
    pScreenMode->Update ();
    };

  return (status);
  };


//-----------------------------------------------------------------------------
EStatus  GameShell::GameInitialize  (VOID)
  {
  static EStatus    status = EStatus::kSuccess;
  
  DBG_INFO ("===== Game Initialize =====\n");

  // Note: This is a good place to test new systems before you roll them out.
  
  // TODO: initialize or load your scene
  
  if (pScreenMode != NULL)
    {
    pScreenMode->Init ();
    };
  
  DBG_INFO ("GameInitialize Done");
  return (status);
  };

  
//------------------------------------------------------------------------------
VOID GameShell::InitOpenGLInternal (VOID)
  {
  DBG_INFO ("GameShell::InitOpenGLInternal()");
  Shell::InitOpenGLInternal ();
  };
  

//------------------------------------------------------------------------------
VOID  GameShell::GameUninitialize         (VOID)
  {
  DBG_INFO ("===== Game Uninitialize =====\n");
  if (pScreenMode != NULL)
    {
    pScreenMode->Uninit ();
    };
  };


//------------------------------------------------------------------------------
VOID  GameShell::GameDraw  (VOID)
  {
  DBG_INFO ("------------------------- GameDraw () -------------------------\n");
  
  // perform the drawing
  
  // Cycle the background from black to white with the clear color command.
  static float grey;
  grey += 0.01f;
  if (grey > 1.0f) {
      grey = 0.0f;
  }
  glClearColor(grey, grey, grey, 1.0f);
  GLCheckError (__FILE__, "glClearColor");
  glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
  GLCheckError (__FILE__, "glClear");

  ComponentDefaultLooper looper;
  
  looper.SetCallbackMode (ComponentDefaultLooper::Display);
  looper.RecurseNodeTree (wldTheWorld.RootNode ());  
  
 
  
  if (pScreenMode != NULL)
    {
    pScreenMode->Draw ();
    };
  };

  
//------------------------------------------------------------------------------
VOID GameShell::LoadWorld (World &       wldTheWorld,
                           const char *  szSceneFileIn)
  {
  SceneLoader loader;
  
  loader.ReadFile (szSceneFileIn, &wldTheWorld);
  
  // wake up all the nodes that were just created.
  WakeNodesLooper looper;
  looper.RecurseNodeTree (wldTheWorld.RootNode ());  

  wldTheWorld.DebugPrint("");
  }

//------------------------------------------------------------------------------
VOID GameShell::ShaderInit (VOID)
  {
  
  // Shaders are re-useable across multiple materials.
  RStrParser  parserFrgBlue;          parserFrgBlue.ReadFromFile ("gfx/shaders/frgBlue.shd");
  RStrParser  parserFrgUnlitColor;    parserFrgUnlitColor.ReadFromFile ("gfx/shaders/frgUnlitColor.shd");
  RStrParser  parserFrgUnlitTexture;  parserFrgUnlitTexture.ReadFromFile ("gfx/shaders/frgUnlitTexture.shd");
  RStrParser  parserFrgDebugUV;       parserFrgDebugUV.ReadFromFile ("gfx/shaders/frgDebugUV.shd");
  RStrParser  parserFrgUnlitMaskClr;  parserFrgUnlitMaskClr.ReadFromFile ("gfx/shaders/frgUnlitMaskedColor.shd");
  RStrParser  parserVtxPos;           parserVtxPos.ReadFromFile ("gfx/shaders/vtxPos.shd");
  RStrParser  parserVtxPosTex;        parserVtxPosTex.ReadFromFile ("gfx/shaders/vtxPosTex.shd");
  RStrParser  parserVtxPosNrmColTex;  parserVtxPosNrmColTex.ReadFromFile ("gfx/shaders/vtxPosNrmColTex.shd");
  
  new Shader ("ShdBlue", parserVtxPos.AsChar (), parserFrgBlue.AsChar ());
  new Shader ("ShdUnlitColor", parserVtxPos.AsChar (), parserFrgUnlitColor.AsChar ());
  new Shader ("ShdDebugUV", parserVtxPosTex.AsChar (), parserFrgDebugUV.AsChar ());
  new Shader ("ShdUnlitTex", parserVtxPosTex.AsChar (), parserFrgUnlitTexture.AsChar ());
  new Shader ("ShdMaskedColor", parserVtxPosTex.AsChar (), parserFrgUnlitMaskClr.AsChar ());
  };
    