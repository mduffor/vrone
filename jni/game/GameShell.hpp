
#ifndef GAMESHELL_HPP
#define GAMESHELL_HPP

#include "Shell.hpp"
#include "World.hpp"

//------------------------------------------------------------------------------
class GameShell : public Shell
  {
  private:
    World         wldTheWorld;
    
    
  public:
                  GameShell  ();
            
    virtual       ~GameShell ();
    
    EStatus       GameInitialize           (VOID);

    VOID          GameUninitialize         (VOID);

    virtual VOID  InitOpenGLInternal       (VOID);

    VOID          Update                   (UINT32  uMsDeltaIn);
    
    EStatus       GameLoop                 (UINT32  uMillisecondDeltaIn);
    
    VOID          GameDraw                 (VOID);
    
    VOID          LoadWorld                (World &       wldTheWorld,
                                            const char *  szSceneFileIn);
    
    VOID          ShaderInit               (VOID);
    
  };

#endif // GAMESHELL_HPP
