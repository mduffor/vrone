/* -----------------------------------------------------------------
                            Screen Mode

     This is the base class for code that writes to the screen
     via the shell.

                            Michael T. Duffy
   ----------------------------------------------------------------- */

// ScreenMode.hpp
// Copyright 2013, Michael T. Duffy.  All Rights Reserved

#ifndef SCREENMODE_HPP
#define SCREENMODE_HPP

//------------------------------------------------------------------------------
class ScreenMode
  {
  public:
                 ScreenMode  ();
    virtual      ~ScreenMode ();

    virtual void Init   () = 0;
    virtual void Uninit () = 0;
    virtual void Draw   () = 0;
    virtual void Update () = 0;
  };

#endif // SCREENMODE_HPP
