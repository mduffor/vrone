/* -----------------------------------------------------------------
                            Screen Mode

     This is the base class for code that writes to the screen
     via the shell.

                            Michael T. Duffy
   ----------------------------------------------------------------- */

// ScreenMode.hpp
// Copyright 2013, Michael T. Duffy.  All Rights Reserved

#ifndef TESTASCREENMODE_HPP
#define TESTASCREENMODE_HPP

#include "ScreenMode.hpp"
#include "World.hpp"
#include "component/TransformComponent.hpp"
#include "component/CameraComponent.hpp"

//------------------------------------------------------------------------------
class TestAScreenMode : public ScreenMode
//------------------------------------------------------------------------------
  {
  public:
    World *               pwldTheWorld;
    Node *                pnodeCamera;
    TransformComponent *  pcmpCameraTransform;
    CameraComponent *     pcmpCamera;
    
  
  public:
    TestAScreenMode  (World *  pWorldIn);
    ~TestAScreenMode ();

    void Init ();
    void Uninit ();
    void Draw ();
    void Update ();
    void RenderFrameDirectGLCalls ();
    
    void DirectInit ();
    void DirectDrawCall ();

    void DisplayMeshInit ();
    void DisplayMeshDraw ();

    void MaterialInit();
    void DisplayViaMaterialDraw (RMatrix *  pmatCamProj);
    VOID LoadFont (const char *  szFontName);
    
    void TestFont  (void);
    void TestWorld (void);

    void LoadWorld (World &       wldTheWorld,
                    const char *  szSceneFileIn);
  };

#endif // TESTASCREENMODE_HPP
