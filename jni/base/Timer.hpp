#ifndef TIMER_HPP
#define TIMER_HPP

#include "Types.hpp"

//------------------------------------------------------------------------------
class Timer
  {
  // subclass this for each type of timer you need
  protected:
    UINT32    uLastTime;
    UINT32    uNextTime;
    UINT32    uCurrTime;
    UINT32    uMillisecondsToWait;
    BOOL      bOneTime;
  
  public:
    Timer *  pNext;
  
                 Timer        ();
                  
                 Timer        (UINT32  uMillisecondsToWaitIn,
                               BOOL    bOneTimeIn = FALSE);
        
   virtual       ~Timer       ();

   VOID          Init         (VOID);
        
   virtual BOOL  Post         (UINT32  uMillisecondsSinceLast);  ///> Call the Post routine.  Return True if this timer should be deleted, false otherwise.
   
   BOOL          IncTime      (UINT32  uMillisecondsIn);
   
   VOID          SetCurrTime  (UINT32  uTimeIn);
  };


//------------------------------------------------------------------------------
class TimerHandler
  {
  private:
    Timer *       pTimerList;
  
  public:
  
    TimerHandler         ();
    
    ~TimerHandler        ();
  
    VOID IncTime         (UINT32  uMillisecondsIn);
    
    VOID AddTimer        (Timer *  pTimerIn);
    
    VOID DeleteAllTimers (VOID);
  };

#endif // TIMER_HPP
