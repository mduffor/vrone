/* -----------------------------------------------------------------
                             Camera

     This module implements a Camera object through which a 3D 
     scene graph is viewed.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Transform.hpp"

/**

  */

//-----------------------------------------------------------------------------
class Camera
  {
  private:  
    enum EMode {kOrtho = 0,
                kPersp = 1};
  
  private:
    EMode      eMode;       /// view mode of the camera
    
    FLOAT      fWidth;      /// Width of the ortho camera view volume, in world units
    FLOAT      fHeight;     /// Height of the ortho camera view volume, in world units
    
    FLOAT      fViewWidth;  /// Width of the viewport, in pixels
    FLOAT      fViewHeight; /// Height of the viewport, in pixels
    
    FLOAT      fNear;       /// Near clip plane distance from camera, in world units
    FLOAT      fFar;        /// Far clip plane distance from camera, in world units
  
    FLOAT      fHorizFOV;   /// Horizontal field of view of the perspective camera, in radians
                            // Note: We may calc perspective a different way in the future.
    BOOL       bDirty;
  
    RMatrix    matProjection;
  
  public:
  
         Camera         ();
        
         ~Camera        ();

    VOID SetOrthographic (BOOL  bIsOrthoIn) {eMode = bIsOrthoIn ? kOrtho : kPersp; bDirty = TRUE;};
         
    VOID SetHorizFOV     (FLOAT  fFOVIn)     {fHorizFOV = fFOVIn; bDirty = TRUE;};
         
    VOID Ortho           (FLOAT  fWidthIn, 
                          FLOAT  fHeightIn);
  
    VOID Persp           (FLOAT  fHorizFOVIn);
    
    VOID SetViewport     (INT  iWidthIn, 
                          INT  iHeightIn);

    VOID SetOrthoSize    (FLOAT  fWidthIn,
                          FLOAT  fHeightIn);

    VOID SetNearFar      (FLOAT  fNearIn,
                          FLOAT  fFarIn);
                      
    VOID CalcProjection  (VOID);

    VOID GetMatrix       (RMatrix &  matOut)  {CalcProjection ();  matOut = matProjection;};

  };
  

  
#endif // CAMERA_HPP