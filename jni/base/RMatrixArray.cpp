/* -----------------------------------------------------------------
                          Array of 3D Matrices

     This module implements an array of matrices.


   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 


#include "Debug.hpp"
ASSERTFILE (__FILE__)


#include "RMatrixArray.hpp"


//-----------------------------------------------------------------------------
//  RMatrixArray
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
RMatrixArray::RMatrixArray  ()
  {
  Init ();
  };


//-----------------------------------------------------------------------------
RMatrixArray::RMatrixArray  (const RMatrixArray &  arrayIn)
  {
  Init ();
  Copy (arrayIn);
  };


//-----------------------------------------------------------------------------
RMatrixArray::RMatrixArray  (INT   iInitialSize)
  {
  Init ();
  SetLength (iInitialSize);
  InitValues (0, iAllocSize);
  };


//-----------------------------------------------------------------------------
RMatrixArray::~RMatrixArray  ()
  {
  DeleteArray (&pArray);
  };


//-----------------------------------------------------------------------------
VOID *  RMatrixArray::AllocArray  (INT  iSizeIn)
  {
  return new RMatrix [iSizeIn];
  };


//-----------------------------------------------------------------------------
VOID  RMatrixArray::DeleteArray  (PVOID *  pvArrayIn)
  {
  delete [] ((RMatrix *) *pvArrayIn);
  *pvArrayIn = NULL;
  };


//-----------------------------------------------------------------------------
VOID  RMatrixArray::InitValues  (INT    iStartOffset,
                                 INT    iNumToInit)
  {
  // zero out the requested range of the array

  INT iEndOffset = iStartOffset + iNumToInit;

  for (INT  iIndex = iStartOffset; iIndex < iEndOffset; ++iIndex)
    {
    ((RMatrix *) pArray) [iIndex].Identity ();
    };
  };


//-----------------------------------------------------------------------------
VOID  RMatrixArray::CopyValues  (PVOID  pvSourceDataIn,
                                 INT    iSourceOffsetIn,
                                 INT    iStartOffsetIn,
                                 INT    iNumToCopyIn)
  {
  for (INT  iIndex = 0; iIndex < iNumToCopyIn; ++iIndex)
    {
    ((RMatrix *) pArray) [iIndex + iStartOffsetIn] = ((RMatrix *) pvSourceDataIn) [iIndex + iSourceOffsetIn];
    };
  };


//-----------------------------------------------------------------------------
VOID  RMatrixArray::CopyValuesRev  (PVOID  pvSourceDataIn,
                                    INT    iSourceOffsetIn,
                                    INT    iStartOffsetIn,
                                    INT    iNumToCopyIn)
  {
  // this routine is the opposite of CopyValues, and is used for
  //  properly shifting values to the right in the array

  for (INT  iIndex = iNumToCopyIn - 1; iIndex >= 0; --iIndex)
    {
    ((RMatrix *) pArray) [iStartOffsetIn + iIndex] = ((RMatrix *) pvSourceDataIn) [iIndex + iSourceOffsetIn];
    };
  };


//-----------------------------------------------------------------------------
VOID  RMatrixArray::AverageValues  (INT    iIndexAverage,
                                    INT    iIndexValueOne,
                                    INT    iIndexValueTwo,
                                    FLOAT  fBias)
  {

  // note:  You can't average matrixes.
  //((RMatrix *) pArray) [iIndexAverage] = ( (((RMatrix *) pArray) [iIndexValueOne] * fBias) +
  //                                         (((RMatrix *) pArray) [iIndexValueTwo] * (1.0f - fBias)) );
  };

//-----------------------------------------------------------------------------
VOID  RMatrixArray::SwapIndexes  (INT  iIndexOne,
                                  INT  iIndexTwo)
  {
  RMatrix                  matTemp = ((RMatrix *) pArray) [iIndexOne];
  ((RMatrix *) pArray) [iIndexOne] = ((RMatrix *) pArray) [iIndexTwo];
  ((RMatrix *) pArray) [iIndexTwo] = matTemp;
  };
  
  
//-----------------------------------------------------------------------------
INT  RMatrixArray::CompareIndexes  (INT  iIndexOne,
                                    INT  iIndexTwo)
  {
  // cannot compare matrixes other than equal
  if (((RMatrix *) pArray) [iIndexOne] == ((RMatrix *) pArray) [iIndexTwo]) return (0);
  return (-1);
  };





//-----------------------------------------------------------------------------
EStatus RMatrixArray::Set (RMatrix   dElement,
                           INT     iIndex)
  {
  if (iIndex >= iLength) return (EStatus::kFailure);
  ((RMatrix *) pArray) [iIndex] = dElement;
  return (EStatus::kSuccess);
  };


//-----------------------------------------------------------------------------
EStatus RMatrixArray::Append (RMatrix  dElement)
  {
  INT  iOldLength = iLength;

  if (SetLength (iOldLength + 1) == EStatus::kFailure) {return EStatus::kFailure;};

  ((RMatrix *) pArray) [iOldLength] = dElement;
  return (EStatus::kSuccess);
  };

//-----------------------------------------------------------------------------
EStatus RMatrixArray::GetArray (RMatrix  aOut []) const
  {
  if (iLength == 0) return (EStatus::kFailure);

  for (INT  iIndex = 0; iIndex < iLength; ++iIndex)
    {
    aOut [iIndex] = ((RMatrix *) pArray) [iIndex];
    };
  return (EStatus::kSuccess);
  };


//-----------------------------------------------------------------------------
EStatus RMatrixArray::Copy (const RMatrixArray &  arraySource)
  {
  if (SetLength (arraySource.iLength) == EStatus::kFailure) {return EStatus::kFailure;};

  CopyValues (arraySource.pArray, 0, 0, arraySource.Length ());

  return (EStatus::kSuccess);
  };


//-----------------------------------------------------------------------------
RMatrix RMatrixArray::operator[]  (INT  iIndex) const
  {
  return ((RMatrix *) pArray) [iIndex];
  };


//-----------------------------------------------------------------------------
RMatrix &  RMatrixArray::operator[]  (INT  iIndex)
  {
  return ((RMatrix *) pArray) [iIndex];
  };


//-----------------------------------------------------------------------------
RMatrixArray &  RMatrixArray::operator=  (const RMatrixArray &  arrayIn)
  {
  Copy (arrayIn);
  return (*this);
  };
  

//-----------------------------------------------------------------------------
BOOL  RMatrixArray::operator==  (const RMatrixArray &  arrayIn)
  {
  if (iLength == 0) return (EStatus::kFailure);

  for (INT  iIndex = 0; iIndex < iLength; ++iIndex)
    {
    if (arrayIn [iIndex] != ((RMatrix *) pArray) [iIndex])
      {
      return (false);
      };
    };
  return (true);
  };
  
