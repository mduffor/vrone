/* -----------------------------------------------------------------
                             Transform

     This module implements a transformation matrix consisting of
     translate, rotate, and scale components.  It also implements
     parenting for the purpose of implementing scene graphs.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Euler.hpp"

/**

  */

//-----------------------------------------------------------------------------
class Transform
  {
  private:

    RVec3        vecTranslate;
    Euler        eulRotate;
    RVec3        vecScale;
    
    RMatrix      matLocal;
    RMatrix      matWorld;
    
    Transform *  pParent;
    Transform *  pSibling;
    Transform *  pChildren;
  
    BOOL         bLocalDirty;
    BOOL         bWorldDirty;
  
  public:
  
               Transform       ();
    
               ~Transform      ();
    
    VOID       Reset           (VOID)  ;
    
    VOID       SetPosition     (FLOAT          fX, 
                                FLOAT          fY,
                                FLOAT          fZ)      {vecTranslate.Set (fX, fY, fX); bLocalDirty = bWorldDirty = TRUE;};

    VOID       SetEuler        (FLOAT          fXDeg, 
                                FLOAT          fYDeg,
                                FLOAT          fZDeg,
                                Euler::EOrder  eOrder)  {eulRotate.Set (fXDeg, fYDeg, fZDeg, eOrder); bLocalDirty = bWorldDirty = TRUE;};
                      
    VOID       SetScale        (FLOAT          fX, 
                                FLOAT          fY,
                                FLOAT          fZ)      {vecScale.Set (fX, fY, fZ); bLocalDirty = bWorldDirty = TRUE;};
    
    VOID       CalcLocalMatrix ();
    
    VOID       CalcWorldMatrix ();

    VOID       GetLocalMatrix  (RMatrix &  matOut);
    
    VOID       GetWorldMatrix  (RMatrix &  matOut);

    RMatrix &  LocalMatrix     (VOID)                   {CalcLocalMatrix (); return matLocal;};
    
    RMatrix &  WorldMatrix     (VOID)                   {CalcWorldMatrix (); return matWorld;};
    
    VOID       SetParent       (Transform *  pParentIn);

    VOID       UnParent        (VOID);
    
  };
  

  
#endif // TRANSFORM_HPP