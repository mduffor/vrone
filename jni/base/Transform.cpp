/* -----------------------------------------------------------------
                             Transform

     This module implements a transformation matrix consisting of
     translate, rotate, and scale components.  It also implements
     parenting for the purpose of implementing scene graphs.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Transform.hpp"

//-----------------------------------------------------------------------------
//  Transform
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
Transform::Transform  ()
  {
  Reset ();
  bLocalDirty = TRUE;
  bWorldDirty = TRUE;
  pParent     = NULL;
  pSibling    = NULL;
  pChildren   = NULL;
  };

  
//-----------------------------------------------------------------------------
Transform::~Transform ()
  {
  };

//-----------------------------------------------------------------------------
VOID Transform::Reset (VOID)  
  {
  vecTranslate.Set (0.0f, 0.0f, 0.0f);
  eulRotate.Set  (0.0f, 0.0f, 0.0f);
  vecScale.Set     (1.0f, 1.0f, 1.0f);
  };
  
  
//-----------------------------------------------------------------------------
VOID Transform::CalcLocalMatrix ()
  {
  if (bLocalDirty)
    {
    RMatrix  matScale (vecScale.fX, 0.0f, 0.0f, 0.0f,
                       0.0f, vecScale.fY, 0.0f, 0.0f,
                       0.0f, 0.0f, vecScale.fZ, 0.0f, 
                       0.0f, 0.0f, 0.0f, 1.0f);
    
    RMatrix  matRotate;
    eulRotate.ToMatrix (matRotate);
    
    matLocal = matScale * matRotate;
    matLocal.SetTrans (vecTranslate.fX, vecTranslate.fY, vecTranslate.fZ);
    
    bLocalDirty = FALSE;
    }
  };

  
//-----------------------------------------------------------------------------
VOID Transform::CalcWorldMatrix ()
  {
  if (bWorldDirty)
    {

    CalcLocalMatrix ();
    
    if (pParent != NULL) 
      {
      RMatrix  matParent;
      pParent->GetWorldMatrix (matParent);
      
      // combine parent and local matrix to get the world matrix.
      matWorld = matParent * matLocal;
      }
    else
      {
      // world matrix is same as local matrix
      matWorld = matLocal;
      };
    bWorldDirty = FALSE;
    };
  };

  
//-----------------------------------------------------------------------------
VOID Transform::GetLocalMatrix (RMatrix &  matOut)
  {
  CalcLocalMatrix ();
  matOut = matLocal;
  };

  
//-----------------------------------------------------------------------------
VOID Transform::GetWorldMatrix (RMatrix &  matOut)
  {
  CalcWorldMatrix ();
  matOut = matWorld;
  };

//-----------------------------------------------------------------------------
VOID Transform::SetParent (Transform *  pParentIn)
  {
  
  UnParent ();
  pParent = pParentIn;
  if (pParent != NULL)
    {
    pSibling = pParent->pChildren;
    pParent->pChildren = this;
    };
  }
  
//-----------------------------------------------------------------------------
VOID Transform::UnParent (VOID)
  {
  // remove from parent's children
  
  if (pParent != NULL)
    {
    Transform *  pCurr;
    Transform *  pPrev = NULL;
    
    pCurr = pParent->pChildren;
    while (pCurr != NULL)
      {
      if (pCurr == this)
        {
        // found ourself in the children list of our parent.  Remove.
        if (pPrev == NULL)
          {
          pParent->pChildren = pCurr->pSibling;
          }
        else
          {
          pPrev->pSibling = pCurr->pSibling;
          }
        pParent = NULL;
        pSibling = NULL;
        return;
        }
      pPrev = pCurr;
      pCurr = pCurr->pSibling;
      };
    // didn't find ourself as a child of our parent.  Error!
    DBG_ERROR ("Transform::Unparent() : Corrupt heirarchy detected!");
    };
  }
  
  