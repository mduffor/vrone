/* -----------------------------------------------------------------
                             Material

     This module implements a renderer class that tracks the
     materials and the display meshes to be displayed with them.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Material.hpp"
#include "mesh/DisplayMesh.hpp"
#include "Material.hpp"


static Material *  pmtlMasterList = NULL;
  
static int aGLBlendFactors [] = {GL_ZERO,                       // 0
                                 GL_ONE,                        // 1 
                                 GL_SRC_COLOR,                  // 2
                                 GL_ONE_MINUS_SRC_COLOR,        // 3
                                 GL_DST_COLOR,                  // 4
                                 GL_ONE_MINUS_DST_COLOR,        // 5
                                 GL_SRC_ALPHA,                  // 6
                                 GL_ONE_MINUS_SRC_ALPHA,        // 7
                                 GL_DST_ALPHA,                  // 8
                                 GL_ONE_MINUS_DST_ALPHA,        // 9
                                 GL_CONSTANT_COLOR,             // 10
                                 GL_ONE_MINUS_CONSTANT_COLOR,   // 11
                                 GL_CONSTANT_ALPHA,             // 12
                                 GL_ONE_MINUS_CONSTANT_ALPHA,   // 13
                                 GL_SRC_ALPHA_SATURATE,         // 14
                                 // Note: These are not not part of OpenGL 2.x.  They are part of ARB_blend_func_extended and v3.3+
                                 //GL_SRC1_COLOR,                 // 15
                                 //GL_ONE_MINUS_SRC1_COLOR,       // 16
                                 //GL_SRC1_ALPHA,                 // 17
                                 //GL_ONE_MINUS_SRC1_ALPHA        // 18
                                };

//-----------------------------------------------------------------------------
//  Material
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
Material::Material  ()
  {
  Init ();
  };

  
//-----------------------------------------------------------------------------
Material::~Material ()
  {
  };

//-----------------------------------------------------------------------------
Material::Material (const char *  szNameIn,
                    const char *  szShaderNameIn)
  {
  Init ();
  SetName (szNameIn);
  pShader = Shader::FindByName (szShaderNameIn);
  };
  
//-----------------------------------------------------------------------------
VOID Material::Init (VOID)
  {
  clrAmbient.Set (255u, 255u, 255u, 255u);
  clrDiffuse.Set (255u, 255u, 255u, 255u);;
  clrSpecular.Set (255u, 255u, 255u, 255u);;
  
  ptexAmbient  = NULL;
  ptexDiffuse  = NULL;
  ptexSpecular = NULL;

  pShader     = NULL;
  pMeshes     = NULL;
  pMasterNext = NULL;
  Register ();
  }
  
//-----------------------------------------------------------------------------
VOID Material::Register (VOID)
  {
  /// Store in the master list of materials.
  this->pMasterNext = pmtlMasterList;
  pmtlMasterList = this;
  };

//-----------------------------------------------------------------------------
VOID Material::Unregister (VOID)
  {
  /// Remove from the master list of materials.
  Material *  pPrev = NULL;
  Material *  pCurr = pmtlMasterList;
  
  while (pCurr != NULL)
    {
    if (pCurr == this)
      {
      if (pPrev == NULL)
        {
        pmtlMasterList = pCurr->pMasterNext;
        }
      else
        {
        pPrev->pMasterNext = pCurr->pMasterNext;
        };
      pMasterNext = NULL;
      return;
      }
    pPrev = pCurr;
    pCurr = pCurr->pMasterNext;
    }
  };  
  
//-----------------------------------------------------------------------------
Material * Material::FindByName (const char *  szNameIn)
  {
  /// Find in the master list of materials.
  Material *  pCurr = pmtlMasterList;
  
  while (pCurr != NULL)
    {
    if (pCurr->strName == szNameIn)
      {
      return (pCurr);
      }
    pCurr = pCurr->pMasterNext;
    };
  return (NULL);
  };

//-----------------------------------------------------------------------------
VOID Material::AddMesh (DisplayMesh *  pMeshIn)
  {
  pMeshIn->pNextInMaterial = pMeshes;
  pMeshes = pMeshIn;
  };
    
//-----------------------------------------------------------------------------
VOID Material::RemoveMesh (DisplayMesh *  pMeshIn)
  {
  DisplayMesh *  pPrev = NULL;
  DisplayMesh *  pCurr = pMeshes;
  
  while (pCurr != NULL)
    {
    if (pCurr == pMeshIn)
      {
      if (pPrev == NULL)
        {
        pMeshes = pCurr->pNextInMaterial;
        }
      else
        {
        pPrev->pNextInMaterial = pCurr->pNextInMaterial;
        }
      pCurr->pNextInMaterial = NULL;
      return;
      }
    pCurr = pCurr->pNextInMaterial;
    }
  };
  
//-----------------------------------------------------------------------------
BOOL Material::HasActiveMeshes (VOID)
  {
  /// Check to see if there are any non-culled meshes
  DisplayMesh *  pCurr = pMeshes;
  
  while (pCurr != NULL)
    {
    if (!pCurr->IsCulled ())
      {
      return (TRUE);
      }
    pCurr = pCurr->pNextInMaterial;
    }
  return (FALSE);
  }
  
//-----------------------------------------------------------------------------
EStatus Material::Activate (RMatrix *  pmatCamProj)  
  {
  if (pShader == NULL)
    {
    DBG_ERROR ("No shader defined for material");
    return (EStatus::kFailure);
    }

  // TODO: Check to see if there is a map associated with each channel
  //         Bind bitmaps for those that need it.
  
  if ((ptexDiffuse == NULL) && (!strDiffuseTexFile.IsEmpty ()))
    {
    ptexDiffuse = new Texture (NULL, strDiffuseTexFile.AsChar ());
    };
  
  EBlendFactor  eSrcBlend = kSrcAlpha;
  EBlendFactor  eDstBlend = kOneMinusSrcAlpha;
  
  int iSrcBlend = aGLBlendFactors[eSrcBlend];
  int iDstBlend = aGLBlendFactors[eDstBlend];

  // Enable shader
  pShader->Enable ();
  INT  iShaderProgram = pShader->GetProgram ();
  
  if (iShaderProgram != -1)
    {
    // set shader parameters
    
    // assign uniform shader values
    INT iCamProjLoc = glGetUniformLocation (iShaderProgram, "u_cameraProj");
    GLCheckError(__FILE__, "glGetUniformLocation Material::Activate()  u_cameraProj");
    
    if (iCamProjLoc != -1)
      {
      FLOAT   afOut[16];
      
      pmatCamProj->GetFloatArray (afOut);
      glUniformMatrix4fv (iCamProjLoc, 1, GL_FALSE, afOut);
      };
    
    FLOAT  afOut[3];
    INT iDiffuseLoc = glGetUniformLocation (iShaderProgram, "u_diffcolor");
    GLCheckError(__FILE__, "glGetUniformLocation Material::Activate()  u_diffcolor");
    
    if (iDiffuseLoc != -1)
      {
      afOut[0] = clrDiffuse.GammaR(); afOut[1] = clrDiffuse.GammaG(); afOut[2] = clrDiffuse.GammaB(); 
      glUniform3fv (iDiffuseLoc, 1, &afOut[0]);
      };
    
    INT  iTexLoc = glGetUniformLocation (iShaderProgram, "u_texture");
    GLCheckError(__FILE__, "glGetUniformLocation Material::Activate()  u_texture");
    if (iTexLoc != -1)
      {
      if (ptexDiffuse != NULL)
        {
        ptexDiffuse->Enable ();
        };
      
      //BindBitmapNode (GL_TEXTURE0, pattrKdmap);
      glUniform1i(iTexLoc, 0);
      };
    
    glBlendFunc(iSrcBlend, iDstBlend);
    glEnable(GL_BLEND);
    };
    
  return (EStatus::kSuccess);  
  };
  
//-----------------------------------------------------------------------------
Material *  Material::GetFirstActive  (VOID)
  {
  if (pmtlMasterList == NULL)
    {
    return (NULL);
    }
  if (pmtlMasterList->HasActiveMeshes ())
    {
    return (pmtlMasterList);
    }
  return (GetNextActive (pmtlMasterList));
  };
  
//-----------------------------------------------------------------------------
Material *  Material::GetNextActive  (Material *  pCurr)
  {
  if (pCurr != NULL)
    {
    pCurr = pCurr->pMasterNext;
    }
  while (pCurr != NULL)
    {
    if (pCurr->HasActiveMeshes ())
      {
      return (pCurr);
      }
    pCurr = pCurr->pMasterNext;
    };
  return (NULL);  
  };

//-----------------------------------------------------------------------------
DisplayMesh *  Material::GetFirstActiveMesh  (VOID)
  {
  DisplayMesh *  pCurr = pMeshes;
  
  while (pCurr != NULL)
    {
    if (!pCurr->IsCulled ())
      {
      return (pCurr);
      }
    pCurr = pCurr->pNextInMaterial;
    }
  return (NULL);
  };
  
//-----------------------------------------------------------------------------
DisplayMesh *  Material::GetNextActiveMesh  (DisplayMesh *  pCurr)
  {
  if (pCurr != NULL)
    {
    pCurr = pCurr->pNextInMaterial;
    }
  while (pCurr != NULL)
    {
    if (!pCurr->IsCulled ())
      {
      return (pCurr);
      }
    pCurr = pCurr->pNextInMaterial;
    }
  return (NULL);
  };
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  