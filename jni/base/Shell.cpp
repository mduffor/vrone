
// SHELL.CPP
// Copyright 1996, Michael T. Duffy.  All Rights Reserved

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "Shell.hpp"
#include "Timer.hpp"
#include "GLUtil.hpp"
//#include "RGlobal.hpp"

TimerHandler    thTimers;

BOOL             Shell::bActive = TRUE;        // is application active?
BOOL             Shell::g_bCloseApp = FALSE;
UINT32           Shell::uLastMsTime = 0;
UINT32           Shell::uCurrMsTime = 0;

UINT32           Shell::uMsTimeDelta = 0; // time since last update in milliseconds

UINT             Shell::uCanvasWidth = 640;
UINT             Shell::uCanvasHeight = 480;
UINT             Shell::uCanvasBitDepth = 32;
UINT             Shell::uShellFlags;
UINT             Shell::uTargetFps = 60;
    
RStr             Shell::strName;
RStr             Shell::strTitle;
    
CHAR             Shell::szCommandLine [256];

INT              Shell::aiFrameCounts [5];
INT              Shell::iCurrFrameCount;    

FLOAT            Shell::fGlobalTime = 0.0f;

Shell *          Shell::pshellSingleton = NULL;
InputManager     Shell::inputManager;


//************************************************************************
//   Code
//************************************************************************

//------------------------------------------------------------------------------
VOID Shell::InitOpenGL (VOID)
  {
  if (pshellSingleton)
    {
    pshellSingleton->InitOpenGLInternal ();
    }
  }
  
//------------------------------------------------------------------------------
VOID Shell::InitOpenGLInternal (VOID)
  {
  GLPrintInfo ();
  
  #ifndef ANDROID_NDK
    glewInit ();
  #endif

  // OpenGL Initialization                              
  glViewport   (0, 0, GetCanvasWidth (), GetCanvasHeight ());

  /*
  // left, right, bottom, top, near, far
  //glOrtho (0, GetCanvasWidth (), GetCanvasHeight (), 0, 1, -1);
  DBG_INFO (" Shell::InitOpenGLInternal creating ortho matrix for canvas sized %d  %d", GetCanvasWidth (), GetCanvasHeight ());
  RMatrix  matOrtho;
  MatrixOrtho (matOrtho, 0, GetCanvasWidth (), GetCanvasHeight (), 0, 1, -1);
  igCtx.SetProjMatrix   (matOrtho);
  */
  
  //igCtx.SetModelMatrix   (RMatrix::kIdentity);
  };
  
  
//------------------------------------------------------------------------------
Shell::Shell ()
  {
  //Init ();
  };

//------------------------------------------------------------------------------
Shell::~Shell ()
  {
  Uninit ();
  };

  
//------------------------------------------------------------------------------
VOID Shell::Init ()
  {
  // initialize internal variables
  g_bCloseApp = FALSE;

  // start the default timer
  thTimers.AddTimer (new GameLoopTimer (1000.0 / GetTargetFps (), false));
  thTimers.AddTimer (new FrameCountTimer (1000.0, false));
  fGlobalTime = 0.0f;
  
  DBG_INFO ("Shell initialization complete");
  };

//------------------------------------------------------------------------------
VOID Shell::Uninit ()
  {
  // We've exited the main loop.  Clean up.
  thTimers.DeleteAllTimers ();
  };

//------------------------------------------------------------------------------
VOID Shell::CloseApplication  (VOID)
  {
  DBG_INFO ("CloseApplication() called\n");
  g_bCloseApp = TRUE;
  };

//------------------------------------------------------------------------------
VOID Shell::FatalError (const char *      szMsgIn)
  {
  CHAR                szMsgOut [256];


  DBG_ERROR ("");
  DBG_ERROR ("---===**************===---");
  DBG_ERROR ("****** FATAL ERROR *******\n  %s  ", szMsgIn);
  DBG_ERROR ("---===**************===---");
  DBG_ERROR ("");

  Shell::CloseApplication ();
  };
 
//------------------------------------------------------------------------------
VOID Shell::ReSizeScene  (INT             iWidthIn,
                          INT             iHeightIn)
  {
  SetCanvasWidth  (iWidthIn);
  SetCanvasHeight (iHeightIn);
  
  // Reset The Current Viewport And Perspective Transformation
  //camMainCamera.SetWindowSize (iWidthIn, iHeightIn);
  };

//------------------------------------------------------------------------------
VOID Shell::StaticUpdate      (UINT32  uMsDeltaIn)
  {
  if (pshellSingleton != NULL)
    {
    pshellSingleton->Update (uMsDeltaIn);
    };
  };

//------------------------------------------------------------------------------
EStatus Shell::StaticGameLoop   (UINT32  uMillisecondDeltaIn)
  {
  if (pshellSingleton != NULL)
    {
    return (pshellSingleton->GameLoop (uMillisecondDeltaIn));
    };
  return (EStatus::kFailure);
  };
  
  
//==============================================================================
//  Game Loop Timer
//==============================================================================

//------------------------------------------------------------------------------
GameLoopTimer::GameLoopTimer  () : Timer ()
  {
  };


//------------------------------------------------------------------------------
GameLoopTimer::GameLoopTimer  (UINT32    uMillisecondsToWaitIn,
                               BOOL      bOneTimeIn) : Timer (uMillisecondsToWaitIn, bOneTimeIn)
  {
  };
  
//------------------------------------------------------------------------------
GameLoopTimer::~GameLoopTimer ()
  {
  // this space intentionally left empty
  };
    
//------------------------------------------------------------------------------
BOOL  GameLoopTimer::Post  (UINT32  uMillisecondsSinceLast)
  {
  EStatus   status;
  
  //DBG_INFO ("GameLoopTimer::Post ()\n");
  Shell::SetGlobalTime (Shell::GetGlobalTime () + FLOAT (uMillisecondsSinceLast) / 1000.0f);
  
  status = Shell::StaticGameLoop (uMillisecondsSinceLast);
  if (status == EStatus::kFailure)
    {
    Shell::FatalError (status.GetDescription ());
    // exit timer
    return (1);
    };
    
  // repeat timer
  return (0);
  };
  

//==============================================================================
//  Framecount Timer
//==============================================================================

//------------------------------------------------------------------------------
FrameCountTimer::FrameCountTimer  () : Timer ()
  {
  // this space intentionally left empty
  };


//------------------------------------------------------------------------------
FrameCountTimer::FrameCountTimer  (UINT32  uMillisecondsToWaitIn,
                                   BOOL    bOneTimeIn) : Timer (uMillisecondsToWaitIn, bOneTimeIn)
  {
  // this space intentionally left empty
  };
  
//------------------------------------------------------------------------------
FrameCountTimer::~FrameCountTimer ()
  {
  // this space intentionally left empty
  };
    
//------------------------------------------------------------------------------
BOOL  FrameCountTimer::Post  (UINT32  uMillisecondsSinceLast)
  {
  INT  iIndex;
  
  //RStr  strOut;  strOut.Format ("Shell is at %x", pShell);  DBG_INFO (strOut.AsChar ());
  for (iIndex = 0; iIndex < 4; ++iIndex)
    {
    Shell::aiFrameCounts [iIndex] = Shell::aiFrameCounts [iIndex + 1];
    };
  Shell::aiFrameCounts [4] = Shell::iCurrFrameCount;
  Shell::iCurrFrameCount = 0;
  
  INT  iFrameCountSum = 0;
  for (iIndex = 0; iIndex < 5; ++iIndex)
    {
    iFrameCountSum += Shell::aiFrameCounts [iIndex];
    };
  DBG_RAW ("====== Frame Count: Avg: %d Curr: %d======\n", iFrameCountSum / 4, Shell::aiFrameCounts [4]);
  
  // repeat
  return (0);
  };
  
