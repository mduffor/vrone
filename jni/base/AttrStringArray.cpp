/* -----------------------------------------------------------------
                             Attr String Array

     This module implements string array attributes
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include <stdlib.h>

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "AttrStringArray.hpp"

//-----------------------------------------------------------------------------
//  AttrStringArray
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
AttrStringArray::AttrStringArray (const char *  szNameIn) : Attr (szNameIn)
  {
  arrayValue.Clear ();
  szType = szStringArrayAttrIdentifier;
  };
  
//-----------------------------------------------------------------------------
AttrStringArray::~AttrStringArray ()
  {
  }; 
    
//-----------------------------------------------------------------------------
AttrStringArray &  AttrStringArray::operator= (const AttrStringArray &  attrIn)
  {
  Attr::CopyCommonVars (attrIn);
  arrayValue = attrIn.arrayValue;
  return (*this);
  }; 
    
//-----------------------------------------------------------------------------
VOID  AttrStringArray::GetAsString  (RStr *  pstrValueOut) const
  {
  if (pstrValueOut == NULL) return;
  RStrParser  parserCurr;
  
  INT  iNumStrings = arrayValue.Length ();
  for (INT  iIndex = 0; iIndex < iNumStrings; ++iIndex)
    {
    parserCurr = arrayValue[iIndex];
    parserCurr.MakeQuoteString ();
    pstrValueOut->AppendString (parserCurr);
    if (iIndex < iNumStrings - 1)
      {
      *pstrValueOut += ", ";
      }
    };
  }; 

//-----------------------------------------------------------------------------
VOID  AttrStringArray::SetByString  (const char *  szValueIn)
  {
  SetArrayByString (0, szValueIn);
  }; 

//-----------------------------------------------------------------------------
VOID  AttrStringArray::SetArrayByString  (INT           iIndexIn,
                                          const char *  szValueIn)
  {
  Attr::SetArrayByString (iIndexIn, szValueIn);
  arrayValue.SetMinLength (iIndexIn + 1);
  arrayValue.Set (szValueIn, iIndexIn);
  }; 
    
//-----------------------------------------------------------------------------
VOID  AttrStringArray::Clear  (VOID)
  {
  arrayValue.Clear ();
  }; 

//-----------------------------------------------------------------------------
EStatus  AttrStringArray::Load  (RStrParser &  parserIn)
  {
  return (EStatus::kFailure);
  }; 
    
//-----------------------------------------------------------------------------
EStatus  AttrStringArray::Save  (RStrParser &  parserIn,
                                 RStr          strLineIndentIn) const
  {
  return (EStatus::kFailure);
  }; 

    
    
      
  