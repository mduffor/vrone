/* -----------------------------------------------------------------
                            Float Array

     This module implements an array of floats.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2002-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef FLOATARRAY_HPP
#define FLOATARRAY_HPP

#include "Types.hpp"
#include "BaseArray.hpp"

//------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Class Definitions
//------------------------------------------------------------------------


//-----------------------------------------------------------------------------
class FloatArray : public BaseArray
  {
  private:

  public:

                   FloatArray       ();
                   FloatArray       (const FloatArray &  arrayIn);
                   FloatArray       (INT   iInitialSize);
                   ~FloatArray      ();

    VOID *         AllocArray       (INT      iSizeIn);
    VOID           DeleteArray      (PVOID *  pvArrayIn);
    VOID           InitValues       (INT      iStartOffset,
                                     INT      iNumToInit,
                                     FLOAT    fValue = 0.0f);
    VOID           CopyValues       (PVOID    pvSourceData,
                                     INT      iSourceOffsetIn,
                                     INT      iStartOffset,
                                     INT      iNumToCopy);
    VOID           CopyValuesRev    (PVOID    pvData,
                                     INT      iSourceOffsetIn,
                                     INT      iStartOffset,
                                     INT      iNumToCopy);
    VOID           AverageValues    (INT      iIndexAverage,
                                     INT      iIndexValueOne,
                                     INT      iIndexValueTwo,
                                     FLOAT    fBias = 0.5f);
    VOID           SwapIndexes      (INT  iIndexOne,
                                     INT  iIndexTwo); 
    INT            CompareIndexes   (INT  iIndexOne,
                                     INT  iIndexTwo); 

    EStatus        Copy             (const  FloatArray &  arraySource);
    EStatus        Set              (FLOAT  fElement, INT  iIndex);
    EStatus        Append           (FLOAT  fElement);
    EStatus        GetArray         (FLOAT  []) const;

    FLOAT          operator[]       (INT  iIndex) const;
    FLOAT &        operator[]       (INT  iIndex);
    FLOAT          Index            (INT  iIndex) const {return (*this)[iIndex];};
    FloatArray &    operator=       (const  FloatArray & arrayIn);
    VOID           Set              (const  FloatArray & arrayIn)  {*this = arrayIn;};
    BOOL           operator==       (const  FloatArray & arrayIn);
    BOOL           operator!=       (const  FloatArray & arrayIn)  {return (!(*this == arrayIn));};

    VOID           DebugPrint       (UINT  uCount);

    VOID           CalcRange        (FLOAT &  fMinOut,
                                     FLOAT &  fMaxOut);

  };
  

VOID  SortIndexes (BaseArray &  arrayToSortIn,
                   FloatArray &   arrayOut);



#endif // FloatARRAY_HPP


