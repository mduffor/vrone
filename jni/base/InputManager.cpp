/* -----------------------------------------------------------------
                            Input Manager

     This module implements a central interface for setting, 
     storing, retrieving, and notifying about input from the 
     mouse, touchscreen, keyboard, joysticks, or HMD.  It is
     the clearing house for input from the user to the app.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "InputManager.hpp"
#include "RVec.hpp"

//------------------------------------------------------------------------------
InputManager::InputManager  ()
  {
    
  iQueueSize = 100;
  iQueueIndex = 0;

  auButtons.SetLength (iQueueSize);
  afX.SetLength (iQueueSize);
  afY.SetLength (iQueueSize);
  afPressure.SetLength (iQueueSize);

  iLeftClicks     = 0;
  iRightClicks    = 0;
  iCenterClicks   = 0;
  iScrollUpClicks = 0;
  iScrollDnClicks = 0;
    
  };
          
//------------------------------------------------------------------------------
InputManager::~InputManager  ()
  {
  };

//------------------------------------------------------------------------------
BOOL  InputManager::GetNextKey  (INT &  iKeyOut,
                                 INT &  iKeyModOut)
  {
  if (aiKeyEntered [iKeyRead] == 0)
    {
    return (0);
    };
  
  // read from the buffer
  iKeyOut    = aiKeyEntered [iKeyRead];
  iKeyModOut = aiKeyMod     [iKeyRead];

  // clear the buffer now that we have the key
  aiKeyEntered [iKeyRead] = 0;
  aiKeyMod     [iKeyRead] = 0;

  // increment to the next read position
  iKeyRead = (iKeyRead + 1) % KEY_BUFFER_SIZE;
  return (1);
  };


//------------------------------------------------------------------------------
VOID  InputManager::StoreKey (INT  iKeyIn,
                              INT  iKeyModIn)
  {
  INT  iKeyNextWrite = (iKeyWrite + 1) % KEY_BUFFER_SIZE;
  
  // make sure the buffer isn't full
  if (iKeyNextWrite == iKeyRead)
    {
    // buffer full
    return;
    };
  
  aiKeyEntered [iKeyWrite] = iKeyIn;
  aiKeyMod     [iKeyWrite] = iKeyModIn;

  // increment to the next write position
  iKeyWrite = iKeyNextWrite;
  };

//------------------------------------------------------------------------------
VOID  InputManager::PushMousePos    (FLOAT  fXIn,
                                     FLOAT  fYIn,
                                     FLOAT  fPressureIn)
  {
  // called by the OS-specific main loop to set the current mouse position
  
  // queue up the mouse position
  ++iQueueIndex;
  if (iQueueIndex >= iQueueSize) iQueueIndex -= iQueueSize;
  auButtons  [iQueueIndex] = (bShiftDown   ? VKFLG_SHIFT   : 0) |
                             (bControlDown ? VKFLG_CONTROL : 0) |
                             (bAltDown     ? VKFLG_ALT     : 0) |
                             (bMetaDown    ? VKFLG_META    : 0);
  afX        [iQueueIndex] = fXIn;
  afY        [iQueueIndex] = fYIn;
  afPressure [iQueueIndex] = fPressureIn;  
  };
  
//------------------------------------------------------------------------------
VOID  InputManager::OnKeyDown (INT  iVKeyIn)
  {
  if (iVKeyIn <= VKEY_MAX)
    {
    bKeyDown [iVKeyIn] = 1;
    };
  
  switch (iVKeyIn)
    {
    case VKEY_CONTROL:
      bControlDown = 1;
      break;
    case VKEY_SHIFT:
      bShiftDown = 1;
      break;
    case VKEY_ALT:
      bAltDown = 1;
      break;
    default:
      StoreKey (iVKeyIn, (bShiftDown   ? VKFLG_SHIFT   : 0) |
                         (bControlDown ? VKFLG_CONTROL : 0) |
                         (bAltDown     ? VKFLG_ALT     : 0) |
                         (bMetaDown    ? VKFLG_META    : 0));
      break;
    };
  };

//------------------------------------------------------------------------------
VOID  InputManager::OnKeyUp (INT  iVKeyIn)
  {

  switch (iVKeyIn)
    {
    case VKEY_CONTROL:
      bControlDown = 1;
      break;
    case VKEY_SHIFT:
      bShiftDown = 1;
      break;
    case VKEY_ALT:
      bAltDown = 1;
      break;
      
    case VKEY_LBUTTON:
      if (IsKeyDown (iVKeyIn)) {++iLeftClicks;}
      break;
      
    case VKEY_RBUTTON:
      if (IsKeyDown (iVKeyIn)) {++iRightClicks;}
      break;
      
    case VKEY_MBUTTON:
      if (IsKeyDown (iVKeyIn)) {++iCenterClicks;}
      break;
      
    case VKEY_SCROLLUP:
      if (IsKeyDown (iVKeyIn)) {++iScrollUpClicks;}
      break;
      
    case VKEY_SCROLLDOWN:
      if (IsKeyDown (iVKeyIn)) {++iScrollDnClicks;}
      break;
      
    default:
      break;
    }
    
  if (iVKeyIn <= VKEY_MAX)
    {
    bKeyDown [iVKeyIn] = 0;
    };
    
  };
  
//------------------------------------------------------------------------------
VOID  InputManager::OnMouseMove (INT    iVKeyIn,
                                 FLOAT  fXIn,
                                 FLOAT  fYIn,
                                 FLOAT  fPressureIn)
  {
  if (iVKeyIn == VKEY_TOUCH_0)
    {
    PushMousePos (fXIn, fYIn, fPressureIn);
    }
  }
  
//------------------------------------------------------------------------------
VOID  InputManager::OnMouseDown (INT    iVKeyIn,
                                 FLOAT  fXIn,
                                 FLOAT  fYIn,
                                 FLOAT  fPressureIn)  
  {
  OnKeyDown (iVKeyIn);
  OnMouseMove (iVKeyIn, fXIn, fYIn, fPressureIn);
  };
  
//------------------------------------------------------------------------------
VOID  InputManager::OnMouseUp (INT    iVKeyIn,
                               FLOAT  fXIn,
                               FLOAT  fYIn,
                               FLOAT  fPressureIn)  
  {
  OnMouseMove (iVKeyIn, fXIn, fYIn, fPressureIn);
  OnKeyUp (iVKeyIn);
  };
  
//------------------------------------------------------------------------------
VOID  InputManager::ClearButtonClicks   (VOID)
  {
  iLeftClicks     = 0;      
  iRightClicks    = 0;     
  iCenterClicks   = 0;  
  iScrollUpClicks = 0;
  iScrollDnClicks = 0;
  };
  
//------------------------------------------------------------------------------
VOID  InputManager::GetPos  (FLOAT *  pfXOut,
                             FLOAT *  pfYOut,
                             UINT *   puButtonsOut,
                             FLOAT *  pfPressureOut)
  {
  // called by any code to get the current mouse position
  
  if (pfXOut        != NULL) *pfXOut        = afX        [iQueueIndex];
  if (pfYOut        != NULL) *pfYOut        = afY        [iQueueIndex];
  if (puButtonsOut  != NULL) *puButtonsOut  = auButtons  [iQueueIndex];
  if (pfPressureOut != NULL) *pfPressureOut = afPressure [iQueueIndex];  
  };

//------------------------------------------------------------------------------
VOID  InputManager::GetLastPos  (FLOAT *  pfXOut,
                                 FLOAT *  pfYOut,
                                 UINT *   puButtonsOut,
                                 FLOAT *  pfPressureOut)
  {
  INT  iLastIndex = iQueueIndex - 1;
  if (iLastIndex < 0) iLastIndex += iQueueSize;
  if (pfXOut        != NULL) *pfXOut        = afX        [iLastIndex];
  if (pfYOut        != NULL) *pfYOut        = afY        [iLastIndex];
  if (puButtonsOut  != NULL) *puButtonsOut  = auButtons  [iLastIndex];
  if (pfPressureOut != NULL) *pfPressureOut = afPressure [iLastIndex];  
  }; 


//------------------------------------------------------------------------------
VOID  InputManager::GetIndexPos (INT      iIndexIn,
                                 FLOAT *  pfXOut,
                                 FLOAT *  pfYOut,
                                 UINT *   puButtonsOut,
                                 FLOAT *  pfPressureOut)
  {
  // called by any code to get the indexed mouse position
  
  // bring iIndexIn into the range of (0, iQueueSize]
  while (iIndexIn < 0)           iIndexIn += iQueueSize;
  while (iIndexIn >= iQueueSize) iIndexIn += iQueueSize;
  
  if (pfXOut        != NULL) *pfXOut        = afX        [iIndexIn];
  if (pfYOut        != NULL) *pfYOut        = afY        [iIndexIn];
  if (puButtonsOut  != NULL) *puButtonsOut  = auButtons  [iIndexIn];
  if (pfPressureOut != NULL) *pfPressureOut = afPressure [iIndexIn];  
  };


//------------------------------------------------------------------------------
FLOAT  InputManager::GetSlashLength (UINT  uPixelToleranceIn)
  {
  // The goal of this routine is to look at the historic mouse data
  //   and determine if we are slashing across the screen.
  
  // We do this by stepping back through the history, and breaking the
  //   history into line segments that are just over a given tolerance
  //   factor.  We count the number of line segments that are roughly
  //   parallel to their previous segment and to the final segment (within
  //   a wider tolerance factor for the later).  We also take into account
  //   button changes that would signal the end of a slash.  When
  //   we have run out of samples or have found the end of a run, we
  //   return the sum of the length of all of the line segments.  If it is
  //   0, there is no slash.  Otherwise we can judge how large of a slash
  //   it is and act appropriately.

  FLOAT   fTotalTolerance = 0.8;
  FLOAT   fLocalTolerance = 0.8;
  
  INT     iToleranceSq = uPixelToleranceIn * uPixelToleranceIn;
  INT     iIndex;
  FLOAT   fTotalLength = 0.0f;
  FLOAT   fCurrLength;
  RVec3   vecSlashDir;
  RVec3   vecPrevDir;
  RVec3   vecCurrDir;
  INT     iIndexStart;

  FLOAT   fFIrstX;        // very tip of the slash.  Current mouse position
  FLOAT   fFirstY;
  UINT    uFirstButtons;
  
  FLOAT   fSegX;         // start of the current segment
  FLOAT   fSegY;
  UINT    uSegButtons;

  FLOAT     fCurrX;        // search point for the end of the segent.
  FLOAT     fCurrY;
  UINT    uCurrButtons;

  
  // calculate the first segment
  iIndex = iIndexStart = iQueueIndex;
  GetIndexPos (iIndexStart, &fFIrstX, &fFirstY, &uFirstButtons, NULL);
  
  --iIndex;
  iIndex = RMin (iQueueSize - 1, RMax (0, iIndex));
  
  fSegX       = fFIrstX;
  fSegY       = fFirstY;
  uSegButtons = uFirstButtons;
  
  INT   iDeltaX;
  INT   iDeltaY;
  BOOL  bEndOfSlash = FALSE;
  while (iIndex != iIndexStart)
    {
    GetIndexPos (iIndex, &fCurrX, &fCurrY, &uCurrButtons);
    
    // end search if button state changes.
    if (uCurrButtons != uSegButtons) 
      {
      bEndOfSlash = TRUE;
      break;
      };
    iDeltaX = fSegX - fCurrX;
    iDeltaY = fSegY - fCurrY;
    if ((iDeltaX * iDeltaX + iDeltaY * iDeltaY) >= iToleranceSq)
      {
      // we've found a segment
      vecCurrDir = RVec3 (FLOAT (iDeltaX), FLOAT (iDeltaY), 0);
      fCurrLength = vecCurrDir.Length ();
      vecCurrDir /= fCurrLength;
      fTotalLength += fCurrLength;

      // prepare for the next run
      fSegX       = fCurrX;
      fSegY       = fCurrY;
      uSegButtons = uCurrButtons;
      break;
      };
    --iIndex;
    iIndex = RMin (iQueueSize - 1, RMax (0, iIndex));
    };
  vecSlashDir = vecCurrDir;
  vecPrevDir  = vecCurrDir;

  if (bEndOfSlash) {return (0);};
  
  // now search out the other segments
  while (iIndex != iIndexStart)
    {
    GetIndexPos (iIndex, &fCurrX, &fCurrY, &uCurrButtons);
    
    // end search if button state changes.
    if (uCurrButtons != uSegButtons) 
      {
      bEndOfSlash = TRUE;
      break;
      };
    iDeltaX = fSegX - fCurrX;
    iDeltaY = fSegY - fCurrY;
    if ((iDeltaX * iDeltaX + iDeltaY * iDeltaY) >= iToleranceSq)
      {
      // we've found a segment
      vecCurrDir = RVec3 (FLOAT (iDeltaX), FLOAT (iDeltaY), 0);
      fCurrLength = vecCurrDir.Length ();
      vecCurrDir /= fCurrLength;
      
      // keep or reject this segment
      if (((vecPrevDir * vecCurrDir)  < fLocalTolerance) || 
          ((vecPrevDir * vecSlashDir) < fTotalTolerance))
        {
        // too bendy.  Reject.
        bEndOfSlash = TRUE;
        break;
        };
      fTotalLength += fCurrLength;

      // prepare for the next run
      fSegX       = fCurrX;
      fSegY       = fCurrY;
      uSegButtons = uCurrButtons;
      break;
      };
    --iIndex;
    iIndex = RMin (iQueueSize - 1, RMax (0, iIndex));
    };
  return (INT (fTotalLength));
  };
  
//------------------------------------------------------------------------------
VOID InputManager::GetKeyName (INT32   iVirtKeyIn,
                               RStr &  strNameOut)
  {
  strNameOut.Empty ();  
  
  if (bKeyDown [VKEY_ALT])     {strNameOut += "Alt";};
  if (bKeyDown [VKEY_CONTROL]) {strNameOut += "Ctrl";};
  if (bKeyDown [VKEY_META])    {strNameOut += "Meta";};
  if (bKeyDown [VKEY_SHIFT])   {strNameOut += "Shft";};
  
  if ((iVirtKeyIn >= 32) && (iVirtKeyIn <= 126))
    {
    strNameOut.AppendChar (iVirtKeyIn);
    }
  else
    {
    switch (iVirtKeyIn)
      {
      case VKEY_BACKSPACE:         strNameOut += "BkSpace"; break;
      case VKEY_TAB:               strNameOut += "Tab"; break;
      case VKEY_ENTER:             strNameOut += "Enter"; break;
      case VKEY_ESC:               strNameOut += "Esc"; break;
      case VKEY_SPACE:             strNameOut += "Space"; break;
      case VKEY_LBUTTON:           strNameOut += "LButton"; break;
      case VKEY_RBUTTON:           strNameOut += "RButton"; break;
      case VKEY_MBUTTON:           strNameOut += "MButton"; break;

      //case VKEY_CANCEL:            strNameOut += "Cancel"; break;
      //case VKEY_CLEAR:             strNameOut += "Clr"; break;
      case VKEY_MENU:              strNameOut += "Menu"; break;
      case VKEY_PAUSE:             strNameOut += "Pause"; break;
      case VKEY_CAPITAL:           strNameOut += "Caps"; break;
      //case VKEY_PRIOR:             strNameOut += "Prev"; break;
      //case VKEY_NEXT:              strNameOut += "Next"; break;
      case VKEY_END:               strNameOut += "End"; break;
      case VKEY_HOME:              strNameOut += "Home"; break;
      case VKEY_LEFT:              strNameOut += "Left"; break;
      case VKEY_UP:                strNameOut += "Up"; break;
      case VKEY_RIGHT:             strNameOut += "Right"; break;
      case VKEY_DOWN:              strNameOut += "Down"; break;
      case VKEY_INSERT:            strNameOut += "Ins"; break;
      case VKEY_DELETE:            strNameOut += "Del"; break;
      case VKEY_HELP:              strNameOut += "Help"; break;
      //case VKEY_SELECT:            strNameOut += "Select"; break;
      //case VKEY_PRINT:             strNameOut += "Print"; break;
      //case VKEY_EXECUTE:           strNameOut += "Exec"; break;
      //case VKEY_SNAPSHOT:          strNameOut += "Snapshot"; break;
      
      case VKEY_NUMPAD0:           strNameOut += "Num0"; break;
      case VKEY_NUMPAD1:           strNameOut += "Num1"; break;
      case VKEY_NUMPAD2:           strNameOut += "Num2"; break;
      case VKEY_NUMPAD3:           strNameOut += "Num3"; break;
      case VKEY_NUMPAD4:           strNameOut += "Num4"; break;
      case VKEY_NUMPAD5:           strNameOut += "Num5"; break;
      case VKEY_NUMPAD6:           strNameOut += "Num6"; break;
      case VKEY_NUMPAD7:           strNameOut += "Num7"; break;
      case VKEY_NUMPAD8:           strNameOut += "Num8"; break;
      case VKEY_NUMPAD9:           strNameOut += "Num9"; break;

      case VKEY_NUMPAD_SPACE:      strNameOut += "NumSpace"; break;
      case VKEY_NUMPAD_TAB:        strNameOut += "NumTab"; break;
      case VKEY_NUMPAD_ENTER:      strNameOut += "NumEnter"; break;
      case VKEY_NUMPAD_F1:         strNameOut += "NumF1"; break;
      case VKEY_NUMPAD_F2:         strNameOut += "NumF2"; break;
      case VKEY_NUMPAD_F3:         strNameOut += "NumF3"; break;
      case VKEY_NUMPAD_F4:         strNameOut += "NumF4"; break;
      case VKEY_NUMPAD_HOME:       strNameOut += "NumHome"; break;
      case VKEY_NUMPAD_LEFT:       strNameOut += "NumLeft"; break;
      case VKEY_NUMPAD_UP:         strNameOut += "NumUp"; break;
      case VKEY_NUMPAD_RIGHT:      strNameOut += "NumRight"; break;
      case VKEY_NUMPAD_DOWN:       strNameOut += "NumDown"; break;

      //case VKEY_NUMPAD_PRIOR:      strNameOut += "NumPrev"; break;
      case VKEY_NUMPAD_PAGEUP:     strNameOut += "NumPgUp"; break;
      //case VKEY_NUMPAD_NEXT:       strNameOut += "NumNext"; break;
      case VKEY_NUMPAD_PAGEDOWN:   strNameOut += "NumPgDn"; break;
      case VKEY_NUMPAD_END:        strNameOut += "NumEnd"; break;
      case VKEY_NUMPAD_BEGIN:      strNameOut += "NumBegin"; break;
      case VKEY_NUMPAD_INSERT:     strNameOut += "NumIns"; break;
      case VKEY_NUMPAD_DELETE:     strNameOut += "NumDel"; break;
      case VKEY_NUMPAD_EQUAL:      strNameOut += "NumEqual"; break;
      case VKEY_NUMPAD_MULTIPLY:   strNameOut += "NumMult"; break;
      case VKEY_NUMPAD_ADD:        strNameOut += "NumAdd"; break;
      case VKEY_NUMPAD_SEPARATOR:  strNameOut += "NumSep"; break;
      case VKEY_NUMPAD_SUBTRACT:   strNameOut += "NumSub"; break;
      case VKEY_NUMPAD_DECIMAL:    strNameOut += "NumDec"; break;
      case VKEY_NUMPAD_DIVIDE:     strNameOut += "NumDiv"; break;

      case VKEY_MULTIPLY:          strNameOut += "Mult"; break;
      case VKEY_ADD:               strNameOut += "Add"; break;
      case VKEY_SEPARATOR:         strNameOut += "Sep"; break;
      case VKEY_SUBTRACT:          strNameOut += "Sub"; break;
      case VKEY_DECIMAL:           strNameOut += "Dec"; break;
      case VKEY_DIVIDE:            strNameOut += "Div"; break;
      case VKEY_F1:                strNameOut += "F1"; break;
      case VKEY_F2:                strNameOut += "F2"; break;
      case VKEY_F3:                strNameOut += "F3"; break;
      case VKEY_F4:                strNameOut += "F4"; break;
      case VKEY_F5:                strNameOut += "F5"; break;
      case VKEY_F6:                strNameOut += "F6"; break;
      case VKEY_F7:                strNameOut += "F7"; break;
      case VKEY_F8:                strNameOut += "F8"; break;
      case VKEY_F9:                strNameOut += "F9"; break;
      case VKEY_F10:               strNameOut += "F10"; break;
      case VKEY_F11:               strNameOut += "F11"; break;
      case VKEY_F12:               strNameOut += "F12"; break;
      case VKEY_F13:               strNameOut += "F13"; break;
      case VKEY_F14:               strNameOut += "F14"; break;
      case VKEY_F15:               strNameOut += "F15"; break;
      case VKEY_NUMLOCK:           strNameOut += "NumLock"; break;
      case VKEY_SCROLL:            strNameOut += "Scroll"; break;
      case VKEY_PAGEUP:            strNameOut += "PgUp"; break;
      case VKEY_PAGEDOWN:          strNameOut += "PgDn"; break;

      case GAMEPAD_BUTTON_1:       strNameOut += "GamepadBtn1"; break;
      case GAMEPAD_BUTTON_2:       strNameOut += "GamepadBtn2"; break;
      case GAMEPAD_BUTTON_3:       strNameOut += "GamepadBtn3"; break;
      case GAMEPAD_BUTTON_4:       strNameOut += "GamepadBtn4"; break;
      case GAMEPAD_BUTTON_5:       strNameOut += "GamepadBtn5"; break;
      case GAMEPAD_BUTTON_6:       strNameOut += "GamepadBtn6"; break;
      case GAMEPAD_BUTTON_7:       strNameOut += "GamepadBtn7"; break;
      case GAMEPAD_BUTTON_8:       strNameOut += "GamepadBtn8"; break;
      case GAMEPAD_BUTTON_9:       strNameOut += "GamepadBtn9"; break;
      case GAMEPAD_BUTTON_10:      strNameOut += "GamepadBtn10"; break;
      case GAMEPAD_BUTTON_11:      strNameOut += "GamepadBtn11"; break;
      case GAMEPAD_BUTTON_12:      strNameOut += "GamepadBtn12"; break;
      case GAMEPAD_BUTTON_13:      strNameOut += "GamepadBtn13"; break;
      case GAMEPAD_BUTTON_14:      strNameOut += "GamepadBtn14"; break;
      case GAMEPAD_BUTTON_15:      strNameOut += "GamepadBtn15"; break;
      case GAMEPAD_BUTTON_16:      strNameOut += "GamepadBtn16"; break;

      case BUTTON_A:               strNameOut += "BtnA"; break;
      case BUTTON_B:               strNameOut += "BtnB"; break;
      case BUTTON_C:               strNameOut += "BtnC"; break;
      case BUTTON_X:               strNameOut += "BtnX"; break;
      case BUTTON_Y:               strNameOut += "BtnY"; break;
      case BUTTON_Z:               strNameOut += "BtnZ"; break;

      case BUTTON_L1:              strNameOut += "BtnL1"; break;
      case BUTTON_L2:              strNameOut += "BtnL2"; break;
      case BUTTON_R1:              strNameOut += "BtnR1"; break;
      case BUTTON_R2:              strNameOut += "BtnR2"; break;
      case BUTTON_THUMBL:          strNameOut += "BtnThumbL"; break;
      case BUTTON_THUMBR:          strNameOut += "BtnThumbR"; break;

      case BUTTON_MODE:            strNameOut += "Btn"; break;
      case BUTTON_SELECT:          strNameOut += "Btn"; break;
      case BUTTON_START:           strNameOut += "Btn"; break;

      case DPAD_CENTER:            strNameOut += "DPadCenter"; break;
      case DPAD_DOWN:              strNameOut += "DPadDown"; break;
      case DPAD_LEFT:              strNameOut += "DPadLeft"; break;
      case DPAD_RIGHT:             strNameOut += "DPadRight"; break;
      case DPAD_UP:                strNameOut += "DPadUp"; break;

      case VKEY_SCROLLUP:          strNameOut += "ScrollUp"; break;
      case VKEY_SCROLLDOWN:        strNameOut += "ScrollDn"; break;
      
      default:
        // unable to map key
        return;
      };
    };
  };
  
  
#ifdef SAMPLE
  
//------------------------------------------------------------------------------
VOID  RHotKeys::MouseToCameraVector (RNode *  pnodeCameraIn,
                                     FLOAT    fMouseXIn,   // viewport parametric position
                                     FLOAT    fMouseYIn)   // viewport parametric position
  {
  RNode *  pnodeHotkeys = GetHotkeyList ();

  if (pnodeCameraIn)
    {
    RAttr *  pattrCamera = pnodeHotkeys->GetAttr ("camera");
    RNodePtr  ptrCamera;
    pnodeCameraIn->SetSmartPtr (ptrCamera);

    RAttr *  pattrCursorVec     = pnodeHotkeys->GetAttr ("cursorVec");
    if (pattrCursorVec == NULL) return;
    if (pattrCamera != NULL) {pattrCamera->SetNodePtr (ptrCamera);};

    
    FLOAT    fFocalLength = pnodeCameraIn->GetAttrFloat ("fl");
    //FLOAT    fApertureX   = pnodeCameraIn->GetAttrFloat ("apx");
    //FLOAT    fApertureY   = pnodeCameraIn->GetAttrFloat ("apy");
    
    //FLOAT    fFovY = atanf ((fApertureY * 0.5f) / fFocalLength);
    //FLOAT    fApertureAspect = fApertureX / fApertureY;
    
    // Mouse position should be passed in the range of 0 to 1, 
   
    // calculate the point on the projection plate where the mouse was clicked.  
    //  This will give us a vector in camera space when we subtract the camera 
    //  position.
    RVec4  vecPointOnAperture (fMouseXIn, 
                               fMouseYIn, 
                               fFocalLength * -1.0f, 
                               1.0f);

    pattrCursorVec->SetVector (vecPointOnAperture);

    //start here

    // take x, y, camera FOV, and calculate the vector in camera space through that point.
    //  store/pass this vector
    
    // in routine:
    //  calc where old camera vector crossed plane at center of interest
    //  calc where new camera vector crosses plane at center of interest
    //  The vector between these two points is the amount to move.
    //  apply tranformations.
    };

  };
    
#endif // SAMPLE
