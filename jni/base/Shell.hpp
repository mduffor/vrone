
// SHELL.HPP
// Copyright 1995, 1996, 2000, Michael T. Duffy.  All Rights Reserved

#ifndef SHELL_HPP
#define SHELL_HPP

#include "Types.hpp"
#include "Timer.hpp"
#include "InputManager.hpp"

#ifndef ANDROID_NDK
  #include <GL/glew.h>
  #include <GL/glu.h>
#endif // ANDROID_NDK

extern TimerHandler    thTimers;

// abstract singleton representing the game app.  From this class, the OS-specific
//  app is derived.  
  
//------------------------------------------------------------------------------
class Shell
  {
  protected:

    static UINT32       uLastMsTime;
    static UINT32       uCurrMsTime;
    
    static UINT32       uMsTimeDelta;
    
    static UINT         uCanvasWidth;
    static UINT         uCanvasHeight;
    static UINT         uCanvasBitDepth;
    static UINT         uShellFlags;
    static UINT         uTargetFps;
    
    static RStr         strName;
    static RStr         strTitle;
    
    static CHAR         szCommandLine [256];

    static BOOL         g_bCloseApp;
    static BOOL         bActive;
    
    static FLOAT        fGlobalTime;

    static InputManager inputManager;
    
    static Shell *      pshellSingleton;
    
    static BOOL         bExiting;

    
  public:
    // performance tracking
    static INT          aiFrameCounts [5];
    static INT          iCurrFrameCount;    
    
  public:
                     Shell ();

    virtual          ~Shell ();

    VOID             Init ();
    
    VOID             Uninit ();
    
    static VOID      InitOpenGL (VOID);
    
    virtual VOID     InitOpenGLInternal (VOID);
    
    static INT       Loop (VOID);

    static UINT      GetCanvasWidth    (VOID)               {return uCanvasWidth;};

    static UINT      GetCanvasHeight   (VOID)               {return uCanvasHeight;};

    static UINT      GetCanvasBitDepth (VOID)               {return uCanvasBitDepth;};

    static UINT      GetShellFlags     (VOID)               {return uShellFlags;};

    static UINT      GetTargetFps      (VOID)               {return uTargetFps;};

    const char *     GetName           (VOID)               {return strName.AsChar ();};

    const char *     GetTitle          (VOID)               {return strTitle.AsChar ();};

    static VOID      SetCanvasWidth    (UINT  uWidthIn)     {uCanvasWidth = uWidthIn;};

    static VOID      SetCanvasHeight   (UINT  uHeightIn)    {uCanvasHeight = uHeightIn;};

    static VOID      SetCanvasBitDepth (UINT  uBitDepthIn)  {uCanvasBitDepth = uBitDepthIn;};

    static VOID      SetShellFlags     (UINT  uFlagsIn)     {uShellFlags = uFlagsIn;};

    static VOID      SetTargetFps      (UINT  uFpsIn)       {uTargetFps = uFpsIn;};

    static VOID      SetName           (const char *  szNameIn)   {strName = szNameIn;};

    static VOID      SetTitle          (const char *  szTitleIn)  {strTitle = szTitleIn;};
    
    VOID             ParseCommandLine  (PCHAR  szCmdLineIn);

    static VOID      SetMilliseconds   (UINT32  uMsIn)      {uLastMsTime = uCurrMsTime; uCurrMsTime = uMsIn;};
    
    static UINT32    GetMilliseconds   (VOID)               {return (uCurrMsTime);};

    virtual VOID     Update            (UINT32  uMsDeltaIn) = 0;
    
    static VOID      StaticUpdate      (UINT32  uMsDeltaIn);
    
    // update internal game state
    virtual EStatus  GameLoop          (UINT32  uMillisecondDeltaIn) = 0;
    
    static EStatus   StaticGameLoop    (UINT32  uMillisecondDeltaIn);

    static FLOAT     GetGlobalTime     (VOID)               {return fGlobalTime;};
    
    static VOID      SetGlobalTime     (FLOAT  fTimeIn)     {fGlobalTime = fTimeIn;};
    
    static VOID      SetTimeDelta      (UINT32  uMsDeltaIn) {uMsTimeDelta = uMsDeltaIn;};
    
    static UINT32    GetTimeDelta      (VOID)               {return uMsTimeDelta;};
    
    // draw current frame to the screen
    virtual VOID     GameDraw          (VOID) = 0;

    VOID             ReSizeScene       (INT  iWidth,
                                        INT  iHeight);
    
    // Other shell related functions
    static PCHAR     GetCommandLine    (VOID)               {return (szCommandLine);};

    static VOID      CloseApplication  (VOID);
    
    static BOOL      AppIsClosing      (VOID)               {return (g_bCloseApp);};
    
    static VOID      FatalError        (const char *  szMsgIn);
    
    static VOID      OnKeyDown         (INT    iVKeyIn)       {inputManager.OnKeyDown (iVKeyIn);};
   
    static VOID      OnKeyUp           (INT    iVKeyIn)       {inputManager.OnKeyUp (iVKeyIn);};
   
    static VOID      OnPointerMove     (INT    iVKeyIn,
                                        FLOAT  fXIn,
                                        FLOAT  fYIn,
                                        FLOAT  fPressureIn)   {inputManager.OnMouseMove (iVKeyIn, fXIn, fYIn, fPressureIn);};
      
    static VOID      OnPointerDown     (INT    iVKeyIn,
                                        FLOAT  fXIn,
                                        FLOAT  fYIn,
                                        FLOAT  fPressureIn)   {inputManager.OnMouseMove (iVKeyIn, fXIn, fYIn, fPressureIn);};

    static VOID      OnPointerUp       (INT    iVKeyIn,
                                        FLOAT  fXIn,
                                        FLOAT  fYIn,
                                        FLOAT  fPressureIn)   {inputManager.OnMouseMove (iVKeyIn, fXIn, fYIn, fPressureIn);};    
    
    static BOOL      GetNextKey        (INT &  iVKeyOut,
                                        INT &  iVKeyModOut)   {return (inputManager.GetNextKey (iVKeyOut, iVKeyModOut));};
    
    static BOOL      IsKeyDown         (INT    iVKeyIn)       {return (inputManager.IsKeyDown (iVKeyIn));};
                                        
  };


//------------------------------------------------------------------------------
class GameLoopTimer  : public Timer
  {
  private:
  
  public: 
                  GameLoopTimer  ();
                  GameLoopTimer  (UINT32    uMillisecondsToWaitIn,
                                  BOOL      bOneTimeIn = FALSE);
    
    virtual       ~GameLoopTimer ();
    
    virtual BOOL  Post  (UINT32  uMillisecondsSinceLast);    ///> Call the Post routine.  Return True if this timer should be deleted, false otherwise.
  };


//------------------------------------------------------------------------------
class FrameCountTimer  : public Timer
  {
  private:
  
  public: 
                  FrameCountTimer  ();
                  FrameCountTimer  (UINT32  uMillisecondsToWaitIn,
                                    BOOL    bOneTimeIn = FALSE);
    
    virtual       ~FrameCountTimer ();
    
    virtual BOOL  Post  (UINT32  uMillisecondsSinceLast);    ///> Call the Post routine.  Return True if this timer should be deleted, false otherwise.
  };

#endif // SHELL_HPP
