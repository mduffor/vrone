/* -----------------------------------------------------------------
                             Shader

     This class handles GPU shaders.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "GLUtil.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Material.hpp"
#include "mesh/DisplayMesh.hpp"
#include "Shader.hpp"

static Shader *  pShaderList = NULL;

//-----------------------------------------------------------------------------
//  Shader
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
Shader::Shader  ()
  {
  Init ();
  };
  
//-----------------------------------------------------------------------------
Shader::~Shader ()
  {
  FreeProgram ();
  Unregister ();
  };
  
//-----------------------------------------------------------------------------
Shader::Shader (const char *  szNameIn,
                const char *  szVertexShaderIn,
                const char *  szFragmentShaderIn)
  {
  Init ();
  
  SetName (szNameIn);
  SetVertexShader (szVertexShaderIn);
  SetFragmentShader (szFragmentShaderIn);
  };

//-----------------------------------------------------------------------------
VOID Shader::Init (VOID)
  {
  uVertexHandle   = 0;
  uFragmentHandle = 0;
  uProgramHandle  = 0;  
  pNext           = NULL;
  Register ();
  };
  
//-----------------------------------------------------------------------------
VOID Shader::Register (VOID)
  {
  /// Store in the master list of shaders.
  this->pNext = pShaderList;
  pShaderList = this;
  };

//-----------------------------------------------------------------------------
VOID Shader::Unregister (VOID)
  {
  /// Remove from the master list of shaders.
  Shader *  pPrev = NULL;
  Shader *  pCurr = pShaderList;
  
  while (pCurr != NULL)
    {
    if (pCurr == this)
      {
      if (pPrev == NULL)
        {
        pShaderList = pCurr->pNext;
        }
      else
        {
        pPrev->pNext = pCurr->pNext;
        };
      pNext = NULL;
      return;
      }
    pPrev = pCurr;
    pCurr = pCurr->pNext;
    }
  };

//-----------------------------------------------------------------------------
VOID  Shader::SetVertexShader  (const char *  szSourceIn)
  {
  strVertexShader = szSourceIn;
  FreeProgram ();
  };
    
//-----------------------------------------------------------------------------
const char * Shader::GetVertexShader  (VOID)
  {
  return (strVertexShader.AsChar ());
  };
    
//-----------------------------------------------------------------------------
VOID  Shader::SetFragmentShader (const char *  szSourceIn)
  {
  strFragmentShader = szSourceIn;
  FreeProgram ();
  };
    
//-----------------------------------------------------------------------------
const char *  Shader::GetFragmentShader (VOID)
  {
  return (strFragmentShader.AsChar ());
  };

//-----------------------------------------------------------------------------
INT  Shader::GetProgram  (VOID)
  {
  if (uProgramHandle == 0)
    {
    CompileProgram ();
    }
  return (uProgramHandle);
  };

//-----------------------------------------------------------------------------
VOID  Shader::CompileProgram  (VOID)
  {
  FreeProgram ();
  
  if (! strVertexShader.IsEmpty ())
    {
    uVertexHandle = GLCreateVertexShader (GetVertexShader ());
    };
  if (! strFragmentShader.IsEmpty ())
    {
    uFragmentHandle = GLCreateFragmentShader (GetFragmentShader ());
    };
  if ((uVertexHandle != 0) && (uFragmentHandle != 0))
    {
    uProgramHandle = GLCreateProgram (uVertexHandle, uFragmentHandle);
    };
    
  // These hard-coded queries and assignments may need to be driven by meta data
  iPositionAttrib = glGetAttribLocation (uProgramHandle, "a_position");
  GLCheckError(__FILE__, "glGetAttribLocation a_position");
  iNormalAttrib = glGetAttribLocation (uProgramHandle, "a_normal");
  GLCheckError(__FILE__, "glGetAttribLocation a_normal");
  iVertColorAttrib = glGetAttribLocation (uProgramHandle, "a_vertColor");
  GLCheckError(__FILE__, "glGetAttribLocation a_vertColor");
  iTexCoordAttrib = glGetAttribLocation (uProgramHandle, "a_texCoord");
  GLCheckError(__FILE__, "glGetAttribLocation a_texCoord");    
  };
  
//-----------------------------------------------------------------------------
VOID  Shader::FreeProgram (VOID)
  {
  if (uProgramHandle != 0)
    {
    GLDeleteProgram (&uProgramHandle);
    }
  if (uVertexHandle != 0)
    {
    GLDeleteShader (&uVertexHandle);
    }
  if (uFragmentHandle != 0)
    {
    GLDeleteShader (&uFragmentHandle);
    }
  };

//-----------------------------------------------------------------------------
Shader * Shader::FindByName (const char *  szNameIn)
  {
  /// Find in the master list of shaders.
  Shader *  pCurr = pShaderList;
  
  while (pCurr != NULL)
    {
    if (pCurr->strName == szNameIn)
      {
      return (pCurr);
      }
    pCurr = pCurr->pNext;
    };
  return (NULL);
  };

  
//-----------------------------------------------------------------------------
VOID  Shader::Enable (VOID)
  {
  glUseProgram (GetProgram ());
  GLCheckError(__FILE__, "glUseProgram");
  };

//-----------------------------------------------------------------------------
VOID  Shader::OnContextLost (VOID)
  {
  Shader *  pCurr = pShaderList;
  
  while (pCurr != NULL)
    {
    pCurr->uVertexHandle   = 0;
    pCurr->uFragmentHandle = 0;
    pCurr->uProgramHandle  = 0;      
    pCurr->CompileProgram ();
    
    pCurr = pCurr->pNext;
    };
  };
  
  
  