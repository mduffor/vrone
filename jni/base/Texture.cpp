/* -----------------------------------------------------------------
                             Texture

     This class handles textures for display via textures.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "GLUtil.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Material.hpp"
#include "mesh/DisplayMesh.hpp"
#include "Texture.hpp"

static Texture *  pTextureList = NULL;

//-----------------------------------------------------------------------------
//  Texture
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
Texture::Texture  ()
  {
  Init ();
  };
  
//-----------------------------------------------------------------------------
Texture::~Texture ()
  {
  FreeTexture ();
  Unregister ();
  };
  
//-----------------------------------------------------------------------------
Texture::Texture (const char *  szNameIn,
                  const char *  szFilenameIn)
  {
  Init ();
  
  if (szNameIn != NULL)
    {
    SetName (szNameIn);
    }
  else
    {
    SetName (FilePath::GetFilenameNoExtFromPath (szFilenameIn));
    }
  SetFilename (szFilenameIn);
  };

//-----------------------------------------------------------------------------
VOID Texture::Init (VOID)
  {
  strName.Empty();
  aiImage.Clear();
  
  uTextureHandle = 0;
  bMipMapped     = FALSE;
  pNext          = NULL;
  Register ();
  };
  
//-----------------------------------------------------------------------------
VOID Texture::Register (VOID)
  {
  /// Store in the master list of textures.
  this->pNext = pTextureList;
  pTextureList = this;
  };

//-----------------------------------------------------------------------------
VOID Texture::Unregister (VOID)
  {
  /// Remove from the master list of textures.
  Texture *  pPrev = NULL;
  Texture *  pCurr = pTextureList;
  
  while (pCurr != NULL)
    {
    if (pCurr == this)
      {
      if (pPrev == NULL)
        {
        pTextureList = pCurr->pNext;
        }
      else
        {
        pPrev->pNext = pCurr->pNext;
        };
      pNext = NULL;
      return;
      }
    pPrev = pCurr;
    pCurr = pCurr->pNext;
    }
  };

//-----------------------------------------------------------------------------
VOID  Texture::SetFilename  (const char *  szFilenameIn)
  {
  FreeTexture ();
  EStatus  status = bitmapLoader.LoadIntoBuffer (szFilenameIn,
                                                 &aiImage,
                                                 iWidth,
                                                 iHeight,
                                                 iDepth,
                                                 iNumChannels,
                                                 TRUE);
  CompileTexture ();                                               
  };
    
//-----------------------------------------------------------------------------
VOID  Texture::CompileTexture  (VOID)
  {
  // textures are currently always compiled as RGBA, 4-channel-1-byte-per-channel
  //  images.  
  
  FreeTexture ();

  GLCreateTexture (&uTextureHandle);
  
  // bind the texture so that you can set it.
  glBindTexture (GL_TEXTURE_2D, uTextureHandle);
  GLCheckError(__FILE__, __LINE__, "glBindTexture");

  //  call glTexImage2D with your desired parameters and GL_TEXTURE_2D
  if (!bMipMapped)
    {
    glTexImage2D (GL_TEXTURE_2D,
                  0,              // level
                  GL_RGBA,
                  GLsizei (iWidth),
                  GLsizei (iHeight),
                  0,                   // border
                  GL_RGBA,
                  GL_UNSIGNED_BYTE,
                  aiImage.GetRawBuffer ());
    GLCheckError(__FILE__, __LINE__, "glTexImage2D");
    }
  else
    {
    // build our texture mipmaps
    INT  iMipLevel = 0;
    INT  iMipWidth = iWidth;
    INT  iMipHeight = iHeight;

    IntArray   iaPixels (aiImage);

    while ((iMipWidth >= 1) && (iMipHeight >= 1))
      {
      glTexImage2D (GL_TEXTURE_2D,
                    iMipLevel,              // level
                    GL_RGBA,
                    GLsizei (iMipWidth),
                    GLsizei (iMipHeight),
                    0,                   // border
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    iaPixels.GetRawBuffer ());
      GLCheckError(__FILE__, __LINE__, "glTexImage2D");
      if ((iMipWidth == 1) || (iMipHeight == 1))
        {
        break;
        };

      INT  iParentStride = iMipWidth; // 4 elements, RGBA.  two rows at a time

      ++iMipLevel;
      iMipWidth  /= 2;
      iMipHeight /= 2;

      INT  iChildStride = iMipWidth; // 4 elements, RGBA

      // scale the bitmap.  Assumes an 8U texture.
      INT  iChildRow = 0;
      INT  iParentRow = 0;
      
      INT  iPixA_1;
      INT  iPixA_2;
      INT  iPixB_1;
      INT  iPixB_2;
      INT  iPixC_1;
      INT  iPixC_2;
      INT  iPixD_1;
      INT  iPixD_2;
      
      for (INT  iY = 0; iY <= iMipHeight; ++iY)
        {
        for (INT  iX = 0; iX <= iMipWidth; ++iX)
          {
          // simple box filter
          
          iPixA_1 = iaPixels [iParentRow + (iX * 2)];
          iPixA_2 = iPixA_1 >> 8;
          iPixB_1 = iaPixels [iParentRow + (iX * 2 + 1)];
          iPixB_2 = iPixB_1 >> 8;
          iPixC_1 = iaPixels [iParentRow + iParentStride + (iX * 2)];
          iPixC_2 = iPixC_1 >> 8;
          iPixD_1 = iaPixels [iParentRow + iParentStride + (iX * 2 + 1)];
          iPixD_2 = iPixD_1 >> 8;

          iaPixels [iX * iChildRow] = ((((iPixA_1 & 0x00ff00ff) + 
                                         (iPixB_1 & 0x00ff00ff) + 
                                         (iPixC_1 & 0x00ff00ff) + 
                                         (iPixD_1 & 0x00ff00ff)) >> 2) & 0x00ff00ff) + 
                                        
                                      (((((iPixA_2 & 0x00ff00ff) + 
                                          (iPixB_2 & 0x00ff00ff) + 
                                          (iPixC_2 & 0x00ff00ff) + 
                                          (iPixD_2 & 0x00ff00ff)) >> 2) & 0x00ff00ff) << 8);
          };
        iChildRow += iChildStride;
        iParentRow += iParentStride * 2;
        };
      };
    };
  };
  
//-----------------------------------------------------------------------------
VOID  Texture::FreeTexture (VOID)
  {

  if (uTextureHandle != 0)
    {
    GLDeleteTexture (&uTextureHandle);
    }
  };

//-----------------------------------------------------------------------------
Texture * Texture::FindByName (const char *  szNameIn)
  {
  /// Find in the master list of textures.
  Texture *  pCurr = pTextureList;
  
  while (pCurr != NULL)
    {
    if (pCurr->strName == szNameIn)
      {
      return (pCurr);
      }
    pCurr = pCurr->pNext;
    };
  return (NULL);
  };

  
//-----------------------------------------------------------------------------
VOID  Texture::Enable (VOID)
  {
  if (uTextureHandle == 0)
    {
    return;
    }

  // TODO: setting everything to clamp now.  Customize this per-bitmap in the future.
  glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // GL_REPEAT
  GLCheckError(__FILE__, "glTexParameterf");
  glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // GL_REPEAT
  GLCheckError(__FILE__, "glTexParameterf");

  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // GL_LINEAR GL_NEAREST
  
  // TODO: Make sure you don't have to set these:
  //glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  //glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
  
  
  // Note:  This is currently setting up for single texture shaders.  For
  //  multi-texture we need to pass the texture handle in as the shader param
  
  // bind our texture
  glActiveTexture(GL_TEXTURE0);
  glBindTexture (GL_TEXTURE_2D, uTextureHandle);
  GLCheckError(__FILE__, "glBindTexture");
  };

//-----------------------------------------------------------------------------
VOID  Texture::OnContextLost (VOID)
  {
  Texture *  pCurr = pTextureList;
  
  while (pCurr != NULL)
    {
    pCurr->uTextureHandle   = 0;
    pCurr->CompileTexture ();
    
    pCurr = pCurr->pNext;
    };
  };
  
  
  