/* -----------------------------------------------------------------
                             Texture

     This class handles textures for display via shaders.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "RStr.hpp"
#include "IntArray.hpp"
#include "Transform.hpp"
#include "mesh/DisplayMesh.hpp"
#include "image/BitmapLoader.hpp"


/**

  */

//-----------------------------------------------------------------------------
class Texture
  {
  private:
    RStr          strName;
    IntArray      aiImage;
    UINT          uTextureHandle;
    
    INT           iWidth;
    INT           iHeight;
    INT           iDepth;
    INT           iNumChannels;
    
    BOOL          bMipMapped;
    
    BitmapLoader  bitmapLoader;

    Texture *     pNext;
  
  public:
  
                      Texture             ();
                
                      ~Texture            ();
                 
                      Texture             (const char *  szNameIn,
                                           const char *  szFilenameIn);

    VOID              Init                (VOID);                                          
                                          
    VOID              Register            (VOID);

    VOID              Unregister          (VOID);
                                        
    const char *      GetName             (VOID)                {return (strName.AsChar());};
    
    VOID              SetName             (const char *  szIn)  {strName.Set (szIn);};
    
    VOID              SetFilename         (const char *  szFilenameIn);
    
    const char *      GetFilename         (VOID);

    static Texture *  FindByName          (const char *  szNameIn);
    
    VOID              Enable              (VOID);
    
    static VOID       OnContextLost       (VOID);

  private: 
  
    VOID              CompileTexture      (VOID);
    
    VOID              FreeTexture         (VOID);
    
    
    
  };
  

  
#endif // TEXTURE_HPP