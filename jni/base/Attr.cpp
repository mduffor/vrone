/* -----------------------------------------------------------------
                             Attr

     This module implements named attributes
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);

#include "Attr.hpp"

//-----------------------------------------------------------------------------
//  Attr
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
Attr::Attr (const char *  szNameIn)
  {
  strName     = szNameIn;
  szType      = NULL; 
  bLocked     = FALSE;
  bPublished  = TRUE;
  bDirty      = TRUE;
  iVersion    = 0;
  iArrayIndex = 0;
  };
  
//-----------------------------------------------------------------------------
Attr::~Attr ()
  {
  }; 
    
//-----------------------------------------------------------------------------
VOID  Attr::CopyCommonVars (const Attr &  attrIn)
  {
  strName     = attrIn.strName;
  szType      = attrIn.szType;
  bLocked     = attrIn.bLocked;
  bPublished  = attrIn.bPublished;
  bDirty      = attrIn.bDirty;
  iVersion    = attrIn.iVersion;
  iArrayIndex = attrIn.iArrayIndex;
  }; 
    
//-----------------------------------------------------------------------------
EStatus  Attr::Load  (RStrParser &  parserIn)
  {
  // TODO
  // make this pure virtual
  return (EStatus::kFailure);
  }; 
    
    
//-----------------------------------------------------------------------------
EStatus  Attr::Save         (RStrParser &  parserIn,
                             RStr          strLineIndentIn) const
  {
  // TODO
  // make this pure virtual
  return (EStatus::kFailure);
  }; 
    


//-----------------------------------------------------------------------------
VOID  Attr::DebugPrint   (const char *  szIndentIn) const
  {
  RStr  strValue;
  GetAsString (&strValue);

  DBG_INFO ("%sAttr (%s): %s [%s]", szIndentIn, Type (), Name (), strValue.AsChar ());
  }; 
    
    
    
      
  