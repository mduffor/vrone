/* -----------------------------------------------------------------
                             Attr

     This module implements named attributes

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef ATTR_HPP
#define ATTR_HPP

#include "Types.hpp"
#include "RStr.hpp"
#include "RStrParser.hpp"

/**

  */


/*
 * TODO:
    //InputConnection
    //OutputConnections
    //Animation Curve (handled through connection?)
    
    ?Comparison
    ?Binary operators  
    Some way to do composite nodes (vector with values mapped to .x, .y, .z, etc)
    
    //GetFullPath // can it get the full path without knowing its parent?
    
    //VOID *  pComponent;   /// Component that contains this attr
    
    
    Connect/Disconnect/DisconnectAll/DisconnectOutputs
    Update from animation
    Write value to animation curve
    ?Functions to access and modify animation curve

 */
 
class Attr;

//-----------------------------------------------------------------------------
class Attr
  {
  protected:
    
    RStr          strName;     /// string by which this attr is identified
    const char *  szType;      /// type of data contained in this attr
    
    BOOL          bLocked;      /// able to be edited
    BOOL          bPublished;   /// visible to the UI
    BOOL          bDirty;       /// True if the value has been 
    INT           iVersion;     /// Version of the data, incremented each time it is changed.  

    INT           iArrayIndex;  /// If the attr represents one element in an array, this is its index into the array.

  protected:

    virtual VOID          CopyCommonVars (const Attr &  attrIn);
    
  public:
                          Attr         (const char *  szNameIn);
    virtual               ~Attr        ();
    
            const char *  Type         (VOID) const   {return szType;};
            const char *  Name         (VOID) const   {return strName.AsChar ();};
    
    virtual Attr *        Instantiate  (const char *  szName) = 0;
    
    virtual VOID          GetAsString       (RStr *  pstrValueOut) const = 0;
    
    virtual VOID          SetByString       (const char *  szValueIn)   {++iVersion;};
    
    virtual VOID          SetArrayByString  (INT           iIndexIn,
                                             const char *  szValueIn)   {++iVersion;};
                                             
    virtual VOID          Clear             (VOID) = 0;

    virtual EStatus       Load         (RStrParser &  parserIn);
    
    virtual EStatus       Save         (RStrParser &  parserIn,
                                        RStr          strLineIndentIn) const;

    virtual VOID          DebugPrint   (const char *  szIndentIn) const;
  };
  
#endif // ATTR_HPP