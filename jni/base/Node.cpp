/* -----------------------------------------------------------------
                             Node

     This module implements scene graph nodes for holding components
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "Node.hpp"
#include "Component.hpp"

static TList<Component *>  listComponentTemplates;
static BOOL                bComponentTemplatesInitialized = FALSE;

//-----------------------------------------------------------------------------
//  Node
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
VOID InitializeComponentTemplates (VOID)
  {
  //listComponentTemplates.PushBack (static_cast<Component*>(new AttrFloat ("float")));
  bComponentTemplatesInitialized = TRUE;  
  };

//-----------------------------------------------------------------------------
VOID  Node::AddComponentTemplate (Component *  componentIn)
  {
  ASSERT (componentIn != NULL);
  listComponentTemplates.PushBack (componentIn);
  };
  
//-----------------------------------------------------------------------------
Node::Node ()
  {
  if (!bComponentTemplatesInitialized)
    {
    InitializeComponentTemplates ();
    };
  
  bActive  = TRUE;
  bVisible = TRUE;
  bAwake   = TRUE;
  bStarted = TRUE;
  
  iResourceID = -1;
  pnodeParent = NULL;
  listComponents.MakeListSentinel ();
  };
  
//-----------------------------------------------------------------------------
Node::~Node ()
  {
  Component *  pcmpNext;
  for (Component*  pcmpCurr = FirstComponent ();
       pcmpCurr->IsValid ();
       )
    {
    pcmpNext = pcmpCurr->Next ();
    delete pcmpCurr;
    pcmpCurr = pcmpNext;
    };
  listComponents.MakeListSentinel ();
  
  
  // The following loop is a little obtuse.  We increment the iterator 
  //  just ahead of the child node deletion, and we allow the
  //  ParentTo(NULL) call in the child's destructor to actually
  //  remove the entries from the listChildren container behind
  //  our iterator's position.
  for (TListItr<Node*>  itrNode = listChildren.First ();
       itrNode.IsValid ();
       ++itrNode)
    {
    Node *  pnodeToDelete = (*itrNode);
    ++itrNode;
    pnodeToDelete->ParentTo (NULL);
    delete pnodeToDelete;
    }
  ParentTo (NULL);
  
  };

//-----------------------------------------------------------------------------
Node *  Node::FindChild  (const char *  szNameIn)
  {
  for (TListItr<Node *> itrCurr = listChildren.First ();
       itrCurr.IsValid ();
       ++itrCurr)
    {
    if (streq ((*itrCurr)->Name (), szNameIn))
      {
      return (*itrCurr);
      };
    };
  // unable to find named child.  
  return (NULL);
  };
    
//-----------------------------------------------------------------------------
Node *  Node::RootParent  (VOID)
  {
  
  Node *  pnodeCurr = this;
  while (pnodeCurr->pnodeParent != NULL)
    {
    pnodeCurr = pnodeCurr->pnodeParent;
    };
  return (pnodeCurr);
  };
    
//-----------------------------------------------------------------------------
VOID  Node::CalcFullPath  (RStr &   strPathOut) const
  {
  INT  iPathLength = 0;

  this->CalcFullPathInternal (strPathOut, iPathLength, FALSE);
  };

//-----------------------------------------------------------------------------
VOID  Node::CalcResourcePath  (RStr &   strPathOut) const
  {
  INT  iPathLength = 0;

  this->CalcFullPathInternal (strPathOut, iPathLength, TRUE);
  };
  
//-----------------------------------------------------------------------------
VOID  Node::CalcFullPathInternal (RStr &   strPathOut, 
                                  INT      iLength,
                                  BOOL     bMatchResourceIDs) const
  {
  
  iLength += strName.GetLength () + 1;
  
  if ((pnodeParent == NULL) || 
      (bMatchResourceIDs && (iResourceID != pnodeParent->iResourceID)))
    {
    // we are the root node
    strPathOut.Grow (iLength);
    strPathOut.Empty ();
    }
  else
    {
    pnodeParent->CalcFullPathInternal (strPathOut, iLength, bMatchResourceIDs);
    }
  if (!strName.IsEmpty ())
    {
    strPathOut.AppendChar ('|');
    strPathOut.AppendString (strName);
    };
  };

//-----------------------------------------------------------------------------
Node *  Node::FindByPath  (const char *  pszPathIn)
  {
  INT  iPathIndex = 0;
  
  // This routine assumes that the path starts with a child's name
  ASSERT (pszPathIn[iPathIndex] != '|');
  Node *  pnodeCurr = this;
  TListItr<Node *> itrCurr;
  
  pnodeCurr = this;
  
  while (pszPathIn[iPathIndex] != '\0')
    {
    INT  iNameLength = 0;
    INT  iNameStart = iPathIndex;
    while ((pszPathIn[iPathIndex] != '\0') && 
          (pszPathIn[iPathIndex] != '|'))
      {
      ++iPathIndex;
      ++iNameLength;
      }
    // we have the name from the path.  Now find the child that matches that name.
    for (itrCurr = pnodeCurr->listChildren.First ();
        itrCurr.IsValid ();
        ++itrCurr)
      {
      if (strncmp ((*itrCurr)->Name (), &pszPathIn[iNameStart], iNameLength) == 0)
        {
        // match at this level
        pnodeCurr = (*itrCurr);
        if (pszPathIn [iPathIndex] == '\0')
          {
          // last entry in the path
          return (pnodeCurr);
          }
        // keep searching
        ++iPathIndex;
        break;
        };
      };
    if (!itrCurr.IsValid ())
      {
      // reached the end of the loop without finding the named child.  Path is invalid.
      break;
      };
    };  
  return (NULL);
  };
    
//-----------------------------------------------------------------------------
VOID  Node::ParentTo  (Node *  pnodeNewParentIn)
  {
  // unparent
  if (pnodeParent != NULL)
    {
    pnodeParent->listChildren.Delete (this);
    }
  pnodeParent = NULL;
    
  // reparent
  if (pnodeNewParentIn != NULL)
    {
    pnodeNewParentIn->listChildren.PushBack (this);
    pnodeParent = pnodeNewParentIn;
    };
  };

//-----------------------------------------------------------------------------
Component *  Node::AddComponent  (const char *  szTypeIn)
  {
  for (TListItr<Component*> itrCurr = listComponentTemplates.First ();
       itrCurr.IsValid ();
       ++itrCurr)
    {
    if (streq ((*itrCurr)->Type (), szTypeIn))
      {
      Component *  pcmpNew = (*itrCurr)->Instantiate ();
      listComponents.InsertBefore (pcmpNew);
      pcmpNew->SetParentNode ((PVOID) this);
      return (pcmpNew);
      }
    }
  // unable to find component type in template list.
  DBG_ERROR ("Unable to find component type \"%s\" in component template list.", szTypeIn);
  return (NULL);
  };

//-----------------------------------------------------------------------------
Component *  Node::FindComponent (const char *  szTypeIn,
                                  Component *   pSearchStartIn)
  {
  Component *  pSearch = pSearchStartIn;
  
  if (pSearch == NULL)
    {
    pSearch = &listComponents;
    }
  pSearch = pSearch->Next ();  
  for (; pSearch->IsValid (); pSearch = pSearch->Next ())
    {
    if (streq (szTypeIn, pSearch->Type ()))
      {
      // found the first component of this type, following pSearchStartIn
      return (pSearch);
      }
    }
  return (NULL);
  };
  
//-----------------------------------------------------------------------------
EStatus  Node::Load  (RStrParser &  parserIn)
  {
  // TODO
  return (EStatus::kFailure);
  };
    
//-----------------------------------------------------------------------------
EStatus  Node::Save  (RStrParser &  parserIn,
                      RStr          strLineIndentIn) const
  {
  // TODO
  return (EStatus::kFailure);
  };

//-----------------------------------------------------------------------------
VOID  Node::Update (VOID)
  {
  Component *  pcmpCurr;
  
  if (!IsStarted ())
    {
    SetStarted (TRUE);
    for (pcmpCurr = FirstComponent (); pcmpCurr->IsValid (); pcmpCurr = pcmpCurr->Next ())
      {
      pcmpCurr->OnStart ();
      };   
    }
  
  for (pcmpCurr = FirstComponent (); pcmpCurr->IsValid (); pcmpCurr = pcmpCurr->Next ())
    {
    pcmpCurr->OnUpdate ();
    };   
  };
  
//-----------------------------------------------------------------------------
VOID  Node::Awake (VOID)
  {
  if (!IsAwake ())
    {
    SetAwake (TRUE); 
    for (Component *  pcmpCurr = FirstComponent (); pcmpCurr->IsValid (); pcmpCurr = pcmpCurr->Next ())
      {
      pcmpCurr->OnAwake ();
      };
    }
  };  
  
//-----------------------------------------------------------------------------
VOID  Node::DebugPrint  (const char *  szIndentIn) const
  {
  RStr  strComponentIndent;
  strComponentIndent.Format ("%s  ", szIndentIn);
  RStr  strFullPath;
  CalcFullPath (strFullPath);
  DBG_INFO ("%sNode: %s", szIndentIn, strFullPath.AsChar ());
  
  for (Component * pcmpCurr = FirstComponent (); pcmpCurr->IsValid (); pcmpCurr = pcmpCurr->Next ())
    {
    pcmpCurr->DebugPrint (strComponentIndent.AsChar ());
    };    
  };

//-----------------------------------------------------------------------------
VOID  Node::DebugPrintHeirarchy  (const char *  szIndentIn) const
  {
  RStr  strChildIndent;
  strChildIndent.Format ("%s    ", szIndentIn);  
  
  this->DebugPrint (szIndentIn);
  
  for (TListItr<Node*> itrCurr = listChildren.First (); itrCurr.IsValid (); ++itrCurr)
    {
    (*itrCurr)->DebugPrintHeirarchy (strChildIndent.AsChar ());
    };    
  }