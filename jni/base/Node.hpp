/* -----------------------------------------------------------------
                             Node

     This module implements scene graph nodes for holding components

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef NODE_HPP
#define NODE_HPP

#include "Types.hpp"
#include "Component.hpp"
#include "TList.hpp"

/**

  Name
  Active Flag
  Visibility Flag
  
  Link to Resource node is in
  ComponentList
  
  ?Smart Pointer (update/callback/links)  
  ?Class/Category/Flags (for filtering.  Drive Camera Visibility)
  
  Heirarchy (maintained in parallel with Transform Component)
    GetFirst/LastChild, NumChildren, FindChildByName/ID
    FindRoot
    ?GetParentDepth
    GetFullPath
    ParentTo

  Save/Load  
  DebugPrintInfo
  
  */
class Node;
//-----------------------------------------------------------------------------
class Node
  {
  private:
    RStr                  strName;        ///< Unique name identifying this object at its sibling level.
    BOOL                  bActive;        ///< whether or not the object is visible to the update loop.
    BOOL                  bVisible;       ///< whether or not the object and its children are seen by the renderer.
    BOOL                  bAwake;
    BOOL                  bStarted;

    UINT                  iResourceID;    ///< Identifies which resource this node is part of.  0 denotes the default resource.
    
    Component             listComponents; ///< List of Components attached to this node.
    
    Node *                pnodeParent;    ///< The parent node in the scene graph heirarchy.
    TList<Node*>          listChildren;   ///< List of children of this node in the scene graph heirarchy.
    
  public:
                          Node              ();
                  
                          ~Node             ();

    VOID                  SetName           (const char *  szNameIn)  {strName = szNameIn;};
    
    const char *          Name              (VOID)                    {return strName.AsChar ();};
    
    INT                   ResourceID        (VOID)                    {return iResourceID;};
    
    VOID                  SetResourceID     (INT  iIDIn)              {iResourceID = iIDIn;};
        
    Component *           FirstComponent    (VOID) const              {return listComponents.Next ();};

    TListItr<Node*>       FirstChild        (VOID) const              {return listChildren.First ();};
    
    INT                   NumChildren       (VOID)                    {return listChildren.Size ();};
    
    Node *                FindChild         (const char *  szNameIn);
    
    Node *                RootParent        (VOID);
    
    VOID                  CalcFullPath      (RStr &   strPathOut) const;
    
    VOID                  CalcResourcePath  (RStr &   strPathOut) const;
    
    Node *                FindByPath        (const char *  pszPathIn);
    
    VOID                  ParentTo          (Node *  pnodeNewParentIn);
    
    Component *           AddComponent      (const char *  szTypeIn);
    
    Component *           FindComponent     (const char *  szTypeIn, 
                                             Component *   pSearchStartIn = NULL);
    
    EStatus               Load              (RStrParser &  parserIn);
    
    EStatus               Save              (RStrParser &  parserIn,
                                             RStr          strLineIndentIn) const;

    VOID                  DebugPrint        (const char *  szIndentIn) const;
    
    VOID                  DebugPrintHeirarchy  (const char *  szIndentIn) const;
    
    static VOID           AddComponentTemplate (Component *  componentIn);
    
    BOOL                  IsAwake              (VOID) const   {return bAwake;};
    
    VOID                  SetAwake             (BOOL  bIn)    {bAwake = bIn;};
    
    BOOL                  IsActive             (VOID) const   {return bActive;};
    
    VOID                  SetActive            (BOOL  bIn)    {bActive = bIn;};
    
    BOOL                  IsStarted            (VOID) const   {return bStarted;};
    
    VOID                  SetStarted           (BOOL  bIn)    {bStarted = bIn;};
    
    VOID                  Update               (VOID);
    
    VOID                  Awake                (VOID);
    
  private:
    VOID                  CalcFullPathInternal (RStr &   strPathOut, 
                                                INT      iLength,
                                                BOOL     bMatchResourceIDs) const;
  
  };
  
#endif // NODE_HPP
