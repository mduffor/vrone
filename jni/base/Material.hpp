/* -----------------------------------------------------------------
                             Material

     This module implements a transformation matrix consisting of
     translate, rotate, and scale components.  It also implements
     parenting for the purpose of implementing scene graphs.
     
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "Types.hpp"
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Transform.hpp"
#include "mesh/DisplayMesh.hpp"
#include "Color8U.hpp"
#include "Shader.hpp"
#include "Texture.hpp"

/**

  */

//-----------------------------------------------------------------------------
class Material
  {
  public:
    enum EBlendFactor  {kZero                    = 0,
                        kOne                     = 1, 
                        kSrcColor                = 2,
                        kOneMinusSrcColor        = 3,
                        kDstColor                = 4,
                        kOneMinusDstColor        = 5,
                        kSrcAlpha                = 6,
                        kOneMinusSrcAlpha        = 7,
                        kDstAlpha                = 8,
                        kOneMinusDstAlpha        = 9,
                        kConstantColor           = 10,
                        kOneMinusConstantColor   = 11,
                        kConstantAlpha           = 12,
                        kOneMinusConstantAlpha   = 13,
                        kSrcAlphaSaturate        = 14,
                        // Note: These are not not part of OpenGL 2.x.  They are part of ARB_blend_func_extended and v3.3+
                        kSrc1Color               = 15,
                        kOneMinusSrc1Color       = 16,
                        kSrc1Alpha               = 17,
                        kOneMinusSrc1Alpha       = 18};  
  
  private:
    RStr           strName;
  
    Color8U        clrAmbient;
    RStr           strAmbientTexFile;
    Texture *      ptexAmbient;
    Color8U        clrDiffuse;
    RStr           strDiffuseTexFile;
    Texture *      ptexDiffuse;
    Color8U        clrSpecular;
    RStr           strSpecularTexFile;
    Texture *      ptexSpecular;
    
    Shader *       pShader;
    
    DisplayMesh *  pMeshes;  /// List of meshes to be displayed by this material
    
    Material *     pMasterNext;  /// Master list of Materials loaded into memory
  
  public:
  
                       Material            ();

                       ~Material           ();

                       Material            (const char *  szNameIn,
                                            const char *  szShaderNameIn);
                                            
    VOID               Init                (VOID);
                                            
    VOID               Register            (VOID);

    VOID               Unregister          (VOID);
                                        
    const char *       GetName             (VOID)                {return (strName.AsChar());};
    
    VOID               SetName             (const char *  szIn)  {strName.Set (szIn);};
        
    static Material *  FindByName          (const char *  szNameIn);
  
    VOID               AddMesh             (DisplayMesh *  pMeshIn);
    
    VOID               RemoveMesh          (DisplayMesh *  pMeshIn);
    
    BOOL               HasActiveMeshes     (VOID);

    EStatus            Activate            (RMatrix *  pmatCamProj);
    
    Shader *           GetShader           (VOID)                {return pShader;};

    static Material *  GetFirstActive      (VOID);
    
    static Material *  GetNextActive       (Material *  pCurr);

    DisplayMesh *      GetFirstActiveMesh  (VOID);
    
    DisplayMesh *      GetNextActiveMesh   (DisplayMesh *  pCurr);
    
    VOID               SetAmbientColor     (FLOAT   fRedIn,
                                            FLOAT   fGreenIn,
                                            FLOAT   fBlueIn)   {clrAmbient.Set (fRedIn, fGreenIn, fBlueIn);};
                                            
    VOID               SetAmbientTexture   (const char *  szFilenameIn)  {strAmbientTexFile = szFilenameIn;};
    
    VOID               SetDiffuseColor     (FLOAT   fRedIn,
                                            FLOAT   fGreenIn,
                                            FLOAT   fBlueIn)   {clrDiffuse.Set (fRedIn, fGreenIn, fBlueIn);};
                                            
    VOID               SetDiffuseTexture   (const char *  szFilenameIn)  {strDiffuseTexFile = szFilenameIn;};
    
    VOID               SetSpecularColor    (FLOAT   fRedIn,
                                            FLOAT   fGreenIn,
                                            FLOAT   fBlueIn)   {clrSpecular.Set (fRedIn, fGreenIn, fBlueIn);};
                                            
    VOID               SetSpecularTexture  (const char *  szFilenameIn)  {strSpecularTexFile = szFilenameIn;};
    
    const Color8U &    GetAmbient          (VOID) const                  {return clrAmbient;};
    
    const Color8U &    GetDiffuse          (VOID) const                  {return clrDiffuse;};
    
    const Color8U &    GetSpecular         (VOID) const                  {return clrSpecular;};
    
    const char *       GetAmbientTexture   (VOID) const                  {return strAmbientTexFile.AsChar ();};
    
    const char *       GetDiffuseTexture   (VOID) const                  {return strDiffuseTexFile.AsChar ();};
    
    const char *       GetSpecularTexture  (VOID) const                  {return strSpecularTexFile.AsChar ();};
    
  };
  

  
#endif // MATERIAL_HPP