
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Types.hpp"
#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "GLUtil.hpp"

//------------------------------------------------------------------------------
void GLPrintString (const char *name, GLenum s) 
  {
  const char *v = (const char *) glGetString(s);
  DBG_RAW ("GL %s = %s\n", name, v);
  }

//------------------------------------------------------------------------------
void GLCheckError (const char * filename, const char* op) 
  {
  for (GLint error = glGetError(); error; error = glGetError()) 
    {
    DBG_RAW ("File %s : after %s() glError (0x%x)\n", filename, op, error);
    }
  }

//------------------------------------------------------------------------------
void GLCheckError (const char * filename, int line, const char* op) 
  {
  for (GLint error = glGetError(); error; error = glGetError()) 
    {
    DBG_RAW ("File %s (%d) : after %s() glError (0x%x)\n", filename, line, op, error);
    }
  }

//------------------------------------------------------------------------------
void GLPrintInfo () 
  {
  GLPrintString("Version", GL_VERSION);
  GLPrintString("Vendor", GL_VENDOR);
  GLPrintString("Renderer", GL_RENDERER);
  GLPrintString("Extensions", GL_EXTENSIONS);
  }
  
//------------------------------------------------------------------------------
UINT GLLoadShader (GLenum        shaderType, 
                   const char *  pSource) 
  {
  GLuint shader = glCreateShader (shaderType);
  GLCheckError (__FILE__, "glCreateShader");
  if (shader) 
    {
    glShaderSource(shader, 1, &pSource, NULL);
    GLCheckError (__FILE__, "glShaderSource");
    glCompileShader(shader);
    GLCheckError (__FILE__, "glCompileShader");
    GLint compiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    GLCheckError (__FILE__, "glGetShaderiv");
    if (!compiled) 
      {
      GLint infoLen = 0;
      glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
      if (infoLen) 
        {
        char* buf = (char*) malloc(infoLen);
        if (buf) 
          {
          glGetShaderInfoLog(shader, infoLen, NULL, buf);
          DBG_ERROR ("Could not compile shader %s:\n%s\n", ((shaderType == GL_VERTEX_SHADER) ? "vertex" : "fragment"), buf);
          free(buf);
          }
        glDeleteShader(shader);
        shader = 0;
        DBG_ERROR (pSource);
        }
      }
    }
  return shader;
  }

//------------------------------------------------------------------------------
UINT  GLCreateVertexShader (const char *  pSourceIn)
  {
  UINT  uShader = GLLoadShader (GL_VERTEX_SHADER, pSourceIn);
  GLCheckError (__FILE__, "GLCreateVertexShader");
  return (uShader);
  }
  
//------------------------------------------------------------------------------
UINT  GLCreateFragmentShader (const char *  pSourceIn)
  {
  UINT  uShader = GLLoadShader (GL_FRAGMENT_SHADER, pSourceIn);
  GLCheckError (__FILE__, "GLCreateFragmentShader");
  return (uShader);
  }

//------------------------------------------------------------------------------
VOID GLDeleteShader (UINT *  puShaderIn)
  {
  glDeleteShader (*puShaderIn);
  *puShaderIn = 0;
  }
  
//------------------------------------------------------------------------------
VOID GLDeleteProgram (UINT *  puProgramIn)
  {
  glDeleteProgram (*puProgramIn);
  *puProgramIn = 0;
  }
  
//------------------------------------------------------------------------------
UINT GLCreateProgram (const char *  pVertexSource, 
                      const char *  pFragmentSource) 
  {
  UINT vertexShader = GLCreateVertexShader (pVertexSource);
  if (!vertexShader) 
    {
    return 0;
    }

  UINT fragmentShader = GLCreateFragmentShader (pFragmentSource);
  if (!fragmentShader) 
    {
    return 0;
    }
    
  return (GLCreateProgram (vertexShader, fragmentShader));
  }
  
//------------------------------------------------------------------------------
UINT GLCreateProgram (UINT  uVertexShader, 
                      UINT  uFragmentShader) 
  {
  GLuint program = glCreateProgram();
  GLCheckError (__FILE__, "glCreateProgram");
  if (program) 
    {
    glAttachShader(program, uVertexShader);
    GLCheckError (__FILE__, "glAttachShader");
    glAttachShader(program, uFragmentShader);
    GLCheckError (__FILE__, "glAttachShader");
    glLinkProgram(program);
    GLCheckError (__FILE__, "glLinkProgram");
    GLint linkStatus = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
    if (linkStatus != GL_TRUE) 
      {
      GLint bufLength = 0;
      glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
      if (bufLength) 
        {
        char* buf = (char*) malloc(bufLength);
        if (buf) 
          {
          glGetProgramInfoLog(program, bufLength, NULL, buf);
          DBG_ERROR ("Could not link program:\n%s\n", buf);
          free(buf);
          }
        }
      glDeleteProgram(program);
      GLCheckError (__FILE__, "glDeleteProgram");
      program = 0;
      }
    }
  return program;
  }

//------------------------------------------------------------------------------
VOID GLCreateTexture (UINT *  puTextureOut)
  {
  GLuint texture;
  glGenTextures (1, &texture);
  GLCheckError(__FILE__, "glGenTextures");
  *puTextureOut = texture;
  }
  
//------------------------------------------------------------------------------
VOID GLDeleteTexture (UINT *  puTextureIn)
  {
  GLuint texture = *puTextureIn;
  glDeleteTextures (1, &texture);
  GLCheckError(__FILE__, "glDeleteTextures");
  *puTextureIn = 0;
  }