/* -----------------------------------------------------------------
                             Component

     This module implements components that can be attached to nodes
     to form compositions.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "Component.hpp"
#include "TList.hpp"
#include "Attr.hpp"
#include "AttrFloat.hpp"
#include "AttrInt.hpp"
#include "AttrString.hpp"
#include "AttrStringArray.hpp"

static TList<Attr *>  listAttrTemplates;
static BOOL           bAttrTemplatesInitialized = FALSE;

//-----------------------------------------------------------------------------
//  Component
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
VOID AttrTemplatesInitialize (VOID)
  {
  listAttrTemplates.PushBack (static_cast<Attr*>(new AttrFloat  ("float")));
  listAttrTemplates.PushBack (static_cast<Attr*>(new AttrInt    ("int")));
  listAttrTemplates.PushBack (static_cast<Attr*>(new AttrString ("string")));
  listAttrTemplates.PushBack (static_cast<Attr*>(new AttrStringArray ("stringarray")));
  bAttrTemplatesInitialized = TRUE;
  }

//-----------------------------------------------------------------------------
Component::Component (VOID)
  {
  if (!bAttrTemplatesInitialized)
    {
    AttrTemplatesInitialize ();
    }
  iVersion = 1;
  bIsListSentinel = FALSE;
  pPrev = NULL;
  pNext = NULL;
  };
  
//-----------------------------------------------------------------------------
Component::~Component ()
  {
  listAttr.Empty();
  };

//-----------------------------------------------------------------------------
Component *  Component::Instantiate  (VOID) const                   
 {
 return new Component;
 };
  
//-----------------------------------------------------------------------------
VOID  Component::OnAwake (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnStart (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnUpdate (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnPostUpdate (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnCollision (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnClick (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnEnable (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnDisable (VOID)
  {
  };

//-----------------------------------------------------------------------------
VOID  Component::OnDisplay (VOID)
  {
  };
  
//-----------------------------------------------------------------------------
Attr *  Component::GetAttr (const char *  szNameIn)
  {
  for (TListItr<Attr*> itrCurr = listAttr.First (); itrCurr.IsValid (); ++itrCurr)
    {
    if (streq ((*itrCurr)->Name(), szNameIn))
      {
      return (*itrCurr);
      };
    };
  return (NULL);
  };

//-----------------------------------------------------------------------------
Attr *  Component::SetAttr (const char *  szNameIn,
                            const char *  szValueIn)
  {
  Attr *  pAttr = GetAttr (szNameIn);
  if (pAttr != NULL)
    {
    pAttr->SetByString (szValueIn);
    };
  return pAttr;
  };

//-----------------------------------------------------------------------------
Attr *  Component::SetArrayAttr (const char *  szNameIn,
                                 INT           iIndex,
                                 const char *  szValueIn)
  {
  Attr *  pAttr = GetAttr (szNameIn);
  if (pAttr != NULL)
    {
    pAttr->SetArrayByString (iIndex, szValueIn);
    };
  return pAttr;
  };
  
//-----------------------------------------------------------------------------
Attr *  Component::AddAttr (const char *  szNameIn,
                            const char *  szTypeIn)
  {
  // TODO
  for (TListItr<Attr*> itrCurr = listAttrTemplates.First ();
       itrCurr.IsValid();
       ++itrCurr)
    {
    if (streq ((*itrCurr)->Type (), szTypeIn))
      {
      // found the type template
      Attr *  pNew = (*itrCurr)->Instantiate (szNameIn);
      listAttr.PushBack (pNew);
      return (pNew);
      };
    };
  DBG_ERROR ("Unable to create Attr named \"%s\" because cannot find type : %s", szNameIn, szTypeIn);
  return (NULL);
  };

//-----------------------------------------------------------------------------
VOID  Component::DeleteAttr (const char *  szNameIn)
  {
  Attr *  pAttr = GetAttr (szNameIn);
  if (pAttr != NULL)
    {
    listAttr.Delete (pAttr);
    };
  };
    
//-----------------------------------------------------------------------------
EStatus  Component::Load (RStrParser &  parserIn)
  {
  // TODO
  return (EStatus::kFailure);
  };
  
//-----------------------------------------------------------------------------
EStatus  Component::Save (RStrParser &  parserIn,
                          RStr          strLineIndentIn) const
  {
  // TODO
  return (EStatus::kFailure);
  };

//-----------------------------------------------------------------------------
VOID  Component::DebugPrint (const char *  szIndentIn) const
  {
  RStr  strAttrIndent;
  strAttrIndent.Format ("%s  ", szIndentIn);
  DBG_INFO ("%sComponent %s", szIndentIn, Type ());
  
  Component *  nonConstThis = const_cast<Component *>(this);
  
  for (TListItr<Attr*> itrCurr = nonConstThis->listAttr.First (); itrCurr.IsValid (); ++itrCurr)
    {
    (*itrCurr)->DebugPrint (strAttrIndent.AsChar ());
    };    
  };
  
//-----------------------------------------------------------------------------
EStatus  Component::InsertAfter  (Component *  pcmpIn)
  {
  if (pNext == NULL) return (EStatus::kFailure);

  // check for an invalid entry
  ASSERT (pNext         != NULL);
  ASSERT (pcmpIn        != NULL);
  ASSERT (pcmpIn->pPrev == NULL);
  ASSERT (pcmpIn->pNext == NULL);


  pNext->pPrev  = pcmpIn;
  pcmpIn->pNext = pNext;
  pcmpIn->pPrev = this;
  pNext         = pcmpIn;

  return (EStatus::kSuccess);
  };

//-----------------------------------------------------------------------------
VOID Component::Remove  (VOID)
  {
  if (pPrev != NULL)   pPrev->pNext = pNext;
  if (pNext != NULL)   pNext->pPrev = pPrev;

  pNext = NULL;
  pPrev = NULL;
  };

//-----------------------------------------------------------------------------
Component *  Component::GetFirstSibling (VOID) const
  {
  const Component *  pSentinel = this;
  
  while (!pSentinel->bIsListSentinel)
    {
    pSentinel = pSentinel->pPrev;
    if (pSentinel == this) return NULL;
    }
  return pSentinel->pNext;
  };
