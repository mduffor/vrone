

#ifndef GLUTIL_HPP
#define GLUTIL_HPP

#define GL_CHECK_ERROR(...) GLCheckError(__FILE__, __LINE__, __VA_ARGS__)

void   GLPrintString          (const char *  name, 
                               GLenum        s);

void   GLCheckError           (const char *  filename, 
                               const char *  op);
                               
void   GLCheckError           (const char *  filename, 
                               int           line,
                               const char *  op);

void   GLPrintInfo            ();

UINT   GLLoadShader           (GLenum        shaderType, 
                               const char *  pSource);

UINT   GLCreateProgram        (const char *  pVertexSource, 
                               const char *  pFragmentSource);

UINT   GLCreateVertexShader   (const char *  pSourceIn);
  
UINT   GLCreateFragmentShader (const char *  pSourceIn);

VOID   GLDeleteShader         (UINT *        puShaderIn);
  
VOID   GLDeleteProgram        (UINT *        puProgramIn);

UINT   GLCreateProgram        (const char *  pVertexSource, 
                               const char *  pFragmentSource);
  
UINT   GLCreateProgram        (UINT          uVertexShader, 
                               UINT          uFragmentShader);
                        
VOID   GLDeleteTexture        (UINT *  puTextureIn);

VOID   GLCreateTexture        (UINT *  puTextureOut);
                        
                        
                        
                        
                        
                        
                        
                        

#endif // GLUTIL_HPP
