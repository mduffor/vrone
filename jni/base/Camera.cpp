/* -----------------------------------------------------------------
                             Camera

     This module implements a Camera object through which a 3D 
     scene graph is viewed.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "RVec.hpp"
#include "RMatrix.hpp"
#include "Camera.hpp"

//-----------------------------------------------------------------------------
//  Camera
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
Camera::Camera ()
  {
  fViewWidth = 800.0f;
  fViewHeight = 480.0f;
  fWidth = 800.0f;
  fHeight = 480.0f;
  fNear = 0.1f;
  fFar = 10000.0f;
  eMode = kOrtho;
  bDirty = TRUE;
  
  matProjection.Identity();
  };

  
//-----------------------------------------------------------------------------
Camera::~Camera ()
  {
  };
  
  
//-----------------------------------------------------------------------------
VOID Camera::Ortho  (FLOAT  fWidthIn, 
                     FLOAT  fHeightIn)
  {
  SetOrthoSize (fWidthIn, fHeightIn);
  SetOrthographic (TRUE);
  CalcProjection ();
  };

  
//-----------------------------------------------------------------------------
VOID Camera::Persp    (FLOAT  fHorizFOVIn)
  {
  SetOrthographic (FALSE);
  SetHorizFOV (fHorizFOVIn);
  CalcProjection ();
  };

  
//-----------------------------------------------------------------------------
VOID Camera::CalcProjection (VOID)
  {
  if (!bDirty) return;
  
  bDirty = FALSE;
  if (eMode == kOrtho)
    {
    MatrixOrtho (matProjection, -fWidth*0.5f, fWidth*0.5f, -fHeight*0.5f, fHeight*0.5f, fNear, fFar);
    }
  else if (eMode == kPersp)
    {
    FLOAT  fAspect = fViewWidth / fViewHeight;
    
    // convert from horizontal FOV to vertical FOV
    
    FLOAT fVertFOV = atanf(tanf(fHorizFOV) / fAspect);
    
    MatrixPerspective (matProjection, fVertFOV, fAspect, fNear, fFar);
    }
  else
    {
    matProjection.Identity();
    }
  };
  
    
//-----------------------------------------------------------------------------
VOID Camera::SetViewport (INT  iWidthIn, 
                          INT  iHeightIn)
  {
  fViewWidth  = (FLOAT) iWidthIn;
  fViewHeight = (FLOAT) iHeightIn;
  bDirty = TRUE;
  };

  
//-----------------------------------------------------------------------------
VOID Camera::SetNearFar (FLOAT  fNearIn,
                         FLOAT  fFarIn)
  {
  fNear = fNearIn;
  fFar  = fFarIn;
  bDirty = TRUE;
  };

  
//-----------------------------------------------------------------------------
VOID Camera::SetOrthoSize (FLOAT  fWidthIn,
                           FLOAT  fHeightIn)
  {
  fWidth  = fWidthIn;
  fHeight = fHeightIn;  
  bDirty = TRUE;
  };


  
  
  
  