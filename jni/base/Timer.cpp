/* -----------------------------------------------------------------
                              TIMER.CPP

     These routines are for implementing polling timers.

                            Michael T. Duffy
   ----------------------------------------------------------------- */

// TIMER.CPP
// Copyright 1997, Michael T. Duffy.  All Rights Reserved

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Types.hpp"
#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "Timer.hpp"

//------------------------------------------------------------------------------
Timer::Timer ()
  {
  Init ();
  };
        
//------------------------------------------------------------------------------
Timer::Timer (UINT32  uMillisecondsToWaitIn,
              BOOL    bOneTimeIn)
  {
  Init ();
  uMillisecondsToWait = uMillisecondsToWaitIn;
  bOneTime            = bOneTimeIn;
  };
        
//------------------------------------------------------------------------------
Timer::~Timer ()
  {
  };

//------------------------------------------------------------------------------
VOID Timer::Init (VOID)
  {
  
  uLastTime = uNextTime = uCurrTime = 0;
  uMillisecondsToWait = 1;
  pNext    = NULL;
  bOneTime = FALSE;
  };

        
//------------------------------------------------------------------------------
BOOL Timer::Post (UINT32  uMillisecondsSinceLast)
  {
  // Call the Post routine.  Return True if this timer should be deleted,
  //   false otherwise.
  
  // This function should be overridden so that posting the timer can be acted
  //  upon.
  
  return (bOneTime);
  };
   
//------------------------------------------------------------------------------
BOOL Timer::IncTime (UINT32  uMillisecondsIn)
  {
  // returns true if the timer should be deleted
  
  uCurrTime += uMillisecondsIn;
  if (uCurrTime >= uNextTime)
    {
    BOOL  bReturn = Post (uCurrTime - uLastTime);
    
    // update time values for next posting
    uLastTime = uCurrTime;
    uNextTime = uCurrTime + uMillisecondsToWait;
    return (bReturn);
    };
  return (FALSE);
  };


//------------------------------------------------------------------------------
VOID Timer::SetCurrTime  (UINT32  uTimeIn)
  {
  uLastTime = uCurrTime = uTimeIn;
  uNextTime = uCurrTime + uMillisecondsToWait;
  };


//==============================================================================
//  TimerHandler
//==============================================================================
  
//------------------------------------------------------------------------------
TimerHandler::TimerHandler ()
  {
  pTimerList = NULL;
  };
    
//------------------------------------------------------------------------------
TimerHandler::~TimerHandler ()
  {
  DeleteAllTimers ();
  };
  
//------------------------------------------------------------------------------
VOID TimerHandler::IncTime (UINT32  uMillisecondsIn)
  {
  Timer *  pCurr;
  Timer *  pPrev = NULL;
  Timer *  pNext;
  
  pCurr = pTimerList;
  while (pCurr != NULL)
    {
    pNext = pCurr->pNext;
    if (pCurr->IncTime (uMillisecondsIn))
      {
      // we need to delete this timer
      delete pCurr;
      if (pPrev != NULL)
        {
        pPrev->pNext = pNext;
        }
      else
        {
        pTimerList = pNext;
        };
      }
    else
      {
      // This timer has reset itself and is sticking around.
      pPrev = pCurr;
      }
    pCurr = pNext;
    };
  };
    
//------------------------------------------------------------------------------
VOID TimerHandler::AddTimer (Timer *  pTimerIn)
  {
  if (pTimerIn == NULL) return;
  
  pTimerIn->pNext = pTimerList;
  pTimerList = pTimerIn;
  };
    
//------------------------------------------------------------------------------
VOID TimerHandler::DeleteAllTimers (VOID)
  {
  Timer *  pDelete;
  
  while (pTimerList != NULL)
    {
    pDelete = pTimerList;
    pTimerList = pTimerList->pNext;
    delete (pDelete);
    };
  };

