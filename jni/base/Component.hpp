/* -----------------------------------------------------------------
                             Component

     This module implements components that can be attached to nodes
     to form compositions.
    

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#ifndef COMPONENT_HPP
#define COMPONENT_HPP

#include "Types.hpp"
#include "Attr.hpp"
#include "TList.hpp"

/**
   TODO:
    Attr Related
      ?GetAttr/SetAttr shortcuts for all types  (All attr names should be const char *)
      GetAttrAllMatches
      GetFirst/LastAttr
      GetNumAttrs, etc.

      
    Store an internal list of templates

  */

  
  
//-----------------------------------------------------------------------------
class Component
  {
  protected:
    
    RStr                  strType;
    TList<Attr*>          listAttr;    
    INT                   iVersion; ///< used to detect when this component was changed/dirtied.

    BOOL                  bIsListSentinel;
    Component *           pPrev;
    Component *           pNext;
    PVOID                 pParentNode;
    
  public:
                          Component    ();
    virtual               ~Component   ();
    
            const char *  Type         (VOID) const              {return strType.AsChar ();};
            
    virtual Component *   GetInterface (const char *  szTypeIn)  {return NULL;};
            
    virtual Component *   Instantiate  (VOID) const;

            INT           Version      (VOID)                    {return iVersion;};
            VOID          MarkAsDirty  (VOID)                    {++iVersion;};
    
    virtual VOID          OnAwake      (VOID);
    virtual VOID          OnStart      (VOID);
    virtual VOID          OnUpdate     (VOID);
    virtual VOID          OnPostUpdate (VOID); // after Update, before Display
    virtual VOID          OnCollision  (VOID);
    virtual VOID          OnClick      (VOID);
    virtual VOID          OnEnable     (VOID);
    virtual VOID          OnDisable    (VOID);
    virtual VOID          OnDisplay    (VOID);

            VOID          SetParentNode (PVOID pParentIn)  {pParentNode = pParentIn;};
    
            Attr *        GetAttr      (const char *  szNameIn);
     
    virtual Attr *        SetAttr      (const char *  szNameIn,
                                        const char *  szValueIn);

    virtual Attr *        SetArrayAttr (const char *  szNameIn,
                                        INT           iIndex,
                                        const char *  szValueIn);

            Attr *        AddAttr      (const char *  szNameIn,
                                        const char *  szTypeIn);

            VOID          DeleteAttr   (const char *  szNameIn);
    
    virtual EStatus       Load         (RStrParser &  parserIn);
    
    virtual EStatus       Save         (RStrParser &  parserIn,
                                        RStr          strLineIndentIn) const;


    virtual VOID          DebugPrint   (const char *  szIndentIn) const;    
    
                                       /** @brief  Returns a pointer to the next entry in the list
                                           @return The pointer to the next entry
                                       */
    Component *           Next         (VOID) const                  {return pNext;};

                                       /** @brief  Returns a pointer to the previous entry in the list
                                           @return The pointer to the previous entry
                                       */
    Component *           Prev         (VOID) const                  {return pPrev;};

                                       /** @brief  Inserts an entry after this one.  The pNext and pPrev pointers are
                                                   updated for this entry, the one that currently follows it, and
                                                   the passed entry.
                                           @param  pcmpIn Pointer to the entry that will be inserted after this entry.
                                           @return None
                                       */
    EStatus               InsertAfter  (Component *  pcmpIn);

                                       /** @brief  Inserts an entry before this one.  The pNext and pPrev pointers are
                                                   updated for this entry, the one that currently precedes it, and
                                                   the passed entry.
                                           @param  pentIn Pointer to the entry that will be inserted before this entry.
                                           @return None
                                       */
    EStatus               InsertBefore (Component *  pentIn) {return (pPrev == NULL) ? EStatus::kFailure : pPrev->InsertAfter (pentIn);};


                                       /** @brief  Removes this entry from the linked list.  The pNext and pPrev pointers
                                                   are updated for the entries that follow and precede this entry,
                                                   and the pNext and pPrev pointers for this entry are set to NULL.
                                           @return None
                                       */
    VOID                  Remove       (VOID);

    Component *           GetFirstSibling  (VOID) const;
    
    BOOL                  IsValid          (VOID) const   {return !bIsListSentinel;};
    
    VOID                  MakeListSentinel (VOID)         {bIsListSentinel = TRUE; pNext = pPrev = this;};
    
   
  };
  
#endif // COMPONENT_HPP