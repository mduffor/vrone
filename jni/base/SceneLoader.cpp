/* -----------------------------------------------------------------
                         Raven Node Script Parser

     This module implements reading and writing Nodes and Attrs
       stored in a Renderman-like attribute definition string.

   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2006-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 


#include "Debug.hpp"
ASSERTFILE (__FILE__)

#include "SceneLoader.hpp"
#include "FilePath.hpp"

/*

node: "|main_camera"
  component: transform
    tz [5]  
    rotateOrder [0]

  component: camera
    focalLength [0.055]
    near [0.5]
    far [10000]
  
    // set aperture to 35 mm Academy (.864 in x .630 in) 1.37
    //  fInchToMeter = 0.025400;
    apertureX [0.0219456] 
    apertureY [0.016002]
    
    centerOfInterest [5.0]

node: "|Placer"    
  component: transform
    
node: "|Placer|gm_TestSquare"    
  component: transform
    tx [0]
    tz [0]
  component: mesh
    file ["gfx/cast/testhead/geometry/square.obj"]

node: "|Placer|Collider"    
  component: transform
  component: boxCollider
    x [1]
    y [1]
    z [1]

node: "|MyGizmo
  component: transform
  reference: "templates/gizmo/gizmoA.ref"
  

*/

//-----------------------------------------------------------------------------
//  SceneLoader
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
SceneLoader::SceneLoader  ()
  {
  };



//-----------------------------------------------------------------------------
SceneLoader::~SceneLoader ()
  {
  };


//-----------------------------------------------------------------------------
EStatus  SceneLoader::ReadFile  (const char *  szFilenameIn,
                                 World *      pwldWorldIn,
                                 BOOL          bLoadAnim)
  {
  EStatus    errorStatus = EStatus::kSuccess;
  RStr       strErrorOut;


  // make sure the file exists.
  if (! FilePath::FileExists (szFilenameIn))
    {
    return (EStatus (EStatus::kFailure, RStr("File does not exist: ") + szFilenameIn));
    };
  DBG_INFO ("SceneLoader::ReadFile (): Reading %s\n", szFilenameIn);

  // get system time in milliseconds (start time)
  //const unsigned long startMillis = XMLPlatformUtils::getCurrentMillis();

  // get system time in milliseconds (end time)
  //const unsigned long  endMillis = XMLPlatformUtils::getCurrentMillis();
  //const unsigned long  duration  = endMillis - startMillis;


  RStrParser   parserBuffer;
  Node *       pnodeCurr = NULL;
  Component *  pcompCurr = NULL;
  
  // slurp the file in
  errorStatus = parserBuffer.ReadFromFile    (szFilenameIn);

  if (errorStatus == EStatus::kSuccess)
    {
    // step through buffer
    
    parserBuffer.SetSkipComments (RStrParser::kCStyle);

    while (! parserBuffer.IsEOFAscii ())
      {
      // read the key and value pairs
      parserBuffer.SkipWhitespace ();
      RStr  strKey   = parserBuffer.GetWord (TRUE, "[]");
      RStrParser  parserValue;
      BOOL  bIsNewNode = FALSE;
      BOOL  bIsNewComponent = FALSE;
      
      if (parserBuffer.PeekChar () == '[')
        {
        // we have an attr
        parserBuffer.GetBracketString (parserValue, RStr ("["), RStr ("]"));
        parserValue.SkipWhitespace ();
        }
      else 
        {
        // we have a node or component.
        parserBuffer.GetQuoteString (&parserValue);

        if (strKey == "node:")
          {
          bIsNewNode = TRUE;
          }
        else if (strKey == "component:")
          {
          bIsNewComponent = TRUE;
          }
        else
          {
          // unknown key!
          }
        };
      parserValue.ResetCursor ();

      // make sure both a key and a value were read.
      if ((!strKey.IsEmpty ()) && (!parserValue.IsEmpty ()))
        {
        // if we have a node key-value pair, create the node
        if (bIsNewNode)
          {
          pnodeCurr = pwldWorldIn->CreateNode (parserValue.AsChar ());
          pcompCurr = NULL;
          }
        else if (bIsNewComponent)
          {
          if (pnodeCurr != NULL)
            {
            pcompCurr = pnodeCurr->AddComponent (parserValue.AsChar ());
            }
          else
            {
            DBG_ERROR ("No node defined for component \"%s\"", parserValue.AsChar ());
            };
          }
        else
          {
          // we have an attr specification, not a node.  Make sure we have
          //  a component to load the attr onto.
          if (pcompCurr != NULL)
            {
            RStrArray  arrayStrOut;
            RStr       strOut;
            
            if (parserValue.PeekChar () != '"') 
              {
              // non-string
              pcompCurr->SetAttr (strKey, parserValue.AsChar ());
              }
            else
              {
              // string or string array
              while (!parserValue.IsEOL ())
                {
                parserValue.GetQuoteString (&strOut);
                arrayStrOut.Append (strOut);
                parserValue.SkipWhitespace ();
                if (parserValue.PeekChar () == ',')
                  {
                  parserValue.SkipChars (1);
                  parserValue.SkipWhitespace ();
                  continue;
                  }
                break;
                };
              INT  iNumStrings = arrayStrOut.Length ();
              if (iNumStrings == 1)
                {
                pcompCurr->SetAttr (strKey, arrayStrOut[0].AsChar ());
                }
              else
                {
                for (INT iIndex = 0; iIndex < iNumStrings; ++iIndex)
                  {
                  pcompCurr->SetArrayAttr (strKey, iIndex, arrayStrOut[iIndex].AsChar ());
                  }
                }
              }
            }
          else
            {
            DBG_ERROR ("SceneLoader.ReadFile (): No component defined for attr \"%s\"", strKey.AsChar ());
            };
          };
        };
      };
    };
  return (errorStatus);
  };





