/* -----------------------------------------------------------------
                             World

     This module implements the top level storage for the scene graph.
     It is designed to be a singleton.
    
   ----------------------------------------------------------------- */

// contact:  mduffor@gmail.com

// Modified BSD License:
//
// Copyright (c) 2004-2014, Michael T. Duffy II.  All rights reserved.
//
// Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
// Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer.
//
// Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
//  THE POSSIBILITY OF SUCH DAMAGE. 

#include "Debug.hpp"
ASSERTFILE (__FILE__);
#include "World.hpp"

//-----------------------------------------------------------------------------
//  World
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
World::World  ()
  {
  };
  
//-----------------------------------------------------------------------------
World::~World  ()
  {
  ClearScene ();
  };
  
//-----------------------------------------------------------------------------
VOID  World::ClearScene  (VOID)
  {
  
  for (TListItr<Node*>  itrNode = nodeRoot.FirstChild ();
       itrNode.IsValid ();
       ++itrNode)
    {
    Node *  pnodeToDelete = (*itrNode);
    ++itrNode;
    pnodeToDelete->ParentTo (NULL);
    delete pnodeToDelete;
    }
  };
    
//-----------------------------------------------------------------------------
Node *  World::CreateNode  (const char *  szFullPathIn)
  {
  DBG_INFO ("World::CreateNode %s", szFullPathIn);
  // separate out the name from the path
  RStr  strNodeName ("NewNode");
  RStr  strPath (szFullPathIn);
  
  INT  iFinalSeparator = strPath.ReverseFindChar  ('|');
  if (iFinalSeparator != -1)
    {
    strNodeName = strPath.SubString (iFinalSeparator + 1, strPath.GetLength () - iFinalSeparator - 1);   
    strPath.TruncateRight (iFinalSeparator - 1);
    };
  
  Node *  pnodeParent = FindNodeByPath (strPath.AsChar ());
  if (pnodeParent == NULL)
    {
    pnodeParent = &nodeRoot;
    };
  Node *  pnodeNew = new Node;
  pnodeNew->SetName (strNodeName.AsChar ());
  pnodeNew->ParentTo (pnodeParent);
  return (pnodeNew);
  };
    
//-----------------------------------------------------------------------------
VOID  World::DeleteNode  (const char *  szFullPathIn)
  {
  Node *  pnodeToDelete = FindNodeByPath (szFullPathIn);
  if (pnodeToDelete != NULL)
    {
    delete (pnodeToDelete);
    };
  };
    
//-----------------------------------------------------------------------------
VOID  World::DeleteNode  (Node *  pnodeIn)
  {
  delete (pnodeIn);
  };
    
//-----------------------------------------------------------------------------
Node *  World::FindNodeByPath  (const char *  szFullPathIn)
  {
  ASSERT (szFullPathIn[0] == '|');
  
  return (nodeRoot.FindByPath (&szFullPathIn[1]));
  };
    
//-----------------------------------------------------------------------------
EStatus  World::Load  (RStrParser &  parserIn)
  {
  // TODO
  return (EStatus::kFailure);
  };
    
//-----------------------------------------------------------------------------
EStatus  World::Save  (RStrParser &  parserIn,
                       RStr          strLineIndentIn) const
  {
  // TODO
  return (EStatus::kFailure);
  };

//-----------------------------------------------------------------------------
VOID  World::DebugPrint  (const char *  szIndentIn) const
  {
  nodeRoot.DebugPrintHeirarchy (szIndentIn);
  };
      
  
  
  
  
  
  
  
  
  
  