package com.mduffor.vrone;

import android.content.res.AssetManager;
import android.util.Log;


//-----------------------------------------------------------------------------
class VRShell
    {
    private static String TAG = "VRShell";
    
    public static void Init (AssetManager assetsIn)
      {
      Log.w(TAG, "VRShell: Calling nativeInit");
      nativeInit (assetsIn);
      }

    public static void Uninit ()
      {
      nativeUninit ();
      }

    public static void Update ()
      {
      if (VRShell.AppIsClosing ())
        {
        System.exit(0);
        return;
        }
      nativeUpdate ();
      }
        
    public static void OnKeyDown (int  iVKeyIn)
      {
      nativeOnKeyDown (iVKeyIn);
      }
        
    public static void OnKeyUp   (int iVKeyIn)
      {
      nativeOnKeyUp (iVKeyIn);
      }
    
    public static void OnPointerMove (int   iVKeyIn,
                                      float fXIn,
                                      float fYIn,
                                      float fPressureIn)
      {
      nativeOnPointerMove (iVKeyIn, fXIn, fYIn, fPressureIn);
      }
                                                    
    public static void OnPointerDown (int   iVKeyIn,
                                      float fXIn,
                                      float fYIn,
                                      float fPressureIn)
      {
      nativeOnPointerDown (iVKeyIn, fXIn, fYIn, fPressureIn);
      }
                                                    
    public static void OnPointerUp   (int   iVKeyIn,
                                      float fXIn,
                                      float fYIn,
                                      float fPressureIn)
      {
      nativeOnPointerUp   (iVKeyIn, fXIn, fYIn, fPressureIn);
      }

    public static boolean AppIsClosing  ()
      {
      return nativeAppIsClosing ();
      }
        
        
    private static native void nativeInit(AssetManager assetManager);
    private static native void nativeUninit();
    private static native void nativeUpdate();
    private static native void nativeOnKeyDown (int iVKeyIn);
    private static native void nativeOnKeyUp   (int iVKeyIn);
    private static native void nativeOnPointerMove (int iVKeyIn,
                                                    float fXIn,
                                                    float fYIn,
                                                    float fPressureIn);
    private static native void nativeOnPointerDown (int iVKeyIn,
                                                    float fXIn,
                                                    float fYIn,
                                                    float fPressureIn);
    private static native void nativeOnPointerUp   (int iVKeyIn,
                                                    float fXIn,
                                                    float fYIn,
                                                    float fPressureIn);
    private static native boolean nativeAppIsClosing  ();

    }