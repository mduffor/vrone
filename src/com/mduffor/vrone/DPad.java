package com.mduffor.vrone;

import android.view.InputDevice;
import android.view.InputEvent;
import android.view.KeyEvent;



/*
    AXIS_HAT_X, AXIS_HAT_Y, DPAD_UP, DPAD_DOWN, DPAD_LEFT, DPAD_RIGHT
    AXIS_X, AXIS_Y, BUTTON_THUMBL
    AXIS_Z, AXIS_RZ, BUTTON_THUMBR
    BUTTON_X
    BUTTON_A
    BUTTON_Y
    BUTTON_B
    BUTTON_R1
    AXIS_RTRIGGER, AXIS_THROTTLE
    AXIS_LTRIGGER, AXIS_BRAKE
    BUTTON_L1

Start game in main menu, or pause/unpause during game   BUTTON_START*
Display menu  BUTTON_SELECT* and KEYCODE_MENU*
Same as Android Back  KEYCODE_BACK
Navigate back to a previous item in a menu  BUTTON_B**
Confirm selection, or perform primary game action   BUTTON_A** and DPAD_CENTER      
      
Move Up   KEYCODE_DPAD_UP   AXIS_HAT_Y (for values 0 to -1.0)
Move Down   KEYCODE_DPAD_DOWN   AXIS_HAT_Y (for values 0 to 1.0)
Move Left   KEYCODE_DPAD_LEFT   AXIS_HAT_X (for values 0 to -1.0)
Move Right  KEYCODE_DPAD_RIGHT  AXIS_HAT_X (for values 0 to 1.0)      
      
*/      
      
//-----------------------------------------------------------------------------
public class DPad 
  {
  final static int UP       = 0;
  final static int LEFT     = 1;
  final static int RIGHT    = 2;
  final static int DOWN     = 3;
  final static int CENTER   = 4;

  int directionPressed = -1; // initialized to -1

  //---------------------------------------------------------------------------
  public int getDirectionPressed(InputEvent event) 
    {
    if (!isDpadDevice(event)) 
      {
      return -1;
      }
        
    // If the input event is a KeyEvent, check its key code.
    if (event instanceof KeyEvent) 
      {
      // Use the key code to find the D-pad direction.
      KeyEvent keyEvent = (KeyEvent) event;
      if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT) 
        {
        directionPressed = DPad.LEFT;
        } 
      else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT) 
        {
        directionPressed = DPad.RIGHT;
        } 
      else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP) 
        {
        directionPressed = DPad.UP;
        } 
      else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN) 
        {
        directionPressed = DPad.DOWN;
        } 
      else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER) 
        {
        directionPressed = DPad.CENTER;
        }
      }

    /*
    // API level 12 and above only
    // If the input event is a MotionEvent, check its hat axis values.
    else if (event instanceof MotionEvent) 
      {
      // Use the hat axis value to find the D-pad direction
      MotionEvent motionEvent = (MotionEvent) event;
      float xaxis = motionEvent.getAxisValue(MotionEvent.AXIS_HAT_X);
      float yaxis = motionEvent.getAxisValue(MotionEvent.AXIS_HAT_Y);

      // Check if the AXIS_HAT_X value is -1 or 1, and set the D-pad
      // LEFT and RIGHT direction accordingly.
      if (Float.compare(xaxis, -1.0f) == 0) 
        {
        directionPressed =  DPad.LEFT;
        } 
      else if (Float.compare(xaxis, 1.0f) == 0) 
        {
        directionPressed =  DPad.RIGHT;
        }
      // Check if the AXIS_HAT_Y value is -1 or 1, and set the D-pad
      // UP and DOWN direction accordingly.
      else if (Float.compare(yaxis, -1.0f) == 0) 
        {
        directionPressed =  DPad.UP;
        } 
      else if (Float.compare(yaxis, -1.0f) == 0) 
        {
        directionPressed =  DPad.DOWN;
        }
      }
    */
        
    return directionPressed;
    }

  //---------------------------------------------------------------------------
  public static boolean isDpadDevice(InputEvent event) 
    {
    // Check that input comes from a device with directional pads.
    // Note:  Is the below correct, or should it be == ?
    return ((event.getSource() & InputDevice.SOURCE_DPAD) != InputDevice.SOURCE_DPAD);
    }
  }

/*
You can use this helper class in your game wherever you want to process D-pad input (for example, in the onGenericMotionEvent() or onKeyDown() callbacks).

For example:

DPad mDpad = new DPad();
...
@Override
public boolean onGenericMotionEvent(MotionEvent event) {

    // Check if this event if from a D-pad and process accordingly.
    if (Dpad.isDpadDevice(event)) {

       int press = mDpad.getDirectionPressed(event);
       switch (press) {
            case LEFT:
                // Do something for LEFT direction press
                ...
                return true;
            case RIGHT:
                // Do something for RIGHT direction press
                ...
                return true;
            case UP:
                // Do something for UP direction press
                ...
                return true;
            ...
        }
    }

    // Check if this event is from a joystick movement and process accordingly.
    ...
}      
*/