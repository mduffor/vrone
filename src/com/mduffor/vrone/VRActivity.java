package com.mduffor.vrone;

import android.app.Activity;
import android.content.res.AssetManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;



//=============================================================================
public class VRActivity extends Activity 
    {
    private static String TAG = "VRActivity";
    private GLSurfaceView mGLView;
    private static int iActivePointerId = -1;


    /** Called when the activity is first created. */
    //-------------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) 
        {
        super.onCreate(savedInstanceState);

        Log.w(TAG, "Activity: Initializing VRShell");
        // initialize C++ layer here.
        AssetManager mngr = getAssets();
        VRShell.Init (mngr);

        // establish the View object that will handle drawing the screen
        mGLView = new VRSurfaceView(getApplication());
        setContentView(mGLView);    
        }

    //-------------------------------------------------------------------------
    @Override
    public void onDestroy() 
        {
        super.onDestroy ();
        VRShell.Uninit ();
        }
        
    //-------------------------------------------------------------------------
    @Override
    protected void onPause() 
        {
        super.onPause();
        mGLView.onPause();
        }

    //-------------------------------------------------------------------------
    @Override
    protected void onResume() 
        {
        super.onResume();
        mGLView.onResume();
        }

    //-------------------------------------------------------------------------
    public int TranslateVKey (int keyCode)
      {
      if ((keyCode >= KeyEvent.KEYCODE_A) && (keyCode <= KeyEvent.KEYCODE_Z))
        {
        return ('A' + keyCode - KeyEvent.KEYCODE_A);
        }
      else if ((keyCode >= KeyEvent.KEYCODE_0) && (keyCode <= KeyEvent.KEYCODE_9))
        {
        return ('0' + keyCode - KeyEvent.KEYCODE_0);
        }
      else
        {
        switch (keyCode) 
          {
          case KeyEvent.KEYCODE_ALT_LEFT:         return (VKey.ALT);
          case KeyEvent.KEYCODE_ALT_RIGHT:        return (VKey.ALT);
          
          case KeyEvent.KEYCODE_APOSTROPHE:       return ('\'');
          case KeyEvent.KEYCODE_AT:               return ('@');
          case KeyEvent.KEYCODE_BACK:             return (VKey.ESC); // hardware back button
          case KeyEvent.KEYCODE_BACKSLASH:        return ('\\');
          case KeyEvent.KEYCODE_BREAK:            return (VKey.PAUSE);
          
          // requires API level 12 or later
          /*
          case KeyEvent.KEYCODE_BUTTON_1:         return (VKey.GAMEPAD_BUTTON_1);
          case KeyEvent.KEYCODE_BUTTON_2:         return (VKey.GAMEPAD_BUTTON_2);
          case KeyEvent.KEYCODE_BUTTON_3:         return (VKey.GAMEPAD_BUTTON_3);
          case KeyEvent.KEYCODE_BUTTON_4:         return (VKey.GAMEPAD_BUTTON_4);
          case KeyEvent.KEYCODE_BUTTON_5:         return (VKey.GAMEPAD_BUTTON_5);
          case KeyEvent.KEYCODE_BUTTON_6:         return (VKey.GAMEPAD_BUTTON_6);
          case KeyEvent.KEYCODE_BUTTON_7:         return (VKey.GAMEPAD_BUTTON_7);
          case KeyEvent.KEYCODE_BUTTON_8:         return (VKey.GAMEPAD_BUTTON_8);
          case KeyEvent.KEYCODE_BUTTON_9:         return (VKey.GAMEPAD_BUTTON_9);
          case KeyEvent.KEYCODE_BUTTON_10:        return (VKey.GAMEPAD_BUTTON_10);
          case KeyEvent.KEYCODE_BUTTON_11:        return (VKey.GAMEPAD_BUTTON_11);
          case KeyEvent.KEYCODE_BUTTON_12:        return (VKey.GAMEPAD_BUTTON_12);
          case KeyEvent.KEYCODE_BUTTON_13:        return (VKey.GAMEPAD_BUTTON_13);
          case KeyEvent.KEYCODE_BUTTON_14:        return (VKey.GAMEPAD_BUTTON_14);
          case KeyEvent.KEYCODE_BUTTON_15:        return (VKey.GAMEPAD_BUTTON_15);
          case KeyEvent.KEYCODE_BUTTON_16:        return (VKey.GAMEPAD_BUTTON_16);
          */
          
          case KeyEvent.KEYCODE_BUTTON_A:         return (VKey.BUTTON_A);
          case KeyEvent.KEYCODE_BUTTON_B:         return (VKey.BUTTON_B);
          case KeyEvent.KEYCODE_BUTTON_C:         return (VKey.BUTTON_C);
          case KeyEvent.KEYCODE_BUTTON_X:         return (VKey.BUTTON_X);
          case KeyEvent.KEYCODE_BUTTON_Y:         return (VKey.BUTTON_Y);
          case KeyEvent.KEYCODE_BUTTON_Z:         return (VKey.BUTTON_Z);
          case KeyEvent.KEYCODE_BUTTON_L1:        return (VKey.BUTTON_L1);
          case KeyEvent.KEYCODE_BUTTON_L2:        return (VKey.BUTTON_L2);
          case KeyEvent.KEYCODE_BUTTON_R1:        return (VKey.BUTTON_R1);
          case KeyEvent.KEYCODE_BUTTON_R2:        return (VKey.BUTTON_R2);
          case KeyEvent.KEYCODE_BUTTON_THUMBL:    return (VKey.BUTTON_THUMBL);
          case KeyEvent.KEYCODE_BUTTON_THUMBR:    return (VKey.BUTTON_THUMBR);
          case KeyEvent.KEYCODE_BUTTON_MODE:      return (VKey.BUTTON_MODE);
          case KeyEvent.KEYCODE_BUTTON_SELECT:    return (VKey.BUTTON_SELECT);
          case KeyEvent.KEYCODE_BUTTON_START:     return (VKey.BUTTON_START);
          case KeyEvent.KEYCODE_COMMA:            return (',');
          case KeyEvent.KEYCODE_EQUALS:           return ('=');
          case KeyEvent.KEYCODE_GRAVE:            return ('`');
          case KeyEvent.KEYCODE_LEFT_BRACKET:     return ('[');
          case KeyEvent.KEYCODE_RIGHT_BRACKET:    return (']');
          case KeyEvent.KEYCODE_MINUS:            return ('-');
          case KeyEvent.KEYCODE_PLUS:             return ('+');
          case KeyEvent.KEYCODE_PERIOD:           return ('.');
          case KeyEvent.KEYCODE_POUND:            return ('#');
          case KeyEvent.KEYCODE_SEMICOLON:        return (';');
          case KeyEvent.KEYCODE_SLASH:            return ('/');
          case KeyEvent.KEYCODE_SPACE:            return (VKey.SPACE);
          case KeyEvent.KEYCODE_STAR:             return ('*');
          case KeyEvent.KEYCODE_TAB:              return (VKey.TAB);
          case KeyEvent.KEYCODE_F1:               return (VKey.F1);
          case KeyEvent.KEYCODE_F2:               return (VKey.F2);
          case KeyEvent.KEYCODE_F3:               return (VKey.F3);
          case KeyEvent.KEYCODE_F4:               return (VKey.F4);
          case KeyEvent.KEYCODE_F5:               return (VKey.F5);
          case KeyEvent.KEYCODE_F6:               return (VKey.F6);
          case KeyEvent.KEYCODE_F7:               return (VKey.F7);
          case KeyEvent.KEYCODE_F8:               return (VKey.F8);
          case KeyEvent.KEYCODE_F9:               return (VKey.F9);
          case KeyEvent.KEYCODE_F10:              return (VKey.F10);
          case KeyEvent.KEYCODE_F11:              return (VKey.F11);
          case KeyEvent.KEYCODE_F12:              return (VKey.F12);
          case KeyEvent.KEYCODE_PAGE_UP:          return (VKey.PAGEUP);
          case KeyEvent.KEYCODE_PAGE_DOWN:        return (VKey.PAGEDOWN);
          case KeyEvent.KEYCODE_SHIFT_LEFT:       return (VKey.SHIFT);
          case KeyEvent.KEYCODE_SHIFT_RIGHT:      return (VKey.SHIFT);
          case KeyEvent.KEYCODE_DEL:              return (VKey.BACKSPACE);
          case KeyEvent.KEYCODE_CTRL_LEFT:        return (VKey.CONTROL);
          case KeyEvent.KEYCODE_CTRL_RIGHT:       return (VKey.CONTROL);
          case KeyEvent.KEYCODE_ENTER:            return (VKey.ENTER);
          case KeyEvent.KEYCODE_ESCAPE:           return (VKey.ESC);
          case KeyEvent.KEYCODE_HOME:             return (VKey.HOME);
          case KeyEvent.KEYCODE_INSERT:           return (VKey.INSERT);
          case KeyEvent.KEYCODE_MENU:             return (VKey.MENU);
          case KeyEvent.KEYCODE_NUMPAD_0:         return (VKey.NUMPAD0);
          case KeyEvent.KEYCODE_NUMPAD_1:         return (VKey.NUMPAD1);
          case KeyEvent.KEYCODE_NUMPAD_2:         return (VKey.NUMPAD2);
          case KeyEvent.KEYCODE_NUMPAD_3:         return (VKey.NUMPAD3);
          case KeyEvent.KEYCODE_NUMPAD_4:         return (VKey.NUMPAD4);
          case KeyEvent.KEYCODE_NUMPAD_5:         return (VKey.NUMPAD5);
          case KeyEvent.KEYCODE_NUMPAD_6:         return (VKey.NUMPAD6);
          case KeyEvent.KEYCODE_NUMPAD_7:         return (VKey.NUMPAD7);
          case KeyEvent.KEYCODE_NUMPAD_8:         return (VKey.NUMPAD8);
          case KeyEvent.KEYCODE_NUMPAD_9:         return (VKey.NUMPAD9);
          case KeyEvent.KEYCODE_NUMPAD_ADD:       return (VKey.NUMPAD_ADD);
          case KeyEvent.KEYCODE_NUMPAD_DIVIDE:    return (VKey.NUMPAD_DIVIDE);
          case KeyEvent.KEYCODE_NUMPAD_DOT:       return (VKey.NUMPAD_DECIMAL);
          case KeyEvent.KEYCODE_NUMPAD_SUBTRACT:  return (VKey.NUMPAD_SUBTRACT);
          case KeyEvent.KEYCODE_NUMPAD_MULTIPLY:  return (VKey.NUMPAD_MULTIPLY);
          case KeyEvent.KEYCODE_NUMPAD_EQUALS:    return (VKey.NUMPAD_EQUAL);
          case KeyEvent.KEYCODE_NUMPAD_ENTER:     return (VKey.NUMPAD_ENTER);
          case KeyEvent.KEYCODE_DPAD_CENTER:      return (VKey.DPAD_CENTER);
          case KeyEvent.KEYCODE_DPAD_DOWN:        return (VKey.DPAD_DOWN);
          case KeyEvent.KEYCODE_DPAD_LEFT:        return (VKey.DPAD_LEFT);
          case KeyEvent.KEYCODE_DPAD_RIGHT:       return (VKey.DPAD_RIGHT);
          case KeyEvent.KEYCODE_DPAD_UP:          return (VKey.DPAD_UP);
          case KeyEvent.KEYCODE_MOVE_END:         return (VKey.END);
          case KeyEvent.KEYCODE_MOVE_HOME:        return (VKey.HOME);
          //case KeyEvent.:  return (VKey.);

          default:
            return (VKey.INVALID);
          }
        }
      }
        
        
    //-------------------------------------------------------------------------
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) 
      {
      
      // event.isShiftPressed()
      
      int iCode = TranslateVKey (keyCode);
      if (iCode == VKey.INVALID)
        {
        return super.onKeyUp(keyCode, event);
        }
      
      VRShell.OnKeyUp (iCode);
      return true;
      }
      
    //-------------------------------------------------------------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) 
      {
      
      int iCode = TranslateVKey (keyCode);
      if (iCode == VKey.INVALID)
        {
        return super.onKeyDown(keyCode, event);
        }
        
      VRShell.OnKeyDown (iCode);
      return true;
      }
      
    //-------------------------------------------------------------------------
    @Override
    public boolean onTouchEvent (MotionEvent event)
      {
      // getX(int), getY(int), getAxisValue(int), getPointerId(int), getToolType(int),
      // findPointerIndex(int)
      //float x = event.getX();
      //float y = event.getY();

      // TODO: Add multi-touch support here.  For now, we only allow one to be active.
      int  iEventIndex = event.getActionIndex ();
      int  iEventPointerId = event.getPointerId (iEventIndex);  
      
      if (iActivePointerId == -1)
        {
        // we don't currently have an active Id, so use this one.
        iActivePointerId = iEventPointerId;
        };
        
      if (iActivePointerId != iEventPointerId)
        {
        // discard this event, since it doesn't match the single one we are looking for.
        return false;
        };
      float  fX        = event.getX (iEventIndex);
      float  fY        = event.getY (iEventIndex);
      float  fPressure = event.getPressure (iEventIndex);
      
      switch (event.getActionMasked()) 
        {
        case MotionEvent.ACTION_UP:
            VRShell.OnPointerUp (VKey.LBUTTON, fX, fY, fPressure);
            break;
            
        case MotionEvent.ACTION_DOWN:
            VRShell.OnPointerDown (VKey.LBUTTON, fX, fY, fPressure);
            break;
        
        case MotionEvent.ACTION_CANCEL:
            VRShell.OnPointerUp (VKey.LBUTTON, fX, fY, fPressure);
            break;
            
        case MotionEvent.ACTION_MOVE:
            VRShell.OnPointerMove (VKey.LBUTTON, fX, fY, fPressure);
            break;
            
        //case MotionEvent.ACTION_SCROLL:
        //    break;

        // AXIS_HAT_X, AXIS_HAT_Y    
            
        }
      return true;
      }
   
   
/*

    dispatchGenericMotionEvent(android.view.MotionEvent)

    dispatchKeyEvent(android.view.KeyEvent)
      
*/    
      
    //-------------------------------------------------------------------------
    static 
        {
        // This static initializer will make sure that our C++ library is loaded.
        System.loadLibrary("vrone");
        }
    }

    
    
    
    
/*
// Unhandled KeyEvent s from TranslateVKey

int   KEYCODE_CLEAR   Key code constant: Clear key.
int   KEYCODE_FORWARD   Key code constant: Forward key.
int   KEYCODE_NUM_LOCK  Key code constant: Num Lock key.
int   KEYCODE_NUMPAD_COMMA  Key code constant: Numeric keypad ',' key (for decimals or digit grouping).
int   KEYCODE_NUMPAD_LEFT_PAREN   Key code constant: Numeric keypad '(' key.
int   KEYCODE_NUMPAD_RIGHT_PAREN  Key code constant: Numeric keypad ')' key.

int   KEYCODE_CAPS_LOCK   Key code constant: Caps Lock key.
int   KEYCODE_3D_MODE   Key code constant: 3D Mode key.
int   KEYCODE_APP_SWITCH  Key code constant: App switch key.
int   KEYCODE_ASSIST  Key code constant: Assist key.
int   KEYCODE_AVR_INPUT   Key code constant: A/V Receiver input key.
int   KEYCODE_AVR_POWER   Key code constant: A/V Receiver power key.
int   KEYCODE_BOOKMARK  Key code constant: Bookmark key.
int   KEYCODE_BRIGHTNESS_DOWN   Key code constant: Brightness Down key.
int   KEYCODE_BRIGHTNESS_UP   Key code constant: Brightness Up key.
int   KEYCODE_CALCULATOR  Key code constant: Calculator special function key.
int   KEYCODE_CALENDAR  Key code constant: Calendar special function key.
int   KEYCODE_CALL  Key code constant: Call key.
int   KEYCODE_CAMERA  Key code constant: Camera key.
int   KEYCODE_CAPTIONS  Key code constant: Toggle captions key.
int   KEYCODE_CHANNEL_DOWN  Key code constant: Channel down key.
int   KEYCODE_CHANNEL_UP  Key code constant: Channel up key.
int   KEYCODE_CONTACTS  Key code constant: Contacts special function key.
int   KEYCODE_DVR   Key code constant: DVR key.
int   KEYCODE_EISU  Key code constant: Japanese alphanumeric key.
int   KEYCODE_ENDCALL   Key code constant: End Call key.
int   KEYCODE_ENVELOPE  Key code constant: Envelope special function key.
int   KEYCODE_EXPLORER  Key code constant: Explorer special function key.
int   KEYCODE_FOCUS   Key code constant: Camera Focus key.
int   KEYCODE_FORWARD_DEL   Key code constant: Forward Delete key.
int   KEYCODE_FUNCTION  Key code constant: Function modifier key.
int   KEYCODE_GUIDE   Key code constant: Guide key.
int   KEYCODE_HEADSETHOOK   Key code constant: Headset Hook key.
int   KEYCODE_HENKAN  Key code constant: Japanese conversion key.
int   KEYCODE_INFO  Key code constant: Info key.
int   KEYCODE_KANA  Key code constant: Japanese kana key.
int   KEYCODE_KATAKANA_HIRAGANA   Key code constant: Japanese katakana / hiragana key.
int   KEYCODE_LANGUAGE_SWITCH   Key code constant: Language Switch key.
int   KEYCODE_MANNER_MODE   Key code constant: Manner Mode key.
int   KEYCODE_MEDIA_AUDIO_TRACK   Key code constant: Audio Track key.
int   KEYCODE_MEDIA_CLOSE   Key code constant: Close media key.
int   KEYCODE_MEDIA_EJECT   Key code constant: Eject media key.
int   KEYCODE_MEDIA_FAST_FORWARD  Key code constant: Fast Forward media key.
int   KEYCODE_MEDIA_NEXT  Key code constant: Play Next media key.
int   KEYCODE_MEDIA_PAUSE   Key code constant: Pause media key.
int   KEYCODE_MEDIA_PLAY  Key code constant: Play media key.
int   KEYCODE_MEDIA_PLAY_PAUSE  Key code constant: Play/Pause media key.
int   KEYCODE_MEDIA_PREVIOUS  Key code constant: Play Previous media key.
int   KEYCODE_MEDIA_RECORD  Key code constant: Record media key.
int   KEYCODE_MEDIA_REWIND  Key code constant: Rewind media key.
int   KEYCODE_MEDIA_STOP  Key code constant: Stop media key.
int   KEYCODE_META_LEFT   Key code constant: Left Meta modifier key.
int   KEYCODE_META_RIGHT  Key code constant: Right Meta modifier key.
int   KEYCODE_MUHENKAN  Key code constant: Japanese non-conversion key.
int   KEYCODE_MUSIC   Key code constant: Music special function key.
int   KEYCODE_MUTE  Key code constant: Mute key.
int   KEYCODE_NOTIFICATION  Key code constant: Notification key.
int   KEYCODE_NUM   Key code constant: Number modifier key.
int   KEYCODE_PICTSYMBOLS   Key code constant: Picture Symbols modifier key.
int   KEYCODE_POWER   Key code constant: Power key.
int   KEYCODE_PROG_BLUE   Key code constant: Blue "programmable" key.
int   KEYCODE_PROG_GREEN  Key code constant: Green "programmable" key.
int   KEYCODE_PROG_RED  Key code constant: Red "programmable" key.
int   KEYCODE_PROG_YELLOW   Key code constant: Yellow "programmable" key.
int   KEYCODE_RO  Key code constant: Japanese Ro key.
int   KEYCODE_SCROLL_LOCK   Key code constant: Scroll Lock key.
int   KEYCODE_SEARCH  Key code constant: Search key.
int   KEYCODE_SETTINGS  Key code constant: Settings key.
int   KEYCODE_SLEEP   Key code constant: Sleep key.
int   KEYCODE_SOFT_LEFT   Key code constant: Soft Left key.
int   KEYCODE_SOFT_RIGHT  Key code constant: Soft Right key.
int   KEYCODE_STB_INPUT   Key code constant: Set-top-box input key.
int   KEYCODE_STB_POWER   Key code constant: Set-top-box power key.
int   KEYCODE_SWITCH_CHARSET  Key code constant: Switch Charset modifier key.
int   KEYCODE_SYM   Key code constant: Symbol modifier key.
int   KEYCODE_SYSRQ   Key code constant: System Request / Print Screen key.
int   KEYCODE_TV  Key code constant: TV key.
int   KEYCODE_TV_INPUT  Key code constant: TV input key.
int   KEYCODE_TV_POWER  Key code constant: TV power key.
int   KEYCODE_UNKNOWN   Key code constant: Unknown key code.
int   KEYCODE_VOLUME_DOWN   Key code constant: Volume Down key.
int   KEYCODE_VOLUME_MUTE   Key code constant: Volume Mute key.
int   KEYCODE_VOLUME_UP   Key code constant: Volume Up key.
int   KEYCODE_WAKEUP  Key code constant: Wakeup key.
int   KEYCODE_WINDOW  Key code constant: Window key.
int   KEYCODE_YEN   Key code constant: Japanese Yen key.
int   KEYCODE_ZENKAKU_HANKAKU   Key code constant: Japanese full-width / half-width key.
int   KEYCODE_ZOOM_IN   Key code constant: Zoom in key.
int   KEYCODE_ZOOM_OUT  Key code constant: Zoom out key.

int   META_ALT_LEFT_ON  

This mask is used to check whether the left ALT meta key is pressed.
int   META_ALT_MASK   This mask is a combination of META_ALT_ON, META_ALT_LEFT_ON and META_ALT_RIGHT_ON.
int   META_ALT_ON   

This mask is used to check whether one of the ALT meta keys is pressed.
int   META_ALT_RIGHT_ON   

This mask is used to check whether the right the ALT meta key is pressed.
int   META_CAPS_LOCK_ON   

This mask is used to check whether the CAPS LOCK meta key is on.
int   META_CTRL_LEFT_ON   

This mask is used to check whether the left CTRL meta key is pressed.
int   META_CTRL_MASK  This mask is a combination of META_CTRL_ON, META_CTRL_LEFT_ON and META_CTRL_RIGHT_ON.
int   META_CTRL_ON  

This mask is used to check whether one of the CTRL meta keys is pressed.
int   META_CTRL_RIGHT_ON  

This mask is used to check whether the right CTRL meta key is pressed.
int   META_FUNCTION_ON  

This mask is used to check whether the FUNCTION meta key is pressed.
int   META_META_LEFT_ON   

This mask is used to check whether the left META meta key is pressed.
int   META_META_MASK  This mask is a combination of META_META_ON, META_META_LEFT_ON and META_META_RIGHT_ON.
int   META_META_ON  

This mask is used to check whether one of the META meta keys is pressed.
int   META_META_RIGHT_ON  

This mask is used to check whether the right META meta key is pressed.
int   META_NUM_LOCK_ON  

This mask is used to check whether the NUM LOCK meta key is on.
int   META_SCROLL_LOCK_ON   

This mask is used to check whether the SCROLL LOCK meta key is on.
int   META_SHIFT_LEFT_ON  

This mask is used to check whether the left SHIFT meta key is pressed.
int   META_SHIFT_MASK   This mask is a combination of META_SHIFT_ON, META_SHIFT_LEFT_ON and META_SHIFT_RIGHT_ON.
int   META_SHIFT_ON   

This mask is used to check whether one of the SHIFT meta keys is pressed.
int   META_SHIFT_RIGHT_ON   

This mask is used to check whether the right SHIFT meta key is pressed.
int   META_SYM_ON   

This mask is used to check whether the SYM meta key is pressed.      
*/     