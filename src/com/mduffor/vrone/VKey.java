package com.mduffor.vrone;

//-----------------------------------------------------------------------------
public class VKey
  {
  final static int INVALID          = 0x0000;
  final static int SPACE            = 0x0020;
  final static int ESC              = 0x0100;
  final static int BACKSPACE        = 0x0101;
  final static int TAB              = 0x0102;
  final static int BACKTAB          = 0x0103;
  final static int PAUSE            = 0x0104;
  final static int PAGEUP           = 0x0105;
  final static int PAGEDOWN         = 0x0106;
  final static int END              = 0x0107;
  final static int HOME             = 0x0108;
  final static int LEFT             = 0x0109;
  final static int UP               = 0x010a;
  final static int RIGHT            = 0x010b;
  final static int DOWN             = 0x010c;
  final static int INSERT           = 0x010d;
  final static int DELETE           = 0x010e;
  final static int ENTER            = 0x010f;
  final static int F1               = 0x0110;
  final static int F2               = 0x0111;
  final static int F3               = 0x0112;
  final static int F4               = 0x0113;
  final static int F5               = 0x0114;
  final static int F6               = 0x0115;
  final static int F7               = 0x0116;
  final static int F8               = 0x0117;
  final static int F9               = 0x0118;
  final static int F10              = 0x0119;
  final static int F11              = 0x011a;
  final static int F12              = 0x011b;
  final static int F13              = 0x011c;
  final static int F14              = 0x011d; 
  final static int F15              = 0x011e;
  final static int CONTROL          = 0x011f;
  final static int SHIFT            = 0x0120;
  final static int ALT              = 0x0121;
  final static int META             = 0x0122;

  final static int MENU             = 0x0123;
  final static int CAPITAL          = 0x0124;
  final static int HELP             = 0x0125;
  final static int LBUTTON          = 0x0126;
  final static int RBUTTON          = 0x0127;
  final static int MBUTTON          = 0x0128;
  final static int NUMPAD0          = 0x0129;
  final static int NUMPAD1          = 0x012a;
  final static int NUMPAD2          = 0x012b;
  final static int NUMPAD3          = 0x012c;
  final static int NUMPAD4          = 0x012d;
  final static int NUMPAD5          = 0x012f;
  final static int NUMPAD6          = 0x0130;
  final static int NUMPAD7          = 0x0131;
  final static int NUMPAD8          = 0x0132;
  final static int NUMPAD9          = 0x0133;
  final static int NUMPAD_SPACE     = 0x0134;
  final static int NUMPAD_TAB       = 0x0135;
  final static int NUMPAD_ENTER     = 0x0136;
  final static int NUMPAD_F1        = 0x0137;
  final static int NUMPAD_F2        = 0x0138;
  final static int NUMPAD_F3        = 0x0139;
  final static int NUMPAD_F4        = 0x013a;
  final static int NUMPAD_HOME      = 0x013b;
  final static int NUMPAD_LEFT      = 0x013c;
  final static int NUMPAD_UP        = 0x013d;
  final static int NUMPAD_RIGHT     = 0x013e;
  final static int NUMPAD_DOWN      = 0x013f;

//  final static int NUMPAD_PRIOR     = 0x0140;
  final static int NUMPAD_PAGEUP     = 0x0141;
//  final static int NUMPAD_NEXT      = 0x0142;
  final static int NUMPAD_PAGEDOWN   = 0x0143;
  final static int NUMPAD_END        = 0x0144;
  final static int NUMPAD_BEGIN      = 0x0145;
  final static int NUMPAD_INSERT     = 0x0146;
  final static int NUMPAD_DELETE     = 0x0147;
  final static int NUMPAD_EQUAL      = 0x0148;
  final static int NUMPAD_MULTIPLY   = 0x0149;
  final static int NUMPAD_ADD        = 0x014a;
  final static int NUMPAD_SEPARATOR  = 0x014b;
  final static int NUMPAD_SUBTRACT   = 0x014c;
  final static int NUMPAD_DECIMAL    = 0x014d;
  final static int NUMPAD_DIVIDE     = 0x014e;

  final static int MULTIPLY          = 0x014f;
  final static int ADD               = 0x0150;
  final static int SUBTRACT          = 0x0151;
  final static int DECIMAL           = 0x0152;
  final static int DIVIDE            = 0x0153;
  final static int NUMLOCK           = 0x0154;
  final static int SCROLL            = 0x0155;
  final static int SEPARATOR         = 0x0156;

  final static int GAMEPAD_BUTTON_1   = 0x0157;
  final static int GAMEPAD_BUTTON_2   = 0x0158;
  final static int GAMEPAD_BUTTON_3   = 0x0159;
  final static int GAMEPAD_BUTTON_4   = 0x015a;
  final static int GAMEPAD_BUTTON_5   = 0x015b;
  final static int GAMEPAD_BUTTON_6   = 0x015c;
  final static int GAMEPAD_BUTTON_7   = 0x015d;
  final static int GAMEPAD_BUTTON_8   = 0x015e;
  final static int GAMEPAD_BUTTON_9   = 0x015f;
  final static int GAMEPAD_BUTTON_10  = 0x0160;
  final static int GAMEPAD_BUTTON_11  = 0x0161;
  final static int GAMEPAD_BUTTON_12  = 0x0162;
  final static int GAMEPAD_BUTTON_13  = 0x0163;
  final static int GAMEPAD_BUTTON_14  = 0x0164;
  final static int GAMEPAD_BUTTON_15  = 0x0165;
  final static int GAMEPAD_BUTTON_16  = 0x0166;

  final static int BUTTON_A           = 0x0167;
  final static int BUTTON_B           = 0x0168; 
  final static int BUTTON_C           = 0x0169;
  final static int BUTTON_X           = 0x016a;
  final static int BUTTON_Y           = 0x016b;
  final static int BUTTON_Z           = 0x016c;

  final static int BUTTON_L1          = 0x016d;
  final static int BUTTON_L2          = 0x016e;
  final static int BUTTON_R1          = 0x016f;
  final static int BUTTON_R2          = 0x0170;
  final static int BUTTON_THUMBL      = 0x0171;
  final static int BUTTON_THUMBR      = 0x0172;

  final static int BUTTON_MODE        = 0x0173;
  final static int BUTTON_SELECT      = 0x0174;
  final static int BUTTON_START       = 0x0175;

  final static int DPAD_CENTER        = 0x0176;
  final static int DPAD_DOWN          = 0x0177;
  final static int DPAD_LEFT          = 0x0179;
  final static int DPAD_RIGHT         = 0x017a;
  final static int DPAD_UP            = 0x017b;


/*
  final static int VKFLG_SHIFT          = 0x8000
  final static int VKFLG_CONTROL        = 0x4000
  final static int VKFLG_ALT            = 0x2000
  final static int VKFLG_META           = 0x1000
  final static int VKFLG_MASK           = 0xf000
*/
  final static int CTRL_A              = 0x4001;
  final static int CTRL_B              = 0x4002;
  final static int CTRL_C              = 0x4003;
  final static int CTRL_D              = 0x4004;
  final static int CTRL_E              = 0x4005;
  final static int CTRL_F              = 0x4006;
  final static int CTRL_G              = 0x4007;
  final static int CTRL_H              = 0x4008;
  final static int CTRL_I              = 0x4009;
  final static int CTRL_J              = 0x400a;
  final static int CTRL_K              = 0x400b;
  final static int CTRL_L              = 0x400c;
  final static int CTRL_M              = 0x400d;
  final static int CTRL_N              = 0x400e;
  final static int CTRL_O              = 0x400f;
  final static int CTRL_P              = 0x4010;
  final static int CTRL_Q              = 0x4011;
  final static int CTRL_R              = 0x4012;
  final static int CTRL_S              = 0x4013;
  final static int CTRL_T              = 0x4014;
  final static int CTRL_U              = 0x4015;
  final static int CTRL_V              = 0x4016;
  final static int CTRL_W              = 0x4017;
  final static int CTRL_X              = 0x4018;
  final static int CTRL_Y              = 0x4019;
  final static int CTRL_Z              = 0x401a;
  
  
  }